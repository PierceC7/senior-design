<?php
/**
 * @package WordPress
 * @subpackage kmdg
 * @author Jake Finley
 *
 * This class provides a wrapper for WordPress' built in shortcode system,
 * it provides a more powerful interface for the shortcode API as well as
 * an easy way to integrate with Visual Composer (if used).
 */

namespace KMDG;

defined( 'ABSPATH' ) or die( 'No direct access' );

abstract class Shortcode {
    protected $vc;

    public function __construct() {
        $this->vc = $this->vc_settings();

        add_shortcode($this->slug(), array($this, '_action_render'));
        add_action('vc_before_init', array($this, '_action_vcmap'));
    }

    /**
     * Default shortcode slug
     * @return string
     */
    public function slug() {
        $classpath = explode('\\', get_called_class());
        $class = array_pop($classpath);
        return strtolower($class);
    }

    /**
     * @param $args object Array of arguments passed into the shortcode
     * @param $content|null String content between the open and close tags
     * @return void
     */
    protected abstract function view(\stdClass $args, $content = null);

    /**
     * Returns an array of Shortcode_Attribute objects
     * @return array
     */
    protected abstract function attributes();

    /**
     * Default visual composer settings
     * @return object
     */
    protected function vc_settings() {
        return (object) array(
            'name'                      => ucwords(str_replace('_', ' ', $this->slug())),
            'base'                      => $this->slug(),
            'description'               => null,
            'class'                     => 'sc_'.$this->slug(),
            'show_settings_on_create'   => true,
            'weight'                    => null,
            'category'                  => 'Theme',
            'group'                     => null,
            'admin_enqueue_js'          => null,
            'admin_enqueue_css'         => null,
            'front_enqueue_js'          => null,
            'front_enqueue_css'         => null,
            'icon'                      => null,
            'custom_markup'             => null,
            'js_view'                   => null,
            'html_template'             => null,
            'deprecated'                => null,
            'content_element'           => null
        );
    }

    /**
     * Parses the user specified shortcode attributes and combines them with the defaults
     * provided by the class. Returns an objectifed array of Shortcode_Attribute objects
     * organized by the name of the attribute (ie, $atts->height).
     *
     * @param $user_atts array User specified attributes from the shortcode
     * @return object
     * @throws \Exception
     */
    protected function parse_atts(array $user_atts) {
        $parsed_atts = array();

        foreach($this->attributes() as $attr) {
            /**
             * @var $attr Shortcode_Attribute
             */
            if ($attr instanceof \KMDG\Shortcode_Attribute) {
                if(!empty($user_atts[$attr->name()])) {
                    $attr->set($user_atts[$attr->name()]);
                }

                $parsed_atts[$attr->name()] = $attr->get();
            } else {
                throw new \Exception("Attribute for shortcode '".$this->slug()."' is not an object of type 'Shortcode_Attribute'.");
            }
        }

        return (object) $parsed_atts;
    }

    /**
     * Processes the frontend action of the shortcode.
     * @param array $atts
     * @param string|null $content
     * @return string
     */
    public function _action_render(array $atts, $content = null) {

        if(!is_null($content)) {
            $content = do_shortcode($content); // Recursively trigger all shortcode contained in this shortcode
        }

        ob_start();

        $this->view($this->parse_atts($atts), $content);

        return ob_get_clean();
    }

    public function _action_vcmap() {
        $params = array();

        foreach($this->attributes() as $attr) {
            /**
             * @var $attr Shortcode_Attribute
             */
            if ($attr instanceof \KMDG\Shortcode_Attribute) {
                $params[] = $attr->vc_map();
            } else {
                throw new \Exception("Attribute for shortcode '".$this->slug()."' does is not an object of type 'Shortcode_Attribute'.");
            }
        }

        $this->vc->params = $params;

        vc_map($this->vc);
    }
}
