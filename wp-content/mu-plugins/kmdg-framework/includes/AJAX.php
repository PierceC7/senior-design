<?php
/**
 * @package WordPress
 * @subpackage kmdg
 * @author Jake Finley
 *
 * This class provides a wrapper for WordPress' built in AJAX handling,
 * it requires you to provide an object that implements AJAX_Interface, this
 * object will be passed to your callback function after it has been initialized.
 */

namespace KMDG;

defined( 'ABSPATH' ) or die( 'No direct access' );

require_once('AJAX_Interface.php');

class AJAX {
    /**
     * Bitwise Flags:
     * Use these by filling out the flags parameter with any number of
     * flags, separated by a pipe (order does not matter), ie:
     * KMDG/AJAX::REQUIRE_AUTH|KMDG/AJAX::IGNORE_NONCE
     */
    const REQUIRE_AUTH = 0x0000001;
    const IGNORE_NONCE = 0x0000010;

    /**
     * @var AJAX_Interface|null
     */
    protected static $default_ajax_handler = null;

    /**
     * @var string
     */
    protected $action;

    /**
     * @var bool
     */
    protected $admin_only = false;

    /**
     * @var bool
     */
    protected $validate_nonce = true;

    /**
     * @var \Closure|string|null
     */
    protected $callback;

    /**
     * @var AJAX_Interface
     */
    protected $ajax_handler;

    /**
     * Defines an action that should be executed in response to an AJAX request.
     * This piggybacks on top of the WordPress AJAX API to provide a few additional
     * options and controls, as well as a nicer way of creating the hooks.
     *
     * @param string $action The action slug identifier that this should be listening for
     * nonce has been provided with the ajax call.
     * @param Callable $callback The file or function to be ran for this action
     * @param int $flags Binary flags to be applied to this AJAX event
     * @param AJAX_Interface $ajax_handler The AJAX_Interface class that will be used to wrap this action
     */
    public function __construct($action, $callback, $flags = 0x0000000, AJAX_Interface $handler = null) {

        $this->action = $action;
        $this->callback = $callback;

        // Read bitwise flags
        $this->admin_only = $flags & static::REQUIRE_AUTH;
        $this->validate_nonce = !($flags & static::IGNORE_NONCE);

        // Set up AJAX handler, must implement AJAX_Interface
        if(!empty($handler)) {
            $this->ajax_handler = $handler;
        } elseif(!empty(static::$default_ajax_handler)) {
            $this->ajax_handler = static::$default_ajax_handler;
        } else {
            throw new \ErrorException("AJAX action added with no defined handler, either define a default handler or add one individually to the action.");
        }

        if(!$this->admin_only) {
            add_action('wp_ajax_nopriv_'.$this->action, array($this, '_action_handle_ajax'));
        } else {
            add_action('wp_ajax_nopriv_'.$this->action, array($this, '_action_no_access'));
        }

        add_action('wp_ajax_'.$this->action, array($this, '_action_handle_ajax'));
    }

    /**
     * Registers a plugin class to satisfy the ajax handler dependency. You can register a
     * class that implements the AJAX_Interface interface. This is to allow you to customize
     * the handling of an AJAX response without having to edit this class for a potentially
     * one-off situation. If no AJAX handler is registered then all AJAX requests must be made
     * via POST and no pre-processing of the requests will be available.
     *
     * @param AJAX_Interface $handler The AJAX handler class that will
     */
    public static function set_default_ajax_handler(AJAX_Interface $handler) {
        static::$default_ajax_handler = $handler;
    }

    /**
     * Runs the callback function associated with the triggered AJAX action
     */
    public function _action_handle_ajax() {
        $this->ajax_handler->start();
        $nonce = $this->ajax_handler->request('api_key');

        if($this->validate_nonce) {
            if(wp_verify_nonce($nonce, 'api-key') === false) {
                $this->ajax_handler->error('Your access key has expired, please obtain a new one and try again.');
                $this->ajax_handler->finish();
            }
        }

        $callback = $this->callback;

        if(is_string($callback) && !is_callable($callback) && file_exists(static::path($callback))) {
            $ajax = $this->ajax_handler; // shortcut access to ajax handler
            include(static::path($callback));
        } elseif(is_callable($callback) && !($callback instanceof \Closure)) {
            call_user_func($this->callback, $this->ajax_handler);
        } elseif ($callback instanceof \Closure) {
            $callback($this->ajax_handler);
        }

        $this->ajax_handler->finish();
    }

    /**
     * A wrapper for get_stylesheet_directory that shortens and makes it a little more user friendly.
     * This should be used over get_stylesheet_directory().
     * @param string $path (optional) A relative path from the theme directory (and/or file name)
     * @return string
     * @filter theme_path
     */
    protected static function path($path = '') {
        return apply_filters('theme_path', str_replace('/', DIRECTORY_SEPARATOR, get_stylesheet_directory().DIRECTORY_SEPARATOR.$path));
    }

    public function _action_no_access() {
        $this->ajax_handler->start();
        $this->ajax_handler->error('You do not have permission to do that action.');
        $this->ajax_handler->finish();
    }
}