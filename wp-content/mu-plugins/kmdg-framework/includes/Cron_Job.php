<?php
/**
 * @package WordPress
 * @subpackage kmdg
 * @author Jake Finley
 *
 * This class creates and manages wp_cron jobs and provides an object-oriented
 * interface for that API. It handles correct adding and removing of jobs without
 * having to worry about orphaned events via a cleanup function that will remove
 * inactive jobs. It will also update job schedule changes automatically without
 * disrupting the existing timing.
 *
 * IMPORTANT: If you want to completely remove this class you will need to load
 * WordPress once with no active jobs in order to clear out any jobs remaining
 * in the cron list. After you do that you can safely remove this class file.
 */

namespace KMDG;

defined( 'ABSPATH' ) or die( 'No direct access' );

add_action('after_setup_theme', array('KMDG\Cron_Job', '_remove_inactive'), 10);

class Cron_Job {
    const SLUG_PREFIX = 'kmdg_auto_cron_';
    private $slug;
    private $callback;
    private $schedule;
    private $args;
    private static $jobs;

    /**
     * Creates a WP_Cron job. Job start times are set to the first time this
     * function is executed.
     * @param string $slug The unique identifier of this cron job
     * @param string $schedule The schedule slug to use
     * @param mixed $callback Any form of callable function or a file path
     * @param array $args (Optional) an array of arguments to be passed to the callback
     */
    public function __construct($slug, $schedule, $callback, array $args = array()) {
        $this->callback = $callback;
        $this->schedule = $schedule;
        $this->slug = self::SLUG_PREFIX.$slug;
        $this->args = $args;
        self::$jobs[] = $this->slug;
        
        add_action('init', array($this, '_action_register'));
        add_action($this->slug, array($this, '_action_run'));

        if(!has_action('cron_schedules', array(__CLASS__, '_filter_cron_add_schedules'))) {
            add_filter('cron_schedules', array(__CLASS__, '_filter_cron_add_schedules'));
        }        
    }

    /**
     * Registers and maintains the job, if the scheduling changes this will
     * unregister and re-register the job with the new schedule but with the
     * original start time.
     *
     * TODO: Maybe refactor this so that it's only called once, rather than once per cron job.
     */
    function _action_register() {
        if(wp_next_scheduled($this->slug)) {
            $cronjobs = array();
            foreach(get_option('cron') as $timestamp=>$data) {
                if(!empty($data) && is_array($data)) {
                    $cron_slug = key($data);
                    $cronjobs[] = $cron_slug;
                    $temp = array_shift($data);
                    $cron_settings = array_shift($temp);
                    $cron_schedule = empty($cron_settings['schedule']) ? '' : $cron_settings['schedule'];
                    if($cron_slug == $this->slug && $cron_schedule != $this->schedule) {
                        $time = wp_next_scheduled($this->slug);
                        wp_clear_scheduled_hook($this->slug);
                        wp_schedule_event($time, $this->schedule, $this->slug);
                    }
                }
            }
        } else {
            wp_schedule_event(time(), $this->schedule, $this->slug);
        }
    }

    /**
     * Executes the callback function/file
     */
    public function _action_run() {
        $callback = $this->callback;
        $args = $this->args;

        if(is_string($callback) && !is_callable($callback) && file_exists(self::path($callback))) {
            include(self::path($callback));
        } elseif(is_callable($callback) && !($callback instanceof \Closure)) {
            call_user_func($callback, $args);
        } elseif ($callback instanceof \Closure) {
            $callback($args);
        }
    }

    /**
     * Removes any inactive jobs, this is hooked in by this file and will be
     * executed as long as this class file is included, allowing you to remove
     * all cron jobs from the code and still have them removed from wordpress.
     */
    public static function _remove_inactive() {
        if(is_array(get_option('cron'))) {
            foreach(get_option('cron') as $timestamp=>$data) {
                if(!empty($data) && is_array($data)) {
                    $slug = key($data);
                    if(strpos($slug, self::SLUG_PREFIX) !== false
                            && (empty(self::$jobs) || !in_array($slug, self::$jobs))) {
                        wp_clear_scheduled_hook($slug);
                    }
                }
            }
        }
    }

    /**
     * Adds a weekly cron job schedule to WordPress' built in scheduling options
     * @param array $schedules
     * @return array
     */
    public static function _filter_cron_add_schedules( $schedules ) {
        $schedules['weekly'] = array(
          'interval' => 604800,
          'display' => __( 'Once Weekly' )
        );

        $schedules['fortnightly'] = array(
          'interval' => 1209600,
          'display' => __( 'Once Every Two Weeks' )
        );

        $schedules['monthly'] = array(
          'interval' => 2592000,
          'display' => __( 'Once Monthly (30 days)' )
        );

        $schedules['semianual'] = array(
          'interval' => 15768000,
          'display' => __( 'Twice a year' )
        );

        $schedules['anual'] = array(
          'interval' => 31536000,
          'display' => __( 'Once a year' )
        );
        return $schedules;
    }
}