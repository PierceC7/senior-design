<?php
/**
 * @package WordPress
 * @subpackage kmdg
 * @author Jake Finley
 *
 * This class can be used to create and represent custom post types and
 * their associated taxonomies.
 */

namespace KMDG;

defined( 'ABSPATH' ) or die( 'No direct access' );

class Post_Type {
    /**
     * @var string
     */
    protected $slug;
    /**
     * @var array
     */
    protected $labels;
    /**
     * @var array
     */
    protected $settings;
    /**
     * @var array
     */
    protected $taxonomies;

    /**
     * Creates a new custom post type with the given slug
     * @param string $slug
     * @return Post_Type
     */
    function &__construct($slug) {
        $this->slug = strtolower($slug);
        $this->labels = array('name' => ucwords(str_replace('_',' ', str_replace('-',' ', $slug))));
        $this->settings = array('labels'=>array());
        $this->taxonomies = array();

        add_action('init', array($this, '__action_create'));

        return $this;
    }

    /**
     * Allows you to define labels for the post type, uses the same format
     * as the labels array for WordPress' register_post_type(). This is a high
     * level method for adding labels.
     * @param array $labels
     * @return Post_Type
     */
    public function &add_labels(array $labels) {
        $this->labels = $labels;
        return $this;
    }

    /**
     * @param $name
     * @param string $singular
     * @return Post_Type
     */
    public function &name($name, $singular = '') {
        $this->labels['name'] = $name;
        $this->labels['singular_name'] = empty($singular) ? $name : $singular;
        return $this;
    }

    /**
     * @param bool $val
     * @return Post_Type
     */
    public function &exclude_from_search($val = true) {
        $this->settings['exclude_from_search'] = $val;
        return $this;
    }

    /**
     * @param bool $val
     * @return Post_Type
     */
    public function &publicly_queryable($val = true) {
        $this->settings['publicly_queryable'] = $val;
        return $this;
    }

    /**
     * @param bool $val
     * @return Post_Type
     */
    public function &show_ui($val = true) {
        $this->settings['show_ui'] = $val;
        return $this;
    }

    /**
     * Allow single posts of this type to be added to a nav menu or not.
     * @param bool $val
     * @return Post_Type
     */
    public function &show_in_nav_menus($val = true) {
        $this->settings['show_in_nav_menus'] = $val;
        return $this;
    }

    /**
     * Does this post type show up in the admin menus.
     * @param bool $val
     * @return Post_Type
     */
    public function &show_in_menu($val = true) {
        $this->settings['show_in_menu'] = $val;
        return $this;
    }

    /**
     * Determines you can use create new [post type] from the admin bar.
     * @param bool $val
     * @return Post_Type
     */
    public function &show_in_admin_bar($val = true) {
        $this->settings['show_in_admin_bar'] = $val;
        return $this;
    }

    /**
     * Sets the menu position for the post type.
     *
     * Values:
     * 5 - below Posts
     * 10 - below Media
     * 15 - below Links
     * 20 - below Pages
     * 25 - below comments
     * 60 - below first separator
     * 65 - below Plugins
     * 70 - below Users
     * 75 - below Tools
     * 80 - below Settings
     * 100 - below second separator
     * @param int $val
     * @return Post_Type
     */
    public function &menu_position($val = 5) {
        $this->settings['menu_position'] = $val;
        return $this;
    }

    /**
     * Creates a menu icon for this custom post type.
     *
     * Examples:
     * - dashicons-video-alt (Uses the video icon from Dashicons[2])
     * - theme()->url("images/cutom-posttype-icon.png") (Use a image located in the current theme)
     * @param null $val
     * @return Post_Type
     */
    public function &menu_icon($val = null) {
        $this->settings['menu_icon'] = $val;
        return $this;
    }

    /**
     * @param bool $val
     * @return Post_Type
     */
    public function &hierarchical($val = true) {
        $this->settings['hierarchical'] = $val;
        return $this;
    }

    /**
     * @param $feature
     * @return Post_Type
     */
    public function &supports($feature) {
        if(is_array($feature)) {
            $this->settings['supports'] = $feature;
        } else {
            $this->settings['supports'][] = $feature;
        }

        return $this;
    }

    /**
     * @param bool $val
     * @return Post_Type
     */
    public function &has_archive($val = true) {
        $this->settings['has_archive'] = $val;
        return $this;
    }

    /**
     * @param string $val
     * @return Post_Type
     */
    public function &rewrite_slug($val) {
        $this->settings['rewrite']['slug'] = $val;
        return $this;
    }

    /**
     * Defines the settings of the post type to be passed to register_post_type(),
     * this is a low-level function that will allow you to directly set all the
     * settings associated with the custom post type.
     *
     * @link http://codex.wordpress.org/Function_Reference/register_post_type
     * @param array $settings
     * @return Post_Type
     */
    public function &add_settings(array $settings) {
        if(!empty($settings['labels'])) {
            $labels = array_merge($this->labels, $settings['labels']);
        } else {
            $labels = $this->labels;
        }

        $settings['labels'] = $labels;
        $this->settings = $settings;

        return $this;
    }

    /**
     * Adds a custom taxonomy to this post type. Follows the same rules
     * as register_taxonomy() except that it automatically applies to this
     * post type.
     *
     * @link http://codex.wordpress.org/Function_Reference/register_taxonomy
     * @param string $slug
     * @param array $args
     * @return Post_Type
     */
    public function &add_taxonomy($slug, array $args) {
        $this->taxonomies[$slug] = $args;
        return $this;
    }

    /**
     * Action executed automatically by WordPress that actually creates the
     * post type and taxonomies.
     */
    public function __action_create() {
        $this->settings['labels'] = array_merge($this->labels, $this->settings['labels']);

        register_post_type($this->slug, $this->settings);
        foreach($this->taxonomies as $slug=>$args) {
            register_taxonomy($slug, $this->slug, $args);
        }
    }
} 