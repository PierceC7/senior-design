<?php

/**
 * @package WordPress
 * @subpackage kmdg
 * @author Jake Finley
 *
 * This class manages the response to an AJAX request and can be queried for the
 * original request info. By setting the view type you can return data in multiple
 * formats.
 */
namespace KMDG;

defined( 'ABSPATH' ) or die( 'No direct access' );

require_once('AJAX_Interface.php');

class AJAX_Handler implements \KMDG\AJAX_Interface {

    /**
     * @var object
     */
    private $response;

    /**
     * @var boolean
     */
    private $canceled;

    /**
     * @var string
     */
    private $view_type;

    /**
     * @var boolean
     */
    private $handleErrors;

    /**
     * @var boolean
     */
    private $started;

    /**
     * Set up default response
     *
     * @param boolean $auto_start Determines if the ajax response handler will start
     * processing immediately, or wait until a call tot the start() function.
     */
    public function __construct($auto_start = true) {
        $this->started = false;
        $this->handleErrors = true;
        $this->view_type = 'json';
        $this->canceled = false;
        $this->response = (object) array(
                    'error' => (object) array('PHPError' => false),
                    'data' => (object) array(),
                    'info' => (object) array('request' => (object) array_merge((array) $_GET, (array) $_POST),
                        'debug' => (object) array()));
        if ($auto_start)
            $this->start();
    }

    /**
     * If the application ends without calling finish (and an AJAX response has been started),
     * finish automatically.
     */
    public function __destruct() {
        if ($this->started)
            $this->finish();
    }

    /**
     * Called before anything else, indicates that an AJAX response is about to be processed.
     * This is simply a hook and does not have to do anything specific - it's just a chance for
     * the hanlder to
     */
    public function start() {
        $this->started = true;
        ob_start('self::error_handler');
    }

    /**
     * Called when the ajax response is created and ready to be sent to the client.
     * This should handle sending the response and can safely exit the PHP process when finished.
     */
    public function finish() {
        if ($this->canceled)
            return; // if canceled don't do anything
        $this->canceled = true; // prevent mulitple finishes (via __destruct())
        // If there are errors don't return any data that has been generated as it will be incomplete,
        // likewise if there are no errors remove the empty object
        if ($this->hasErrors()) {
            unset($this->response->error->PHPError); // PHP errors wont go through here
            unset($this->response->data);
        } else {
            unset($this->response->error);
        }

        //If we have no errors, remove debug information unless specific debugging info has been requested
        if (count(get_object_vars($this->response->info->debug)) == 0)
            unset($this->response->info);

        $this->showView(); // Display in the correct format
    }

    /**
     * Adds variables and data to the ajax response, to be sent to the client durring finish()
     *
     * @param string $key The name/identifier of the data
     * @param mixed $data Any data to be returned to the client when the response
     * finalizes - should be output (and encoded) according to the view settings
     */
    public function add($key, $data) {
        $this->response->data->$key = $data;
        return $this;
    }

    /**
     * Gets data that was sent to the server from the client by key name.
     *
     * @param string $key Corresponds to the $_POST (or $_GET/$_COOKIE/$_REQUEST/etc) key
     * you want to retrieve data from.
     * @return mixed
     */
    public function request($key) {
        if (!isset($this->response->info->request->$key))
            return null;
        return $this->response->info->request->$key;
    }

    /**
     * Eats PHP errors and returns them as JSON
     *
     * @param string $str The current output string
     * @return string $str The post-processes output string
     */
    private function error_handler($str) {
        if (!$this->handleErrors)
            return $str;
        $error = error_get_last();
        if ($error) { // Cant use view types or call any functions while in error state
            $this->response->error->PHPError = $error;
            unset($this->response->data);
            return json_encode($this->response);
        }
        return $str; // No errors, return output from the buffer as normal
    }

    /**
     * If called the Ajax request has been canceled and any further calls to this classes
     * methods produce no permanent results (ie, finish() doesn't do anything)
     */
    public function cancle() {
        ob_end_flush();
        $this->canceled = true;
    }

    /**
     * Sends headers and content for the different view types
     */
    private function showView() {
        switch ($this->view_type) {
            case 'none':
                exit(ob_get_clean());
                break;
            case 'html':
                header('Content-Type: text/html');
                if (!empty($this->response->data)) { // If add() was used output the array
                    foreach ($this->response->data as $key => $value) {
                        if (is_array($value) || is_object($value)):
                            echo '<pre>';
                            var_dump($value);
                            echo '</pre>';
                        else:
                            echo $value;
                        endif;
                    }
                }
                exit(ob_get_clean());
                break;
            case 'csv':
                $filename = $this->response->data->filename;
                header('Content-Type: text/csv; charset=UTF-8');
                header('Content-Disposition: attachment; filename="' . $filename . '.csv"');
                header('Pragma: no-cache');
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                exit(ob_get_clean());
                break;
            case 'json':
            default:
                ob_end_clean(); // discard any uncontrolled output
                header('Content-Type: application/json');
                exit(json_encode($this->response));
                break;
        }
    }

    /**
     * Sets the view type - is able to handle the following values: "none", "csv",
     * "json", and "html". The view type should be used durring finish() to
     * determine how the output is formatted and what headers are sent. If "none",
     * the finish() function should not output anything and assume that all output
     * has already been sent - if output buffering is enabled it should end and output
     * the unaltered buffer.
     *
     * @param string $view The slug of the view type that should be used
     */
    public function view($view) {
        $this->view_type = $view;
    }

    /**
     * Create an error response, if fatal is true then this will immediately call finish(),
     * otherwise errors are stored and accumulate until the response is complete.
     *
     * @param string $key The unique identifier of the error, or the error message if
     * not sending formatted data (this will force view('none')).
     * @param mixed $data Any type of data you wish to include with the error - should be output
     * (and encoded) according to the view settings
     * @param boolean $fatal Indicates that the error should cause the ajax response to immidately
     * terminate and output the selected view
     */
    public function error($key, $data = '', $fatal = false) {
        if (empty($data)) {
            $this->view('none');
            echo $key;
        } else {
            $this->view('json');
            $this->response->error->$key = $data;
        }
        
        if ($fatal)
            $this->finish();
    }

    // Checks to see if any errors exist
    public function hasErrors() {
        if (count(get_object_vars($this->response->error)) > 1)
            return true;
        return false;
    }

    /**
     * Call when the response needs to record some non-error, non-succes info.
     * You should consider not handling/storing this info if WP_DEBUG is false.
     *
     * @param string $key The unique identifier for the data
     * @param mixed $data Any type of data you want to send to the client - should be output
     * (and encoded) according to the view settings
     */
    public function debugInfo($key, $data) {
        if (defined('WP_DEBUG') && WP_DEBUG == true) {
            $this->response->info->debug->$key = $data;
        }
        return $this;
    }

    /**
     * Turns off error handling for this Ajax response
     */
    public function disableErrorHandler() {
        $this->handleErrors = false;
    }

}

?>