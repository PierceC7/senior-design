<?php
/**
 * @package WordPress
 * @subpackage kmdg
 * @author Jake Finley
 *
 * This is a wrapper class for the WordPress metabox functionality, it provides an object-oriented and
 * simplified way of adding metaboxes to pages and posts. Also provided are several static methods that
 * can be used to generate form elements that are out-of-the-box compatible with metaboxes.
 */

namespace KMDG;

defined( 'ABSPATH' ) or die( 'No direct access' );

class MetaBox {
    /**
     * @var string|int
     */
    protected $id;
    /**
     * @var string
     */
    protected $title;
    /**
     * @var string
     */
    protected $post_type;
    /**
     * @var string
     */
    protected $context;
    /**
     * @var string
     */
    protected $priority;
    /**
     * @var array
     */
    protected $fields;
    /**
     * @var Callable|string
     */
    protected $callback;

    /**
     * Registers a new metabox and allocates space for it on the edit screen.
     *
     * @param string $id The css ID of the metabox - also used as a slug identifier in some places
     * @param string $title The visible title of the MetaBox
     * @param mixed $post_type An array or string containing the post type(s) the metabox should be shown on (or 'dashboard')
     * @param string $context The part of the page where the metabox should be shown ('normal', 'advanced', or 'side')
     * @param string $priority The priority within the context where the boxes should show ('high', 'core', 'default' or 'low')
     */
    function &__construct($id, $title, $post_type = "page", $context = "advanced", $priority = "default") {
        $this->id = $id;
        $this->title = $title;
        $this->post_type = $post_type;
        $this->context = $context;
        $this->priority = $priority;
        $this->fields = array();

        add_action('add_meta_boxes', array($this, '_action_add_metabox'));
        add_action('save_post', array($this, '_action_save_metabox'));
        add_action('wp_dashboard_setup', array($this, '_action_add_metabox'));

        return $this;
    }

    /* Public */

    /**
     * Use this to register a view for the metabox. The view can be the path to a file,
     * the name of a function (string), or a Closure / Anonymous function. The content of
     * the view is shown when the metabox is displayed on the page - you should omit any
     * <form> tags even if you use input elements as the entire edit page is a form.
     *
     * @param mixed $callback The file or function to be loaded when the metabox is displayed
     */
    public function &view($callback) {
        $this->callback = $callback;

        return $this;
    }

    /**
     * You must register all fields that a metabox needs to save. Pass in the name of each
     * field (it's HTML name attribute) that should be saved in the database (as an array,
     * or one at a time as a string). This will cause them to be registered in the database
     * with the same name and you can retrieve them using get_post_meta($field_name).
     */
    public function &register($field_name) {
        if(!is_array($field_name)) {
            $field_name = array($field_name);
        }
        $this->fields = array_merge($this->fields, $field_name);

        return $this;
    }

    /**
     * Create an input element with the provided attributes, this element will
     * automatically be added to the MetaBox save/load process.
     * @param string $name
     * @param array $attrs
     */
    public static function input($name, array $attrs = array('type'=>'text')) {
        global $post;
        echo '<input ';

        echo 'name="'.$name.'" ';
        echo 'value="'.get_post_meta($post->ID, $name, true).'" ';

        foreach($attrs as $attr=>$value) {
            echo $attr.'="'.$value.'" ';
        }
        echo '/>';
    }

    /**
     * Create an textarea with the provided attributes, this element will
     * automatically be added to the MetaBox save/load process.
     * @param string $name
     * @param array $attrs
     */
    public static function textarea($name, array $attrs = array()) {
        global $post;
        echo '<textarea ';

        echo 'name="'.$name.'" ';

        foreach($attrs as $attr=>$value) {
            echo $attr.'="'.$value.'" ';
        }

        echo '>'.get_post_meta($post->ID, $name, true).'</textarea>';
    }

    /**
     * Create an select element with the provided attributes, this element will
     * automatically be added to the MetaBox save/load process. Add options as
     * an associative array in the second parameter.
     * @param string $name
     * @param array $options
     * @param array $attrs
     */
    public static function select($name, array $options, array $attrs = array()) {
        global $post;

        echo '<select ';
        echo 'name="'.$name.'" ';

        foreach($attrs as $attr=>$value) {
            echo $attr.'="'.$value.'" ';
        }

        echo '>';

        foreach($options as $value=>$label) {
            if(get_post_meta($post->ID, $name, true) == $value) {
                echo '<option value="'.$value.'" selected="selected">'.$label.'</option>';
            } else {
                echo '<option value="'.$value.'">'.$label.'</option>';
            }
        }

        echo '</select>';
    }

    /**
     * Create an wysiwyg editor with the provided attributes, this
     * element will automatically be added to the MetaBox save/load process.
     * @param string $name
     * @param array $attrs
     */
    public static function wysiwyg($name, array $attrs = array()) {
        global $post;

        wp_editor(get_post_meta($post->ID, $name, true), $name, $attrs);
    }

    /* Actions */

    /**
     * Registers the metabox, called automatically by wordpress.
     */
    public function _action_add_metabox() {
        add_meta_box($this->id, $this->title, array($this, '_callback_view_metabox'), $this->post_type, $this->context, $this->priority);
    }

    /**
     * WordPress calls this automatically when you save a post, it will update
     * the post meta info with the new values in entered into your metabox.
     */
    public function _action_save_metabox($post_id) {
        if($this->verify_save($post_id)) {
            foreach($this->fields as $field) {
                update_post_meta($post_id, $field, esc_sql($_POST[$field]));
            }
        }
    }

    /* Callback */

    /**
     * This function is called automatically by WordPress to display the metabox content.
     *
     * @param \WP_Post $post The object for the current post/page.
     */
    public function _callback_view_metabox(\WP_POST $post) {
        $callback = $this->callback;
        if(is_string($callback) && !is_callable($callback) && file_exists(self::path($callback))) {
            include(self::path($callback));
        } elseif(is_callable($callback) && !($callback instanceof \Closure)) {
            call_user_func($callback, $post);
        } elseif($this->callback instanceof \Closure) {
            $callback($post);
        } else {
            die('Invalid metabox callback for Metabox: '.$this->id);
        }

        // Add an nonce field so we can check for it later.
        wp_nonce_field($this->id.'_meta_box', $this->id.'_meta_box_nonce');
    }

    /* Private */

    /**
     * A wrapper for get_stylesheet_directory that shortens and makes it a little more user friendly.
     * This should be used over get_stylesheet_directory().
     * @param string $path (optional) A relative path from the theme directory (and/or file name)
     * @return string
     */
    private static function path($path = '') {
        return str_replace('/', DIRECTORY_SEPARATOR, get_stylesheet_directory().DIRECTORY_SEPARATOR.$path);
    }

    /*
     * We need to verify this came from our screen and with proper authorization,
     * because the save_post action can be triggered at other times.
     *
     * Derived from wordpress.org code
     */
    private function verify_save($post_id) {

        // Check if our nonce is set.
        if ( ! isset( $_POST[$this->id.'_meta_box_nonce'] ) ) {
            return false;
        }

        // Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $_POST[$this->id.'_meta_box_nonce'], $this->id.'_meta_box' ) ) {
            return false;
        }

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return false;
        }

        // Check the user's permissions.
        if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

            if ( ! current_user_can( 'edit_page', $post_id ) ) {
                return false;
            }

        } else {

            if ( ! current_user_can( 'edit_post', $post_id ) ) {
                return false;
            }
        }

        return true;
    }
}
