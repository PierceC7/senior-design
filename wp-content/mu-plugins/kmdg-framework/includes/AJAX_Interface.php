<?php
/**
 * @package WordPress
 * @subpackage kmdg
 * @author Jake Finley
 *
 * This interface describes a class that manages the response to an AJAX request
 * and can be queried for the original request info.
 */
namespace KMDG;

defined( 'ABSPATH' ) or die( 'No direct access' );

interface AJAX_Interface {

    /**
     * Called before anything else, indicates that an AJAX response is about to be processed.
     * This is simply a hook and does not have to do anything specific - it's just a chance for
     * the hanlder to
     */
    public function start();

    /**
     * Called when the ajax response is created and ready to be sent to the client.
     * This should handle sending the response and can safely exit the PHP process when finished.
     */
    public function finish();

    /**
     * Used to register an error in the AJAX request. The implementing class can choose
     * how to handle errors, but after some errors are registered finish() may be called
     * if the request cannot be completed after experiencing this error.
     * @param string $msg
     */
    public function error($msg);

    /**
     * Gets data that was sent to the server from the client by key name. You can use this
     * to define which of the HTTP methods of passing data to the server will be used.
     *
     * @param string $key Corresponds to the $_POST (or $_GET/$_COOKIE/$_REQUEST/etc) key
     * you want to retrieve data from.
     * @return mixed
     */
    public function request($key);
}
