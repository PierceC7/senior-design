<?php
namespace KMDG;

defined( 'ABSPATH' ) or die( 'No direct access' );

/**
 * @package WordPress
 * @subpackage kmdg
 * @version 3.0
 *
 * This class provides helper functions for KMDG themes, it simplifies some
 * WordPress functionality and provides easy access to some commonly needed
 * features. It also allows a JSON settings file to be created which when loaded
 * overrides the default settings defined in the class.
 */

class Framework {
    const DEFAULT_JQUERY_VERSION = '3.4.1';
    const SETTINGS_START = '# BEGIN Theme Settings';
    const SETTINGS_END = '# END Theme Settings';

    /**
     * @var Framework
     */
    protected static $instance;
    /**
     * @var array
     */
    protected $settings;
    /**
     * @var string
     */
    protected $settings_file;
    /**
     * @var array
     */
    protected $save_actions;
    /**
     * @var array
     */
    protected $deferred_styles;
    /**
     * @var array
     */
    protected $include_responsive_image_css;

    /**
     * Private/protected constructor prevents outside initialization, use Framework::get_instance()
     * to retrieve a reference to the singleton.
     */
    protected function __construct() {}
    protected function __clone() {}
    protected function __wakeup() {}

    /**
     * This bootstrap method is for child classes to extend, it is called
     * immediately after the framework is set up.
     */
    protected function boot() {
        // Does nothing, override me
    }

    /**
     * Returns an instance of this class, if none exists it will create one.
     * This class uses the singleton pattern and cannot be initialized more
     * than once.
     * @api
     * @return {static}
     */
    public static function get_instance() {
        if(is_null(static::$instance)) {
            static::$instance = new static;
        }
        return static::$instance;
    }

    /**
     * This sets up the class and loads the settings.
     * @api
     */
    public function init() {
        $this->include_responsive_image_css = true; // By default responsive images styles are automatically included in the head

        $this->initialize_settings();
        $this->setup_theme_hooks();
        $this->boot();
    }

    /**
     * Preforms all initialization steps required for activating the theme.
     */
    protected function setup_theme_hooks() {

        /* ACTIONS */
        add_action('init', array($this, '_action_verify_htaccess'));
        add_action('init', array($this, '_action_register_styles'));
        add_action('after_switch_theme', array($this, '_action_add_user_role'));
        add_action('switch_theme', array($this, '_action_unmodify_htaccess'), 10, 2);
        add_action('wp_enqueue_scripts', array($this, '_action_register_scripts'));
        add_action('admin_enqueue_scripts', array($this, '_action_register_scripts'));
        add_action('after_setup_theme', array($this, '_action_register_theme_support'));
        add_action('after_setup_theme', array($this, '_action_register_menus'));
        add_action('after_setup_theme', array($this, '_action_register_options_pages'));
        add_action('admin_menu', array($this, '_action_remove_admin_menus'), 9999);
        add_action('wp_head', array($this, '_action_add_ajaxurl'), 1);
        add_action('wp_head', array($this, '_action_responsive_image_css'));
        add_action('save_post', array($this, '_action_save_post'));
        add_action('wp_footer', array($this, '_action_deferred_styles'));

        /* FILTERS */
        add_filter('excerpt_more', array($this, '_filter_custom_more_text'));
        add_filter('upload_mimes', array($this, '_filter_custom_upload_mimes'));
        add_filter('manage_posts_columns', array($this, '_filter_custom_columns'));
        add_filter('manage_pages_columns', array($this, '_filter_custom_columns'));
        add_filter('the_content', array($this, '_filter_shortcode_empty_paragraph_fix'));

        /* WP-HEAD CLEANUP */
        remove_action('wp_head', 'feed_links_extra', 3);
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'parent_post_rel_link', 10, 0);
        remove_action('wp_head', 'start_post_rel_link', 10, 0);
        remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
        remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
        remove_action('wp_head', 'wp_generator');

        /* Remove stupid filters */
        remove_filter( 'the_title', 'capital_P_dangit', 11 );
        remove_filter( 'the_content', 'capital_P_dangit', 11 );
        remove_filter( 'comment_text', 'capital_P_dangit', 31 );

        /* SHORTCODES */
        $this->load_shortcodes();
    }

    ####################### DEPENDENCIES #########################

    /**
     * Retrieves the utilities class. If you extend KMDG\Utils overload this
     * function in your Theme class.
     * @return \KMDG\Utils
     */
    public function utilities() {
        return Utils::get_instance();
    }

    ########################### PUBLIC ###########################

    /**
     * Creates a link to a page on the site using a relative path,
     * if an absolute link is provided then it is passed through unaltered.
     *
     * @api
     * @param string|null $path
     * @return string
     */
    public function link($path = '') {
        $url = $path;
        $qualified = strpos($path, '//');
        if($qualified === false || $qualified > 10){
            $url = home_url($path);
        }

        return $url;
    }

    /**
     * @see \KMDG\Utils::url()
     */
    public function url($path = '') {
        return $this->utilities()->url($path);
    }

    /**
     * @see \KMDG\Utils::path()
     */
    public function path($path = '') {
        return $this->utilities()->path($path);
    }

    /**
     * An expanded version of get_template_part() that will except up to three parameters
     * and attempt to load files in the format "$path/$slug/$name-$subtype.php". This will
     * act like get_template_part() and check the child theme first, then the parent. If
     * the first filename is not found in either theme then fallback checking will occur
     * just like with get_template_part(). Name formats to be checked are as follows and
     * in this specific order starting from highest to lowest:
     *
     * "$path/$slug/$name-$subtype.php"
     * "$path/$slug/$name.php"
     * "$path/$slug/any-$subtype.php"
     * "$path/$slug/$slug.php"
     *
     * Note that unlike get_template_part() this function specifically looks in the theme
     * parts folder (by default /parts), and if you need to override this you can use the
     * provided theme_parts_path filter to return a different path. You may also modify
     * the checked path formats using the theme_parts_formats filter.
     *
     * This will also allow you to include an array of data that should be passed to the
     * template part as the $args[] variable.
     *
     * @api
     * @param string $slug
     * @param string|null $name
     * @param string|null $subtype
     * @param array|null $args
     * @filter theme_parts_path
     * @filter theme_parts_formats
     */
    public function part($slug, $name = null, $subtype = null, array $args = null) {
        $path = apply_filters('theme_parts_path', 'parts');
        $search = apply_filters('theme_parts_formats', array(
            "$path/$slug/$name-$subtype.php",
            "$path/$slug/$name.php",
            "$path/$slug/any-$subtype.php",
            "$path/$slug/$slug.php"
        ), $slug, $name, $subtype);

        $part_info = $this->locate_template($search, false, false);

        if(!empty($part_info)) {
            $part = $part_info['path'];
            $theme = $part_info['theme'];

            if(WP_DEBUG) { // If debugging is on print out fully qualified part names as comments
                $full_part_name = "$theme/$slug/".str_ireplace('.php', '', basename($part));

                echo "<!-- [START $full_part_name] -->";
                $this->load_template($part, false, $args);
                echo "<!-- [END $full_part_name] -->";

            } else {
                $this->load_template($part, false, $args);
            }
        }
    }

    /**
     * Sets the text used to indicate that a post excerpt has been truncated.
     * @api
     * @param string $text The text to be appended to truncated excerpts.
     */
    public function set_post_more_text($text) {
        $this->settings['excerpt_more_text'] = $text;
    }

    /**
     * Adds an uploadable mime type, permitting files of the given extension and mime type
     * to be uploaded via the WordPress media library.
     * @api
     * @param string $extension The file extension of the uploadable type (ex: "pdf")
     * @param string $mime The mime type of the uploadable type (ex: "application/pdf")
     */
    public function add_uploadable_type($extension, $mime) {
        $this->settings['upload_types'][$extension] = $mime;
    }

    /**
     * Added .htaccess directives that will be added when the theme is activated,
     * these will also be removed when the theme is de-activated. Directives added
     * in this manner should meet the follow qualifications:
     *
     * - They should be required for the theme (or plugins) to function correctly
     * - They must not be server specific (no hard-coded paths for example)
     * - They should not be required when the theme is removed
     *
     * If this directive will not work on a server running PHP in CGI mode, you can
     * specify a two additional parameters containing a PHP.ini varname and value,
     * which will be used instead of the .htaccess directive (this uses ini_set() and
     * does not modify the php.ini file).
     *
     * Any other directives should be placed directly into the .htaccess file like normal
     * @api
     * @param string $directive An .htaccess directive required by the theme
     * @param array $cgi_version (Optional)
     */
    public function add_directive($directive, $cgi_var = NULL, $cgi_val = NULL) {
        if(strpos(php_sapi_name(), 'cgi') !== false && !empty($cgi_var)) {
            ini_set($cgi_var, $cgi_val);
        } else {
            $this->settings['directives'][] = $directive;
        }
    }

    /**
     * Adds a menu location to the theme that users can assign WordPress menu's to.
     * @api
     * @param string $slug A short slug identifying the menu (no spaces, no capitals, etc)
     * @param string $description The visible name of the menu, should describe it's location/use
     */
    public function add_menu($slug, $description) {
        $this->settings['menu-locations'][$slug] = $description;
    }

    /**
     * Adds a new type of theme support. These can be both WordPress built in features
     * and optional features supported by KMDG themes.
     * @api
     * @param string $slug The slug of the feature to be enabled
     * @param array $options (Optional) An array of options that can be associated with a feature
     */
    public function add_feature($slug, array $options = array()) {
        $this->settings['supports'][$slug] = $options;
    }

    /**
     * Checks if a theme supports a given feature. This can be a built in WordPress feature or
     * a theme specific feature.
     * @api
     * @param string $slug The slug of the feature to be checked
     * @return boolean
     */
    public function supports($slug) {
        return !empty($this->settings['supports'][$slug]);
    }

    /**
     * Adds a JavaScript file that will automatically be included on each page. This uses the
     * WordPress sanctioned way of including scripts and will remove conflicting versions of
     * scripts that may be included by plugins. It will also concatinate and minify scripts
     * if those options are enabled within WordPress.
     * @api
     * @param string $slug A unique identifier for the script (used for specifying it as a dependency)
     * @param string $url An absolute OR relative URL (to the theme directory) where the script is located
     * @param boolean $footer If true this script will be loaded in the footer, rather than the head
     * @param array $dependencies An array of script slugs which this script requires to be loaded first
     * @param array $access Indicates if the script should be loaded on the front (public) or back end (admin), or both.
     * @param string $version The version number of the script, if any
     */
    public function add_script($slug, $url, $footer = false, array $dependencies = array(), array $access = array('public'), $version = NULL) {
        $this->settings['scripts'][$slug] = array(
            'url'           => $url,
            'footer'        => $footer,
            'dependencies'  => $dependencies,
            'access'        => $access,
            'version'       => $version
        );
    }

    /**
     * Returns all settings related to a theme feature.
     * @api
     * @param string $slug The slug of the feature to retrieve settings for
     * @return array
     */
    public function get_feature_options($slug) {
        return $this->settings['supports'][$slug];
    }

    /**
     * This adds a new column to the admin screen when viewing Pages, Posts, or Custom Post Types.
     * You can control the content of each column using the $callback parameter as detailed below.
     *
     * @api
     * @param string $slug The unique identifier of the column
     * @param string $name The friendly name of the column (used as the Header title)
     * @param mixed $post_type A string or array of post type names that this column should be shown for
     * @param int $position The numerical position that the column should be placed in
     * @param mixed $callback Any of the following:
     *
     * - A Closure (anonymous) callback function
     * - A string containing the name of a callable function
     * - An array containing the class and name of a callable function
     * - A string containing the name and theme relative path of a file to be included and parsed
     */
    public function add_custom_column($slug, $name, $post_type, $position, $callback) {
        $this->settings['custom_columns'][$slug] = array(
            'name'      => $name,
            'post_type' => $post_type,
            'position'  => $position,
            'callback'  => $callback
        );
    }

    /**
     * Returns the template slug of the currently active template.
     * @api
     * @return string
     * @filter theme_template_name
     */
    public function template_name() {
        $template = 'page';

        if(is_home()) {
            $template = 'index';
        } elseif(is_author()) {
            $template = 'author';
        } elseif(is_post_type_archive()) {
            $template = 'archive-'.get_query_var('post_type');
        } elseif (is_tax()) {
            $template = 'archive-'.get_query_var('taxonomy');
        } elseif(is_archive()) {
            $template = 'archive';
        } elseif(is_404()) {
            $template = '404';
        } elseif(is_singular() && !is_page()) {
            $template = get_post_type();
        } else {
            $template = str_replace('.php', '', basename(get_page_template()));
        }

        return apply_filters('theme_template_name', $template);
    }

    /**
     * Checks to see if a post is being displayed as in an archive type view or as a
     * single post. Returns the text single or archive to indicate the display mode.
     * @filter theme_post_display_mode
     * @return string
     */
    public function post_display_mode() {
        $template = 'single';

        if(!is_single()) {
            $template = 'archive';
        }

        return apply_filters('theme_post_display_mode', $template);
    }

    /**
     * Returns the slug of the current page, if the page is an automatically generated page
     * the slug will be the name of the template that is generating it (such as "archive", "index", etc).
     * @api
     * @return string
     * @filter theme_page_slug
     */
    public function page_slug() {
        global $post;
        $slug = '';

        if(is_home()) {
            $slug = 'index';
        } elseif(is_author()) {
            $slug = 'author';
        } elseif(is_post_type_archive()) {
            $slug = 'archive-'.get_query_var('post_type');
        } elseif (is_tax()) {
            $slug = 'archive-'.get_query_var('taxonomy');
        } elseif(is_archive()) {
            $slug = 'archive';
        } elseif(is_404()) {
            $slug = '404';
        } else {
            $slug = $post->post_name;
        }

        return apply_filters('theme_page_slug', $slug);
    }

    /**
     * Attempts to display the correct title based on what type of page
     * or archive is being displayed.
     *
     * @api
     * @filter theme_search_title
     */
    public function the_page_title() {
        if(is_category()) {
            single_cat_title();
        } elseif(is_tag()) {
            single_tag_title();
        } elseif(is_author()) {
            echo 'Posts by '.get_the_author();
        } elseif(is_tax()) {
            single_term_title();
        } elseif(is_search()) {
            echo apply_filters('theme_search_title', 'Search Results');
        } elseif(is_post_type_archive()) {
            post_type_archive_title();
        } elseif(is_home()) {
            echo get_the_title(get_option('page_for_posts' ));
        } else {
            the_title();
        }
    }

    /**
     * Checks to see if the currently logged in user has a specific role (such as "administrator")
     * and returns true if they do, false if they dont. This also returns false if the user isn't
     * logged in.
     * @api
     * @param string $role The slug of the user role to be tested for
     * @return boolean
     */
    public function current_user_is($role) {
        $is_role = false;

        if(get_current_user_id() != 0) {
            $user = get_userdata(get_current_user_id());
            if(in_array($role, $user->roles)) $is_role = true;
        }

        return $is_role;
    }

    /**
     * Creates a new ACF options page. The first page added will be the parent
     * page of all others. Use the direct method of creating options pages for
     * more control (this is just a shortcut).
     * @api
     * @param string $name
     */
    public function add_options_page($name) {
        $this->settings['acf_options_pages'][] = $name;
    }

    /**
     * Creates a responsive-compatible image and markup that is specifically tailored
     * to suit any situation. Image size, container size, resizing, and proportions
     * are all handled. The image itself is displayed as a background image, while
     * a second copy of the image (or a dynamically generated spacer, if re-proportioning
     * the image) is included as a visibility:hidden image tag within the background
     * div (set to display:inline-block). The image tag gives the div it's automatic
     * width and height, and provides normal image responsive scaling while css
     * background-size:cover automatically crops the image to the desired proportions.
     *
     * @api
     * @param mixed $image Image attachment ID or URL of image (relative URLs default to the theme folder)
     * @param int $width (Optional) Expands or reduces the width of the image until it's this wide. Default: NULL
     * @param int $height (Optional) Expands or reduces the height of the image until it's this tall. Defualt: NULL
     * @param string $size (Optional) WordPress image size if using an attachment. Default: "medium"
     * @param string $alt (Optional) The alt attribute to be included on the image
     * @param string $link (Optional) A url that the image should link to when clicked
     * @param string $target (Optional) The target attribute of the link (defaults to "_self")
     */
    public function responsive_image($image, $width = NULL, $height = NULL, $size = 'medium', $alt = null, $link = null, $target = '_self') {
        $use_spacer = false;

        if(is_numeric($image)) {
            list($url, $base_width, $base_height)  = wp_get_attachment_image_src($image, $size);
        } elseif(!is_null($image)) {
            $url = $this->url($image);

            if(strpos($image, '//') !== false) {
                list($base_width, $base_height) = getimagesize($image);
            } else {
                list($base_width, $base_height) = getimagesize($this->path($image));
            }
        } else {
            $url = $this->get_placeholder_image($width, $height);
        }

        if(empty($width)) {
            $width = $base_width;
        } else {
            $use_spacer = true;
        }

        if(empty($height)) {
            $height = $base_height;
        } else {
            $use_spacer = true;
        }

        if($use_spacer) {
            $spacer = $this->get_spacer_url($width, $height);
        } else {
            $spacer = $url;
        }

        if(!empty($link)) {
            $this->linked_responsive_image_html($url, $spacer, $alt, $link, $target);
        } else {
            $this->responsive_image_html($url, $spacer, $alt);
        }
    }

    /**
     * Registers functions to be run when a post of the given post type is saved, if
     * "any" is used as the post_type then the function will be run for all saved
     * posts regardless of post type. The post_id is passed in as an argument to
     * the provided callback function.
     *
     * @api
     * @param string $post_type The post type slug that the callback applies to
     * @param callable $callback The function to be executed on save
     */
    public function do_on_save($post_type, $callback) {
        $this->save_actions[] = array('post_type'=>$post_type, 'callback'=>$callback);
    }

    /**
     * Creates pagination links for a given WP_Query object (defaults to the global one).
     * By default this shows the currently selected page (along with two pages to
     * either side of it) as well as the first and last page, along with previous
     * and next buttons. All other pages are hidden behind an elipse.
     *
     * You may pass this function any array you could normally send to WordPress'
     * built in paginate_links() function and it will be merged with the default
     * options to create the paginated links.
     *
     * This function bypasses an issue with paginate_links not working on search
     * result pages.
     *
     * @api
     * @global \WP_Query $wp_query
     * @global \WP_Rewrite $wp_rewrite
     * @param Array $options An array compatible with the args for paginate_links()
     * @param \WP_Query $query
     * @see http://codex.wordpress.org/Function_Reference/paginate_links
     */
    public function pagination($options = array(), \WP_Query $query = null) {
        global $wp_query, $wp_rewrite;

        if(is_null($query)) {
            $query = $wp_query;
        }

        $current = get_query_var( 'paged' ) > 1 ? get_query_var('paged') : 1;

        $pagination = array_merge(array(
            'base'      => add_query_arg( 'paged', '%#%'),
            'current'   => $current,
            'total'     => $query->max_num_pages,
            'type'      => 'list',
            'end_size'  => 2,
            'mid_size'  => 2,
            'prev_text' => 'Previous',
            'next_text' => 'Next'
        ), $options);

        if ( $wp_rewrite->using_permalinks() && empty($options['base']) ) {
            $pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ).'page/%#%/', 'paged' );
        }

        if ( ! empty( $wp_query->query_vars['s'] ) ) {
            $pagination['add_args'] = array( 's' => get_query_var( 's' ) );
        }

        echo paginate_links($pagination);
    }

    /**
     * A wrapper for get_placeholder_image() which immediately echo's the placeholder's
     * data URL.
     * @see get_placeholder_image
     * @param $width
     * @param $height
     * @param int $font_size
     * @filter theme_placeholder_font
     * @link http://stackoverflow.com/questions/14517094/aligning-php-generated-image-dynamic-text-in-center
     */
    public function the_placeholder_image($width, $height, $font_size = 16) {
        echo $this->get_placeholder_image($width, $height, $font_size);
    }

    /**
     * Outputs a data URL for a placeholder image of the given width and height,
     * puts the dimensions of the image centered in the picture. Optionally you can
     * specify a font size used to display the dimensions.
     * @param $width
     * @param $height
     * @param int $font_size
     * @return string
     */
    public function get_placeholder_image($width, $height, $font_size = 16) {
        $font       = apply_filters('theme_placeholder_font', KMDG_FRAMEWORK.'/assets/fonts/Vera.ttf');
        $text       = "{$width} x {$height}";

        // Create image and set background
        $img = imagecreate($width, $height);
        imagesavealpha($img, true);
        $color = imagecolorallocate($img, 0xCC, 0xCC, 0xCC);
        imagefill($img, 0, 0, $color);

        // Get text bounding box
        $text_box = imagettfbbox($font_size, 0, $font, $text);

        // Get your Text Width and Height
        $text_width = $text_box[2]-$text_box[0];
        $text_height = $text_box[3]-$text_box[1];

        // Calculate coordinates of the text
        $x = ($width/2) - ($text_width/2);
        $y = (($height/2) - ($text_height/2)) + ($font_size/2); // Without the font size adjustment the baseline for the text is centered vertically

        // Write text to image
        $text_color = imagecolorallocate($img, 0x77, 0x77, 0x77);
        imagettftext($img, $font_size, 0, $x, $y, $text_color, $font, $text);

        // Create image and base64 encode it
        ob_start();
        imagepng($img);
        $imageData = base64_encode(ob_get_clean());
        imagedestroy($img);

        // Output data URL
        return 'data:image/png;base64,'.$imageData;
    }

    /**
     * Performs a <pre></pre> styled var_dump, by passing in the name of a class
     * variable you can also read the value of protected and private properties
     * within this class.
     * @api
     * @param mixed $var
     */
    public function debug($var) {
        echo '<pre>';
        if(@empty($this->$var)) {
            var_dump($var);
        } else {
            var_dump($this->$var);
        }
        echo '</pre>';
    }

    ########################### PROTECTED ###########################

    /**
     * Ensures that all require setting fields are present (even if empty).
     * The passed in values will override the defaults when there is a conflict.
     */
    protected function initialize_settings() {
        $base = array(
            'css_pie'               => array(),
            'stylesheets'           => array(),
            'scripts'               => array(),
            'menu_locations'        => array(),
            'custom_columns'        => array(),
            'admin_menus'           => array(),
            'supports'              => array(),
            'upload_types'          => array(),
            'htaccess_directives'   => array(),
            'excerpt_more_text'     => null,
            'acf_options_pages'     => array()
        );

        $standard = $this->default_settings();
        $this->settings  = $this->utilities()->array_merge_recursive_distinct($base, $standard);
    }

    protected function default_settings() {
        return array(
            'css_pie'           => array(
                'active'            => false,
                'selectors'         => 'styles/pie.css',
                'HTC'               => 'styles/PIE.htc'
            ),
            'stylesheets'        => array(
                'main'              => array(
                    'url'               => 'styles/css/style.css',
                    'media'             => 'screen',
                    'location'          => 'public',
                    'defer'             => false,
                    'dependencies'      => array('bootstrap')
                ),
                'admin'             => array(
                    'url'               => 'styles/css/admin.css',
                    'media'             => 'screen',
                    'location'          => 'admin',
                    'defer'             => false
                ),
                'editor'            => array(
                    'url'               => 'styles/css/editor.css',
                    'media'             => 'screen',
                    'location'          => 'editor',
                    'defer'             => false
                ),
                'bootstrap'         => array(
                    'url'               => 'styles/css/bootstrap.min.css',
                    'media'             => 'screen',
                    'location'          => 'public',
                    'defer'             => false,
                    'version'           => '3.3.2'
                )
            ),
            'scripts'           => array(
                'jquery'            => array(
                    'url'               => '//ajax.googleapis.com/ajax/libs/jquery/'.static::DEFAULT_JQUERY_VERSION.'/jquery.min.js',
                    'dependencies'      => NULL,
                    'footer'            => false,
                    'version'           => static::DEFAULT_JQUERY_VERSION,
                    'access'            => array('admin','public')
                ),
                'theme_library'      => array(
                    'url'               => 'js/library.js',
                    'dependencies'      => array('jquery'),
                    'footer'            => false,
                    'version'           => false,
                    'access'            => array('admin','public')
                ),
                'theme_global'       => array(
                    'url'               => 'js/global.js',
                    'dependencies'      => array('jquery','theme_library','ajax_api'),
                    'footer'            => true,
                    'version'           => false,
                    'access'            => array('public')
                ),
                'bootstrap'         => array(
                    'url'               => 'js/bootstrap.min.js',
                    'dependencies'      => array('jquery'),
                    'footer'            => false,
                    'version'           => '3.3.2',
                    'access'            => array('public')
                ),
                'ajax_api'          => array(
                    'url'               => KMDG_FRAMEWORK_URL.'/assets/js/ajax.js',
                    'dependencies'      => array('jquery'),
                    'footer'            => false,
                    'version'           => KMDG_FRAMEWORK_VERSION,
                    'access'            => array('admin', 'public')
                )
            ),
            'menu_locations'        => array(),
            'custom_columns'        => array(
                'featured-image'        => array(
                    'post_type'             => 'post',
                    'name'                  => 'Featured Image',
                    'position'              => 0,
                    'callback'              => array($this, 'default_post_custom_column')
                )
            ),
            'admin_menus'           => array(
                'acf'                   => array(
                    'page'                  => 'edit.php?post_type=acf',
                    'allow'                 => array('webdev')
                )
            ),
            'supports'              => array(
                'menus',
                'post-thumbnails',
                'html5'
            ),
            'upload_types'          => array(),
            'htaccess_directives'   => array(),
            'excerpt_more_text'     => '...',
            'acf_options_pages'     => array()
        );
    }

    /**
     * Wrapper for $include_responsive_image_css that allows it to be turned off
     * on sites where it needs to be manually configured. If you want to disable
     * automatic inclusion of the CSS just override this function to always
     * return false
     * @return boolean
     */
    protected function check_include_responsive_image_css() {
        return $this->include_responsive_image_css;
    }

    /**
     * Locates and if needed creates the .htaccess file.
     * @return string
     */
    protected function get_htaccess() {
        // Determine absolute path to the base site
        if(home_url() != site_url()){
            $basedir = dirname(ABSPATH);
        } else {
            $basedir = ABSPATH;
        }

        //Full name/path of .htaccess file
        $filename =  $basedir.DIRECTORY_SEPARATOR.'.htaccess';

        // Open and read .htaccess file (create a blank if it doesn't exist)
        $f = fopen($filename, "c+");
        fclose($f); // file now exists if it didn't before

        return $filename;
    }

    /**
     * Creates or modifies the .htaccess file with the all required directives.
     */
    protected function create_htaccess() {

        // Locate and open .htaccess file
        $filename = $this->get_htaccess();
        $htaccess = file_get_contents($filename);

        // Format for entry
        $settings = $this->htaccess_directive_text();

        // Wipe any existing settings
        $htaccess = trim(preg_replace("/".static::SETTINGS_START."[\w\W]*".static::SETTINGS_END."/mi", '', $htaccess), "\r\n");

        // Replace/Add settings to .htaccess
        $htaccess .= $settings;
        file_put_contents($filename, $htaccess);
    }

    /**
     * Returns formatted .htaccess directives that can be inserted directly into the file
     */
    protected function htaccess_directive_text() {
        if(empty($this->settings['directives']) || !is_array($this->settings['directives']))
            $this->settings['directives'] = array();

        return "\r\n\r\n".
            static::SETTINGS_START."\r\n".
            'RedirectMatch 404 .*/\.git.*'."\r\n". // Block access to git files
            implode("\r\n", $this->settings['directives'])."\r\n".
            static::SETTINGS_END."\r\n".
            "\r\n";
    }

    /**
     * Checks to see if we're viewing wp-login.php or wp-register.php, neither of
     * which are considered "Admin" pages and therefor do not register as is_admin().
     * @return boolean
     */
    protected function is_login_page() {
        return in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'));
    }

    /**
     * Creates a transparent PNG and base64 encodes it as a URL. The new image
     * is not saved in a file as all the information is contained within the
     * URL string. The resulting string can be used as the src attribute for
     * an image tag.
     *
     * @param int $width
     * @param int $height
     * @return string
     */
    protected function get_spacer_url($width, $height) {
        $img = imagecreate($width, $height);
        imagesavealpha($img, true); // Have transparency
        $color = imagecolorallocatealpha($img, 0, 0, 0, 127); // Create transparent color
        imagefill($img, 0, 0, $color); // Fill BG with transparent

        // Create image and base64 encode it
        ob_start();
        imagepng($img);
        $imageData = base64_encode(ob_get_clean());
        imagedestroy($img);

        // Return data URL
        return 'data:image/png;base64,'.$imageData;
    }

    /**
     * Creates the HTML (and JS) for a responsive image that has a link.
     *
     * @param string $image
     * @param string $spacer
     * @param string $alt
     * @param string $link
     * @param string $target
     */
    protected function linked_responsive_image_html($image, $spacer, $alt, $link, $target) {
        echo '<div class="theme-responsive-image linked" style="background-image: url('.$image.'); cursor:pointer;" onclick="javascript: window.open(jQuery(this).children(\'.theme-responsive-image-link\').attr(\'href\'), \''.$target.'\');">'
            .'  <a class="theme-responsive-image-link" href="'.$link.'" target="'.$target.'">'
            .'      <img src="'.$spacer.'" alt="'.$alt.'" />'
            .'  </a>'
            .'</div>';
    }

    /**
     * Creates the HTML for a responsive image without a link.
     *
     * @param string $image
     * @param string $spacer
     * @param string $alt
     */
    protected function responsive_image_html($image, $spacer, $alt) {
        echo '<div class="theme-responsive-image" style="background-image: url('.$image.');">'
            .'  <img src="'.$spacer.'" alt="'.$alt.'" />'
            .'</div>';
    }

    /**
     * UNFINISHED
     * Retrieves and loads shortcodes from the parent and child theme directories
     * (by default /theme/shortcodes/). Shortcodes of the same file name in the child
     * theme will override shortcodes in the parent theme. Shortcode files which contain
     * classes that extend KMDG\Shortcode and which match this naming pattern will be
     * automatically instantiated:
     *
     * Filename: my_shortcode_class.php
     * Class Name: My_Shortcode_Class
     *
     * @filter theme_child_theme_shortcodes_directory
     * @filter theme_parent_theme_shortcodes_directory
     * @filter theme_shortcode_namespace
     */
    protected function load_shortcodes() {
        // Directories to look in under the theme roots
        $child_dirname = apply_filters('theme_child_theme_shortcodes_directory', 'shortcodes');
        $parent_dirname = apply_filters('theme_parent_theme_shortcodes_directory', 'shortcodes');

        // gather and merge shortcodes from parent and child themes
        if(get_stylesheet_directory() !== get_template_directory()) {
            $parent_shortcodes = $this->get_shortcodes_from_path(get_template_directory().DIRECTORY_SEPARATOR.$parent_dirname);
            $child_shortcodes = $this->get_shortcodes_from_path(get_stylesheet_directory().DIRECTORY_SEPARATOR.$child_dirname);
            $shortcodes = $this->utilities()->array_merge_recursive_distinct($parent_shortcodes, $child_shortcodes);
        } else {
            $shortcodes = $this->get_shortcodes_from_path(get_stylesheet_directory().DIRECTORY_SEPARATOR.$child_dirname);
        }

        // Load all shortcodes from the appropriate theme, shortcodes with namespaces
        // should return their namespace at the bottom of the file, otherwise \Theme
        // or the value returned by the filter "theme_shortcode_namespace" is used.
        // Shortcodes without namespaces will work without modification.
        if(!empty($shortcodes) && is_array($shortcodes)) {
            foreach($shortcodes as $shortcode=>$path) {
                $namespace = $this->load_template($path.DIRECTORY_SEPARATOR.$shortcode);
                $shortcode_class = $this->get_shortcode_class_name($shortcode);

                if(empty($namespace) || $namespace === 1) {
                    $namespace = apply_filters('theme_shortcode_namespace', 'Theme');
                }

                if(is_subclass_of("$namespace\\$shortcode_class", 'KMDG\Shortcode')) {
                    $shortcode_class = "$namespace\\$shortcode_class";
                    new $shortcode_class();
                } elseif(is_subclass_of($shortcode_class, 'KMDG\Shortcode')) {
                    new $shortcode_class();
                }
            }
        }
    }

    /**
     * Returns an array of shortcodes and their paths
     * @param $path
     * @return array|null
     */
    protected function get_shortcodes_from_path($path) {
        $shortcodes = array();

        try {
            $dir = new \DirectoryIterator($path);
            foreach ($dir as $fileinfo) {
                if (!$fileinfo->isDot() && $fileinfo->isFile()) {
                    $shortcodes[$fileinfo->getFilename()] = $path;
                }
            }
        } catch(\UnexpectedValueException $e) {
            return array();
        }

        return $shortcodes;
    }

    /**
     * Changes a shortcode file name into a class name for automatic inclusion and
     * instantiation.
     * @param $shortcode
     * @return mixed
     */
    protected function get_shortcode_class_name($shortcode) {
        $base = str_replace('.php', '', $shortcode);
        $noPunct = $this->utilities()->strip_punctuation($base, ' ');
        return str_replace(' ', '_', ucwords($noPunct));
    }



    ########################### ACTIONS ###########################

    /**
     * When a user is viewing the backend check to make sure that the .htaccess settings
     * are up to date, and if not, update them. Called automatically durring the init action hook.
     * @access protected
     */
    public function _action_verify_htaccess() {
        if(is_admin()) {
            $htaccess = file_get_contents($this->get_htaccess());
            if(strpos($htaccess, $this->htaccess_directive_text()) === false) {
                $this->create_htaccess();
            }
        }
    }

    /**
     * Removes theme-specific directives from the .htaccess file.
     * This is called automatically using the switch_theme action hook.
     * @access protected
     */
    public function _action_unmodify_htaccess() {
        // Remove theme specific settings from the .htaccess file

        $filename = $this->get_htaccess();
        $htaccess = file_get_contents($filename);
        $htaccess = trim(preg_replace("/# ".static::SETTINGS_START."[\w\W]*# ".static::SETTINGS_END."/mi", "", $htaccess), "\r\n");

        file_put_contents($filename, $htaccess);
    }

    /**
     * Used to include supporting styles for responsive images, it is considered best
     * practice to include small css snippets inline rather than create another http
     * request, however you can also include these in your stylesheet, and use
     * add_filter('theme_include_responsive_image_css', false) to remove the css here.
     * @access protected
     */
    public function _action_responsive_image_css() {
        if($this->check_include_responsive_image_css()) {
            echo '<style type="text/css">'.
                '.theme-responsive-image {max-width: 100%;max-height: 100%;background-size: cover;background-position: center center;background-repeat: no-repeat;display: inline-block;font-size: 0;overflow: hidden;}'.
                '.theme-responsive-image img {width: 100%;max-width: 100%;max-height: 100%;height: auto;visibility: hidden;}'.
                '</style>';
        }
    }

    /**
     * Outputs stylesheets based on the current theme settings.
     * Styles are only shown if the location setting matches the type of style
     * being shown. Deferred styles are loaded via javascript in the footer and
     * cannot take advantage of wp_register_style(), so only the URL and media
     * properties will be used. Non-public css cannot be deferred and the defer
     * property will be ignored if location is not 'public'.
     * @access protected
     */
    public function _action_register_styles() {
        if(is_array($this->settings['stylesheets'])) {
            foreach($this->settings['stylesheets'] as $slug => $options) {
                if(!empty($options)) {
                    $options = array_merge(array('dependencies'=>NULL, 'version'=>false, 'media'=>'all', 'location'=>'public', 'defer'=>false), $options);

                    $is_admin = is_admin() || $this->is_login_page();

                    if( ($options['location'] == 'public' && !$is_admin) ||
                        ($options['location'] == 'admin' && $is_admin)) {

                        if($options['defer'] && $options['location'] == 'public') {
                            $this->deferred_styles[$slug] = array('url'=>$this->url($options['url']), 'media'=>$options['media']);
                        } else {
                            wp_register_style($slug, $this->url($options['url']), $options['dependencies'], $options['version'], $options['media']);
                            wp_enqueue_style($slug);
                        }

                    } elseif($options['location'] == 'editor') {
                        add_editor_style($this->url($options['url']));
                    }
                }
            }
        }
    }

    /**
     * Generates JavaScript used to link in stylesheets after the main content
     * of the page has been loaded. This is best used for alternative media
     * stylesheets and any style rules not required for layout purposes.
     * @access protected
     */
    public function _action_deferred_styles() {
        if(!empty($this->deferred_styles)):
            ?>
            <script type="text/javascript" async="async" defer="defer">
                var styles = <?php echo json_encode($this->deferred_styles); ?>;
                for(var style in styles) {
                    jQuery("head").append('<link rel="stylesheet" id="'+style+'" href="'+styles[style].url+'" type="text/css" media="'+styles[style].media+'" />')
                }
            </script>
        <?php
        endif;
    }

    /**
     * Registers all theme required scripts with WordPress
     * @access protected
     */
    public function _action_register_scripts() {

        // Enqueue comment script where appropriate
        if (is_singular() && comments_open())
            wp_enqueue_script('comment-reply');

        if(is_array($this->settings['scripts'])) {
            foreach($this->settings['scripts'] as $name => $data) {
                // Check access level of script before inclusion
                $data = array_merge(array('dependencies'=>NULL, 'footer'=>false, 'access'=>array('public'), 'version'=>NULL), $data);
                $is_admin = is_admin() || $this->is_login_page();

                if( ($is_admin && in_array('admin', $data['access'])) ||
                    (!$is_admin && in_array('public', $data['access']))) {
                    if(!$is_admin || ($is_admin && $name != 'jquery')) { // Leave the core jQuery alone
                        wp_deregister_script($name); // If the same script is included more than once (from plugins) we want our version loaded
                    }
                    wp_register_script($name, $this->url($data['url']), $data['dependencies'], $data['version'], $data['footer']);
                    wp_enqueue_script($name);
                }
            }
        }
    }

    /**
     * Creates WordPress menu locations usable by the theme
     * @access protected
     */
    public function _action_register_menus() {
        if(empty($this->settings['menu_locations'])) {
            register_nav_menus(array('main-nav' => 'Main Navigation', 'footer-nav' => 'Footer Navigation'));
        } else {
            register_nav_menus($this->settings['menu_locations']);
        }
    }

    /**
     * Registers all supported theme features
     * @access protected
     */
    public function _action_register_theme_support() {
        if(is_array($this->settings['supports'])) {
            foreach($this->settings['supports'] as $feature => $options) {
                if(is_array($options)) {
                    add_theme_support($feature, $options);
                } else {
                    add_theme_support($options); // Handles cases where only a slug is used in the settings file/defaults
                }
            }
        }
    }

    /**
     * Creates custom columns using the data already registered with the theme.
     * The content of each column is generated based on the presence of one of the following:
     *
     * - A Closure (anonymous) callback function
     * - A string containing the name of a callable function
     * - An array containing the class and name of a callable function
     * - A string containing the name of a file to be included and parsed
     *
     * @access protected
     * @param string $column_name The slug of the column to be displayed
     * @param int $id The post ID of the row currently being rendered
     *
     */
    public function _action_custom_column_callback($column_name, $id) {
        if(!empty($this->settings['custom_columns'][$column_name])) {
            $callback = $this->settings['custom_columns'][$column_name]['callback'];

            if(is_string($callback) && !is_callable($callback) && file_exists($this->path($callback))) {
                include($this->path($callback));
            } elseif(is_callable($callback) && !($callback instanceof \Closure)) {
                call_user_func($callback, $id);
            } elseif ($callback instanceof \Closure) {
                $callback($id);
            }
        }
    }

    /**
     * Removed administrative permissions using rules that are based on user roles.
     * Note: Allow and Deny are mutually exclusive. Using Deny creates a blacklist, while
     * ommiting it and using Allow creates a whitelist. By default all rules whitelist the
     * WebDev and Administrator roles.
     * @access protected
     */
    public function _action_remove_admin_menus()  {
        if(is_array($this->settings['admin_menus'])) {
            foreach($this->settings['admin_menus'] as $rule => $options) {
                $options = array_merge(array('subpage'=>array(), 'allow'=>array('webdev','administrator'), 'deny'=>array()), $options);
                $user = get_user_by('id', get_current_user_id());
                if( (count(array_intersect($user->roles, $options['allow'])) == 0 && empty($options['deny'])) ||
                    count(array_intersect($user->roles, $options['deny'])) != 0) {

                    if(empty($options['subpage'])) {
                        remove_menu_page($options['page']);
                    } else {
                        if(is_array($options['subpage'])) {
                            foreach($options['subpage'] as $subpage) {
                                remove_submenu_page($options['page'], $subpage);
                            }
                        }
                    }

                }
            }
        }
    }

    /**
     * Sets up a new user role for Web Developers. This is equivilent to an Administrator
     * role but we could hide things that shouldnt be touched by non-technical users from
     * Administrators and not from Web Developers (things like ACF), leaving users with full
     * admin access but not an easy way to break things.
     * @access protected
     */
    public function _action_add_user_role() {
        if(get_role('webdev') === NULL) {
            $admin = get_role('administrator');
            add_role('webdev', 'Web Developer', $admin->capabilities);
        }
    }

    /**
     * Adds a javascript variable to the head that you can use to access WordPress' built-in ajax handler.
     * @access protected
     */
    public function _action_add_ajaxurl() {
        echo '<script type="text/javascript">'."\n".
            '  var ajaxurl = "'.str_replace('http://', '//', admin_url( 'admin-ajax.php' )).'";'."\n".
            '  var apikey = "'.wp_create_nonce('api-key').'";'."\n".
            '</script>'."\n";
    }

    /**
     * Registers ACF options pages
     * @access protected
     */
    public function _action_register_options_pages() {
        if(function_exists('acf_add_options_page')) {
            if(!empty($this->settings['acf_options_pages'])) {
                if(count($this->settings['acf_options_pages']) == 1) {
                    acf_add_options_page($this->settings['acf_options_pages'][0]);
                } else {
                    acf_add_options_page();
                    foreach($this->settings['acf_options_pages'] as $page) {
                        acf_add_options_sub_page($page);
                    }
                }
            }
        }
    }

    /**
     * Runs callback functions associated with a post type when a post of that type is saved.
     * @access protected
     * @global \WP_Post $post
     * @param int $post_id
     */
    public function _action_save_post($post_id) {
        global $post;
        if(!empty($this->save_actions)) {
            foreach($this->save_actions as $action) {
                if(!isset($post->post_type)
                    || ($post->post_type != $action['post_type'] && $action['post_type'] != 'any')
                    || (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
                    || wp_is_post_revision($post_id)) {
                    continue;
                } else {
                    $callback = $action['callback'];
                    if(is_string($callback) && !is_callable($callback) && file_exists($this->path($callback))) {
                        include($this->path($callback));
                    } elseif(is_callable($callback) && !($callback instanceof \Closure)) {
                        call_user_func($callback, $post_id);
                    } elseif ($callback instanceof \Closure) {
                        $callback($post_id);
                    }
                }
            }
        }
        return $post_id;
    }

    ########################### FILTERS ###########################

    /**
     * Filter for excerpt_more, changes the default truncation text.
     * @access protected
     */
    public function _filter_custom_more_text($default) {
        return $this->settings['excerpt_more_text'];
    }

    /**
     * Registers uploadable mime types as defined by the theme settings
     * @access protected
     */
    public function _filter_custom_upload_mimes($mimes = array()) {
        return array_merge($mimes, $this->settings['upload_types']);
    }

    /**
     * Registers all custom admin columns, registering actions to handle the callback functions
     * and inserting the columns into their position in the table.
     * @access protected
     */
    public function _filter_custom_columns($defaults){
        global $wp_query;

        $post_type = $wp_query->query['post_type'];
        if(!empty($this->settings['custom_columns'])) {
            foreach($this->settings['custom_columns'] as $slug => $options) {
                if( is_array($options['post_type']) && in_array($post_type, $options['post_type']) ||
                    $options['post_type'] == $post_type) {

                    $first_section = array_slice($defaults, 0, $options['position'], true);
                    $second_section = array_slice($defaults, $options['position'], null, true);
                    $defaults = array_merge($first_section, array($slug => $options['name']), $second_section);
                    add_action('manage_posts_custom_column', array($this, '_action_custom_column_callback'), 5, 2);
                    add_action('manage_pages_custom_column', array($this, '_action_custom_column_callback'), 5, 2);
                }
            }
        }

        return $defaults;
    }

    /**
     * Fixes a 6 year old bug in WordPress shortcode handling, usually manifests when
     * nesting shortcodes (such as with VisualComposer). You can set up a whitelist
     * by filtering theme_filtered_shortcodes or a blacklist by filtering theme_unfiltered_shortcodes.
     * Use the whitelist and blacklist if you run into issues with shortcodes not
     * being formatted correctly.
     *
     * @param $content
     * @return string
     * @filter theme_filtered_shortcodes
     * @filter theme_unfiltered_shortcodes
     * @access protected
     */
    public function _filter_shortcode_empty_paragraph_fix( $content ) {

        // Whitelist: define your shortcodes to filter, '' filters all shortcodes
        $shortcodes = apply_filters('theme_filtered_shortcodes', array(''));
        // Blacklist: define any shortcodes that specifically should not be filtered
        $excluded = apply_filters('theme_unfiltered_shortcodes', array());

        foreach ( $shortcodes as $shortcode ) {
            if(!in_array($shortcode, $excluded)) {
                $array = array (
                    '<p>[' . $shortcode => '[' .$shortcode,
                    '<p>[/' . $shortcode => '[/' .$shortcode,
                    $shortcode . ']</p>' => $shortcode . ']',
                    $shortcode . ']<br />' => $shortcode . ']'
                );

                $content = strtr( $content, $array );
            }
        }

        return $content;
    }

    ######################## WORDPRESS REPLACEMENTS ########################

    /*
     *  The following functions are derived from the WordPress core code and
     *  are used instead of their counterparts by this class, in some cases
     *  they may also be available for public use but this should not be
     *  commonly done.
    */

    /**
     * Retrieve the name of the highest priority template file that exists.
     *
     * Searches in the STYLESHEETPATH before TEMPLATEPATH so that themes which
     * inherit from a parent theme can just overload one file.
     *
     * This is a modified version of the same function included in the WordPress core,
     * it returns an array consisting of the template and the theme it is found in (parent
     * or child).
     *
     * @since 2.7.0
     * @see https://core.trac.wordpress.org/browser/tags/4.2.1/src/wp-includes/template.php#L0
     * @param string|array $template_names Template file(s) to search for, in order.
     * @param bool $load If true the template file will be loaded if it is found.
     * @param bool $require_once Whether to require_once or require. Default true. Has no effect if $load is false.
     * @return array('theme'=>string, 'path'=>string, 'status'=>mixed) The template filename and containing theme if
     * one is located. Also any return value provided by the included template (status).
     */
    private function locate_template($template_names, $load = false, $require_once = true ) {
        $located = '';
        $theme = '';

        foreach ( (array) $template_names as $template_name ) {
            if ( !$template_name )
                continue;
            if ( file_exists(STYLESHEETPATH . '/' . $template_name)) {
                $theme = 'child';
                $located = STYLESHEETPATH . '/' . $template_name;
                break;
            } else if ( file_exists(TEMPLATEPATH . '/' . $template_name) ) {
                $theme = 'parent';
                $located = TEMPLATEPATH . '/' . $template_name;
                break;
            }
        }

        if(STYLESHEETPATH === TEMPLATEPATH) {
            $theme = 'theme';
        }

        $status = null;
        if ( $load && '' != $located )
            $status = $this->load_template( $located, $require_once );

        if($located != '') {
            return array('theme'=>$theme, 'path'=>$located, 'status'=> $status);
        }

        return null;
    }

    /**
     * Require the template file with WordPress environment.
     *
     * The globals are set up for the template file to ensure that the WordPress
     * environment is available from within the function. The query variables are
     * also available.
     *
     * This is a modified version from the WordPress core, which also passes in arbitrary
     * arguments by way of the $args[] variable. This will also return any value returned
     * by the included file.
     *
     * @since 1.5.0
     * @see https://core.trac.wordpress.org/browser/tags/4.2.1/src/wp-includes/template.php#L0
     * @param string $_template_file Path to template file.
     * @param bool $require_once Whether to require_once or require. Default true.
     * @param array|null $args An array of data that you wish to be available to this template file
     * @return mixed Return value of included file
     */
    private function load_template($_template_file, $require_once = true , $args = array()) {
        global $posts, $post, $wp_did_header, $wp_query, $wp_rewrite, $wpdb, $wp_version, $wp, $id, $comment, $user_ID;

        if (!is_null($wp_query) && is_array( $wp_query->query_vars ) )
            extract( $wp_query->query_vars, EXTR_SKIP );

        if(!empty($args))
            extract($args, EXTR_SKIP);

        if ( $require_once )
            return require_once( $_template_file );
        else
            return require( $_template_file );
    }
}
