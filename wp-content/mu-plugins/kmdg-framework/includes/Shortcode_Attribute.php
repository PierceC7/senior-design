<?php
/**
 * @package WordPress
 * @subpackage kmdg
 * @author Jake Finley
 *
 * Provides structure, automatic validation, and additional properties that can
 * be associated with a shortcode attribute.
 */

namespace KMDG;


class Shortcode_Attribute {
    protected $name;
    protected $value;
    protected $type;
    protected $vc;

    public function &__construct($name, $value = null) {

        if(is_null($value)) {
            $type = 'string';
        } else {
            $type = gettype($value);
        }

        $this->name = $name;
        $this->value = $value;
        $this->type = $type;
        $this->vc = (object) array(
            'type'              => 'textfield',
            'holder'            => null,
            'class'             => 'sca_'.$this->name,
            'heading'           => ucwords(str_replace('_', ' ', $name)),
            'param_name'        => $this->name,
            'value'             => $this->value,
            'description'       => null,
            'admin_label'       => true,
            'dependency'        => null,
            'edit_field_class'  => null,
            'weight'            => null,
            'group'             => null
        );

        return $this;
    }

    public function &set($val = null) {
        if($val != null) {
            if(strtolower($val) === "false" && ($this->type == 'bool' || $this->type == 'boolean')) {
                $val = false;
            } else {
                $orginal = $val;
                $conversion = settype($val, $this->type);
                if(!$conversion || (!is_null($orginal) && is_null($val))) {
                    throw new \Exception("Could not convert shortcode attribute '{$this->name}' to type '{$this->type}'. Value '{$val}' (".gettype($val).") given.");
                }
            }

            $this->value = $val;
        }

        return $this;
    }

    public function name() {
        return $this->name;
    }

    public function get() {
        return $this->value;
    }

    public function &type($type) {
        $this->type = $type;
        return $this;
    }

    // VC Settings

    /**
     * Attribute type. In the "Available type values" table you can see all available variations
     * @param $type string
     * @return $this
     * @see https://wpbakery.atlassian.net/wiki/pages/viewpage.action?pageId=524332#
     */
    public function &vc_type($type) {
        $this->vc->type = $type;
        return $this;
    }

    /**
     * HTML tag name where Visual Composer will store attribute value in Visual Composer edit mode.
     * Default: hidden input
     * @param $element string
     * @return $this
     * @see https://wpbakery.atlassian.net/wiki/pages/viewpage.action?pageId=524332#
     */
    public function &vc_holder($element) {
        $this->vc->holder = $element;
        return $this;
    }

    /**
     * Class name that will be added to the "holder" HTML tag. Useful if you want to target some CSS rules to specific
     * items in the backend edit interface
     * @param $class string
     * @return $this
     * @see https://wpbakery.atlassian.net/wiki/pages/viewpage.action?pageId=524332#
     */
    public function &vc_class($class) {
        $this->vc->class = $class;
        return $this;
    }

    /**
     * Human friendly title of your param. Will be visible in shortcode's edit screen
     * @param $text string
     * @return $this
     * @see https://wpbakery.atlassian.net/wiki/pages/viewpage.action?pageId=524332#
     */
    public function &vc_heading($text) {
        $this->vc->heading = $text;
        return $this;
    }

    /**
     * Default attribute's value
     * @param $val array|string
     * @return $this
     * @see https://wpbakery.atlassian.net/wiki/pages/viewpage.action?pageId=524332#
     */
    public function &vc_value($val) {
        $this->vc->value = $val;
        return $this;
    }

    /**
     * Human friendly description of your param. Will be visible in shortcode's edit screen
     * @param $text
     * @return $this
     * @see https://wpbakery.atlassian.net/wiki/pages/viewpage.action?pageId=524332#
     */
    public function &vc_description($text) {
        $this->vc->description = $text;
        return $this;
    }

    /**
     * Show value of param in Visual Composer editor
     * @param $show string
     * @return $this
     * @see https://wpbakery.atlassian.net/wiki/pages/viewpage.action?pageId=524332#
     */
    public function &vc_admin_label($show) {
        $this->vc->show = $show;
        return $this;
    }

    /**
     * Define param visibility depending on other field value. Please read Param Dependencies
     * @param array $dep
     * @return $this
     * @see https://wpbakery.atlassian.net/wiki/pages/viewpage.action?pageId=524332#
     */
    public function &vc_dependency(array $dep) {
        $this->vc->dependency = $dep;
        return $this;
    }

    /**
     * Set param container width in content element edit window. According to Bootstrap logic
     * eg. col-md-4. (Available from Visual Composer 4.0)
     * @param $text
     * @return $this
     * @see https://wpbakery.atlassian.net/wiki/pages/viewpage.action?pageId=524332#
     */
    public function &vc_edit_field_class($text) {
        $this->vc->edit_field_class = $text;
        return $this;
    }

    /**
     * Params with greater weight will be rendered first. (Available from Visual Composer 4.4)
     * @param $weight
     * @return $this
     * @see https://wpbakery.atlassian.net/wiki/pages/viewpage.action?pageId=524332#
     */
    public function &vc_weight($weight) {
        $this->vc->weight = $weight;
        return $this;
    }

    /**
     * Use it to divide your params within groups (tabs)
     * @param $text
     * @return $this
     * @see https://wpbakery.atlassian.net/wiki/pages/viewpage.action?pageId=524332#
     */
    public function &vc_group($text) {
        $this->vc->group = $text;
        return $this;
    }

    /**
     * Returns all visual composer settings for this attribute in array format. The format of the array
     * directly matches what is expected by Visual Composer.
     * @return array
     */
    public function vc_map() {
        return (array) $this->vc;
    }
}