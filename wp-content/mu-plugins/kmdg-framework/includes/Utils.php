<?php
namespace KMDG;

defined( 'ABSPATH' ) or die( 'No direct access' );

class Utils {
    protected static $instance;

    protected function __construct() {}
    protected function __clone() {}
    protected function __wakeup() {}

    /**
     * Returns an instance of this class, if none exists it will create one.
     * This class uses the singleton pattern and cannot be initialized more
     * than once.
     * @api
     * @return static
     */
    public static function &get_instance() {
        if(is_null(static::$instance)) {
            static::$instance = new static;
        }
        return static::$instance;
    }

    /**
     * Strips all punctuation from a string and replaces all spaces with dashes.
     * @api
     * @param string $text The string you want stripped of punctuation
     * @param string $replace (Optional) replace spaces with text of your choice
     * @return string $regex (Optional) Specify the regular expression to be used
     * when stripping punctuation.
     * @filter theme_punctuation_regex
     * @filter theme_punctuation_replace
     */
    public function strip_punctuation($text, $replace = '', $regex = '/[^a-zA-Z\s]/') {
        $regex = apply_filters('theme_punctuation_regex', $regex);
        $replace_with = apply_filters('theme_punctuation_replace', $replace);
        return preg_replace($regex, $replace_with, $text);
    }

    /**
     * Returns the URL of an attachment image - used for instances when we don't
     * care about the pixel dimensions of the image (most of the time in responsive
     * design) and just want the file URL.
     * @param int $id The attachment ID
     * @param string $size (Optional) WordPress image size (defaults to "medium")
     * @return string
     */
    public function src($id, $size = 'medium')
    {
        $image = wp_get_attachment_image_src($id, $size);
        return $image[0];
    }

    /**
     * array_merge_recursive does indeed merge arrays, but it converts values with duplicate
     * keys to arrays rather than overwriting the value in the first array with the duplicate
     * value in the second array, as array_merge does. I.e., with array_merge_recursive,
     * this happens (documented behavior):
     *
     * array_merge_recursive(array('key' => 'org value'), array('key' => 'new value'));
     *     => array('key' => array('org value', 'new value'));
     *
     * array_merge_recursive_distinct does not change the datatypes of the values in the arrays.
     * Matching keys' values in the second array overwrite those in the first array, as is the
     * case with array_merge, i.e.:
     *
     * array_merge_recursive_distinct(array('key' => 'org value'), array('key' => 'new value'));
     *     => array('key' => array('new value'));
     *
     * Parameters are passed by reference, though only for performance reasons. They're not
     * altered by this function.
     *
     * @param array $array1
     * @param array $array2
     * @return array
     * @author Daniel <daniel (at) danielsmedegaardbuus (dot) dk>
     * @author Gabriel Sobrinho <gabriel (dot) sobrinho (at) gmail (dot) com>
     */
    public function array_merge_recursive_distinct( array &$array1, array &$array2 )
    {
        $merged = $array1;

        foreach ( $array2 as $key => &$value )
        {
            if ( is_array ( $value ) && isset ( $merged [$key] ) && is_array ( $merged [$key] ) )
            {
                $merged [$key] = static::array_merge_recursive_distinct ( $merged [$key], $value );
            } else {
                $merged [$key] = $value;
            }
        }

        return $merged;
    }

    /**
     * A wrapper for get_stylesheet_directory() and get_template_directory() that shortens and makes it a
     * little more user friendly. This should be used over get_stylesheet_directory(). If the file or folder
     * is found in the child theme then it will be used, otherwise the parent theme will be used.
     *
     * @param string $path (optional) A relative path from the theme directory (and/or file name)
     * @return string
     * @filter theme_parent_path
     * @filter theme_child_path
     */
    public function path($path = '') {
        $base = apply_filters('theme_child_path', str_replace('/', DIRECTORY_SEPARATOR, get_stylesheet_directory().DIRECTORY_SEPARATOR));
        $full = str_replace('/', DIRECTORY_SEPARATOR, $base.$path);

        if(!(file_exists($full) || is_dir($full))) {
            $base = apply_filters('theme_parent_path', str_replace('/', DIRECTORY_SEPARATOR, get_template_directory().DIRECTORY_SEPARATOR));
            $full = str_replace('/', DIRECTORY_SEPARATOR, $base.$path);
        }

        return $full;
    }

    /**
     * Returns the full URL to a file based on the provided path. When using a child
     * theme this will attempt to locate the file in the child theme first and if not
     * found the link will be to the parent theme. If a fully qualified URL is passed
     * in as the path (it must contain "//" near the beginning) then the URL will be
     * passed through unaltered. In this way both absolute and relative URLs may be
     * passed through this function without needing to worry what type you have.
     *
     * @api
     * @param string|null $path A relative path from the theme directory (and/or file name)
     * @return string
     */
    public function url($path = '') {
        $url = $path;
        $qualified = strpos($path, '//');
        if($qualified === false || $qualified >  10) {
            $url = get_stylesheet_directory_uri().'/'.$path;
            if(!file_exists($this->path($path))) {
                $url = get_template_directory_uri().'/'.$path;
            }
        }

        return $url;
    }

    /**
     * Compatibility function for PHP <= 5.3, simply returns any object passed into it
     * @param mixed $object
     * @return Cron_Job|Post_Type|Shortcode|\stdClass
     */
    public function &with($object) {
        return $object;
    }

    /**
     * Returns post thumbnail src, in size specified ('thumbnail', 'small', 'medium', 'large', or 'full')
     *
     * @param integer $post_id (Optional) The id of the post to get the thumbnail for - defaults to the current post
     * @param string $size (Optional) The size of the image you want - defaults to 'medium'
     * @return string
     */
    public function get_thumb($post_id = null, $size='medium') {
        global $post;

        if(empty($post_id)) {
            $post_id = $post->ID;
        }

        $thumb = get_post_thumbnail_id($post_id);
        $src = wp_get_attachment_image_src($thumb, $size);
        return $src[0];
    }
}