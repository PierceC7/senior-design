<?php
/*
Plugin Name: KMDG Framework
Plugin URI: http://kmdg.com
Description: Provides developer extensions for WordPress. If you are running a KMDG theme this plugin is REQUIRED. If you are not running a KMDG theme feel free to uninstall it (Unsure? Don't risk it!).
Version: 5.0
Author: KMDG, Inc., Jake Finley
Author URI: http://kmdg.com
License:
    Copyright (c) KMDG, Inc - All Rights Reserved.
    Unauthorized copying of this file, via any medium is strictly prohibited
    Proprietary and confidential
*/

defined( 'ABSPATH' ) or die( 'No direct access' );

// Plugin Path & URL
define('KMDG_FRAMEWORK', plugin_dir_path( __FILE__ ));
define('KMDG_FRAMEWORK_URL', plugin_dir_url( __FILE__ ));

spl_autoload_register(function ($class) {

    // project-specific namespace prefix
    $prefix = 'KMDG\\';

    // base directory for the namespace prefix
    $base_dir = KMDG_FRAMEWORK . 'includes/';

    // does the class use the namespace prefix?
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        // no, move to the next registered autoloader
        return;
    }

    // get the relative class name
    $relative_class = substr($class, $len);

    // replace the namespace prefix with the base directory, replace namespace
    // separators with directory separators in the relative class name, append
    // with .php
    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

    // if the file exists, require it
    if (file_exists($file)) {
        require $file;
    }
});

// Load theme specific implementation(s) of KMDG Framework
$theme = get_stylesheet_directory().'/framework.php';

// Load theme specific implementation, if any (should extend KMDG\Framework and must be named Theme)
if(file_exists($theme)) {
    require_once($theme);
}

if(!function_exists('theme')) {
    /*
     * Default theme wrapper function, returns an instance of the KMDG class
     * usable anywhere in the theme.
     *
     * Extend the Framework class in your framework.php file to return an instance
     * of the local Theme class instead.
     * @returns \KMDG\Framework|\Theme
     */
    function theme() {
       if(class_exists('Theme')) {
           return Theme::get_instance();
       } else {
           return \KMDG\Framework::get_instance();
       }
    }

    //Initialize the framework and get things started.
    theme()->init();
}