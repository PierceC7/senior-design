/**
 * AJAX API:
 * This is a wrapper for jQuery's .ajax function that does not limit what you can
 * do, but provides defaults that are pre-configured to work well with WordPress
 * and the AJAX_Handler class packaged with this theme. You can override these
 * defaults by passing in an object conforming to the jQuery API documentation for
 * the .ajax function. Anything in the passed in object will override properties of
 * the same value in the defaults array - this is done recursively so if a property
 * is itself an object it's properties will be merged into it's default version.
 * For example, passing in {data: {test: true}} will ADD the property "test" to the
 * data object, but will not remove the default "action" and "api_key" values.
 *
 * Note: if you redefine the {success: function()} you can call ajax.success(response)
 * from within your custom success function to take advantage of the default actions
 * while still providing custom handling - api.success() will return true if there
 * were no errors, and false otherwise. Therefor you can do if(ajax.success(response))
 * and skip creating your own error handling code IF the default error handling
 * works for your purposes. Also, the ajaxurl and apikey variables are defined in
 * wp_head() and are more-or-less required for WordPress, see the KMDG class for
 * more info.
 *
 * @param String action - WordPress Ajax action
 * @param Object options - A jQuery.ajax() object options, or a query string
 * @link http://api.jquery.com/jquery.ajax/
 */
var ajax = function(action, options) {
    "use strict";
    ajax.isProcessing = true;

    var defaults = {
        url: ajaxurl,
        type: 'GET',
        data: {
            action: action,
            api_key: apikey
        },
        cache: false,
        success: ajax.success,
        error: ajax.fail
    };

    if(typeof options === 'string')
        options = {data: options};

    if(typeof options !== 'object')
        options = {};

    if(options.data) {
        if(typeof options.data === 'string')
            options.data += '&action='+action+'&api_key='+apikey;
    } else {
        options.data = defaults.data;
    }

    jQuery.ajax(jQuery.extend(true, defaults, options));
};

ajax.success = function(response) {
    "use strict";

    ajax.isProcessing = false;
    if(response.info) {
        console.info(response.info);
    }

    if(response.error) {
        for(var i in response.error) {
            if(i === 'PHPError') {
                console.error('PHPError', response.error[i]);
            } else {
                alert(i + ":\n\n" + response.error[i]);
            }
        }

        return false;
    } else if (response.data) {
        if(response.data.msg) alert(response.data.msg);
        if(response.data.force_refresh) window.location.reload();
        return true;
    }

    return null;
};

ajax.fail = function(data, status, error) {
    "use strict";

    ajax.isProcessing = false;
    console.warn("Ajax Error:", [data,status,error]);
};