<?php
/*
Plugin Name: KMDG Loader
Plugin URI: http://kmdg.com
Description: Loads required plugins (typically the KMDG Framework)
Version: 5.0
Author: KMDG, Inc., Jake Finley
Author URI: http://kmdg.com
License:
    Copyright (c) KMDG, Inc - All Rights Reserved.
    Unauthorized copying of this file, via any medium is strictly prohibited
    Proprietary and confidential
*/
$framework = WPMU_PLUGIN_DIR.'/kmdg-framework/kmdg-framework.php';

if(file_exists($framework)) {
    require_once($framework);
} else {
    die('<h2>Please install the KMDG Framework in "'.WPMU_PLUGIN_DIR.'"');
}
