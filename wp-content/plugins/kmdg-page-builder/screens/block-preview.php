<?php use KMDG\PageBuilder\Builder; ?>

<!DOCTYPE html>
<html lang="en-US">
<head>
    <?php wp_head(); ?>
</head>
<body <?php body_class("kmdg-pb-block-preview"); ?>>
    <?= Builder::build('', Builder::BLOCK_BUILDER_ACF_FIELD, true) ?>
    <?php wp_footer(); ?>
</body>
</html>