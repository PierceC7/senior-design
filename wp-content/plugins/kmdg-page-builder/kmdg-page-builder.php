<?php
/*
Plugin Name: KMDG Page Builder
Plugin URI: http://kmdg.com
Description: The plugin enables page builder functionality on the default template. Requires Advanced Custom Fields PRO.
Version: 1.0
Author: KMDG, Inc., Jake Finley
Author URI: http://kmdg.com
License:
    Copyright (c) KMDG, Inc - All Rights Reserved.
    Unauthorized copying of this file, via any medium is strictly prohibited
    Proprietary and confidential
*/

use KMDG\PageBuilder\Builder;
use KMDG\PageBuilder\Theme;
use KMDG\PageBuilder\SectionFactory;
use KMDG\PageBuilder\LayoutFactory;
use KMDG\PageBuilder\ColumnFactory;
use KMDG\PageBuilder\ComponentFactory;


define('KMDG_PAGE_BUILDER', plugin_dir_path(__FILE__));
define('KMDG_PAGE_BUILDER_ASSETS', plugin_dir_url(__FILE__).'assets/');
define('KMDG_PAGE_BUILDER_ASSETS_PATH', plugin_dir_path(__FILE__).'assets/');

spl_autoload_register(function ($class) {

    // project-specific namespace prefix
    $prefix = 'KMDG\\PageBuilder\\';

    // base directory for the namespace prefix
    $base_dir = KMDG_PAGE_BUILDER . 'includes/';

    // does the class use the namespace prefix?
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        // no, move to the next registered autoloader
        return;
    }

    // get the relative class name
    $relative_class = substr($class, $len);

    // replace the namespace prefix with the base directory, replace namespace
    // separators with directory separators in the relative class name, append
    // with .php
    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

    // if the file exists, require it
    if (file_exists($file)) {
        require $file;
    }
});

// Fields
add_action('acf/include_field_types', function() {
    require_once('fields/acf-builder-theme-field-v5.php');

    // initialize
    new acf_field_builder_theme_field(['version'	=> '1.0.0',
                                       'url'		=> plugin_dir_url( __FILE__ ),
                                       'path'		=> plugin_dir_path( __FILE__ )]);
});

// Third Party Config

if(!defined("ACFGFS_API_KEY")) {
    define('ACFGFS_API_KEY', 'AIzaSyC8yPAKTT0uF8z_octeT0tV6skuDfCMM9Y');
}

if(!defined('ACFGFS_REFRESH')) {
    define( 'ACFGFS_REFRESH', 259200 );
}

add_filter('acf/settings/remove_wp_meta_box', '__return_true');

// Third Party Includes
require_once('vendor/acf-code-field/acf-code-field.php');
require_once('vendor/acf-google-font-selector-field-FORK/acf-google_font_selector.php');
require_once('vendor/acf-auto-generated-value/acf-auto_generated_value.php');

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page([
        "page_title"    => "KMDG Page Builder",
        "menu_title"    => "Builder Settings",
        "menu_slug"     => "kmdg-page-builder",
        "capability"    => "manage_options",
        "redirect"      => false
     ]);
}

Builder::init(
    new Theme(KMDG_PAGE_BUILDER_ASSETS_PATH, KMDG_PAGE_BUILDER_ASSETS, 'styles/css/theme.css'),
    new SectionFactory(),
    new LayoutFactory(),
    new ColumnFactory(),
    new ComponentFactory()
);