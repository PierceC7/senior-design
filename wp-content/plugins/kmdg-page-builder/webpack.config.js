var webpack = require('webpack');

module.exports = {
    entry: {
        admin: "./assets/js/admin.js",
        builder: "./assets/js/builder.js",
    },
    output: {
        path: __dirname + "/assets/js",
        filename: "[name].min.js"
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({minimize: true}),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
    ]
};