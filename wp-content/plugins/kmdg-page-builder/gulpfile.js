var gulp = require('gulp');
var browserify = require('browserify');
var sass = require('gulp-sass');
var prefix = require('gulp-autoprefixer');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var es = require('event-stream');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync');
var cssnano = require('gulp-cssnano');
var cache = require('gulp-cache');
var del = require('del');
var runSequence = require('run-sequence');
var gutil = require('gulp-util');

// Development Tasks
// -----------------

// Compile SCSS
gulp.task('sass', function () {
    return gulp.src('assets/styles/scss/*.scss') // Gets all files ending with .scss in styles/scss and children dirs
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'})) // Passes it through a gulp-sass
        .on('error', function(err) {
            gutil.log(err.message);
            browserSync.notify(err.message, 3000);
            this.emit('end');
        })
        .pipe(prefix({
            overrideBrowserslist: [
                'ie >= 6',
                'chrome >= 20',
                'safari >= 1',
                'firefox >= 3',
                'iOS >= 1'
            ],
            cascade: false
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('assets/styles/css')); // Outputs it in the css folder
});

// Compile JavaScript
gulp.task('js', function() {

    var sourcePath = 'assets/js/';

    var files = [
        'builder.js',
        'admin.js'
    ];

    var tasks = files.map(function(entry) {
        return browserify({ entries: [sourcePath + entry], debug: false })
            .bundle()
            .pipe(source(entry))
            .pipe(rename({
                extname: '.min.js'
            }))
            .pipe(buffer())
            .pipe(sourcemaps.init({loadMaps: true}))
            // Add transformation tasks to the pipeline here.
            .pipe(uglify())
            .on('error', gutil.log)
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest('assets/js/'))
            .pipe(browserSync.reload({ // Reloading with Browser Sync
                stream: true
            }));
    });

    return es.merge.apply(null, tasks);
});

// Watchers
gulp.task('watch', function () {
    gulp.watch('assets/js/**/*.js', ['js']);
    gulp.watch('assets/styles/scss/**/*.scss', ['sass']);
});

// Build Sequences
// ---------------

gulp.task('default', function (callback) {
    runSequence(['js', 'sass', 'browserSync', 'fonts'], 'watch',
        callback
    )
});

gulp.task('build', function (callback) {
    runSequence(
        'js',
        'sass',
        callback
    )
});