<?php /* @var \KMDG\PageBuilder\Section $section */ ?>
<?php /* @var \KMDG\PageBuilder\Layout $layout */ ?>
<?php /* @var \KMDG\PageBuilder\Column $column */ ?>

<p>Error Loading Column, "<?= $column; ?>" does not have an associated layout file.</p>
<p>Template files may be placed in the "<?= trailingslashit(get_stylesheet_directory().'/'.\KMDG\PageBuilder\Builder::get_column_dir()) ?>" directory,
and must be named after the component layout slug ("<?= $column ?>.php").</p>
