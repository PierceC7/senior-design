<?php /* @var \KMDG\PageBuilder\Section $section */ ?>
<?php /* @var \KMDG\PageBuilder\Layout $layout */ ?>
<?php /* @var \KMDG\PageBuilder\Column $column */ ?>

<div class="<?= $column->classes() ?>">
    <div class="builder-column-body">
        <div class="builder-content">
            <?= $column->loop(); ?>
        </div>
    </div>
</div>