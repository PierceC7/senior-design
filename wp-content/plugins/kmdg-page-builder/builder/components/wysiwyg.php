<?php /* @var \KMDG\PageBuilder\Section $section */ ?>
<?php /* @var \KMDG\PageBuilder\Layout $layout */ ?>
<?php /* @var \KMDG\PageBuilder\Column $column */ ?>
<?php /* @var \KMDG\PageBuilder\Component $component */ ?>

<div <?= $component->animations() ?> class="<?= $component->classes() ?>">
    <?= $component->field("content"); ?>
</div>
