<?php
/* @var \KMDG\PageBuilder\Section $section */
/* @var \KMDG\PageBuilder\Layout $layout */
/* @var \KMDG\PageBuilder\Column $column */
/* @var \KMDG\PageBuilder\Component $component */

use KMDG\PageBuilder\Utilities;

$width = $component->field('width') ? ('width: '.$component->field('width').'px;') : '';
$height = $component->field('height') ? ('height: '.$component->field('height').'px;') : '';
$lightboxClass = null;
$lightbox = null;
$subcontainerClass = null;
$subcontainerWidth = $width;

if($component->field('enable_lightbox')) {
    $lightbox = 'data-featherlight="'.Utilities::src($component->field('image'),'full').'"';
    $lightboxClass = 'js-triggers-lightbox';
}

if(!empty($height) && $component->field('sizing') === 'normal') {
    $height = 'max-'.$height;
}

// if the size is cropped or zoomed, display block the subcontainer so its width fills the area set by the container
if($component->field('sizing') === 'zoom') {
    $subcontainerClass = 'builder-zoomed-image';
}

// If the sizing is proportional, don't set a width on the subcontainer (let the image size it)
if($component->field('sizing') === 'normal') {
    $subcontainerWidth = '';
}

$image = $component->field('image');
$bg = "background-image:url(".Utilities::getPreloadImgSrc($image).");";
?>

<div <?= $component->animations() ?>
        class="<?= $component->classes(["builder-component-image-size-" . $component->field('sizing'), $lightboxClass, 'align-' . $component->field('alignment')]) ?>">
    <?php if ($component->field('link')): ?>
        <a href="<?= $component->field('link') ?>" class="image-container builder-lazy-img" style="<?= $bg . $width . $height ?>" data-srcs="<?= Utilities::getImageSrcJson($image) ?>">
            <?= Utilities::preloadImage($image) ?>
        </a>
    <?php else: ?>
        <div class="image-container builder-lazy-img" style="<?= $bg . $width . $height ?>" data-srcs="<?= Utilities::getImageSrcJson($image) ?>">
            <div class="image-subcontainer <?= $subcontainerClass ?>" style="max-height: inherit;<?= $subcontainerWidth ?>" <?= $lightbox ?>>
                <?= Utilities::preloadImage($image) ?>
            </div>
        </div>
    <?php endif; ?>
</div>
