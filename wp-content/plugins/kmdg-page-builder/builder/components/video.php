<?php
/* @var \KMDG\PageBuilder\Section $section */
/* @var \KMDG\PageBuilder\Layout $layout */
/* @var \KMDG\PageBuilder\Column $column */
/* @var \KMDG\PageBuilder\Component $component */

use KMDG\PageBuilder\Utilities;

$width = $component->field('width') ? ('width: '.$component->field('width').'px;') : '';

$controls = $component->field('controls', true);
$autoplay = $component->field('autoplay', false);
$muted = $autoplay ? true : $component->field('muted', false);
$loop = $component->field('loop');
$preload = $component->field('preload');

$extra_classes = [];

if($autoplay) {
    $extra_classes[] = 'js-autoplay';
}

?>

<div <?= $component->animations() ?> class="<?= $component->classes($extra_classes) ?>">
    <div class="video-container" style="<?= $width ?>">
        <video style="<?= $component->field('thumbnail') ? 'background-image: url(' . Utilities::src($component->field('thumbnail'), 'large') . ');' : '' ?>"
            <?= $muted ? 'muted' : '' ?>
            <?= $loop ? 'loop' : '' ?>
            <?= $controls ? 'controls' : '' ?>
            <?= $preload ? 'preload' : 'preload="none"' ?>
        >
            <?php

                foreach($component->field('sources', [], true) as $field) {
                    if(!empty($field['file'])) {
                        $file = $field['file'];
                        echo "<source src='{$file['url']}' type='{$file['mime_type']}'>";
                    }
                }

            ?>
        </video>
    </div>
</div>
