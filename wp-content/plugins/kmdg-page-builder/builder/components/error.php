<?php /* @var \KMDG\PageBuilder\Section $section */ ?>
<?php /* @var \KMDG\PageBuilder\Layout $layout */ ?>
<?php /* @var \KMDG\PageBuilder\Column $column */ ?>
<?php /* @var \KMDG\PageBuilder\Component $component */ ?>

<p>Error Loading Component, "<?= $component; ?>" does not have an associated layout file.</p>
<p>Template files may be placed in the "<?= trailingslashit(get_stylesheet_directory().'/'.\KMDG\PageBuilder\Builder::get_component_dir()) ?>" directory,
and must be named after the component layout slug ("<?= $component ?>.php").</p>
