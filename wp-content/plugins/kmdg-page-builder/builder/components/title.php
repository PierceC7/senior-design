<?php
/* @var \KMDG\PageBuilder\Section $section */
/* @var \KMDG\PageBuilder\Layout $layout */
/* @var \KMDG\PageBuilder\Column $column */
/* @var \KMDG\PageBuilder\Component $component */

$tag = $style = $component->field('style');

if($component->field('seo_importance') != 'same'):
    $tag = $component->field('style');
endif;
?>

<div <?= $component->animations() ?> class="<?= $component->classes() ?>">
    <div class="builder-column-header">
        <<?= $tag ?> class="builder-title builder-style--<?= $style ?>" style="text-align: <?= $component->field('alignment'); ?>;">
            <?= $component->field('text', '&nbsp;') ?>
        </<?= $tag ?>>
    </div>
</div>
