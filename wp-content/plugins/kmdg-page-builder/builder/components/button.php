<?php
/* @var \KMDG\PageBuilder\Section $section */
/* @var \KMDG\PageBuilder\Layout $layout */
/* @var \KMDG\PageBuilder\Column $column */
/* @var \KMDG\PageBuilder\Component $component */

$link = $component->field('link');

if($component->field('allow_external_links')) {
    $link = $component->field('url');
} elseif($component->field('anchor')) {
    $link .= "#".$component->field('anchor');
}

?>


<div <?= $component->animations() ?> class="<?= $component->classes() ?>" style="text-align: <?= $component->field('alignment') ?>">
    <a href="<?= $link ?>" target="<?= $component->field('open_in_new_window') ? '_blank' : "_self" ?>" class="t-<?= $component->field('style', 'accent1') ?>--bg">
        <?= $component->field('text') ?>
    </a>
</div>
