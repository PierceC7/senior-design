<?php /* @var \KMDG\PageBuilder\Layout $layout */ ?>

<?php if(have_rows('elements')): ?>
    <?php while(have_rows('elements')): the_row(); ?>
        <?= $layout->column('subtitle-column', 'element') ?>
    <?php endwhile; ?>
<?php endif; ?>
