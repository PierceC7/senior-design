<?php
/*
 * Label: Two Thirds Column
 * Name: two_thirds_column
 * Description: Displays a two content areas side by side, the left hand side takes up 66% of the row
 * while the right hand side takes up 33%.
 */

/* @var \KMDG\PageBuilder\Layout $layout */
?>

<div class="builder-table">
    <?php $layout->column('column', 'large_column'); ?>
    <?php $layout->column('column', 'small_column'); ?>
</div>