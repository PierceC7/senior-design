
<p>Error Loading Layout, "<?= get_row_layout(); ?>" does not have an associated layout file.</p>
<p>Template files may be placed in the "<?= trailingslashit(get_stylesheet_directory().'/'.\KMDG\PageBuilder\Builder::get_layout_dir()) ?>" directory,
    and must be named after the row layout slug ("<?= get_row_layout(); ?>.php").</p>
