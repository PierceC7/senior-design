<?php
/*
 * Label: One Third Column
 * Name: one_third_column
 * Description: Displays a two content areas side by side, the left hand side takes up 33% of the row
 * while the right hand side takes up 66%.
 */

/* @var \KMDG\PageBuilder\Layout $layout */
?>

<div class="builder-table">
    <?php $layout->column('column', 'small_column'); ?>
    <?php $layout->column('column', 'large_column'); ?>
</div>
