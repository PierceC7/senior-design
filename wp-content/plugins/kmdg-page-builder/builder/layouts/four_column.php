<?php
/*
 * Label: Four Column
 * Name: four_column
 * Description: Displays a four content areas side by side, each taking up 25% of the available space
 */

/* @var \KMDG\PageBuilder\Layout $layout */
?>

<div class="builder-table">

    <?php $layout->column('column', '1st_column'); ?>
    <?php $layout->column('column', '2nd_column'); ?>
    <?php $layout->column('column', '3rd_column'); ?>
    <?php $layout->column('column', '4th_column'); ?>

</div>