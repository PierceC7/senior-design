<?php
/*
 * Label: Two Column
 * Name: two_column
 * Description: Displays a two content areas side by side, each taking up 50% of the available space
 */

/* @var \KMDG\PageBuilder\Layout $layout */
?>

<div class="builder-table">
    <?php $layout->column('column', 'left_column'); ?>
    <?php $layout->column('column', 'right_column'); ?>
</div>