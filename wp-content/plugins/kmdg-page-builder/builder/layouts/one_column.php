<?php
/*
 * @package WordPress
 * @subpackage kmdg
 *
 * Label: One Column
 * Name: one_column
 * Description: Displays a single content area (similar to standard wordpress)
 */

/* @var KMDG\PageBuilder\Layout $layout */
?>

<div class="builder-table">
    <?php $layout->column('column', 'column') ?>
</div>

