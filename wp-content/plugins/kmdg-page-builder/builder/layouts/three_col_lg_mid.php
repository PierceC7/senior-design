<?php
/*
 * Label: Three Column
 * Name: three_column
 * Description: Displays a three content areas side by side, each taking up 33% of the available space
 */

/* @var \KMDG\PageBuilder\Layout $layout */
?>

<div class="builder-table">
    <?php $layout->column('column', 'left_column'); ?>
    <?php $layout->column('column', 'middle_column'); ?>
    <?php $layout->column('column', 'right_column'); ?>
</div>
