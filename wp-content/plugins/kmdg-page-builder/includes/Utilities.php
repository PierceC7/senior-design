<?php
/**
 * Created by PhpStorm.
 * User: KMDG
 * Date: 4/27/2017
 * Time: 1:13 PM
 */

namespace KMDG\PageBuilder;


class Utilities
{
    public static function src($id, $size = "medium") {
        $image = wp_get_attachment_image_src($id, $size);
        return $image[0];
    }

    public static function srcsetImg($id, $img_atts = '', $sizes = array()) {
        $srcset = wp_get_attachment_image_srcset($id, 'full');
        $img = wp_get_attachment_image_src($id, 'full');

        $default_sizes = array(
            'full'              => "1200px",
            'large'             => "768px",
            'medium_large'      => "500px",
            'medium'            => "0px"
        );

        if(strpos($img_atts, 'alt=') === false) {
            $alt = self::img_alt_text($id);
            $img_atts .= " alt='{$alt}'";
        }

        if($srcset) {
            $img_sizes = '';

            foreach(self::array_merge_recursive_distinct($default_sizes, $sizes) as $size => $max_width) {
                $data = wp_get_attachment_image_src($id, $size);
                $img_sizes .= "(min-width: {$max_width}) {$data[1]}px, ";
            }

            $img_sizes .= '100vw';

            return "<img {$img_atts} src='{$img[0]}' width='{$img[1]}' height='{$img[2]}' srcset='{$srcset}' sizes='{$img_sizes}' />";
        } else {
            return "<img {$img_atts} src='{$img[0]}' width='{$img[1]}' height='{$img[2]}' />";
        }
    }

    public static function img_alt_text($id) {
        $alt = get_post_meta($id, '_wp_attachment_image_alt', true);

        if(empty($alt)) {
            $alt = get_the_title($id);
        }

        return $alt;
    }

    public static function getPreloadImgSrc($imageID) {
        return static::src($imageID, 'pb-preload');
    }

    public static function preloadImage($imageID, $img_atts = '') {
        $img = wp_get_attachment_image_src($imageID, 'pb-preload');
        $full = wp_get_attachment_image_src($imageID, 'full');

        if(strpos($img_atts, 'alt=') === false) {
            $alt = self::img_alt_text($imageID);
            $img_atts .= " alt='{$alt}'";
        }

        return "<img {$img_atts} src='{$img[0]}' width='{$full[1]}' height='{$full[2]}' />";
    }

    public static function getImageSrcJson($imageID) {
        $srcs = [
            'full'  => [
                'src'   => Utilities::src($imageID,'full'),
                'width' => '-1'
            ]
        ];

        $imgData = wp_get_attachment_metadata($imageID, true);

        if($imgData && !empty($imgData['sizes'])) { // Sanity check
            $supportedSizes = array_flip(apply_filters('KMDG/PB/background/sizes', ['large', 'pb-medium','medium']));

            foreach(array_intersect_key($imgData['sizes'], $supportedSizes) as $size => $data) {
                $srcs[$size] = [
                    'src'   => Utilities::src($imageID, $size),
                    'width' => $data['width']
                ];
            }
        }

        return esc_attr(json_encode($srcs));
    }

    /**
     * array_merge_recursive does indeed merge arrays, but it converts values with duplicate
     * keys to arrays rather than overwriting the value in the first array with the duplicate
     * value in the second array, as array_merge does. I.e., with array_merge_recursive,
     * this happens (documented behavior):
     *
     * array_merge_recursive(array('key' => 'org value'), array('key' => 'new value'));
     *     => array('key' => array('org value', 'new value'));
     *
     * array_merge_recursive_distinct does not change the datatypes of the values in the arrays.
     * Matching keys' values in the second array overwrite those in the first array, as is the
     * case with array_merge, i.e.:
     *
     * array_merge_recursive_distinct(array('key' => 'org value'), array('key' => 'new value'));
     *     => array('key' => array('new value'));
     *
     * Parameters are passed by reference, though only for performance reasons. They're not
     * altered by this function.
     *
     * @param array $array1
     * @param array $array2
     * @return array
     * @author Daniel <daniel (at) danielsmedegaardbuus (dot) dk>
     * @author Gabriel Sobrinho <gabriel (dot) sobrinho (at) gmail (dot) com>
     */
    public static function array_merge_recursive_distinct( array &$array1, array &$array2 )
    {
        $merged = $array1;

        foreach ( $array2 as $key => &$value )
        {
            if ( is_array ( $value ) && isset ( $merged [$key] ) && is_array ( $merged [$key] ) )
            {
                $merged [$key] = static::array_merge_recursive_distinct ( $merged [$key], $value );
            } else {
                $merged [$key] = $value;
            }
        }

        return $merged;
    }
}