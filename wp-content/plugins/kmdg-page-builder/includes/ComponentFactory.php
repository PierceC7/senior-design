<?php

namespace KMDG\PageBuilder;


class ComponentFactory
{
    protected $class;

    /**
     * ColumnFactory constructor.
     *
     * @param string|null $class The base column class name
     * @throws BuilderException
     */
    public function __construct($class = null)
    {
        $base = __NAMESPACE__.'\\Component';

        if(is_a($class, $base, true)) {
            $this->class = $class;
        } elseif(is_null($class)) {
            $this->class = $base;
        } else {
            throw new BuilderException("Registered Component class does not extend $base");
        }
    }

    /**
     * @param string $name
     * @param $index
     * @param Section $section
     * @param Layout $layout
     * @param Column $column
     *
     * @return Component
     */
    public function make($name, $index, Section $section, Layout $layout, Column $column) {
        return new $this->class($name, $index, $section, $layout, $column);
    }
}