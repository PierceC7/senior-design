<?php
/**
 * Created by PhpStorm.
 * User: KMDG
 * Date: 5/26/2017
 * Time: 5:41 PM
 */

namespace KMDG\PageBuilder;


class Settings
{
    /** @var array **/
    private $settings = [];

    public function __construct(array $settings)
    {
        $this->settings = $settings;
    }

    public function __get($name)
    {
        return empty($this->settings[$name]) ? null : $this->settings[$name];
    }

    public function __set($name, $value)
    {
        $this->settings[$name] = $value;
    }
}