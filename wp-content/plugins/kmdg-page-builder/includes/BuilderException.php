<?php
/**
 * Created by PhpStorm.
 * User: KMDG
 * Date: 4/26/2017
 * Time: 6:10 PM
 */

namespace KMDG\PageBuilder;


class BuilderException extends \Exception
{
    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = "Builder Exception: " . $message;
    }
}