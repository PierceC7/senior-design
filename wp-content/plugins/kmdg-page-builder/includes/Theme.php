<?php

namespace KMDG\PageBuilder;


class Theme
{
    /**
     * @var object[] Each theme's settings in object array format
     */
    protected $settings = [];

    /**
     * @var object[] Google fonts used by the theme
     */
    protected $fonts = [];

    /**
     * @var int[] Associative array of size names (ie, "mobile") names mapped to their px value
     */
    protected $query_sizes = [];

    /**
     * @var string filesystem path to stylesheet
     */
    protected $path;

    /**
     * @var string path to public URL for stylesheet
     */
    protected $url;

    /**
     * @var string Cache Buster, automatically regenerated every time themes are saved
     */
    protected $version;


    /**
     * Theme constructor.
     *
     * @param string $base_path Base filesystem path to folder
     * @param string $base_url Base URL path to folder
     * @param string $rel_path Relative path from base path/url to css file
     */
    public function __construct($base_path, $base_url, $rel_path)
    {
        $this->path = $base_path.$rel_path;
        $this->url = $base_url.$rel_path;
        $this->query_sizes = apply_filters('KMDG/PB/theme/responsive_sizes', [
            'small'     => 1350,
            'tablet'    => 1000,
            'mobile'    => 500
        ]);
    }

    /**
     * Loads the theme, creating and registering styles as needed.
     */
    public function load() {
        global $post;
        $cache = get_transient("kmdg_pb_settings");

        // Avoid expensive stylesheet creation process if we have a up to date cached version
        if(!$cache || !file_exists($this->path) || $cache['version'] > filemtime($this->path) ) {

            // [Legacy] Pull ACF settings and reformat them into an easy to loop through array of objects
            while(have_rows('themes', 'option')) {
                the_row();
                $theme = sanitize_title(get_sub_field('theme_id'));
                $this->settings[$theme] = $this->loadLegacySettings();

                $this->fonts[] = $this->settings[$theme]->gfont;

                while(have_rows('fonts')) {
                    the_row();

                    $this->settings[$theme]->elements[] = $this->loadElementSettings();
                }
            }

            // Pull theme settings from Theme posts
            $themeQuery = new \WP_Query([
                'post_type'     => Builder::THEME_POST_TYPE,
                'posts_per_page'=> -1,
                'order_by'      => 'menu_order',
                'order'         => 'ASC',
                'post_status'   => 'publish',
            ]);

            while($themeQuery->have_posts()) {
                $themeQuery->the_post();
                $theme = $post->post_name;
                $this->settings[$theme] = $this->loadThemeSettings();

                $this->fonts[] = $this->settings[$theme]->gfont;

                while(have_rows('fonts')) {
                    the_row();

                    $this->settings[$theme]->elements[] = $this->loadElementSettings();
                }
            }

            wp_reset_postdata();
            $this->generateCSS();

            // Cache results so we don't have to generate them again until they change
            set_transient("kmdg_pb_settings", [
                'settings'  => $this->settings,
                'fonts'     => $this->fonts,
                'version'   => time(),
            ]);

        } else {
            // Use cached settings
            $data = get_transient("kmdg_pb_settings");
            $this->settings = $data['settings'];
            $this->fonts = $data['fonts'];
            $this->version = $data['version'];
        }

        // Call to 3rd party ACF Google Font Selector plugin
        if(function_exists("acfgfs_google_font_enqueue")) {
            \acfgfs_google_font_enqueue($this->fonts);
        }

        // Hooks
        add_action('wp_enqueue_scripts', [$this, '_enqueue_stylesheet']);

        add_action('acf/save_post', function($postID) {
            if(strpos(get_current_screen()->id, 'kmdg-page-builder') !== false) {
                $this->refreshCache();
            } elseif(get_post_type($postID) == Builder::THEME_POST_TYPE) {
                $this->refreshCache();
            }
        }, 20);
    }

    /**
     * Retrieves theme settings in object format for reference in components/layouts/columns
     *
     * @param $theme A theme ID
     * @return object
     */
    public function getSettings($theme) {
        if(empty($this->settings[$theme])) {
            $this->settings[$theme] = $this->defaultThemeSettings(); // make a blank theme
        }
        return $this->settings[$theme];
    }


    /**
     * Hooks into wp_enqueue_scripts to add stylesheet to wp_head
     */
    public function _enqueue_stylesheet() {
        wp_enqueue_style('builder-custom-theme', $this->url, ['kmdg-page-builder'], $this->version);
    }

    /**
     * Clears the theme CSS cache, causing the file to be regenerated with a
     * new version number on next page load.
     */
    public function refreshCache() {
        delete_transient("kmdg_pb_settings");
    }

    /**
     * Returns a properly formatted settings object
     * @return object
     */
    protected function loadLegacySettings() {
        return (object) [
            'color'     => get_sub_field('foreground_color')        ?: '#000000',
            'background'=> get_sub_field('background_color')        ?: 'transparent',
            'gfont'     => get_sub_field('default_font')            ?: 'Default',
            'size'      => get_sub_field('default_font_size')       ?: 100,
            'line'      => get_sub_field('default_line_height')     ?: 1,
            'weight'    => get_sub_field('default_font_weight')     ?: 400,
            'bold'      => get_sub_field('default_bold_weight')     ?: 700,
            'accent1'   => get_sub_field('primary_accent_color')    ?: '#aaaaaa',
            'accent2'   => get_sub_field('secondary_accent_color')  ?: '#cccccc',
            'accent3'   => get_sub_field('tertiary_accent_color')   ?: '#eaeaea',
            'inverse1'  => get_sub_field('primary_inverse_color')   ?: '#ffffff',
            'inverse2'  => get_sub_field('secondary_inverse_color') ?: '#ffffff',
            'inverse3'  => get_sub_field('tertiary_inverse_color')  ?: '#000000',
            'gutter'    => get_sub_field('column_gutter')           ?: 0,
            'border'    => get_sub_field('column_borders')          ?: ['size' => 0, 'style' => 'none', 'color' => '#ffffff'],
            'channel'   => get_sub_field('custom_channel')          ?: false,
            'width'     => get_sub_field('channel_width')           ?: 0,
            'elements'  => []
        ];
    }

    protected function loadThemeSettings() {
        return (object) [
            'color'     => get_field('foreground_color')        ?: '#000000',
            'background'=> get_field('background_color')        ?: 'transparent',
            'gfont'     => get_field('default_font')            ?: 'Default',
            'size'      => get_field('default_font_size')       ?: 100,
            'line'      => get_field('default_line_height')     ?: 1,
            'weight'    => get_field('default_font_weight')     ?: 400,
            'bold'      => get_field('default_bold_weight')     ?: 700,
            'accent1'   => get_field('primary_accent_color')    ?: '#aaaaaa',
            'accent2'   => get_field('secondary_accent_color')  ?: '#cccccc',
            'accent3'   => get_field('tertiary_accent_color')   ?: '#eaeaea',
            'inverse1'  => get_field('primary_inverse_color')   ?: '#ffffff',
            'inverse2'  => get_field('secondary_inverse_color') ?: '#ffffff',
            'inverse3'  => get_field('tertiary_inverse_color')  ?: '#000000',
            'gutter'    => get_field('column_gutter')           ?: 0,
            'border'    => get_field('column_borders')          ?: ['size' => 0, 'style' => 'none', 'color' => '#ffffff'],
            'channel'   => get_field('custom_channel')          ?: false,
            'width'     => get_field('channel_width')           ?: 0,
            'elements'  => []
        ];
    }

    protected function defaultThemeSettings() {
        return (object) [
            'color'     => '#000000',
            'background'=> 'transparent',
            'gfont'     => 'Default',
            'size'      => 100,
            'line'      => 1,
            'weight'    => 400,
            'bold'      => 700,
            'accent1'   => '#aaaaaa',
            'accent2'   => '#cccccc',
            'accent3'   => '#eaeaea',
            'inverse1'  => '#ffffff',
            'inverse2'  => '#ffffff',
            'inverse3'  => '#000000',
            'gutter'    => 0,
            'border'    => ['size' => 0, 'style' => 'none', 'color' => '#ffffff'],
            'channel'   => false,
            'width'     => 0,
            'elements'  => []
        ];
    }

    /**
     * Returns a properly formatted element settings object
     * @return object
     */
    protected function loadElementSettings() {
        return (object) [
            'element'       => get_sub_field('element')                     ?: 'p',
            'gfont'         => get_sub_field('font')                        ?: 'Default',
            'size'          => get_sub_field('size')                        ?: 100,
            'line'          => get_sub_field('line_height')                 ?: 1,
            'margin'        => get_sub_field('bottom_margin')               ?: 0,
            'weight'        => get_sub_field('normal_weight')               ?: 400,
            'bold'          => get_sub_field('bold_weight')                 ?: 700,
            'color'         => get_sub_field('color')                       ?: '#000000',
            'shadow'        => get_sub_field('text_shadow_amount')          ?: 0,
            'shadowX'       => get_sub_field('shadow_horizontal_offset')    ?: 0,
            'shadowY'       => get_sub_field('shadow_vertical_offset')      ?: 0,
            'shadow_color'  => get_sub_field('shadow_color')                ?: 0,
            'media'         => get_sub_field('media_queries')               ?: []
        ];
    }

    /**
     * Creates the theme CSS file
     */
    protected function generateCSS() {
        ob_start();

        $this->genericStyles();

        foreach(['.builder-section', '.builder-theme-override'] as $class) {
            foreach($this->settings as $theme => $settings) {
                $this->themeStyles($class, $theme, $settings);

                if(!empty($settings->elements)) {
                    foreach($settings->elements as $key => $data) {
                        $this->elementStyles($class, $theme, $data);

                        if(!empty($data->media)) {
                            foreach($data->media as $i => $query) {
                                $this->mediaQueries($class, $theme, $data, $query);
                            }
                        }
                    }
                }
            }
        }

        $css = ob_get_clean();

        // Lightly Minify CSS if not in Debug Mode
        if(!WP_DEBUG) {
            $css  = trim(preg_replace('/\s+/', ' ', preg_replace("~[\r\n]~", '', $css)));
        }

        file_put_contents($this->path, $css);
    }

    protected function genericStyles() {
        ?>

        .builder-section .builder-container,
        .u-builder-container-width {
            width: <?= get_field('builder_channel_width', 'option') ?>px;
        }

        <?php
    }

    protected function themeStyles($class, $theme, $settings) {
        ?>
        <?= $class ?>.builder-theme--<?= $theme ?>,
        .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> {
            color: <?= $settings->color ?>;
            background-color: <?= $settings->background ?>;
            font-family: <?= $settings->gfont['font'] == 'Default' ? 'inherit' : $settings->gfont['font']  ?>;
            font-weight: <?= $settings->weight ?>;
            font-size: <?= $settings->size ?>%;
            line-height: <?= $settings->line ?>;
        }

        <?php if($settings->channel): ?>
            <?= $class ?>.builder-theme--<?= $theme ?> .builder-container,
            <?= $class ?>.builder-theme--<?= $theme ?> .u-builder-channel-width,
            .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> .builder-container,
            .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> .u-builder-channel-width {
                width: <?= $settings->width ?>px;
            }
        <?php endif; ?>

        <?= $class ?>.builder-theme--<?= $theme ?> .builder-table,
        .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> .builder-table {
            margin-left: <?= -1 * ($settings->gutter/2) ?>%;
            margin-right: <?= -1 * ($settings->gutter/2) ?>%;
        }

        <?= $class ?>.builder-theme--<?= $theme ?> .builder-column,
        .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> .builder-column {
            padding-left: <?= $settings->gutter/2 ?>%;
            padding-right: <?= $settings->gutter/2 ?>%;
            border-right: <?= $settings->border['size'] ?>px <?= $settings->border['style'] ?> <?= $settings->border['color'] ?>;
        }

        <?= $class ?>.builder-theme--<?= $theme ?> strong,
        .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> strong {
            font-weight: <?= $settings->bold ?>;
        }

        <?= $class ?>.builder-theme--<?= $theme ?> .t-accent1--color,
        .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> .t-accent1--color {
            color: <?= $settings->accent1 ?>;
        }

        <?= $class ?>.builder-theme--<?= $theme ?> .t-accent1--inverse-color,
        .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> .t-accent1--inverse-color {
            color: <?= $settings->inverse1 ?>;
        }

        <?= $class ?>.builder-theme--<?= $theme ?> .t-accent1--bg,
        .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> .t-accent1--bg {
            background-color: <?= $settings->accent1 ?>;
            color: <?= $settings->inverse1 ?>;
        }

        <?= $class ?>.builder-theme--<?= $theme ?> .t-accent1--border,
        .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> .t-accent1--border {
            border-color: <?= $settings->accent1 ?>;
        }

        <?= $class ?>.builder-theme--<?= $theme ?> .t-accent2--color,
        .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> .t-accent2--color {
            color: <?= $settings->accent2 ?>;
        }

        <?= $class ?>.builder-theme--<?= $theme ?> .t-accent2--inverse-color,
        .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> .t-accent2--inverse-color {
            color: <?= $settings->inverse2 ?>;
        }

        <?= $class ?>.builder-theme--<?= $theme ?> .t-accent2--bg,
        .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> .t-accent2--bg {
            background-color: <?= $settings->accent2 ?>;
            color: <?= $settings->inverse2 ?>;
        }

        <?= $class ?>.builder-theme--<?= $theme ?> .t-accent2--border,
        .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> .t-accent2--border {
            border-color: <?= $settings->accent2 ?>;
        }

        <?= $class ?>.builder-theme--<?= $theme ?> .t-accent3--color,
        .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> .t-accent3--color {
            color: <?= $settings->accent3 ?>;
        }

        <?= $class ?>.builder-theme--<?= $theme ?> .t-accent3--inverse-color,
        .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> .t-accent3--inverse-color {
            color: <?= $settings->inverse3 ?>;
        }

        <?= $class ?>.builder-theme--<?= $theme ?> .t-accent3--bg,
        .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> .t-accent3--bg {
            background-color: <?= $settings->accent3 ?>;
            color: <?= $settings->inverse3 ?>;
        }

        <?= $class ?>.builder-theme--<?= $theme ?> .t-accent3--border,
        .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> .t-accent3--border {
            border-color: <?= $settings->accent3 ?>;
        }

        <?php
    }

    protected function elementStyles($class, $theme, $data) {
        $this->fonts[] = $data->gfont;
        ?>

        <?= $class ?>.builder-theme--<?= $theme ?> <?= $data->element ?>,
        <?= $class ?>.builder-theme--<?= $theme ?> .builder-style--<?= $data->element ?>,
        .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> <?= $data->element ?>,
        .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> .builder-style--<?= $data->element ?> {
            font-family: <?= $data->gfont['font'] == 'Default' ? 'inherit' : $data->gfont['font'] ?>;
            color: <?= $data->color ?>;
            font-weight: <?= $data->weight ?>;
            font-size: <?= $data->size ?>%;
            line-height: <?= $data->line ?>;
            margin-bottom: <?= $data->margin ?>em;
            <?php if($data->shadow > 0 || $data->shadowX != 0 || $data->shadowY != 0): ?>
                text-shadow: <?= $data->shadowX ?>px <?= $data->shadowY ?>px <?= $data->shadow ?>px <?= $data->shadow_color ?>;
            <?php endif; ?>
        }

        <?= $class ?>.builder-theme--<?= $theme ?> <?= $data->element ?> strong,
        <?= $class ?>.builder-theme--<?= $theme ?> .builder-style--<?= $data->element ?> strong,
        .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> <?= $data->element ?> strong,
        .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> .builder-style--<?= $data->element ?> strong{
            font-weight: <?= $data->bold ?>;
        }

        <?php
    }

    protected function mediaQueries($class, $theme, $data, $query) {
        if($query['device_size'] != 'custom') {
            $size = $this->query_sizes[$query['device_size']];
        } else {
            $size = $query['device_size'];
        }
        ?>

        @media (max-width: <?= $size ?>px) {
            <?= $class ?>.builder-theme--<?= $theme ?> <?= $data->element ?>,
            <?= $class ?>.builder-theme--<?= $theme ?> .builder-style--<?= $data->element ?>,
            .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> <?= $data->element ?>,
            .kmdg-nested-builder-content <?= $class ?>.builder-theme--<?= $theme ?> .builder-style--<?= $data->element ?>{
                font-size: <?= $query['font_size'] ?>%;
            }
        }
        <?php
    }
}