<?php

namespace KMDG\PageBuilder;


class LayoutFactory
{
    protected $class;

    /**
     * LayoutFactory constructor.
     *
     * @param string|null $class The base section class name
     * @throws BuilderException
     */
    public function __construct($class = null)
    {
        $base = __NAMESPACE__.'\\Layout';

        if(is_a($class, $base, true)) {
            $this->class = $class;
        } elseif(is_null($class)) {
            $this->class = $base;
        } else {
            throw new BuilderException("Registered Layout class does not extend $base");
        }
    }

    /**
     * @param string $name
     * @param Section $section
     * @param ColumnFactory $clF
     * @param ComponentFactory $comF
     *
     * @return Layout
     */
    public function make($name, Section $section, ColumnFactory $clF, ComponentFactory $comF) {
        return new $this->class($name, $section, $clF, $comF);
    }
}