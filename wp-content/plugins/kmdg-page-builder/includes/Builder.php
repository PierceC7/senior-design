<?php
/**
 * Created by PhpStorm.
 * User: KMDG
 * Date: 7/21/2016
 * Time: 1:34 PM
 */

namespace KMDG\PageBuilder;

class Builder
{
    const VERSION                = '1.0.2.1';
    const DEFAULT_LAYOUT_DIR     = 'builder/layouts';
    const DEFAULT_COMPONENT_DIR  = 'builder/components';
    const DEFAULT_COLUMN_DIR     = 'builder/columns';
    const SCREENS_DIR            = 'screens';
    const PAGE_BUILDER_ACF_FIELD = 'kmdg_page_builder';
    const BLOCK_BUILDER_ACF_FIELD= 'kmdg_block_builder';
    const CACHE_PREFIX           = 'kmdg_pb_cache_entry_';
    const BLOCK_POST_TYPE        = 'kmdg-pb-blocks';
    const THEME_POST_TYPE        = 'kmdg-pb-themes';

    /**
     * @var bool
     */
    protected static $started = false;

    /**
     * @var array[]
     */
    protected static $file_map_cache = [];

    /**
     * @var Theme
     */
    protected static $theme;

    /**
     * @var SectionFactory
     */
    protected static $sectionFactory;

    /**
     * @var LayoutFactory
     */
    protected static $layoutFactory;

    /**
     * @var ColumnFactory
     */
    protected static $columnFactory;

    /**
     * @var ComponentFactory
     */
    protected static $componentFactory;

    /**
     * @var BuilderException
     */
    protected static $factoryException;

    /**
     * @var array
     */
    protected static $sectionTypes;

    /**
     * @var Section
     */
    protected static $currentSection;

    /**
     * @var bool Used to prevent infinite loops when updating post content
     */
    protected static $updatingCache = false;

    /**
     * Sets up the builder actions, and initial values
     *
     * @param Theme|null $theme the page builder theme dependency
     * @param SectionFactory $sf factory for creating section objects
     * @param LayoutFactory $lf factory for creating layout objects
     * @param ColumnFactory $clF factory for creating column objects
     * @param ComponentFactory $comF factory for creating component objects
     */
    public static function init(Theme $theme, SectionFactory $sf, LayoutFactory $lf, ColumnFactory $clF, ComponentFactory $comF) {

        if(class_exists('acf')) {

            // Dependencies
            static::$theme = $theme;
            static::$sectionFactory = $sf;
            static::$layoutFactory = $lf;
            static::$columnFactory = $clF;
            static::$componentFactory = $comF;

            static::$factoryException = new BuilderException("Cannot change factory class after builder has begun rendering");

            static::$sectionTypes = apply_filters('KMDG/PB/section_types', [
                'section'   => [get_class(), 'buildSection'],
                'block'     => [get_class(), 'buildBlock'],
                'slider'    => [get_class(), 'buildSlider'],
                'rollover'  => [get_class(), 'buildRollover'],
            ]);

            // Custom Support
            add_image_size("pb-preload", 100, 100, false);
            add_image_size("pb-medium", 550, 550, false);

            // Hooks
            add_action('init', [get_class(), 'load_theme']);
            add_action('init', [get_class(), 'register_post_types']);
            add_filter('the_content', [get_class(), 'build']);
            add_filter('acf/settings/load_json', [get_class(), 'load_acf']);
            add_action('wp_enqueue_scripts', [get_class(), 'register_assets'], 1);
            add_action('admin_enqueue_scripts', [get_class(), 'register_admin_assets'], 99);
            add_action('wp_footer', [get_class(), 'font_loader']);
            add_filter('template_include', [get_class(), 'setup_screens']);
            add_action('admin_init', [get_class(), 'add_capabilities']);
            add_action('save_post', [get_class(), 'regenerate_content'], 10000);
            add_filter('run_wptexturize', '__return_false');
            add_filter('acf/fields/wysiwyg/toolbars', [get_class(),'tiny_mce_settings'], 100);

            // Updates
            //add_action('wp_ajax_update_kmdg_pb', [get_class(), 'doUpdates']);

            // Shortcode
            add_shortcode('kmdg_builder_block', [get_class(), 'block_shortcode']);
        }
    }

    /**
     * Creates a structured array that can be used to look up file data, this is only done once
     * so additional calls do nothing. This function is called at the start of build(), or when
     * anything requests the location of a builder template file.
     */
    protected static function setup_file_cache_once() {
        if(empty(self::$file_map_cache)) {
            self::$file_map_cache = [
                "layout" => [
                    "default" => static::DEFAULT_LAYOUT_DIR,
                    "filtered" => static::get_layout_dir()
                ],
                "column" => [
                    "default" => static::DEFAULT_COLUMN_DIR,
                    "filtered" => static::get_column_dir()
                ],
                "component" => [
                    "default" => static::DEFAULT_COMPONENT_DIR,
                    "filtered" => static::get_component_dir()
                ]
            ];
        }
    }

    /**
     * This is the main entry point for the builder when rendering templates. Normally, this function is hooked into
     * the_content(), but it can be called manually if appropriate. This will render all page builder fields using
     * their provided template files, and include any external block elements as if they were normal sections. It also
     * includes some generic markup for wrapping the builder content, and caches generated builder content so that the
     * HTML does not need to constantly be regenerated. Caching is disabled while WP_DEBUG is turned on.
     *
     * @param string $content The HTML to use if the page builder is not active on the current page/post
     * @param string $field the ACF field to be used by the page builder, for using multiple builders on a single page
     * @param bool $forceSingleSectionMode Display only a single section, for use in Builder Block preview mode.
     *
     * @param null $postID
     *
     * @return string
     */
    public static function build($content, $field = null, $forceSingleSectionMode = false, $postID = null) {
        $postID = !is_null($postID) ? $postID : get_the_ID();
        $builder = static::PAGE_BUILDER_ACF_FIELD;
        $flushCache = false;

        if(!empty($field)) {
            $builder = $field;
        }

        if(!empty($_GET['pbcache']) && $_GET['pbcache'] == 'false') {
            $flushCache = true;
        }

        self::setup_file_cache_once();

        if(!is_admin()
            && trim($content) != ''
            && apply_filters('KMDG/PB/cache/enable', true, $postID)
            && !static::$updatingCache
            && !WP_DEBUG
            && !$flushCache) {

            remove_filter('the_content', 'wpautop');

            return "<!-- Start Cached -->".$content."<!-- End Cached -->";
        } else {
            if(have_rows($builder, $postID)) {
                ob_start();

                if(!static::$started) {
                    echo apply_filters('KMDG/PB/main/html_start',
                        '<div class="kmdg-builder-content"><div class="builder-font-controller">');
                } else {
                    echo apply_filters('KMDG/PB/main/nested_html_start',
                        '<div class="kmdg-nested-builder-content"><div class="builder-font-controller">');
                }

                static::$started = true;

                // Main Builder Loop
                while (have_rows($builder, $postID)) {
                    the_row();

                    if($forceSingleSectionMode) {
                        static::buildSection('block');
                        reset_rows();
                        break;
                    } else {
                        if(!empty(static::$sectionTypes[get_row_layout()])) {
                            call_user_func(static::$sectionTypes[get_row_layout()]);
                        } else {
                            static::buildSection();
                        }
                    }
                }

                echo apply_filters('KMDG/PB/main/html_end', '</div></div>');

                $content = ob_get_clean();
            }
        }

        static::$started = false;

        return $content;
    }

    /**
     * Builds a single block from the Builder Block post type. This is for external use when you need to include a single
     * builder block without including the full page builder.
     *
     * @param \WP_Post $blockPost
     * @param bool $nested Controls if pagebuilder wrapper divs are output with normal classes or nested classes
     *
     * @return string
     */
    public static function block(\WP_Post $blockPost, $nested = false) {
        global $post;
        $post = $blockPost;
        setup_postdata($post);

        static::$started = $nested;

        $content = static::build('', Builder::BLOCK_BUILDER_ACF_FIELD, true);

        wp_reset_postdata();

        return $content;
    }

    public static function block_shortcode($args, $content = null) {
        $atts = (object) shortcode_atts([
            'id' => null
        ], $args);

        if(!empty($atts->id)) {
            return static::block(get_post((int) $atts->id), true);
        }
    }

    /**
     * Create an individual section
     *
     * @param string $type
     */
    protected static function buildSection($type = 'section') {

        $section = static::$sectionFactory->make(get_sub_field('options'), $type);
        static::$currentSection = $section;

        $section_styles = static::section_styles($section);

        echo apply_filters('KMDG/PB/section/html_start',
            '<section '.($section->option('custom_id') ? 'id="'.$section->option('custom_id').'"' : '').' class="'.$section->classes().'" '.$section_styles.'>', $section, $section_styles);

        self::progressiveBackground($section);

        if(have_rows('layout')) {

            echo apply_filters('KMDG/PB/section/before_layouts',
                '<div class="builder-layouts-container">', $section);

            while (have_rows('layout')) {
                the_row();

                $layout = static::$layoutFactory->make(get_row_layout(), $section, static::$columnFactory, static::$componentFactory);

                echo apply_filters('KMDG/PB/layout/html_start',
                    '<div class="' . $layout->classes() . '">', $section, $layout);

                echo apply_filters('KMDG/PB/layout/container/html_start',
                    '<div class="builder-container">', $section, $layout);

                // Encapsulate user scope
                call_user_func(function ($file) use ($section, $layout) {

                    // Include layout file
                    include($file);

                }, static::locate_layout($layout->getName()));

                echo apply_filters('KMDG/PB/layout/container/html_end', '</div>', $section, $layout);

                echo apply_filters('KMDG/PB/layout/html_start', '</div>', $section, $layout);
            }

            echo apply_filters('KMDG/PB/section/after_layouts',
                '</div>', $section);
        }

        echo apply_filters('KMDG/PB/section/html_end',  '</section>');
    }

    /**
     * Load a section from an external block
     *
     * @param string $type
     * @param string $field
     */
    protected static function buildBlock($type = 'block', $field = 'builder_block') {
        // Null used for rendering single blocks in preview mode.
        $block = !is_null($field) ? get_sub_field($field) : null;

        if(!is_int($block)) {
            $block = $block->ID;
        }

        echo "[kmdg_builder_block id='{$block}']";
    }

    protected static function buildRollover() {
        // @Todo: Build rollover section code
    }

    protected static function buildSlider() {
        $count = get_row_index();

        // Create a pseudo-section for the slider, wont be fully utilized but enables compatibility with normal sections
        $slider = static::$sectionFactory->make(get_sub_field('slide_options'), 'slider-pseudosection');
        static::$currentSection = $slider;

        $slideCount = count(get_sub_field('blocks'));

        if(have_rows('blocks')) {

            $atts = [];
            $classes = ['builder-block-slider'];
            $styles = [];

            $theme = self::theme()->getSettings($slider->option('controls_theme')->get());

            // set up cycle attributes and classes
            if($slider->option('transition_delay')->get()) {
                $atts['transition_delay'] = 'data-cycle-timeout="'.($slider->option('transition_delay')->get() * 1000).'"';
            } else {
                $atts['transition_delay'] = 'data-cycle-timeout="0"';
            }

            if($slider->option('transition_speed')->get()) {
                $atts['transition_speed'] = 'data-cycle-speed="'.($slider->option('transition_speed')->get() * 1000).'"';
            }

            if($slider->option('use_arrows')->get()) {
                $classes['has-arrows'] = 'builder-slider--has-arrows';
                $atts['use_arrows'] = 'data-cycle-prev="#builder_slider_prev_'.$count.'" data-cycle-next="#builder_slider_next_'.$count.'"';
            }

            if($slider->option('use_pagers')->get()) {
                $classes['has-pager'] = 'builder-slider--has-pager';
                $atts['use_pagers'] = 'data-cycle-pager="> div > .builder-block-slider-pagers" data-cycle-pager-active-class="t-'.$slider->option('pager_color').'--inverse-color"';
            }

            if($slider->option('pause_on_mouseover')->get()) {
                $atts['pause_on_mouseover'] = 'data-cycle-pause-on-hover="true"';
            }

            if($slider->option('background_image')->get()) {
                $classes['has-bg'] = 'builder-slider--has-background';
            }

            if($slider->option('enable_parallax')->get()) {
                $classes['parallax'] = 'builder-parallax';
            }

            $styles['bgcolor'] = "background-color: {$theme->background}";

            $atts['fx'] = 'data-cycle-fx='.$slider->option('transition_effect');
            $atts['style'] = 'style="'.implode(' ', apply_filters('KMDG/PB/block_slider/styles', $styles, $slider, $theme)).'"';
            $atts = apply_filters('KMDG/PB/block_slider/atts', $atts, $slider, $styles, $theme);
            $classes = apply_filters('KMDG/PB/block_slider/classes', $classes, $slider, $theme);

            echo apply_filters('KMDG/PB/block_slider/html_start',
                '<div class="'.implode(' ', $classes).'" '.implode(' ', $atts).' '.static::section_styles($slider).'>', $slider, $classes, $atts, $slideCount);

            self::progressiveBackground($slider);

            $vertical = true; // used for direction alternating transitions
            while(have_rows('blocks')) {
                the_row();

                $blockID = get_sub_field('builder_block');
                $vertical = !$vertical;
                $thumb = null;

                if(has_post_thumbnail($blockID) && $slider->option('thumbnail_pagers')->get()) {
                    $thumb = "data-cycle-pager-template='".'<a href="#"><img src="'.Utilities::src(get_post_thumbnail_id($blockID), 'pb-medium').'"></a>'."'";
                }

                echo apply_filters('KMDG/PB/single_block_slide/start',
                    '<div class="builder-block-slide" '.$thumb.' '.$atts['fx'].' data-cycle-tile-vertical="'.($vertical ? 'true' : 'false').'" data-cycle-tile-count='.rand(7, 15).'>', $slider, $vertical, $blockID, $thumb, get_row_index(), $slideCount);

                $link = get_sub_field('full_slide_link');
                if($link) {
                    echo apply_filters('KMDG/PB/single_block_slide/link',
                        '<a href="'.$link.'" class="builder-block-slide-link" target="'.
                        (get_sub_field('new_window') ? '_blank' : '_self').
                        '"><span>Slideshow Link: '.$link.'</span></a>', $slider);
                }

                static::buildBlock('block-slide');

                echo apply_filters('KMDG/PB/single_block_slide/end',
                    '</div>', $slider);
            }

            $controls = $slider->option('controls_theme')->get();
            if(is_numeric($controls)) {
                $controls = get_post_field('post_name', $controls);
            }

            echo apply_filters('KMDG/PB/block_slider/theme/start',
                '<div class="builder-theme-override builder-theme--'.$controls.'">', $slider, $controls);

            if($slider->option('use_arrows')->get()) {
                echo apply_filters('KMDG/PB/block_slider/arrows',
                    '<div class="builder-block-slider-arrows u-builder-container-width t-'.$slider->option('arrow_color').'--color">'.
                    '<span id="builder_slider_prev_'.$count.'" class="builder-slide-prev"><i class="fa fas fa-chevron-left" aria-hidden="true"></i></span>'.
                    '<span id="builder_slider_next_'.$count.'" class="builder-slide-next"><i class="fa fas fa-chevron-right" aria-hidden="true"></i></span>'.
                    '</div>', $slider, $count);
            }

            if($slider->option('use_pagers')->get()) {

                $pagerClass = '';

                if($slider->option('thumbnail_pagers')->get()) {
                    $pagerClass = 'builder-block-slider--thumbnail-pagers';
                }

                $pagerHTML = '<div class="builder-block-slider-pagers t-' . $slider->option('pager_color') . '--color '.$pagerClass.'"></div>';

                echo apply_filters('KMDG/PB/block_slider/pagers', $pagerHTML, $slider);
            }

            echo apply_filters('KMDG/PB/block_slider/theme/end',
                '</div>', $slider);


            echo apply_filters('KMDG/PB/block_slider/end',
                '</div>', $slider);
        }
    }

    public static function progressiveBackground(Section $section) {
        if($section->option('background_image')->get() && $section->option('enable_parallax')->get()) {
            echo self::get_parallax_bg_html($section);
        } elseif($section->option('background_video')->get()) {
            echo self::get_video_bg_html($section);
        } elseif($section->option('background_image')->get()) {
            $image = $section->option('background_image')->get();
            $opacity = $section->option('background_opacity')->get() ?: 100;
            $origin = $section->option('background_origin')->getFixedSelectField() ?: 'center';
            echo self::get_bg_image_html($section, $image, $opacity, 100, $origin);
        }
    }

    /**
     * Retrieves the currently loaded section data
     * @return Section
     */
    public static function getCurrentSection() {
        return static::$currentSection;
    }

    /**
     * Retrieves the builder theme object
     *
     * @return Theme
     */
    public static function theme() {
        return static::$theme;
    }

    /**
     * @param $name
     *
     * @return string
     */
    public static function locate_layout($name) {
        return self::locate_builder_template('layout', $name);
    }

    /**
     * @param $name
     *
     * @return string
     */
    public static function locate_column($name) {
        return self::locate_builder_template('column', $name);
    }

    /**
     * @param $name
     *
     * @return string
     */
    public static function locate_component($name) {
        return self::locate_builder_template('component', $name);
    }

    /**
     * Allows themes and plugins to customize the section factory class
     * @param SectionFactory $sf
     *
     * @throws BuilderException
     */
    public static function setSectionFactory(SectionFactory $sf) {
        if(!static::$started) {
            static::$sectionFactory = $sf;
        } else {
            throw static::$factoryException;
        }
    }

    /**
     * Allows themes and plugins to customize the layout factory class
     * @param LayoutFactory $lf
     *
     * @throws BuilderException
     */
    public static function setLayoutFactory(LayoutFactory $lf) {
        if(!static::$started) {
            static::$layoutFactory = $lf;
        } else {
            throw static::$factoryException;
        }
    }

    /**
     * Allows themes and plugins to customize the column factory class
     * @param ColumnFactory $cf
     *
     * @throws BuilderException
     */
    public static function setColumnFactory(ColumnFactory $cf) {
        if(!static::$started) {
            static::$columnFactory = $cf;
        } else {
            throw static::$factoryException;
        }
    }

    /**
     * Allows themes and plugins to customize the component factory class
     * @param ComponentFactory $cf
     *
     * @throws BuilderException
     */
    public static function setComponentFactory(ComponentFactory $cf) {
        if(!static::$started) {
            static::$componentFactory = $cf;
        } else {
            throw static::$factoryException;
        }
    }

    /**
     * Finds template files for individual builder parts. This uses filterable file paths as its first option,
     * but if no theme-specific files are found it will choose the defaults provided with the page builder.
     *
     * Returns the path to the default error.php if no files matching the description are found.
     *
     * @param $type Only accepts one of "layout", "column", or "component"
     * @param $name The file slug you are looking for
     *
     * @return string
     */
    protected static function locate_builder_template($type, $name) {
        self::setup_file_cache_once(); // Ensure file cache is generated

        $template = locate_template(self::$file_map_cache[$type]['filtered']."/$name.php", false, false);

        if($template == '') {
            if (file_exists(KMDG_PAGE_BUILDER . self::$file_map_cache[$type]['default'] . "/$name.php")) {
                $template = KMDG_PAGE_BUILDER . self::$file_map_cache[$type]['default'] . "/$name.php";
            } else {
                $template = KMDG_PAGE_BUILDER . self::$file_map_cache[$type]['default'] . "/error.php";
            }
        }

        return $template;
    }

    /**
     * @return string
     */
    public static function get_layout_dir() {
        return apply_filters('KMDG/PB/layout_path', static::DEFAULT_LAYOUT_DIR);
    }

    /**
     * @return string
     */
    public static function get_column_dir() {
        return apply_filters('KMDG/PB/column_path', static::DEFAULT_COLUMN_DIR);
    }

    /**
     * @return string
     */
    public static function get_component_dir() {
        return apply_filters('KMDG/PB/component_path', static::DEFAULT_COMPONENT_DIR);
    }

    /**
     * @param $paths
     *
     * @return array
     */
    public static function load_acf( $paths ) {
        $paths[] = KMDG_PAGE_BUILDER . 'json';
        return $paths;
    }

    public static function load_theme() {
        static::theme()->load();
    }

    /**
     * @hook wp_enqueue_scripts
     */
    public static function register_assets() {
        wp_enqueue_style('kmdg-page-builder', KMDG_PAGE_BUILDER_ASSETS."styles/css/default.css", [], static::VERSION, 'all');
        wp_enqueue_script('jquery');
        wp_enqueue_script('kmdg-page-builder-scripts', KMDG_PAGE_BUILDER_ASSETS."js/builder.min.js", ['jquery'], static::VERSION, true);
    }

    /**
     * @hook admin_enqueue_scripts
     */
    public static function register_admin_assets() {
        wp_enqueue_style('kmdg-page-builder', KMDG_PAGE_BUILDER_ASSETS."styles/css/admin.css", [], static::VERSION, 'all');
        wp_enqueue_script('kmdg-page-builder-js', KMDG_PAGE_BUILDER_ASSETS."js/admin.js", [], static::VERSION, true);
    }

    /**
     * @param Section $section
     *
     * @return string
     */
    protected static function section_styles(Section $section) {
        $atts = '';

        $custom = esc_attr(apply_filters('KMDG/PB/section/custom_styles', $section->option('custom_styles') , $section));

        $bgcolor = $section->option('background_color');

        $properties = apply_filters('KMDG/PB/section/styles',
            ($bgcolor->exists() ? ['background-color' => $bgcolor->get()] : []),
            $section);

        foreach ($properties as $name=>$value) {
            $atts .= "{$name}: {$value};";
        }

        $atts = esc_attr($atts);

        return "style='{$atts}{$custom}'";
    }

    protected static function get_bg_image_html(Section $section, $images, $percent_opacity = 100, $height = 100, $origin = 'center') {
        $opacity = $percent_opacity / 100;
        $html = '';

        // Backwards compatibility check
        if(!is_array($images)) {
            $images = [$images];
        }

        if(count($images) > 1) {
            $transition = $section->option('background_transition_mode')->get();
            $speed = $section->option('background_transition_speed')->get() * 1000;

            $html .= apply_filters('KMDG/PB/background/slideshow/html_start',
                "<div class='builder-bg-image-slideshow' style='height:{$height}%' data-cycle-fx='{$transition}' data-cycle-timeout='{$speed}'>",
                $section, $images, $height, $transition, $speed
            );
        } else {
            $html .= apply_filters('KMDG/PB/background/single/html_start',
                "<div class='builder-bg-image-wrap' style='height:{$height}%'>",
                $section, $images, $height
            );
        }

        foreach ($images as $image) {
            // Backwards compatability check
            if(is_array($image)) {
                $id = $image['ID'];
            } else {
                $id = $image;
            }

            $srcs = Utilities::getImageSrcJson($id);

            // Can filter to return false to disable preload function
            if($enablePreload = apply_filters('KMDG/PB/background/preload', true)) {
                $preloadImage = Utilities::getPreloadImgSrc($id);
            } else {
                $preloadImage = Utilities::src($id, 'full');
            }

            $style = "background-image: url({$preloadImage});opacity:{$opacity};background-position-y:{$origin};";

            $html .= apply_filters('KMDG/PB/background/html',
                "<div class='builder-img-bg ".($enablePreload ? '' : 'loaded')."' style='{$style}' ".($enablePreload ? "data-srcs='". $srcs ."'" : '')."><img src='{$preloadImage}' alt='' role='presentation'></div>",
                $id, $height, $opacity, $preloadImage, $enablePreload, $srcs, $style);

        }

        if(count($images) > 1) {
            $html .= apply_filters('KMDG/PB/background/slideshow/html_end',
                "</div>");
        } else {
            $html .= apply_filters('KMDG/PB/background/single/html_end',
                "</div>");
        }

        return $html;
    }

    /**
     * @param Section $section
     *
     * @return mixed|void
     */
    protected static function get_parallax_bg_html(Section $section) {
        $html = apply_filters('KMDG/PB/parallax/container_start', '<div class="builder-parallax-container">', $section);

        if(!$section->option('background_video')->get()) {
            $img = $section->option('background_image')->get();
            $effect = 100 + $section->option('parallax_effect')->get();
            $opacity = $section->option('background_opacity')->get() ?: 100;
            $origin = $section->option('background_origin')->getFixedSelectField() ?: 'center';

            $html .= static::get_bg_image_html($section, $img, $opacity, $effect, $origin);
        } else {
            $html .= self::get_video_bg_html($section, false);
        }

        $html .= apply_filters('KMDG/PB/parallax/container_end', '</div>', $section);

        return apply_filters('KMDG/PB/parallax/html', $html, $section);
    }

    /**
     * @param Section $section
     * @param bool $wrap
     *
     * @return string
     */
    protected static function get_video_bg_html(Section $section, $wrap = true) {
        $html = '';
        $effect = 100 + $section->option('parallax_effect')->get();
        $opacity = ($section->option('background_opacity')->get() ?: 100) / 100;

        if($wrap) {
            $html .= apply_filters('KMDG/PB/video/bg_wrapper_start', "<div class='builder-video-bg-container' style='opacity:{$opacity};'>", $section);
        }

        $html .= apply_filters('KMDG/PB/video/bg_start',
            "<video class='builder-video-bg' style='min-height: {$effect}%;' autoplay muted loop poster='".Utilities::src($section->option('background_image')->get(), 'large')."'>",
            $effect, $section);

        foreach($section->option('background_video')->next() as $field) {
            if(!empty($field['movie_source'])) {
                $type = get_post_mime_type($field['movie_source']);
                $url = wp_get_attachment_url($field['movie_source']);
                $html .= apply_filters('KMDG/PB/video/bg_source',
                    "<source src='{$url}' type='{$type}'>",
                    $section, $url, $type);
            }
        }

        $bgImage = $section->option('background_image')->get();

        if(!empty($bgImage) ) {
            // Backwards compatibility check
            if(is_array($bgImage)) {
                $bgImage = $bgImage[0];
            }

            $html .= apply_filters('KMDG/PB/video/bg_image',
                Utilities::srcsetImg($bgImage),
                $section, $bgImage);
        }

        $html .=  apply_filters('KMDG/PB/video/bg_end', "</video>");

        if($wrap) {
            $html .= apply_filters('KMDG/PB/video/bg_wrapper_end', "</div>");
        }

        return apply_filters('KMDG/PB/video_bg', $html);
    }

    /**
     * Used to load built in fonts, such as FontAwesome. Can be disabled via a filter if the theme or another plugin
     * includes the required font(s).
     *
     * @hook wp_footer
     */
    public static function font_loader() {
        if(apply_filters('KMDG/PB/enable_font_loader', true)) {
            ?>
            <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
            <script>
                WebFont.load({
                    custom: {
                        families: ['FontAwesome'],
                        urls: ['<?= KMDG_PAGE_BUILDER_ASSETS.'styles/css/font-awesome.css' ?>']
                    }
                });
            </script>
            <?php
        }
    }

    public static function register_post_types() {
        register_post_type(static::BLOCK_POST_TYPE, [
            'publicly_queryable'    => true,
            'exclude_from_search'   => true,
            'show_ui'               => true,
            'show_in_nav_menus'     => false,
            'menu_position'         => 20,
            'menu_icon'             => 'dashicons-welcome-widgets-menus',
            'capability_type'       => 'page',
            'hierarchical'          => false,
            'supports'              => ['title', 'revisions', 'thumbnail'],
            'has_archive'           => false,
            'query_var'             => false,
            'labels' => [
                'name'                  => 'Builder Blocks',
                'singular_name'         => 'Block',
                'add_new_item'          => 'Add New Block',
                'edit_item'             => 'Edit Block',
                'new_item'              => 'New Block',
                'view_item'             => 'View Block',
                'view_items'            => 'View Blocks',
                'search_items'          => 'Search Blocks',
                'not_found'             => 'No blocks found',
                'not_found_in_trash'    => 'No blocks found in trash',
                'all_items'             => 'All Blocks',
                'archives'              => 'All Blocks',
                'attributes'            => 'Block Attributes',
                'insert_into_item'      => 'Insert into block',
                'uploaded_to_this_item' => 'Uploaded to this block',
                'name_admin_bar'        => 'Builder Block'
            ]
        ]);

        $type = 'builder_theme';
        $types = 'builder_themes';

        register_post_type(static::THEME_POST_TYPE, [
            'publicly_queryable'    => true,
            'exclude_from_search'   => true,
            'show_ui'               => true,
            'show_in_nav_menus'     => false,
            'menu_position'         => 100,
            'menu_icon'             => 'dashicons-editor-paste-text',
            'capabilities'          => [
                'edit_post'             => "edit_$type",
                'edit_posts'            => "edit_$types",
                'edit_others_posts'     => "edit_others_$types",
                'publish_posts'         => "publish_$types",
                'read_post'             => "read_$type",
                'read_private_posts'    => "read_private_$types",
                'delete_post'           => "delete_$type",
                'delete_posts'          => "delete_$types",
                'delete_others_posts'   => "delete_others_$types",
            ],
            'capability_type'       => [$type, $types],
            'map_meta_cap'          => true,
            'hierarchical'          => false,
            'supports'              => ['title', 'revisions', 'author'],
            'has_archive'           => false,
            'query_var'             => false,
            'labels' => [
                'name'                  => 'Builder Themes',
                'singular_name'         => 'Theme',
                'add_new_item'          => 'Add New Theme',
                'edit_item'             => 'Edit Theme',
                'new_item'              => 'New Theme',
                'view_item'             => 'View Theme',
                'view_items'            => 'View Themes',
                'search_items'          => 'Search Themes',
                'not_found'             => 'No themes found',
                'not_found_in_trash'    => 'No themes found in trash',
                'all_items'             => 'All Themes',
                'archives'              => 'All Themes',
                'attributes'            => 'Theme Attributes',
                'insert_into_item'      => 'Insert into theme',
                'uploaded_to_this_item' => 'Uploaded to this theme',
                'name_admin_bar'        => 'Builder Theme'
            ]
        ]);

    }

    public static function doUpdates() {
        /** @var \wpdb $wpdb */
        global $wpdb;

        /** @var \WP_Post */
        global $post;

        $query = "SELECT meta_id, meta_value FROM {$wpdb->postmeta} WHERE meta_key = 'kmdg_page_builder'";

        $result = $wpdb->get_results($query);
        $count = 0;

        foreach($result as $row) {
            if(is_numeric($row->meta_value)) {
                $count++;
                $data = serialize(array_fill(0, $row->meta_value, 'section'));
                $wpdb->update($wpdb->postmeta, ['meta_value' => $data], ['meta_id' => $row->meta_id]);
            }
        }

        echo "Records Updated: $count";
        wp_die();
    }

    /**
     * Used by template_include filter to include appropriate templates for viewing page builder parts
     * @param $template
     *
     * @return string
     */
    public static function setup_screens($template) {
        /** @var $wp_query \WP_Query */
        global $wp_query;

        if($wp_query->is_singular(static::BLOCK_POST_TYPE)) {
            if ($wp_query->is_preview() || current_user_can('edit_posts')) {
                return KMDG_PAGE_BUILDER . self::SCREENS_DIR . '/' . 'block-preview.php';
            } else {
                $wp_query->set_404();
                status_header(404);

                return get_404_template();
            }
        }

        return $template;
    }

    public static function unload_editor() {
        if(!empty($_GET['post'])) {
            $id = (int) $_GET['post'];

            if(get_field(static::PAGE_BUILDER_ACF_FIELD, $id)) {
                remove_filter('the_content', 'wptexturize');

                add_filter('user_can_richedit', function() {
                    return false;
                });
            }
        }
    }

    public static function add_capabilities() {
        $type = 'builder_theme';
        $types = 'builder_themes';

        $admins = get_role('administrator');
        $editors = get_role('editor');
        $authors = get_role('author');

        $admins->add_cap("edit_$type");
        $admins->add_cap("edit_$types");
        $admins->add_cap("edit_others_$types");
        $admins->add_cap("publish_$types");
        $admins->add_cap("read_$type");
        $admins->add_cap("read_private_$types");
        $admins->add_cap("delete_$type");
        $admins->add_cap("edit_published_$types");
        $admins->add_cap("delete_published_$types");
        $admins->add_cap("delete_others_$types");

        $editors->add_cap("edit_$type");
        $editors->add_cap("edit_$types");
        $editors->add_cap("publish_$types");
        $editors->add_cap("read_$type");
        $editors->add_cap("read_private_$types");
        $editors->add_cap("delete_$type");
        $editors->add_cap("edit_published_$types");
        $editors->add_cap("delete_published_$types");

        $authors->add_cap("read_$type");
        $authors->add_cap("read_private_$types");
    }

    /**
     * Modify TinyMCE editor.
     *
     * @param $init
     *
     * @return mixed
     */
    public static function tiny_mce_settings($toolbars) {

        // return $toolbars - IMPORTANT!
        return $toolbars;

    }

    /**
     * Rebuilds the builder content cache (stored in page content) when the page is updated.
     *
     * @param int $post_id The post ID.
     */
    public static function regenerate_content($post_id) {
        remove_action('save_post', [get_class(), 'regenerate_content'], 10000);

        if(!static::$updatingCache && get_field(static::PAGE_BUILDER_ACF_FIELD, $post_id)) {
            static::$updatingCache = true;

            remove_all_shortcodes();
            remove_filter('the_content', [get_class(), 'build']);
            remove_filter('the_content', 'wpautop');
            add_filter('run_wptexturize', '__return_false');

            wp_update_post([
                'ID' => $post_id,
                'post_content' => html_entity_decode(static::build('', static::PAGE_BUILDER_ACF_FIELD, false, $post_id))
            ]);
        }
    }
}