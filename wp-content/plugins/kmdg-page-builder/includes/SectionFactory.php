<?php

namespace KMDG\PageBuilder;


class SectionFactory
{
    protected $class;

    /**
     * SectionFactory constructor.
     *
     * @param string|null $class The base section class name
     * @throws BuilderException
     */
    public function __construct($class = null)
    {
        $base = __NAMESPACE__.'\\Section';

        if(is_a($class, $base, true)) {
            $this->class = $class;
        } elseif(is_null($class)) {
            $this->class = $base;
        } else {
            throw new BuilderException("Registered Section class does not extend $base");
        }
    }

    /**
     * @param array $data
     * @param string $type
     *
     * @return Section
     */
    public function make($data, $type) {
        return new $this->class($data, $type);
    }
}