<?php

namespace KMDG\PageBuilder;


class ColumnFactory
{
    protected $class;

    /**
     * ColumnFactory constructor.
     *
     * @param string|null $class The base column class name
     * @throws BuilderException
     */
    public function __construct($class = null)
    {
        $base = __NAMESPACE__.'\\Column';

        if(is_a($class, $base, true)) {
            $this->class = $class;
        } elseif(is_null($class)) {
            $this->class = $base;
        } else {
            throw new BuilderException("Registered Column class does not extend $base");
        }
    }

    /**
     * @param string $type
     * @param string $name
     * @param Section $section
     * @param Layout $layout
     * @param ComponentFactory $cf
     *
     * @return Column
     */
    public function make($type, $name, Section $section, Layout $layout, ComponentFactory $cf) {
        return new $this->class($type, $name, $section, $layout, $cf);
    }
}