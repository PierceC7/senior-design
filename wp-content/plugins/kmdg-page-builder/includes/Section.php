<?php
/**
 * Created by PhpStorm.
 * User: KMDG
 * Date: 9/2/2016
 * Time: 4:13 PM
 */

namespace KMDG\PageBuilder;


class Section
{
    /**
     * @var array
     */
    protected $options = [];

    /**
     * @var string
     */
    protected $type;

    /**
     * Section constructor.
     *
     * @param array $data
     * @param string $type
     */
    public function __construct($data, $type)
    {
        list($this->options) = $data;
        $this->type = $type;
    }

    /**
     * @param string $extra
     *
     * @return string
     */
    public function classes($extra = '')
    {
        $theme = $this->option('theme')->get();

        if(is_numeric($theme)) {
            $theme = get_post_field('post_name', $theme);
        }

        return implode(' ', apply_filters('KMDG/PB/section/classes', [
            "builder-section",
            "builder-section-type--".$this->getType(),
            $theme ? "builder-custom-theme builder-theme--". $theme : "builder-theme--none",
            $this->option('section_padding_top')->get() ? "builder-section-padding-top-".$this->option('section_padding_top') : "",
            $this->option('section_padding_bottom')->get() ? "builder-section-padding-bottom-".$this->option('section_padding_bottom') : "",
            $this->option('layout_padding')->get() ? "builder-layout-padding-".$this->option('layout_padding') : "",
            $this->option('font_size')->get() ? "builder-font-".$this->option('font_size') : "",
            $this->option('force_full_width')->get() ? "builder-breakout" : "",
            $this->option('enable_parallax')->get() ? "builder-parallax" : "",
            $this->option('column_alignment')->get() ? "builder-valign-".$this->option('column_alignment') : "",
            $this->option('custom_classes')->get() ? $this->option('custom_classes') : "",
            $this->option('background_video')->get() ? "builder-animated-bg" : "",
            $this->option('equalize_component_heights')->get() ? "builder-js-equalize" : "",
            $this->option('disable_channel')->get() ? "builder-no-channel" : "",
            $this->option('alternate_mobile_background_style')->get() ? 'builder-alt-bg-mobile-style' : ''
        ], $this, $theme)) . " " . $extra;
    }

    /**
     * @param $name
     *
     * @return bool|Option
     */
    public function option($name) {
        if(!empty($this->options[$name])) {
            return new Option($this->options[$name]);
        }

        return new Option(false);
    }

    public function getType() {
        return $this->type ?: 'section';
    }
}