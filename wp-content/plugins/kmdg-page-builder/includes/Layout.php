<?php

namespace KMDG\PageBuilder;


class Layout
{
    protected $name;
    protected $options;
    protected $section;
    protected $columnFactory;
    protected $componentFactory;

    /**
     * Layout constructor.
     *
     * @param $name
     * @param Section $section
     * @param ColumnFactory $clF
     * @param ComponentFactory $comF
     */
    public function __construct($name, Section $section, ColumnFactory $clF, ComponentFactory $comF)
    {
        $this->name = $name;
        $this->section = $section;
        $this->columnFactory = $clF;
        $this->componentFactory = $comF;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $extra
     *
     * @return string
     */
    public function classes($extra = '')
    {
        return implode(' ', [
            "builder-layout",
            "builder-layout-".str_replace('_', '-', $this->name),
        ]) . " " . $extra;
    }

    /**
     * @param $type
     * @param $name
     *
     * @throws BuilderException
     */
    public function column($type, $name)
    {
        $this->columnFactory->make($type, $name, $this->section, $this, $this->componentFactory);
    }
}