<?php

namespace KMDG\PageBuilder;

use KMDG\PageBuilder\Settings;

class Component
{
    /** @var Section */
    protected $section;

    /** @var Layout */
    protected $layout;

    /** @var Column */
    protected $column;

    /** @var  string */
    protected $name;

    /** @var string */
    protected $path;

    /** @var int */
    protected $rowIndex;

    /** @var object */
    protected $animation;

    /**
     * Component constructor.
     *
     * @param Section $section
     * @param Layout $layout
     * @param Column $column
     * @param string $name
     * @param int $index
     */
    public function __construct($name, $index, Section $section, Layout $layout, Column $column)
    {
        $this->section = $section;
        $this->layout = $layout;
        $this->name = $name;
        $this->path = Builder::locate_component($name);
        $this->rowIndex = $index;
        $this->animation =  new Settings($this->field('animation', [], true));
        $this->render();
    }

    /**
     * Renders the component layout with restricted variable access
     */
    protected function render() {
        $section = $this->section;
        $layout = $this->layout;
        $column = $this->column;
        $component = $this;

        call_user_func(function() use ($section, $layout, $column, $component) {
            include($component->path);
        });
    }

    /**
     * @param string $name
     * @param mixed $default
     * @param bool $is_array
     * @return bool|mixed|null|void
     */
    public function field($name, $default = false, $is_array = false) {
        $data = get_sub_field($name);

        if($data) {
            if(!$is_array && is_array($data) && count($data) == 1) {
                return $data[0];
            }

            return $data;
        }

        return $default;
    }

    /**
     * @return int
     */
    public function index() {
        return $this->rowIndex;
    }

    /**
     * @param string[] $extra
     *
     * @return string
     */
    public function classes(array $extra = [])
    {
        return implode(' ', array_merge([
            "builder-component",
            "builder-component-".str_replace('_', '-', $this->name),
            "builder-component-index-".$this->index(),
            ($this->animation->enable_animations) ? "builder-component-animated" : '',
            ($this->animation->animation_visibility == "hidden") ? 'builder-animation-hidden' : '',
            ($this->animation->animation_mobile != "none") ? 'builder-mobile-animation' : ''
        ], $extra));
    }

    public function animations() {
        $html = '';

        if($this->animation->enable_animations) {
            $html .= implode(' ', ['data-animation="'.$this->animation->animation_type.'"',
                      'data-animation-delay="'.$this->animation->animation_delay.'"',
                      'data-animation-loop="'.$this->animation->animation_loop.'"',
                      'data-animation-visibility="'.$this->animation->animation_visibility.'"',
                      'data-animation-mobile="'.$this->animation->animation_mobile.'"']);
        }

        return $html;
    }

    public function __toString()
    {
        return $this->name;
    }
}