<?php
/**
 * Created by PhpStorm.
 * User: KMDG
 * Date: 2/23/2017
 * Time: 2:04 PM
 */

namespace KMDG\PageBuilder;


class Option
{
    protected $value = false;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function next() {
        $vals = $this->value;

        if(!is_array($this->value)) {
            $vals = [$this->value];
        }

        foreach($vals as $value) {
            yield $value;
        }
    }

    public function get($default = false) {
        return $this->value ?: $default;
    }

    /**
     * Retrieves a select field value correctly if the page hasn't yet been saved since the field was added.
     *
     * This can be used on any select field where only a single result is expected.
     *
     * @param bool $default
     *
     * @return bool|mixed
     */
    public function getFixedSelectField($default = false) {
        $val = $this->get($default);

        if(is_array($val)) {
            return $val[0];
        }

        return $val;
    }

    public function exists() {
        return !empty($this->value);
    }

    public function __toString()
    {
        return (string) $this->get();
    }
}