<?php
/**
 * Created by PhpStorm.
 * User: KMDG
 * Date: 4/25/2017
 * Time: 12:29 PM
 */

namespace KMDG\PageBuilder;


class Column
{
    const DEFAULT_COMPONENT_CLASS = __NAMESPACE__.'\\Component';

    /** @var Section */
    protected $section;

    /** @var Layout */
    protected $layout;

    /** @var  string */
    protected $name;

    /** @var  string */
    protected $type;

    /** @var string */
    protected $path;

    /** @var ComponentFactory */
    protected $componentFactory;

    /**
     * Column constructor.
     *
     * @param string $type
     * @param string $name
     * @param Section $section
     * @param Layout $layout
     * @param ComponentFactory $cf
     */
    public function __construct($type, $name, Section $section, Layout $layout, ComponentFactory $cf)
    {
        $this->section = $section;
        $this->layout = $layout;
        $this->type = $type;
        $this->name = $name;
        $this->path = Builder::locate_column($type);
        $this->componentFactory = $cf;
        $this->render();
    }

    /**
     * @param string $extra
     *
     * @return string
     */
    public function classes($extra = '')
    {
        return implode(' ', [
                "builder-column",
                "builder-column-type--".str_replace('_', '-', $this->type),
            ]) . " " . $extra;
    }

    /**
     * @param string $name
     * @param mixed $default
     * @param bool $is_array
     * @return bool|mixed|null|void
     */
    public function field($name, $default = false, $is_array = false) {
        $data = get_sub_field($name);

        if($data) {
            if(!$is_array && is_array($data) && count($data) == 1) {
                return $data[0];
            }

            return $data;
        }

        return $default;
    }

    /**
     * @param string $name
     * @param int $index
     *
     * @throws BuilderException
     */
    protected function component($name, $index)
    {
        $this->componentFactory->make($name, $index, $this->section, $this->layout, $this);
    }

    /**
     * @throws BuilderException
     */
    public function loop() {

        if(have_rows('components')) {
            while (have_rows('components')) {
                the_row();

                $type = get_row_layout();

                if(!$type) {
                    throw new BuilderException("Invalid column, components is not a valid flexible content field");
                }

                $this->component($type, get_row_index());
            }
        }
    }

    /**
     * Renders the component layout with restricted variable access
     */
    protected function render() {
        $section = $this->section;
        $layout = $this->layout;
        $column = $this;

        if(have_rows($this->name)) {
            while(have_rows($this->name)) {
                the_row();

                call_user_func(function() use ($section, $layout, $column) {
                    include($column->path);
                });
            }
        }
    }

    public function __toString()
    {
        return $this->type;
    }
}