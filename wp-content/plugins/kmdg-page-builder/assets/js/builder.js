window.KMDGPB = {};
window.KMDGPB.jQuery = jQuery.noConflict();

(function (jQuery) {
    require("./vendor/materialize/parallax");
    require("./vendor/waypoints/waypoint");
    require("./vendor/waypoints/group");
    require("./vendor/waypoints/context");
    require("./vendor/waypoints/adapters/jquery");
    require("./vendor/waypoints/shortcuts/inview");
    require("./vendor/jquery.smoothwheel/jquery.smoothwheel");
    require("./vendor/jquery.cycle2/jquery.cycle2");
    require("./vendor/jquery.cycle2/jquery.cycle2.tile");
    require("./vendor/jquery.cycle2/jquery.cycle2.swipe");
    require("./vendor/jquery.cycle2/jquery.cycle2.carousel");
    require("./vendor/jquery.cycle2/jquery.cycle2.scrollVert");
    require("./vendor/jquery.cycle2/jquery.cycle2.fadeInUp");

// Components
    require("./components/image");
    require("./components/accordion");
    require("./components/video");
})(KMDGPB.jQuery);


function PBuilder() {
    console.info('Builder Loaded');
    this.mobileSize = 900;
}

PBuilder.prototype.detectIE = function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
};

PBuilder.prototype.equalizeHeights = function(group, parent) {
    parent = parent || "body";
    jQuery(parent).each(function() {
        var tallest = 0;
        jQuery(this).find(group).css('min-height', 0);

        jQuery(this).find(group).each(function() {
            var $this = jQuery(this);
            var thisHeight = $this.height();

            if($this.css('box-sizing') === 'content-box') {
                if(thisHeight > tallest) {
                    tallest = thisHeight;
                }
            } else {
                var thisPadding = Number($this.css('padding-top').replace('px', ''))
                    + Number($this.css('padding-bottom').replace('px', ''));

                if((thisHeight + thisPadding) > tallest) {
                    tallest = thisHeight + thisPadding;
                }
            }
        });
        jQuery(this).find(group).css('min-height',tallest);
    });
};

PBuilder.prototype.animationTriggerEnter = function(el) {
    var mobileSize = this.mobileSize;
    var $components = $(el).find('.builder-component');

    $components.each(function(i, el) {
        $(el).trigger("builderAnimationStart", [el]);
    });

    $(el).find('.builder-component-animated').each(function(i, component) {
        var type = $(component).attr("data-animation");
        var delay = ($(component).attr("data-animation-delay") || 0) + 's';
        var loop = $(component).attr("data-animation-loop");
        var visibility = $(component).attr("data-animation-visibility");
        var mobile = $(component).attr("data-animation-mobile") || 'normal';

        if($(window).width() <= mobileSize) {
            if(mobile == 'normal') {
                $(component).css('animation-delay', delay);
            } else if(mobile == 'none') {
                $(component).removeClass('builder-animation-hidden');
                return;
            } else {
                $(component).css('animation-delay', "0s");
            }
        } else {
            $(component).css('animation-delay', delay);
        }

        if(visibility == 'hide') {
            $(component).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                $(component).addClass('builder-animation-hidden');
            });
        }

        if(loop == "infinite") {
            $(component).addClass("infinite");
        }

        $(component)
            .addClass(type)
            .addClass('animated')
            .removeClass('builder-animation-hidden');
    });
};

PBuilder.prototype.animationPause = function(el) {
    var mobileSize = this.mobileSize;

    $(el).find('.builder-component-animated').each(function(i, component) {
        var mobile = $(component).attr("data-animation-mobile") || 'normal';

        if(mobile == 'none' && $(window).width() <= mobileSize) return;

        if($(component).hasClass('animated')) {
            $(component).addClass('disable-animation');
            $(component).removeClass('animated');
        }
    });
};

PBuilder.prototype.animationUnpause = function(el) {
    var mobileSize = this.mobileSize;

    $(el).find('.builder-component-animated').each(function(i, component) {
        var mobile = $(component).attr("data-animation-mobile") || 'normal';

        if(mobile == 'none' && $(window).width() <= mobileSize) return;

        if($(component).hasClass('disable-animation')) {
            $(component).addClass('animated');
            $(component).removeClass('disable-animation');
        }
    });
};

PBuilder.prototype.animationTriggerExit = function(el) {
    var mobileSize = this.mobileSize;
    var $components = $(el).find('.builder-component');

    $components.each(function(i, el) {
        $(el).trigger("builderAnimationEnd", [el]);
    });

    $(el).find('.builder-component-animated').each(function(i, component) {
        var type = $(component).attr("data-animation");
        var loop = $(component).attr("data-animation-loop");
        var visibility = $(component).attr("data-animation-visibility");
        var mobile = $(component).attr("data-animation-mobile") || 'normal';

        if(mobile == 'none' && $(window).width() <= mobileSize) return;

        if (loop != "infinite") {
            $(component).removeClass('animated');

            if (loop != "once") {
                $(component).removeClass(type)

                if(visibility == 'hidden') {
                    $(component).addClass('builder-animation-hidden');
                } else if(visibility == 'hide') {
                    $(component).removeClass('builder-animation-hidden');
                }
            }
        }
    });
};

PBuilder.prototype.breakout = function(element) {
    var $element = $(element);

    if($element.length > 0) {
        var windowW = document.documentElement.clientWidth;

        $element.css({"margin-left": 0, "display": "block", "visibility": "hidden"});

        var pos = $element.offset();
        $element.css({"width": windowW, "margin-left": -pos.left});

        $element.find(".builder-container").css("max-width", windowW - (pos.left * 2));
        $element.css('visibility', 'visible');
    }
};

PBuilder.prototype.loadBG = function($el) {
    if($el.hasClass('loaded') || $el.hasClass('loading') || !$el.attr('data-srcs')) return;

    $el.addClass('loading');

    var $parent = $el.parent();
    var sizes = JSON.parse($el.attr('data-srcs'));
    var bestFit = {size: null, width: 999999999};

    for(var i in sizes) {
        if(sizes[i].width == -1) {
            sizes[i].width = $parent.width() * 100;
        }

        if(sizes[i].width >= $parent.width() && sizes[i].width < bestFit.width) {
            bestFit = {
                size: i,
                width: sizes[i].width
            };
        }
    }

    var preload = new Image();
    preload.src = sizes[bestFit['size']].src;

    preload.addEventListener('load', function() {
        $el.css('background-image', 'url(' + preload.src + ')').addClass('loaded').removeClass('loading');
    });
};

var builder = new PBuilder();
window.KMDGPB.builder = builder;

(function ($) {

    $('.builder-parallax .builder-parallax-container').parallax();

// Hijack scrolling on IE/Edge because it's terrible
    if(builder.detectIE() !== false) {
        $(window).on('load', function() {
            $(window).smoothWheel();
        });
    }

// Force selected sections to go full width despite container size
    $(window).on('load ready resize', function() {
        builder.breakout('.builder-section.builder-breakout');
        builder.equalizeHeights('.builder-block-slide .builder-section', '.builder-block-slider');
    });

// Trigger a scroll event on page load to make sure parallax is updated
    $(window).scroll();
    $(window).on('load', function() {
        $(window).scroll();
    });

// Equalize heights of components in the same "row" (option)
    $(window).on('load', function() {
        $('.builder-section.builder-js-equalize .builder-layout').each(function(i, layout) {
            $(layout).find('.builder-column').each(function(i, column) {
                $(column).find('.builder-component').each(function(i, component) {
                    builder.equalizeHeights(".builder-component-index-" + (i + 1), $(layout));
                });
            });
        });
    });

    $(window).on('load', function() {

        $('.builder-section, .builder-block-slider').not('.builder-section-type--block-slide').each(function(i, section) {
            var $section = $(section);

            $section.data("animSettings", new Waypoint.Inview({
                element: $section,
                enter: function(dir) {
                    $section.trigger('builderSectionOnScreen', [$section]);
                },
                exited: function(dir) {
                    $section.trigger('builderSectionOffScreen', [$section]);
                }
            }));

            $section.find('.builder-column').each(function(i, column) {
                var $column = $(column);

                // Create animation trigger element
                $column.prepend('<div class="builder-js-animation-trigger"></div>');
                var animTrigger = $column.children('.builder-js-animation-trigger');

                // Scroll Watcher for animations and videos, Waypoints stored in DOM data for each column
                $column.data("animSettings", new Waypoint.Inview({
                    element: animTrigger,
                    entered: function(dir) {
                        builder.animationTriggerEnter($column);
                    },
                    exited: function(dir) {
                        builder.animationTriggerExit($column);
                    }
                }));
            });
        });
    });

    $('.builder-block-slider').cycle({
        slides: '> .builder-block-slide',
        swipe: true,
        swipeFx: 'scrollHorz',
        autoHeight: 'calc',
        log: false
    }).on('cycle-after', function(event, optionHash, outgoingSlideEl, incomingSlideEl) {

        // Previous slide tracking (used for some bugfixes)
        $(outgoingSlideEl).parent().find('.builder-cycle--previous').removeClass('builder-cycle--previous');
        $(outgoingSlideEl).addClass('builder-cycle--previous');

        // Trigger animations on incomming slides
        $(incomingSlideEl).trigger('builderSectionOnScreen', [incomingSlideEl]);
        builder.animationTriggerEnter(incomingSlideEl);

    }).on('cycle-before', function(event, optionHash, outgoingSlideEl, incomingSlideEl) {
        var speed = $(this).data('cycle.opts').speed;

        // Pause animations while transitioning out
        builder.animationPause(outgoingSlideEl);

        setTimeout(function() {
            $(outgoingSlideEl).trigger('builderSectionOffScreen', [outgoingSlideEl]);

            // Reset animation states
            builder.animationUnpause(outgoingSlideEl);
            builder.animationTriggerExit(outgoingSlideEl);
        }, speed);
    });

    $('.builder-bg-image-slideshow').cycle({
        slides: '> .builder-img-bg',
        autoHeight: false,
        swipe: false,
        pauseOnHover: false,
        timeout: 5000
    });

    builder.loadBG($('.builder-img-bg:first'));

    $('.builder-section, .builder-slider--has-background').on('builderSectionOnScreen', function(ev, section) {
        $(section).find('.builder-img-bg, .builder-lazy-img').each(function(i, el) {
            builder.loadBG($(el));
        });
    });

})(KMDGPB.jQuery);
