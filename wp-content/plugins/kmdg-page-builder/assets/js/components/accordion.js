// Toggle accordion
(function($) {
    $('.builder-layout-accordion .builder-column-header').on('click', function() {
        var $column = $(this).parent();
        $column.toggleClass('open');
        $column.find('.builder-column-body').slideToggle({
            duration: 250,
            progress: function() {
                $(window).trigger('resize');
            }
        });
    });
})(jQuery)
