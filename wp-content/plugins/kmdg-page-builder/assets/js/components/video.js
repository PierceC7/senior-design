//
(function($) {

    //Events thrown by builder.js in waypoint code
    $('.builder-component-video.js-autoplay').on('builderAnimationStart', function(e) {
        var video = $(this).find('video')[0];

        if($(this).data("played") !== true || $(video).attr('loop')) {
            $(this).data("played", true);
            video.play();
        }
    });

    $('.builder-component-video').on('builderAnimationEnd', function(e) {
        var video = $(this).find('video')[0];

        if($(video).attr('loop')) {
            video.pause();
        }
    });

})(jQuery)
