(function($) {
    console.log("Builder Admin Loaded");
    var activated = null;


    $('body').on('mouseenter', '.dropdown-toolbars [data-type="wysiwyg"]', function(e) {
        if(activated && !$(this).hasClass('activated')) {
            $(activated).removeClass('activated');
        }

        activated = this;

        $(this).addClass('activated');
    });

    $('body').on('change', '.js-builder-theme-select', function(e) {
        var $selected = $(this).find('option:selected');

        $(this).siblings('.js-theme-descriptions').find('.js-theme-desc').css('display', 'none');
        $(this).siblings('.js-theme-descriptions').find('.'+$selected.val()).css('display', 'block');
    });

})(jQuery);