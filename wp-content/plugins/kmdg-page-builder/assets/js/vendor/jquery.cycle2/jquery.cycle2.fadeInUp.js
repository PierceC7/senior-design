/*! Custom reversed scrollVert transition plugin for Cycle2; */
(function($) {
    "use strict";

    $.fn.cycle.transitions.fadeInUp = {
        postInit: function( opts ) {
            var transitionSpeed = (opts.cycleSpeed / 1000) * 2;
            opts.cssAfter = { opacity: 0 };
            opts.cssBefore = { opacity: 1, display: 'block', visibility: 'visible' , transition: 'opacity '+transitionSpeed+'s', 'transition-timing-function': 'ease-in-out'};
        },

        before: function( opts, curr, next, fwd ) {
            opts.API.stackSlides( opts, curr, next, fwd );
            var height = opts.container.css('overflow','hidden').height();
            var transitionSpeed = (opts.cycleSpeed / 1000) * 2;

            opts.cssAfter = { opacity: 0 };
            opts.cssBefore = { top: fwd ? height : -height, left: 0, opacity: 1, display: 'block', visibility: 'visible' , transition: 'opacity '+transitionSpeed+'s', 'transition-timing-function': 'ease-in-out'};
            opts.animIn = { top: 0 };
            opts.animOut = { top: fwd ? -height : height };
        },
    };

})(jQuery);