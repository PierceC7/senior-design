var Mustache = require('mustache');

// Resource Search
kmdgResourceCenter = kmdgResourceCenter || {}; // Global object set with wp_localize_script

(function ($) {
    var RC = kmdgResourceCenter;

    RC.fn.initPlayerAPI = RC.fn.initPlayerAPI || function(el) {
        if($(el).hasClass('resource--video-type__vimeo')) {
            var iframe = $(el).find('iframe');

            if(iframe && iframe[0]) {
                $(el).data('vimeo', new Vimeo.Player(iframe[0]));
                $('.resource--video-jump-list__wrapper').addClass('loaded');
            }
        }
    };

    RC.fn.getSelectedFilters = RC.fn.getSelectedFilters || function() {
        var filters = [];

        RC.$filters.find('select').each(function() {
            var name = $(this).attr('name');

            if($(this).val()) {
                filters[name] = filters[name] || [];
                filters[name].push($(this).val());
            }
        });

        for(var i in filters) {
            if(filters.hasOwnProperty(i)) {
                filters[i] = filters[i].join();
            }
        }

        return filters;
    };

    RC.fn.getPosts = RC.fn.getPosts || function () {
        if(RC.widgetMode) return;

        var options = {
            '_embed': '',
            'page': RC.currentPage,
            'per_page': RC.postsPerPage,
            'orderby': RC.order.mode,
            'order': RC.order.dir,
            'meta_key': RC.order.key,
            'meta_type': RC.order.type
        };

        if(RC.location) {
            options['resource-location'] = RC.location;
        }

        if(RC.$searchBar.val().length >= 3) {
            options.search = RC.$searchBar.val();
        }

        options = $.extend({}, options, RC.fn.getSelectedFilters());

        RC.currentRequest = $.ajax({
            'url': RC.restURL,
            'method': 'GET',
            'data': options,
            'beforeSend': function() {
                RC.$loading.show();

                if(RC.currentRequest != null) {
                    RC.currentRequest.abort();
                }
            }
        }).always(function() {

            RC.currentRequest = null;
            RC.$loading.hide();

        }).fail(function(response) {

            if(response.responseJSON && response.responseJSON.code) {
                RC.$searchResults.html([
                    '<div class="error">',
                    '<h4>There was an error processing your reqest:</h4>',
                    '<p>' + response.responseJSON.message + '</p>',
                    '<p><a href="#/">Click here</a> to reset all filters and try again.</p>',
                    '</div>'
                ].join('\n'));
            }

        }).done(RC.fn.renderPosts);
    };

    RC.fn.setHashData = RC.fn.setHashData || function(key, value, noprefix) {
        var source = window.location;
        var rawhash = '#search';

        if(RC.widgetMode) source = RC.currentLink;

        var hashData = RC.fn.getHashData(source);
        hashData[key] = value;

        if(noprefix && window.location.hash.indexOf(rawhash) === -1) {
            rawhash = "#";
        }

        if(value == undefined || value == null || value == '') {
            delete hashData[key];
        }

        if(!$.isEmptyObject(hashData)) {
            //TODO: Refactor this, it can probably be done with a single loop
            // Order hash properties so that they appear in the URL in the order we want (visual only)
            var properties = [];
            for(var i in hashData) {
                if(hashData.hasOwnProperty(i)) {
                    if(i != 'page') { // everything but page
                        properties.push(i);
                    }
                }
            }

            // put page on the end
            if(hashData['page']) {
                properties.push('page');
            }

            // Create query string
            for(var i in properties) {
                if(properties.hasOwnProperty(i)) {
                    rawhash += '/' + properties[i] + '/' + hashData[properties[i]];
                }
            }
        } else {
            rawhash += '/';
        }

        if(!RC.widgetMode) {
            window.location.hash = rawhash;
            $(RC.currentLink).attr('href', window.location.href);
        } else {
            $(RC.currentLink).attr('href', RC.$searchForm.attr('data-action')+rawhash)
        }

        RC.$searchForm.attr('action', RC.currentLink);
    };

    RC.fn.getHashData = RC.fn.getHashData || function (source) {
        source = source || window.location;
        var rawHash = source.hash.split('/');
        var hashData = {};

        // Loop through the hash creating key/value pairs
        for(var i = 1; i < rawHash.length; i += 2) {
            if(rawHash[i] && rawHash[i+1]) {
                hashData[encodeURIComponent(rawHash[i])] = encodeURIComponent(rawHash[i+1]);
            }
        }

        return hashData;
    };

    RC.fn.getPageNumFromHash = RC.fn.getPageNumFromHash || function () {
        return Number(RC.fn.getHashData().page) || 1;
    };

    RC.fn.setFiltersFromHash = RC.fn.setFiltersFromHash || function () {
        var hashData = RC.fn.getHashData();

        for(var i in hashData) {
            if(hashData.hasOwnProperty(i)) {
                var $select = RC.$filters.find('select[data-friendly='+i+']');
                if($select.length > 0) {
                    var $option = $select.find('option[data-friendly='+hashData[i]+']');
                    if($option.length > 0) {
                        $option.attr('selected', 'selected');
                    }
                }
            }
        }
    };

    RC.fn.formatDate = RC.fn.formatDate || function (date) {
        var year = date.getFullYear();
        var month = date.getMonth()+1;
        var day = date.getDate();

        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }

        return month + '/' + day + '/' + year
    };

    // Returns a function, that, as long as it continues to be invoked, will not
    // be triggered. The function will be called after it stops being called for
    // N milliseconds. If `immediate` is true, trigger the function on the
    // leading edge, instead of the trailing.
    RC.fn.debounce = RC.fn.debounce || function(wait, immediate, func) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };

    RC.fn.renderPosts = RC.fn.renderPosts || function (posts, status, request) {
        var output = '';
        var maxPages = Number(request.getResponseHeader('X-WP-TotalPages'));

        RC.$loading.hide();

        RC.fn.createPagination(RC.currentPage, maxPages);

        if(posts && posts.length > 0) {
            for(var i in posts) {
                if(posts.hasOwnProperty(i)) {
                    output += RC.fn.renderSinglePost(posts[i])
                }
            }

            RC.$searchResults.html(output);

            $(window).trigger('kmdg_rc_render_posts', {'success': true});
        } else {
            RC.$searchResults.html('<div class="resource--search-error">No Resources found. Try refining your search filters.</div>');

            $(window).trigger('kmdg_rc_render_posts', {'success': false});
        }
    };

    RC.fn.renderSinglePost = RC.fn.renderSinglePost || function (post) {
        var imageSrc = null;
        var resourceLink = post.link;
        var resourceDate = post.date;

        // Pull out embed data (featured image, terms)
        if(post['image']) {
            if(post['image'].thumb) {
                imageSrc = post['image'].thumb;
            } else if(post['image'].full) {
                imageSrc = post['image'].full;
            } else {
                imageSrc = post['image'];
            }
        }

        if(post.banner && post.banner.thumb) {
            imageSrc = post.banner.thumb;
        }

        var templateData = {
            'imageSrc': imageSrc,
            'resourceLink': resourceLink,
            'resourceDate': resourceDate,
            'resourceType': post.resourceType.parent.name,
            'excerpt': post.excerpt.rendered || post.excerpt,
            'resourceTitle': post.title.rendered || post.title,
            'downloadType' : post.downloadType,
            'resourceDownloadText': post.buttonText
        };

        return Mustache.render(RC.resourceTemplate, $.extend({}, templateData, post['_customMustacheTags']));
    }

    RC.fn.createPagination = RC.fn.createPagination || function (current, max) {
        var show = false;
        var showPrev = true;
        var showNext = true;
        var visibleRange = 2;
        var startPages = current - visibleRange;
        var endPages = current + visibleRange;
        var prevPage = current - 1;
        var nextPage = current + 1;
        var pages = [];

        if(max > 1) {
            show = true;
        }

        if(startPages < 1) {
            startPages = 1;
        }

        if(endPages > max) {
            endPages = max;
        }

        if(prevPage < 1) {
            prevPage = 1;
        }

        if(nextPage > max) {
            nextPage = max;
        }

        if(current == 1) {
            showPrev = false;
        }

        if(current == max) {
            showNext = false;
        }

        for(var i = startPages; i <= endPages; i++) {
            var activeClass = '';

            if(current == i) {
                activeClass = 'current';
            }

            pages.push({
                'page': i,
                'class': activeClass,
            });
        }

        RC.$pagination.html(Mustache.render(RC.paginationTemplate, {
            'show': show,
            'showPrev': showPrev,
            'showNext': showNext,
            'linkBase': RC.paginationLinkBase,
            'firstPage': '1',
            'prevPage': prevPage,
            'pages': pages,
            'nextPage': nextPage,
            'lastPage': max
        }));
    };

    $(document).ready(function() {
        RC.$app = $('.resource--search');
        RC.$searchResults = RC.$app.find('.resource--search-results');
        RC.$searchForm = RC.$app.find('.resource--search-form');
        RC.$searchBar = RC.$app.find('.resource--search-bar').find('[type="search"]');
        RC.$filters = RC.$app.find('.resource--search-filters');
        RC.$loading = RC.$filters.find('.resource--search-loading');
        RC.$pagination = RC.$app.find('.resource--search-pagination');

        RC.widgetMode = RC.$app.is('.resource--widget');
        RC.currentLink = document.createElement('a');
        
        RC.paginationLinkBase = '#/page/';

        RC.resourceTemplate = $(".resource--post-template").html();
        RC.paginationTemplate = $(".resource--pagination-template").html();
        RC.currentPage = RC.fn.getPageNumFromHash();
        RC.currentRequest = null;

        if(RC.$app.length > 0) {

            RC.fn.setHashData('page', RC.currentPage, true);

            RC.$loading.hide().html('<img src="'+RC.loadingGraphic+'" alt="spinner">');

            RC.$searchResults.html([
                '<div class="resource--loading">',
                    '<img src="'+RC.loadingGraphic+'" alt="spinner">',
                    '<span>'+RC.loadingText+'</span>',
                '</div>'
            ].join('\n'));

            Mustache.parse(RC.resourceTemplate);
            Mustache.parse(RC.paginationTemplate);

            RC.fn.setFiltersFromHash();

            RC.fn.getPosts();
        }

        RC.$searchBar.on('keyup', RC.fn.debounce(500, false, function() {
            if($(this).val().length >= 3 || $(this).val().length == 0) {
                RC.fn.setHashData('page', 1);
                RC.fn.getPosts();
            }
        }));

        RC.$filters.find('select').each(function() {
            $(this).on('change', function() {
                RC.fn.setHashData('page', 1);
                RC.fn.setHashData($(this).attr('data-friendly'), $(this).find(':selected').attr('data-friendly'));
                RC.fn.getPosts();
            });
        });

        RC.$searchForm.on('submit', function(e) {

            if(!RC.widgetMode) {
                e.preventDefault();

                //window.location = RC.$searchForm.attr('action');
            }
        });

        $(window).on('hashchange', function(e) {

            if(RC.$searchResults.length > 0) {
                RC.currentPage = RC.fn.getPageNumFromHash();
                RC.fn.setFiltersFromHash();
                RC.fn.getPosts();
                e.preventDefault();
            }

        });

        RC.$pagination.on('click', 'a', function(e) {
            e.preventDefault();

            var page = RC.fn.getHashData(this).page;
            RC.fn.setHashData('page', page);
        });

        $(window).trigger('kmdg_rc_ready', {'RC': RC, 'Mustache': Mustache, 'jQuery': $});
    });

})(jQuery);

// Resource Single
(function($) {

    var $videoPlayer = $('.resource--video-player');

    $(document).on('ready', function() {
        $videoPlayer.each(function(i, el) {
            kmdgResourceCenter.fn.initPlayerAPI(el);
        });
    });

    $(window).on('kmdg_rc_form_submitted', function (event, email, form, callback) {
        console.log(email, form);
        var RC = kmdgResourceCenter;

        if(RC.videoURL && RC.form == form) {
            // When the resource form is submitted and play on page is selected, load the video player iframe
            $videoPlayer.find('.resource--video-wrapper').html('<iframe src="'+ RC.videoURL + '?autoplay=1">');
            $videoPlayer.addClass('loaded');

            $videoPlayer.each(function(i, el) {
                RC.fn.initPlayerAPI(el);
            });

            $.ajax({
                url: RC.ajaxURL,
                method: "GET",
                cache: false,
                data: {
                    action: "kmdg_rc_ajax_video_play",
                    email: email,
                    post_id: RC.resource
                }
            }).done(callback);
        } else {
            $.ajax({
                url: RC.ajaxURL,
                method: "GET",
                cache: false,
                data: {
                    action: "kmdg_rc_ajax_email_file",
                    email: email,
                    post_id: RC.resource
                }
            }).done(callback);
        }
    });


    $('.resource--video-jump-list__item').on('click', function(e) {
        var $player = $('.resource--video-player');

        if($player.data('vimeo')) {
            $player.data('vimeo').setCurrentTime($(this).attr('data-time'));
        }
    });

})(jQuery);
