<?php

$basePath = plugin_dir_path(__FILE__);

return [
    'path'              => $basePath,
    'path/templates'    => $basePath.'resource-templates/',

    // Dependencies
    'dependencies'              => "dependencies.config.php",
    'dependencies/autowiring'   => true,

    'routes'  => "routes.config.php",

];