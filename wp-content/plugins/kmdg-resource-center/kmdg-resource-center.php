<?php
/*
Plugin Name: KMDG Resource Center
Plugin URI: http://KMDG.com
Description: Creates the resource custom post type and provides an API for accessing and manipulating them.
Author: Jake Finley
Version: 2.1.0
Author URI: http://kmdg.com
*/

namespace KMDG\ResourceCenter;

use Aesir\v1\Aesir;
use Aesir\v1\Components\Request;
use Aesir\v1\Components\Router;
use Aesir\v1\Internal\Config;

const SLUG          = 'resource-post';
const AJAXSLUG      = 'resource-download';

spl_autoload_register(function ($class) {
    $prefix = "KMDG\\ResourceCenter\\";

    $base_dir = __DIR__ . "/classes";

    $len = strlen($prefix);

    if (strncmp($prefix, $class, $len) !== 0) {
        return;
    }

    $relative_class = substr($class, $len);

    $class_file = $base_dir . '/' . str_replace('\\', '/', $relative_class) . '.php';

    if (file_exists($class_file)) {
        require $class_file;
    }
});

include('aesir/bootstrap.php');

$aesir = new Aesir(
    new Config(plugin_dir_path(__FILE__).'aesir.config.php'),
    new Router()
);

$rc = ResourceCenter::getInstance();

$rc->load(
    __FILE__,
    $aesir,
    SLUG,
    AJAXSLUG,
    plugin_dir_url(__FILE__),
    plugin_dir_path(__FILE__),
    'dist',
    new ResourceModel(SLUG)
);

$rc->getModel()->registerPostType(
    new ResourcePostType(SLUG)
);

include('routes.php');

$aesir->router()->listen(
    Request::getInstance()
);

include('helpers.php');