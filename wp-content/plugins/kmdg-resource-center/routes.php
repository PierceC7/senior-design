<?php
/** @var $aesir Aesir\v1\Aesir */
/** @var $rc \KMDG\ResourceCenter\ResourceCenter */

use KMDG\ResourceCenter\ResourceCenter;
use KMDG\ResourceCenter\ResourceController;
use KMDG\ResourceCenter\ResourceLocationTax;

$router = $aesir->router();
$rc = ResourceCenter::getInstance();

foreach($rc->getModel()->getPostTypeSlugs() as $slug) {
    $router->any($slug, 'single', '*', [ResourceController::class, 'single']);
}

$router->any('tax', ResourceLocationTax::slug(), '*', [ResourceController::class, 'archive']);
$router->any('resource-post', 'archive', '*', [ResourceController::class, 'error404']);

$router->ajax('GET', $rc->getAjaxDownloadSlug(), [ResourceController::class, 'fileDownload']);
$router->ajax('GET','kmdg_rc_ajax_email_file', [ResourceController::class, 'emailFile']);
$router->ajax('GET','kmdg_rc_ajax_video_play', [ResourceController::class, 'playVideo']);

// Search Widget Shortcode
$router->shortcode('kmdg-rc-search', [ResourceController::class, 'shortcode']);
$router->shortcode('kmdg-rc-recent-posts', [ResourceController::class, 'shortcodeRecentResources']);