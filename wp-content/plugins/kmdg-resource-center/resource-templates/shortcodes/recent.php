<?php
/** @var \KMDG\ResourceCenter\ResourceModel $model */
/** @var string $json */
/** @var bool $foundPosts */
?>

<?php if($foundPosts): ?>
    <div class="kmdg-resource-center">
        <div class="resource--recent-posts resource--widget">
            <?= $tilePart ?>
        </div>

        <script type="text/javascript">
            (function() {
                const me = document.currentScript;

                jQuery(window).on('kmdg_rc_ready', function(e, p) {
                    const posts = <?= $json ?>;

                    const $app = p.jQuery(me).siblings('.resource--recent-posts');
                    const template = $app.find('.resource--post-template').html();

                    let output = '';

                    p.Mustache.parse(template);

                    for(let i in posts) {
                        if(posts.hasOwnProperty(i)) {
                            output += p.RC.fn.renderSinglePost(posts[i]);
                        }
                    }

                    $app.html(output);
                });
            })();
        </script>
    </div>
<?php endif; ?>
