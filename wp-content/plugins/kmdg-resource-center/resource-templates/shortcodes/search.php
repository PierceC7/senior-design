<?php
/** @var \KMDG\ResourceCenter\ResourceModel $model */
/** @var \Aesir\v1\Components\PostType $postType */
?>
<div class="kmdg-resource-center">
    <div class="resource--search resource--widget" data-scroll="search">
        <div class="resource--search-filters">
            <form method="POST" data-action="<?= $formAction ?>" class="resource--search-form">
                <div class="resource--search-bar">
                    <label>
                        Search: <input type="search" name="resource-search">
                    </label>
                    <button class="resource--search-button" type="submit">Search</button>
                </div>

                <div class="resource--filters">
                    <p class="resource--filter-text">Filter by:</p>

                    <?php foreach ($filterData as $tax => $terms): ?>
                        <?= $filterPart([
                            'terms' => $terms,
                            'tax' => get_taxonomy($tax),
                            'friendly' => $postType->getTaxonomy($tax)->friendly()
                        ]); ?>
                    <?php endforeach; ?>

                </div>
            </form>
        </div>
    </div>
</div>
