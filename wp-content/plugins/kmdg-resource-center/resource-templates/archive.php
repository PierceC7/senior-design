<?php
/** @var \KMDG\ResourceCenter\ResourceModel $model */
?>
<?php get_header(); ?>

<div class="kmdg-resource-center">
    <div class="resource--channel">
        <?= $searchPart ?>
    </div>
</div>

<?php get_footer(); ?>
