<?php get_header() ?>

    <div class="resource--access-denied">
        <div class="resource--wrapper">
            <h2>Access Denied</h2>
            <p>Sorry, you do not have permission to access this file.</p>
        </div>
    </div>

<?php get_footer() ?>
