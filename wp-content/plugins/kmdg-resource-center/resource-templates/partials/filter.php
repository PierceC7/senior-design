<?php if($terms) : ?>
    <div class="resource--fliter resource--filter__<?= esc_attr($tax->name) ?>">
        <select name="<?= esc_attr($tax->name) ?>" data-friendly="<?= esc_attr($friendly) ?>">
            <option value="">All <?= $tax->label ?></option>

            <?php foreach($terms as $term) : ?>
                <option value="<?= esc_attr($term->term_id) ?>" data-friendly="<?= sanitize_title($term->name) ?>">
                    <?= $term->name ?>
                </option>
            <?php endforeach; ?>
        </select>
    </div>
<?php endif; ?>