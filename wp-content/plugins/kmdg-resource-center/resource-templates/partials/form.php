<?php /** @var \KMDG\ResourceCenter\ResourceModel $model */ ?>
<?php if($hasForm): ?>
    <div class="resource--form" id="download">
        <?php do_action('KMDG/ResourceCenter/Single/Form', 'Action: KMDG/ResourceCenter/Single/Form', $form, $model); ?>
    </div>
<?php endif; ?>