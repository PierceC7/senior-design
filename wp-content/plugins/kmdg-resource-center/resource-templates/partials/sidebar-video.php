<?php /** @var \KMDG\ResourceCenter\ResourceModel $model */ ?>
<div class="resource--video-player resource--video-type__<?= $video->type ?>">
    <?php if($hasForm): ?>
        <div class="resource--video-wrapper">
            <div class="resource--video-placeholder" style="background-image: url(<?= $placeholder->full ?>);"></div>
            <a href="<?php the_permalink() ?><?= is_single() ? '#download' : '' ?>" class="resource--video-overlay"><i class="fa fa-play-circle-o"></i></a>
        </div>
    <?php else: ?>
        <div class="resource--video-wrapper">
            <iframe src="<?= $video->url ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>

        <?php if($model->hasJumpListRows()): ?>
            <div class="resource--video-jump-list-wrapper">
                <h3>Bookmarks</h3>
                <ol class="resource--video-jump-list">
                    <?php while($model->hasJumpListRows()): $data = $model->getJumpData() ?>
                        <li class="resource--video-jump-list-item" data-time="<?= $data->time ?>"><?= $data->text ?> <time>(<?= $data->formattedTime ?>)</time></li>
                    <?php endwhile; ?>
                </ol>
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div>