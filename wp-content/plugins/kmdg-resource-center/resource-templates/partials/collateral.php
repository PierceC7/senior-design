<?php /** @var \KMDG\ResourceCenter\ResourceModel $model */ ?>
<?php if($hasDownloads): ?>
    <div class="resource--collateral-downloads">
        <h3>Additional Downloads</h3>
        <ul>
            <?php while($model->hasVideoExtras()): $data = $model->getVideoCollateralData(); ?>
                <li><a href="<?= $data->file ?>" download="<?= $data->fileName ?>"><?= $data->name ?></a></li>
            <?php endwhile; ?>
        </ul>
    </div>
<?php endif; ?>