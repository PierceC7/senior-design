<?php /* This is a mustache template referenced by JS, treat it like HTML with variables */ ?>
<script class="resource--post-template" type="text/html">
    <div class="resource--post resource--download-type__{{ downloadType }}">
        <div class="resource--post-wrapper">

            <a href="{{ resourceLink }}" class="resource--post-image">
                <div class="resource--featured-img" style="background-image: url('{{ imageSrc }}');">
                    <span class="screen-reader-text">{{ resourceTitle }}</span>
                </div>
            </a>

            <div class="resource--inner-wrap">
                <div class="resource--post-type">
                    {{ resourceType }}
                </div>
                <div class="resource--post-date">
                    {{ resourceDate }}
                </div>
                <div class="resource--post-title">
                    {{ resourceTitle }}
                </div>
                <div class="resource--post-download">
                    <a href="{{ resourceLink }}">
                        {{ resourceDownloadText }}
                    </a>
                </div>
            </div>

        </div>
    </div>
</script>