<?php /** @var \KMDG\ResourceCenter\ResourceModel $model */ ?>
<div class="resource--content">
    <h1><?php the_title() ?></h1>
    <?php the_content() ?>
    <?php if($showButton): ?>
        <div class="resource--download-button">
            <a href="<?= $downloadURL ?>" download="<?= $fileName ?>">
                Download
            </a>
        </div>
    <?php endif; ?>
</div>