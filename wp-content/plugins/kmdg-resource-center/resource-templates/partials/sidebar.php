<?php if(has_post_thumbnail() || $embedPDF): ?>

        <?php if($image && !$embedPDF): ?>
            <img src="<?= $image->full ?>">
        <?php elseif($embedPDF): ?>
            <iframe src="<?= $fileURL ?>"></iframe>
        <?php endif; ?>

        <?php if($showButton): ?>
            <a href="<?= $downloadURL ?>" download="<?= $fileName ?>">
                Download
            </a>
        <?php endif; ?>
    </aside>
<?php endif; ?>