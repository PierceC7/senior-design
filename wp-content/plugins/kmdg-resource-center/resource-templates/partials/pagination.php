<?php /* This is a mustache template referenced by JS, treat it like HTML with variables */ ?>
<div class="resource--search-pagination">
    <script class="resource--pagination-template" type="text/html">
        {{#show}}
            {{#showPrev}}
                <a href="{{ linkBase }}{{ firstPage }}"><i class="fa fa-angle-double-left"></i></a>
                <a href="{{ linkBase }}{{ prevPage }}"><i class="fa fa-angle-left"></i></a>
            {{/showPrev}}

            <div class="resource--pages">
                {{#pages}}
                    <a href="{{ linkBase }}{{ page }}" class="{{ class }}">{{ page }}</a>
                {{/pages}}
            </div>

            {{#showNext}}
                <a href="{{ linkBase }}{{ nextPage }}"><i class="fa fa-angle-right"></i></a>
                <a href="{{ linkBase }}{{ lastPage }}"><i class="fa fa-angle-double-right"></i></a>
            {{/showNext}}
        {{/show}}
    </script>
</div>