<?php
/** @var \KMDG\ResourceCenter\ResourceModel $model */
/** @var \Aesir\v1\Components\PostType $postType */
?>
<div class="resource--search" data-scroll="search">
    <h3>All Resources</h3>

    <div class="resource--search-filters">
        <form class="resource--search-form">
            <div class="resource--search-bar">
                <label>
                    Search: <input value="<?= $currentSearch ?>" type="search" name="resource-search" />
                </label>
                <span class="resource--search-loading"></span>
            </div>

            <div class="resource--filters">
                <p class="resource--filter-text">Filter by:</p>

                <?php foreach ($filterData as $tax => $terms): ?>
                    <?= $filterPart([
                            'terms' => $terms,
                            'tax' => get_taxonomy($tax),
                            'friendly' => $postType->getTaxonomy($tax)->friendly()
                    ]); ?>
                <?php endforeach; ?>

            </div>
        </form>
    </div>

    <div class="resource--search-results">
        <?= $tilePart ?>
    </div>

    <?= $paginationPart ?>
</div>