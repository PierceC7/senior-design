<?php
/** @var \KMDG\ResourceCenter\ResourceModel $model */
?>
<?php get_header(); ?>
<?php if(have_posts()): the_post(); ?>
    <section class="kmdg-resource-center resource resource--single <?= $classes ?>">

        <div class="resource--channel">
            <div class="resource--layout">
                <?php if($hasSidebar): ?>
                    <aside class="resource--sidebar">
                        <?= $sidebarContent ?>
                        <?= $collateralContent ?>
                    </aside>
                <?php endif;?>

                <main class="resource--main">
                    <?= $mainContent ?>
                    <?= $formContent ?>
                </main>
            </div>
        </div>

    </section>
<?php endif; ?>
<?php get_footer(); ?>
