<?php
use KMDG\ResourceCenter\ResourceCenter;

if(!function_exists('ResourceCenter')) {
    function ResourceCenter() {
        return ResourceCenter::getInstance()->getModel();
    }
}
