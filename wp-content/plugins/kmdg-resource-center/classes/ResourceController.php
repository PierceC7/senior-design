<?php
/**
 * Created by PhpStorm.
 * User: KMDG
 * Date: 10/8/2018
 * Time: 10:20 AM
 */

namespace KMDG\ResourceCenter;


use Aesir\v1\Aesir;
use Aesir\v1\Components\Request;
use Aesir\v1\Components\Template\TemplateLocator;
use Aesir\v1\Components\View;

use Aesir\v1\Factories\PartFactory;
use Aesir\v1\Traits\TrimmedExplode;
use WP_Query;

class ResourceController
{
    use TrimmedExplode;

    protected $model;
    protected $locator;
    protected $partFactory;

    public function __construct(ResourceModel $model, Aesir $aesir, TemplateLocator $locator, PartFactory $partFactory)
    {
        $this->model = $model;

        $locator->addPath('rc-plugin', $aesir->config()->get('path/templates'));

        $partFactory->setLocator($locator);

        $this->partFactory = $partFactory;
        $this->locator = $locator;
    }

    public function error404(\WP_Query $query) {
        return function() use ($query) {
            $query->set_404();
            status_header(404);
        };
    }

    public function single(View $view, \WP_Post $post) {

        $type       = $this->model->downloadType();
        $template   = $this->locator->find('single', $type);

        $form           = $this->model->getFormObject();
        $fileURL        = $this->model->getDownloadURL(null, true);
        $fileName       = sanitize_title(pathinfo($this->model->getDownloadURL(null, true), PATHINFO_FILENAME));
        $image          = $this->model->getResourceImage();
        $hasDownload    = $this->model->hasDownload();
        $showButton     = $this->model->displayDownloadButton();
        $hasForm        = $this->model->requireForm() && !empty($form);
        $embedPDF       = $this->model->embedPDF() && $hasDownload;
        $downloadURL    = $this->model->getDownloadURL();
        $hasSidebar     = true;

        $classes = [
            "resource--download-type__".$this->model->downloadType()
        ];

        if($this->model->embedPDF()) {
            $classes[] = 'resource--pdf-embedded';
        }

        if($this->model->displayDownloadButton()) {
            $classes[] = 'resource--has-download-button';
        }

        if($this->model->requireForm()) {
            $classes[] = 'resource--form-required';
        }

        if(has_post_thumbnail() || $this->model->embedPDF() || $this->model->downloadType() == 'video') {
            $classes[] = 'resource--has-sidebar';
        } else {
            $hasSidebar = false;
        }

        $sidebarArgs = [];
        $hasCollateral = false;

        if($type == 'video') {
            $sidebarArgs = [
                'model'         => $this->model,
                'hasForm'       => $hasForm,
                'video'         => $this->model->getVideoInfo(),
                'placeholder'   => $image,
            ];

            $hasCollateral = $this->model->hasVideoExtras();
        } else {
            $sidebarArgs = [
                'model'         => $this->model,
                'image'         => $image,
                'embedPDF'      => $embedPDF,
                'fileURL'       => $fileURL,
                'downloadURL'   => $downloadURL,
                'fileName'      => $fileName,
                'showButton'    => $showButton,
            ];
        }

        $sidebarContent = null;

        if($hasSidebar) {
            $sidebarContent = $this->partFactory->make('partials', 'sidebar', $type, $sidebarArgs);
        }

        $collateralContent = null;

        if($hasCollateral) {
            $collateralContent = $this->partFactory->make('partials', 'collateral', null, [
                'hasCollateral' => $hasCollateral,
                'model'         => $this->model
            ]);
        }

        $mainContent = $this->partFactory->make('partials', 'content', null, [
            'model'             => $this->model,
            'downloadURL'       => $downloadURL,
            'fileName'          => $fileName,
            'showButton'        => $showButton,
        ]);

        $formContent = $this->partFactory->make('partials', 'form', null, [
            'model'         => $this->model,
            'form'          => $form,
            'hasForm'       => $hasForm
        ]);

        return $view->template($template)->make([
            'resource'          => $post,
            'model'             => $this->model,
            'form'              => $form,
            'classes'           => implode(' ', $classes),
            'mainContent'       => $mainContent,
            'formContent'       => $formContent,
            'sidebarContent'    => $sidebarContent,
            'hasSidebar'        => $hasSidebar,
            'collateralContent' => $collateralContent,
            'hasCollateral'     => $hasCollateral,
        ]);
    }

    public function archive(View $view, ResourceCenter $rc, Request $request, WP_Query $query) {
        $postType = get_post_type() ?: $request->type();
        $template = $this->locator->find('archive', $postType);

        $filterData = $this->model->getFilters($postType, $request->slug());
        $filterPart = $this->partFactory->make('partials', 'filter');

        $tilePart = $this->partFactory->make('partials', 'tile');

        $paginationPart = $this->partFactory->make('partials', 'pagination');

        $searchPart = $this->partFactory->make('partials', 'search', null, [
            'model'         => $this->model,
            'resourceCenter'=> $rc,
            'filterData'    => $filterData,
            'filterPart'    => $filterPart,
            'tilePart'      => $tilePart,
            'paginationPart'=> $paginationPart,
            'postType'      => $this->model->getPostType($postType),
            'currentSearch' => esc_html(esc_attr($request->post('resource-search')))
        ]);

        return $view->template($template)->make([
            'searchPart'    => $searchPart,
        ]);
    }

    public function emailFile(Request $request, View $view, ResourceCenter $rc) {
        global $post;

        $email = $request->get('email');
        $post = get_post($request->get('post_id'));
        setup_postdata( $post );

        if(get_post_type() != $rc->getSlug()) {
            return false;
        }

        $to = $email;

        $file = false;

        if($this->model->downloadType() == 'file') {
            $file = $this->model->getFileID();
        } elseif($this->model->downloadType() != 'none') {
            $file = $this->model->getDownloadURL();
        }

        if($file) {
            $access_key = substr(sha1('file_'.$to.'_'.uniqid(true)), 0, 30);
            $link = $rc->getAjaxEndpoint().'/'.$access_key;
            $expires = apply_filters('KMDG/ResourceCenter/Download/Expires', 2 * DAY_IN_SECONDS);

            $data = [
                'file'      => $file,
                'email'     => $to,
                'post'      => get_the_ID(),
                'link'      => $link,
                'requested' => date('m/d/Y - h:i A'),
                'expires'   => date('m/d/Y - h:i A', time() + $expires)
            ];

            set_transient($access_key, $data, $expires);

            if($to != NULL && $this->model->emailEnabled()) {
                $subject = $this->model->doEmailShortcodes($this->model->emailDownloadSubject(), $link);
                $body = $this->model->emailDownloadBody();

                $message = $this->model->doEmailShortcodes($body, $link);

                add_filter('wp_mail_from', function () {
                    return $this->from_addr;
                });

                add_filter('wp_mail_from_name', function () {
                    return get_bloginfo('name');
                });

                add_filter('wp_mail_content_type', function () {
                    return "text/html";
                });

                $success = wp_mail($to, $subject, $message);


                if ($success) {
                    $template = $this->locator->find('download', 'success', 'thankyou');

                    return $view->template($template)->make();
                } else {
                    // If email failed
                    $template = $this->locator->find('download', 'error', 'no_email');

                    return $view->template($template)->make([
                        'link' => $link
                    ]);
                }
            } else {
                // If no email address, or email sending turned off, provide download link
                $template = $this->locator->find('download', 'success', 'download');

                return $view->template($template)->make([
                    'isDownload'=> $this->model->downloadType() == 'file',
                    'link'      => $link,
                    'fileName'  => sanitize_title(get_the_title())
                ]);
            }
        } else {
            // No download file

            if($this->model->downloadType() == 'none') { // Form only, show thank you message
                $template = $this->locator->find('download', 'success', 'thankyou');

            } else { // Error: No download but there should be one
                $template = $this->locator->find('download', 'error', 'file');

                $rc->send_error_email(get_the_title(), $to, 'creation of download link');
            }

            return $view->template($template)->make();
        }
    }

    public function fileDownload(Request $request, View $view, ResourceCenter $rc) {
        $key = $request->get('download_key');
        $fileInfo = get_transient($key);

        if($fileInfo && !empty($fileInfo['file'])) {
            $file = $fileInfo['file'];

            if(is_int($file) && get_attached_file($file)) {
                // Download file from server
                $file_path = get_attached_file($file);

                if (file_exists($file_path)) {
                    return function() use ($file_path) {
                        header('Content-Description: File Transfer');
                        header('Content-Type: application/octet-stream');
                        header('Content-Disposition: attachment; filename="'.basename($file_path).'"');
                        header('Expires: 0');
                        header('Cache-Control: must-revalidate');
                        header('Pragma: public');
                        header('Content-Length: ' . filesize($file_path));
                        readfile($file_path);
                    };
                }

            } elseif (filter_var($file, FILTER_VALIDATE_URL) !== FALSE) {
                // Redirect to URL
                return function() use ($file) {
                    wp_redirect($file, 302);
                    exit;
                };
            } else {
                // Something went wrong...
                header("HTTP/1.1 500 Internal Server Error");

                $template = $this->locator->find('download', 'error', 'file');

                $this->send_error_email($fileInfo, 'the download attempt');

                return $view->template($template)->make();
            }
        } else {
            header("HTTP/1.1 401 Unauthorized");

            $template = $this->locator->find('download', 'error', 'access');

            return $view->template($template)->make();

        }
    }

    public function shortcode($args, $content, View $view) {

        $atts = (object) shortcode_atts([
            'postType'  => null,
            'location'  => 'all',
            'filters'   => 'show'
        ], $args);

        $template = $this->locator->find('shortcodes', 'search');

        $postType = $atts->postType;
        $hasFilters = $atts->filters == 'show';

        if(is_null($atts->postType)) {
            $postTypes = $this->model->getPostTypeSlugs();
            $postType = $postTypes[0];
        }

        $filterData = $this->model->getFilters($postType, $atts->location);
        $filterPart = $this->partFactory->make('partials', 'filter');

        $action = '';

        if($atts->location == 'all') {
            $action = get_post_type_archive_link($postType);
        } else {
            $action = get_term_link($atts->location, ResourceLocationTax::slug());
        }

        return $view->template($template)->make([
            'model'         => $this->model,
            'postType'      => $this->model->getPostType($postType),
            'formAction'    => $action,
            'filterData'    => $filterData,
            'filterPart'    => $filterPart,
            'hasFilters'    => $hasFilters
        ]);
    }

    public function playVideo(View $view) {
        // Note, the video is played via JS, this only handles responding with the thank-you message
        $template = $this->locator->find('download', 'video', 'thankyou');

        return $view->template($template)->make();
    }

    public function shortcodeRecentResources($args, $content, View $view) {
        $atts = (object) shortcode_atts([
            'count'     => 3,
            'type'      => null,
            'location'  => null,
            'template'  => null
        ], $args);

        $template = $this->locator->find('shortcodes', 'recent');
        $tilePart = $this->partFactory->make('partials', 'tile', $atts->template);

        // Transform shortcode pseudo-arrays into real ones
        $type = $this->explodeAndTrim($atts->type) ?: $atts->type;
        $location = $this->explodeAndTrim($atts->location) ?: $atts->location;

        $query = $this->model->queryPosts($atts->count, $type, $location);

        $data = [];

        while($query->have_posts()) {
            $query->the_post();

            $data[] = [
                'downloadType'  => $this->model->downloadType(),
                'link'          => get_permalink(),
                'image'         => $this->model->getResourceImage(),
                'date'          => $this->model->getDate(),
                'resourceType'  => $this->model->findMainTerm(ResourceTypeTax::slug()),
                'excerpt'       => get_the_excerpt(),
                'title'         => get_the_title(),
                'buttonText'    => $this->model->buttonText()
            ];
        }

        return $view->template($template)->make([
            'model'     => $this->model,
            'json'      => json_encode($data),
            'tilePart'  => $tilePart,
            'foundPosts'=> $query->have_posts()
        ]);
    }
}