<?php

namespace KMDG\ResourceCenter;

interface IResourceTaxonomy
{
    /**
     * Returns a human readable slug for the URL of the resource search
     *
     * @return string
     */
    public function friendly();

    /**
     * Returns true or false based on if the taxonomy should shown as a filter
     *
     * @return mixed
     */
    public function showFilter();
}