<?php
/**
 * Created by PhpStorm.
 * User: KMDG
 * Date: 2/20/2018
 * Time: 12:06 PM
 */

namespace KMDG\ResourceCenter;

use Aesir\v1\Components\Taxonomy;

class ResourceLocationTax extends Taxonomy implements IResourceTaxonomy
{

    /**
     * Returns the slug of the post type
     *
     * @return string
     */
    public static function slug()
    {
        return 'resource-location';
    }

    protected function rewrite() {
        return apply_filters('KMDG\ResourceCenter\ResourceLocationTax\rewrite', ['slug' => 'resource-center', 'with_front' => false]);
    }

    /**
     * Returns a friendly slug for human readability.
     */
    public function friendly() {
        return 'location';
    }

    public function showFilter()
    {
        return false;
    }

    /**
     * Returns the singular readable name of the post type
     *
     * @return string
     */
    public function singular()
    {
        return 'Location';
    }

    /**
     * Returns the plural readable name of the post type
     *
     * @return string
     */
    public function plural()
    {
        return 'Locations';
    }

    /**
     * Include a description of the taxonomy
     *
     * @return string
     */
    protected function description()
    {
        return 'For organizing resources by location';
    }

    /**
     * Whether a taxonomy is intended for use publicly either via the admin interface or by front-end users.
     *
     * @return bool
     */
    protected function isPublic()
    {
        return true;
    }

    /**
     * Whether to include the taxonomy in the REST API.
     *
     * @return bool
     */
    protected function showInRest()
    {
        return true;
    }

    /**
     * Whether to allow the Tag Cloud widget to use this taxonomy.
     *
     * @return bool
     */
    protected function showTagcloud()
    {
        return false;
    }

    /**
     * Is this taxonomy hierarchical (have descendants) like categories or not hierarchical like tags.
     *
     * @return bool
     */
    protected function isHierarchical()
    {
        return true;
    }

    /**
     * Whether to allow automatic creation of taxonomy columns on associated post-types table.
     *
     * @return bool
     */
    protected function showAdminColumn()
    {
        return true;
    }

    protected function showAdminFilter()
    {
        return true;
    }

    /**
     * Whether this taxonomy should remember the order in which terms are added to objects.
     *
     * @return bool
     */
    protected function sort()
    {
        return true;
    }
}