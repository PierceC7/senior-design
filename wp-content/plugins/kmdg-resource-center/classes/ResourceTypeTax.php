<?php
/**
 * Created by PhpStorm.
 * User: KMDG
 * Date: 2/20/2018
 * Time: 12:06 PM
 */

namespace KMDG\ResourceCenter;

use Aesir\v1\Components\Taxonomy;

class ResourceTypeTax extends Taxonomy implements IResourceTaxonomy
{

    /**
     * Returns the slug of the post type
     *
     * @return string
     */
    public static function slug()
    {
        return 'resource-type';
    }

    /**
     * Set to false to prevent automatic URL rewriting a.k.a. "pretty permalinks". Pass an array to override default
     * URL settings for permalinks as outlined below:
     *
     * 'slug' - Used as pretty permalink text (i.e. /tag/) - defaults to $taxonomy (taxonomy's name slug)
     * 'with_front' - allowing permalinks to be prepended with front base - defaults to true
     * 'hierarchical' - true or false allow hierarchical urls (implemented in Version 3.1) - defaults to false
     * 'ep_mask' - (Required for pretty permalinks) Assign an endpoint mask for this taxonomy - defaults to EP_NONE.
     *              If you do not specify the EP_MASK, pretty permalinks will not work. For more info see this Make
     *              WordPress Plugins summary of endpoints.
     *
     * Note: You may need to flush the rewrite rules after changing this. You can do it manually by going to the
     * Permalink Settings page and re-saving the rules -- you don't need to change them -- or by
     * calling $wp_rewrite->flush_rules(). You should only flush the rules once after the taxonomy has been created,
     * not every time the plugin/theme loads.
     *
     * @return bool|array
     */
    protected function rewrite()
    {
        return [
            'slug' => static::slug(),
            'with_front' => false,
        ];
    }

    /**
     * Returns a friendly slug for human readability.
     */
    public function friendly() {
        return 'type';
    }

    public function showFilter()
    {
        return true;
    }

    /**
     * Returns the singular readable name of the post type
     *
     * @return string
     */
    public function singular()
    {
        return 'Type';
    }

    /**
     * Returns the plural readable name of the post type
     *
     * @return string
     */
    public function plural()
    {
        return 'Types';
    }

    /**
     * Include a description of the taxonomy
     *
     * @return string
     */
    protected function description()
    {
        return 'For organizing resources by type';
    }

    /**
     * Whether a taxonomy is intended for use publicly either via the admin interface or by front-end users.
     *
     * @return bool
     */
    protected function isPublic()
    {
        return true;
    }

    /**
     * Whether to include the taxonomy in the REST API.
     *
     * @return bool
     */
    protected function showInRest()
    {
        return true;
    }

    /**
     * Whether to allow the Tag Cloud widget to use this taxonomy.
     *
     * @return bool
     */
    protected function showTagcloud()
    {
        return false;
    }

    /**
     * Is this taxonomy hierarchical (have descendants) like categories or not hierarchical like tags.
     *
     * @return bool
     */
    protected function isHierarchical()
    {
        return true;
    }

    /**
     * Whether to allow automatic creation of taxonomy columns on associated post-types table.
     *
     * @return bool
     */
    protected function showAdminColumn()
    {
        return true;
    }

    protected function showAdminFilter()
    {
        return true;
    }

    /**
     * Whether this taxonomy should remember the order in which terms are added to objects.
     *
     * @return bool
     */
    protected function sort()
    {
        return true;
    }

    /**
     * Provide a callback function name for the meta box display
     *
     * @return callable|null
     */
    protected function registerMetaBox()
    {
        return false;
    }
}