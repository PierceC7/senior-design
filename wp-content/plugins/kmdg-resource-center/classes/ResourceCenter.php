<?php

namespace KMDG\ResourceCenter;


use Aesir\v1\Aesir;
use Aesir\v1\Components\Plugin as AesirPlugin;
use Aesir\v1\Components\PostType;
use Aesir\v1\Exceptions\AesirException;
use Aesir\v1\Traits\Filterable;
use KMDG\AJAX;
use KMDG\AJAX_Handler;

class ResourceCenter extends AesirPlugin {

    const EXCERPT_LENGTH = 35;

    protected $slug;
    protected $download_ajax_slug;
    protected $ajax_endpoint;
    protected $from_addr;
    protected $basePath;
    protected $assetsURL;
    protected $assetsPath;
    protected $currentSearchTaxQuery;
    protected $pluginData = [];

    /** @var ResourceModel */
    protected $model;

    // Public API

    /**
     * Loads the plugin, setting up all initial data
     *
     * @param $path
     * @param Aesir|null $aesir
     * @param string $slug
     * @param string $ajaxDownloadSlug
     * @param string $baseURL
     * @param string $basePath
     * @param string $assetsDir
     * @param ResourceModel|null $model
     *
     * @throws AesirException
     */
    public function load($path, Aesir $aesir = null, $slug = '', $ajaxDownloadSlug = '', $baseURL = '', $basePath = '', $assetsDir = '', ResourceModel $model = null) {
        parent::load($path, $aesir);

        $this->slug                 = $slug;
        $this->download_ajax_slug   = $ajaxDownloadSlug;
        $this->model                = $model;
        $this->basePath             = trailingslashit($basePath);
        $this->assetsURL            = trailingslashit($baseURL.$assetsDir);
        $this->assetsPath           = trailingslashit($basePath.$assetsDir);
        $this->ajax_endpoint        = home_url($this->download_ajax_slug);
        $this->from_addr            = defined('RESOURCE_OUTGOING_EMAIL_ADDR') ? RESOURCE_OUTGOING_EMAIL_ADDR : $this->model->emailFromAddr();

        $this->addFilter('acf/settings/load_json', 'load_acf');
        $this->addFilter('excerpt_length', 'excerpt_length', 999);
        $this->addFilter("rest_prepare_{$this->slug}", 'rest_response_data', 10, 1);
        $this->addFilter('posts_join', 'custom_posts_join', 10, 2);
        $this->addFilter('posts_where', 'custom_posts_where', 10, 2);
        $this->addFilter('posts_groupby', 'custom_posts_groupby', 10, 2);
        $this->addFilter('template_include', 'masked_download_link', 1, 1);
        $this->addFilter('rest_resource-post_collection_params', 'rest_request_parameters', 10, 1);
        $this->addFilter('rest_resource-post_query', 'rest_request_query', 10, 2);
        $this->addFilter('pre_get_posts', 'resource_query', 10, 1);

        // These filters handle redirecting away from the resource page to the download URL when Skip Landing Page is used
        $this->addFilter('template_redirect', 'redirect_to_override_url', 2, 0);
        $this->addFilter('post_type_link', 'override_resource_url', 10, 2);

        //$this->addAction('gform_after_submission', 'gf_process_form', 10, 2);
        $this->addAction('wp_enqueue_scripts', 'enqueue_scripts');
        $this->addAction('admin_menu', 'acf_add_options', 10);
        $this->addAction('save_post', 'save_resource_meta', 10, 1);

        add_shortcode('resource_name', [$this, 'shortcode_resource_name']);
        add_shortcode('resource_type', [$this, 'shortcode_resource_type']);
    }

    /**
     * Returns data passed into the plugin in its load step, return the data or null if not found.
     * @param $key
     *
     * @return mixed|null
     */
    protected function data($key) {
        if(!empty($this->pluginData[$key])) {
            return $this->pluginData[$key];
        }

        return null;
    }

    /**
     * Hooked to WP Init, this is where you should register post types and anything else that must happen each page load.
     */
    protected function __init()
    {
        // Setup Post Type(s)
        foreach ($this->model->getPostTypeSlugs() as $slug) {
            $this->model->getPostType($slug)->init();
        }

        // Creates rewrite rules that mask the use of Admin Ajax when downloading files
        $ajax_url = str_replace(home_url().'/', '', admin_url());
        add_rewrite_tag('%action%', '([^&]+)');
        add_rewrite_tag('%download_key%', '([^&]+)');
        add_rewrite_rule($this->download_ajax_slug.'/(.*)/?', $ajax_url.'admin-ajax.php?action='.$this->download_ajax_slug.'&download_key=$1', 'top');
    }

    protected function __load_acf( $paths ) {
        $paths[] = $this->basePath."acf/json";
        return $paths;
    }

    protected function __acf_add_options() {
        acf_add_options_page([
             /* (string) The title displayed on the options page. Required. */
             'page_title' => 'Resource Options',

             /* (string) The title displayed in the wp-admin sidebar. Defaults to page_title */
             'menu_title' => 'Resource Options',

             /* (string) The URL slug used to uniquely identify this options page.
             Defaults to a url friendly version of menu_title */
             'menu_slug' => 'resource-options',

             /* (string) The capability required for this menu to be displayed to the user. Defaults to edit_posts.
             Read more about capability here: http://codex.wordpress.org/Roles_and_Capabilities */
             'capability' => 'edit_posts',

             /* (int|string) The position in the menu order this menu should appear.
             WARNING: if two menu items use the same position attribute, one of the items may be overwritten so that only one item displays!
             Risk of conflict can be reduced by using decimal instead of integer values, e.g. '63.3' instead of 63 (must use quotes).
             Defaults to bottom of utility menu items */
             'position' => '99.123',

             /* (string) The slug of another WP admin page. if set, this will become a child page. */
             'parent_slug' => null,

             /* (string) The icon class for this menu. Defaults to default WordPress gear.
             Read more about dashicons here: https://developer.wordpress.org/resource/dashicons/ */
             'icon_url' => false,

             /* (boolean) If set to true, this options page will redirect to the first child page (if a child page exists).
             If set to false, this parent page will appear alongside any child pages. Defaults to true */
             'redirect' => false,

             /* (int|string) The '$post_id' to save/load data to/from. Can be set to a numeric post ID (123), or a string ('user_2').
             Defaults to 'options'. Added in v5.2.7 */
             'post_id' => 'resources',

             /* (boolean)  Whether to load the option (values saved from this options page) when WordPress starts up.
             Defaults to false. Added in v5.2.8. */
             'autoload' => false,

             /* (string) The update button text. Added in v5.3.7. */
             'update_button'		=> __('Update', 'acf'),

             /* (string) The message shown above the form on submit. Added in v5.6.0. */
             'updated_message'	=> __("Options Updated", 'acf'),
        ]);

        acf_add_options_page([
            'page_title' => 'Resource Landing Page',
            'menu_title' => 'Landing Page',
            'menu_slug' => 'resource-landing-options',
            'parent_slug' => 'resource-options',
            'post_id' => 'resources',
        ]);
    }

    protected function __activate() {
        flush_rewrite_rules();
    }

    protected static function __deactivate() {
        flush_rewrite_rules();
    }

    protected function __excerpt_length($length) {

        if(in_array(get_post_type(), $this->model->getPostTypeSlugs())) {
            return static::EXCERPT_LENGTH;
        }

        return $length;
    }

    protected function __shortcode_resource_name() {
        return get_the_title();
    }

    protected function __shortcode_resource_type() {
        return $this->type();
    }

    protected function __resource_query(\WP_Query $query) {
        $postType = (array) $query->get('post_type');

        if(!$query->is_admin && in_array($this->getSlug(), $postType) && $query->is_main_query()) {
            $location = $query->is_tax(ResourceLocationTax::slug()) ? $query->get_queried_object_id() : null;
            $order = $this->model->getLocationSorting($location);

            $query->set('orderby', $order['mode']);
            $query->set('order', $order['dir']);
            $query->set('meta_type', $order['type']);
            $query->set('meta_key', $order['key']);
        }

        return $query;
    }

    protected function __enqueue_scripts() {
        global $wp_query;

        if(apply_filters('KMDG/ResourceCenter/JS/load',  true, $this->model)) {
            wp_enqueue_script('vimeo-api', 'https://player.vimeo.com/api/player.js', [], false, true);
            wp_enqueue_script('kmdg-resource-center', $this->assetsURL.'js/resources.min.js', ['vimeo-api'], filemtime($this->assetsPath.'js/resources.min.js'), true);

            $location = is_tax(ResourceLocationTax::slug()) ? get_queried_object_id() : null;

            $order = $this->model->getLocationSorting($location);

            $postType = get_post_type() ?: $wp_query->get('post_type');

            wp_localize_script('kmdg-resource-center', 'kmdgResourceCenter', [
                'restURL'           => get_rest_url(null, 'wp/v2/'.$postType),
                'ajaxURL'           => admin_url('admin-ajax.php'),
                'loadingGraphic'    => apply_filters('KMDG/ResourceCenter/Search/loading/graphic', $this->assetsURL.'img/loading.svg'),
                'loadingText'       => apply_filters('KMDG/ResourceCenter/Search/loading/text', 'Loading...'),
                'location'          => $location,
                'resource'          => is_singular($this->slug) ? get_the_ID() : null,
                'resourceName'      => is_singular($this->slug) ? get_the_title() : null,
                'form'              => is_singular($this->slug) && $this->model->requireForm() ? $this->model->getFormID() : null,
                'videoURL'          => is_singular($this->slug) && $this->model->downloadType() == 'video' && $this->model->playOnPage() ? $this->model->getVideoInfo()->url : null,
                'postsPerPage'      => apply_filters('KMDG/ResourceCenter/Search/postsPerPage', 9, $this->model, $location),
                'order'             => $order,
                'fn'                => (object) [],
            ]);
        }

        if(apply_filters('KMDG/ResourceCenter/css/load', true, $this->model)) {
            wp_enqueue_style('kmdg-resource-center', $this->assetsURL.'css/resources.css', filemtime($this->assetsPath.'css/resources.css'));
        }
    }

    protected function __masked_download_link($template) {
        global $wp;

        $request = home_url($wp->request);
        $pID = url_to_postid(dirname($request));

        if( get_post_type($pID) == $this->slug
            && basename($request) == 'download') {

            if(!$this->model->requireForm($pID)
                && $this->model->hasDownload($pID)
                && apply_filters('KMDG/ResourceCenter/Download/Access', true, $pID, $request, $this->model)) {

                $type = apply_filters('KMDG/ResourceCenter/Download/Type', $this->model->downloadType($pID), $pID, $this->model);

                if($type == 'file') {
                    $fileID = apply_filters('KMDG/ResourceCenter/Download/File', $this->model->getFileID($pID), $pID, $this->model);
                    $file = get_attached_file($fileID);

                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream', true, 200);
                    header('Content-Disposition: attachment; filename=' . basename($file));
                    header('Content-Transfer-Encoding: binary');
                    header('Connection: Keep-Alive');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($file));

                    ob_clean();
                    flush();

                    readfile($file);
                    exit;
                } else {
                    $url = apply_filters('KMDG/ResourceCenter/Download/Redirect', $this->model->getDownloadURL($pID, true), $pID, $this->model);
                    header('Location: '.$url, true, 301);
                    exit;
                }
            } else {
                header('HTTP/1.0 403 Forbidden', true, 403);

                // TODO: This is very out of date, added die() as temp fix

                die("Access denied.");

                $path = apply_filters('KMDG/ResourceCenter/Templates/BasePath', $this->slug);

                $template = locate_template("{$path}/403.php");

                if(empty($template)) {
                    $template = $this->getPath().'templates/403.php';
                }
            }

        }

        return $template;
    }

    protected function __override_resource_url($url, $post) {

        if(in_array(get_post_type($post), $this->model->getPostTypeSlugs())) {
            $id = is_object($post) ? $post->ID : $post;

            if($this->model->skipLandingPage($id)) {
                if($this->model->downloadType() == 'file') {
                    $this::disableFilters('post_type_link');
                    $url = $this->model->getDownloadURL($id, false);
                    $this::enableFilters('post_type_link');
                } else {
                    $url = $this->model->getDownloadURL($id);
                }
            }
        }

        return $url;
    }

    protected function __redirect_to_override_url() {
        if(is_singular($this->slug) && $this->model->skipLandingPage()) {
            $this::disableFilters('post_type_link');
            wp_redirect($this->model->getDownloadURL(), 302);
            exit();
        }
    }

    protected function __gf_process_form(array $entry, array $form) {

        $email_field_id = null;
        $email = null;

        foreach($form['fields'] as $field) {
            if($field->type == 'email') {
                $email_field_id = $field->id;
                break;
            }
        }

        if(!empty($email_field_id) && !empty($entry[$email_field_id])) {
            $email = $entry[$email_field_id];
        }

        $this->emailResourceFile(get_the_ID(), $email);
    }

    protected function send_error_email($resource, $when) {
        $title = get_the_title($resource['post']);
        $email_addr = $resource['email'];

        add_filter('wp_mail_from', function() {
            return $this->from_addr;
        });

        add_filter('wp_mail_from_name', function() {
            return get_bloginfo('name');
        });

        add_filter('wp_mail_content_type', function() {
            return "text/html";
        });

        $message =
            "<p>Administrator,</p>".
            "<p>An error has been encountered when trying to process the ".
                "download for the resource '{$title}' during {$when}. Please ".
                "check the configuration of this resource.".
            "</p>".
            "<p>The email address of the person who encountered this error is: {$email_addr}</p>".
            "<p>To assist with debugging &amp; troubleshooting, the raw download record is provided below:</p>".
            "<pre style='border: 1px solid #660000;background-color:#eaeaea;color:#000000;padding:10px;margin-top: 30px;'>".
                print_r($resource, true).
            "</pre>";

        wp_mail(get_option('admin_email'), "[Error Report] Resource Download", $message);
    }

    /**
     * Filters the WP Query SQL Join statement to add in taxonomy terms in searches
     *
     * @link https://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
     *
     * @param string $join The sql JOIN clause.
     * @param \WP_Query $query The current WP_Query instance.
     *
     * @return string $join The sql JOIN clause.
     */
    protected function __custom_posts_join($join, \WP_Query $query)
    {
        global $wpdb;

        if (defined('REST_REQUEST') && $query->is_search()) {

            $join .= "
                LEFT JOIN
                (
                    {$wpdb->term_relationships} as TermRel
                    INNER JOIN
                        {$wpdb->term_taxonomy} ON {$wpdb->term_taxonomy}.term_taxonomy_id = TermRel.term_taxonomy_id
                    INNER JOIN
                        {$wpdb->terms} ON {$wpdb->terms}.term_id = {$wpdb->term_taxonomy}.term_id
                )
                ON {$wpdb->posts}.ID = TermRel.object_id ";

            $join .= $this->currentSearchTaxQuery['join'];
        }

        return $join;
    }

    /**
     * Callback for WordPress 'posts_where' filter.
     *
     * Modify the where clause to include searches against a WordPress taxonomy.
     *
     * Taxonomies can be added to the search by
     *
     * @see https://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
     *
     * @param string $where The where clause.
     * @param \WP_Query $query The current WP_Query.
     *
     * @return string The where clause.
     */
    protected function __custom_posts_where($where, \WP_Query $query)
    {
        global $wpdb;

        $isResourcePost = false;
        $postType = (array) $query->get('post_type');

        foreach ($this->model->getPostTypeSlugs() as $slug) {
            if(in_array($slug, $postType)) {
                $isResourcePost = true;
                break;
            }
        }

        if ($isResourcePost && defined('REST_REQUEST') && $query->is_search()) {

            $taxonomies = apply_filters('KMDG/ResourceCenter/Taxonomies/Searchable', []);

            if(is_array($taxonomies) && count($taxonomies) > 0) {

                $taxSearch = "\n OR (({$wpdb->term_taxonomy}.taxonomy IN( ";

                foreach($taxonomies as $tax) {
                    $taxSearch .= "'{$tax}',";
                }

                $taxSearch = rtrim($taxSearch, ',');

                $taxSearch .= ") AND {$wpdb->terms}.name LIKE '%" . esc_sql($_GET['search']) . "%')";

                // Save this query for use in Join statement
                $this->currentSearchTaxQuery = $query->tax_query->get_sql($query->tax_query->primary_table, $query->tax_query->primary_id_column);

                $taxSearch .= $this->currentSearchTaxQuery['where'];

                $taxSearch .= " AND {$wpdb->posts}.post_type = '".$this->slug."' AND {$wpdb->posts}.post_status = 'publish')";

                $where .= $taxSearch;
            }
        }

        //die(print_r($where, true));

        return $where;
    }

    /**
     * Callback for WordPress 'posts_groupby' filter.
     *
     * Set the GROUP BY clause to post IDs.
     *
     * @global $wpdb https://codex.wordpress.org/Class_Reference/wpdb
     *
     * @param string $groupby The GROUPBY caluse.
     * @param WP_Query $query The current WP_Query object.
     *
     * @return string The GROUPBY clause.
     */
    protected function __custom_posts_groupby( $groupby, $query ) {

        global $wpdb;

        if (defined('REST_REQUEST') && $query->is_search() ) {
            $groupby = "{$wpdb->posts}.ID";
        }

        return $groupby;

    }

    /**
     * Prepares the rest responses for resource posts so that they contain custom field information.
     *
     * @param \WP_REST_Response $response
     *
     * @return WP_REST_Response
     */
    protected function __rest_response_data(\WP_REST_Response $response)
    {
        $post = $response->get_data();
        $id = $post['id'];

        if(!$this->model->requireForm($id) && $this->model->skipLandingPage($id)) {
            $post['link'] = $this->model->getDownloadURL($id);
        }

        if(!empty($post['excerpt']) && !empty($post['excerpt']['rendered'])) {
            $excerpt = apply_filters('KMDG/ResourceCenter/REST/Response/PostExcerpt', $post['excerpt']['rendered'], $this->model, $id);

            $post['excerpt']['rendered'] = wp_trim_words($excerpt, static::EXCERPT_LENGTH);
        }

        $post['downloadType'] = $this->model->downloadType($id);

        $post['resourceType'] = $this->model->findMainTerm(ResourceTypeTax::slug(), $id);

        $post['image'] = $this->model->getResourceImage($id);

        $post['buttonText'] = $this->model->buttonText($id);

        $post['date'] = $this->model->getDate(apply_filters('KMDG/ResourceCenter/REST/Response/PostDateFormat', 'n.j.Y', $this->model, $id), $id);

        $post['_customMustacheTags'] = (object) apply_filters('KMDG/ResourceCenter/REST/Response/MustacheTags', [], $id, $post, $this->model);

        $post = apply_filters('KMDG/ResourceCenter/REST/Response/Post', $post, $id, $this->model);

        $response->set_data($post);

        return apply_filters('KMDG/ResourceCenter/REST/Response', $response);
    }

    protected function __rest_request_parameters($params) {
        $params['orderby']['enum'][] = 'menu_order';
        $params['orderby']['enum'][] = 'meta_value';
        $params['orderby']['enum'][] = 'meta_value_date';
        $params['orderby']['enum'][] = 'meta_value_num';
        $params['meta_key']['enum'][] = 'sort_date';
        return $params;
    }

    protected function __rest_request_query($args, $request) {
        if ($key = $request->get_param('meta_key')) {
            $args['meta_key'] = $key;
        }

        return $args;
    }

    /**
     * @return ResourceModel
     */
    public function getModel() {
        return $this->model;
    }

    /**
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getAjaxDownloadSlug() {
        return $this->download_ajax_slug;
    }

    /**
     * @return string
     */
    public function getAjaxEndpoint() {
        return $this->ajax_endpoint;
    }

    protected function __save_resource_meta($post_id) {
        if(get_post_type($post_id) != $this->getSlug()) return;

        // creates a "sort_date" meta key that can be used for date-based display of resources
        if (get_field('show_date', $post_id) && !get_field('use_post_date', $post_id)) {
            update_post_meta($post_id, 'sort_date', get_field('start_date', $post_id));
        } else {
            update_post_meta($post_id, 'sort_date', get_the_date('Ymd', $post_id));
        }
    }
}