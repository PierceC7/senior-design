<?php
/**
 * Created by PhpStorm.
 * User: KMDG
 * Date: 2/20/2018
 * Time: 2:11 PM
 */

namespace KMDG\ResourceCenter;


use Aesir\v1\Components\PostType;
use Aesir\v1\Components\Taxonomy;
use Aesir\v1\Exceptions\AesirException;

class ResourceModel
{
    protected $cache;
    protected $slug;
    protected $youTubeAPIKey;
    protected $vimeoAPIKey;
    protected $postTypes = [];

    public function __construct($slug)
    {
        $this->slug = $slug;
        $this->youTubeAPIKey = defined("YOUTUBE_API_KEY") ? YOUTUBE_API_KEY : "AIzaSyBEfVRXip7ITwAhAHubOcN8uZf-hXN71eU";
        $this->vimeoAPIKey = defined("VIMEO_API_KEY") ? VIMEO_API_KEY : "b8609b2948245d7c7db650060a465363";
        $this->cache = (object) [];
        $this->cache->postsTerms= [];
        $this->cache->taxTerms = [];
    }

    /**
     * Returns the current location slug, or false if not on a location
     *
     * @return string|bool
     */
    public function getCurrentLocation() {
        $term = get_queried_object();

        if($term instanceof \WP_Term && is_tax(ResourceLocationTax::slug())) {
            return $this->findRootTerm($term)->slug;
        } else {
            return false;
        }
    }

    /**
     * Retrieves a single term for a given taxonomy, and its parent term if one exists. If no parent term exists the found
     * term will be listed as the parent (and as the term).
     *
     * @param string $tax
     * @param int|null $post_id
     * @param bool $no_cache
     *
     * @return object
     */
    public function findMainTerm($tax, $post_id = null, $no_cache = false) {

        if(empty($post_id)) $post_id = get_the_ID();

        if(empty($this->cache->postTerms[$tax])) {
            $this->cache->postTerms[$tax] = [];
        }

        if(empty($this->cache->postTerms[$tax]['posts'])) {
            $this->cache->postTerms[$tax]['posts'] = [];
        }

        // Use cache if present
        if(!$no_cache && !empty($this->cache->postsTerms[$tax]['posts'][$post_id])) return $this->cache->postsTerms[$tax]['posts'][$post_id];

        $emptyTerm = (object) [
            'term_id'           => 0,
            'name'              => 'Uncategorized',
            'slug'              => 'uncategorized',
            'term_group'        => 0,
            'term_taxonomy_id'  => 0,
            'taxonomy'          => 0,
            'description'       => 'No term selected, this is the default empty term',
            'parent'            => 0,
            'count'             => 0
        ];

        $categories = (object) ["parent" => $emptyTerm, "term" => $emptyTerm];
        $terms = wp_get_post_terms($post_id, $tax);

        if(is_array($terms) && count($terms) > 0) {
            /**
             * @var $term \WP_Term
             */
            foreach($terms as $term) {
                if($term->parent != 0) {
                    $categories->parent = get_term($term->parent);
                } else {
                    $categories->parent = $term;
                }

                $categories->term = $term;
            }
        }

        $this->cache->postsTerms[$tax]['posts'][$post_id] = $categories;
        return $categories;
    }

    /**
     * Returns a list of location terms this post appears in.
     *
     * @param null $post_id
     *
     * @return \WP_Term[]|\WP_Error
     */
    public function getResourceLocations($post_id = null) {
        if(is_null($post_id)){
            $post_id = get_the_ID();
        }

        return wp_get_post_terms($post_id, ResourceLocationTax::slug());
    }

    /**
     * Get all terms associated with the taxonomy
     *
     * @param string $tax
     * @param string $location
     * @param bool $hideEmpty
     * @param bool $nocache
     *
     * @return array|int|\WP_Error
     */
    public function allTerms($tax, $location = 'all', $hideEmpty = true, $nocache = false) {

        if(empty($this->cache->taxTerms[$tax])) {
            $this->cache->taxTerms[$tax] = [];
        }

        if(!$nocache && !empty($this->cache->taxTerms[$tax][$location]))
            return $this->cache->taxTerms[$tax][$location];

        $limit = null;
        if($location != 'all') {
            $limit = new \WP_Query([
                'posts_per_page' => -1,
                'fields' => 'ids',
                'tax_query' => [
                    [
                        'taxonomy' => ResourceLocationTax::slug(), // TODO: un-hardcode this
                        'field' => 'slug',
                        'terms' => $location
                    ]
                ]
            ]);
        }

        $terms = get_terms([
            'taxonomy'      => $tax,
            'hide_empty'    => $hideEmpty,
            'orderby'       => 'menu-order',
            'order'         => 'ASC',
            'object_ids'    => ($location != 'all') ? $limit->posts : null
        ]);

        $this->cache->taxTerms[$tax] = [];

        foreach($terms as $term) {
            /** @var \WP_Term $term */
            $this->cache->taxTerms[$tax][$term->slug] = $term;
        }

        return $this->cache->taxTerms[$tax];
    }

    public function hasTerms($tax, $post_id = null) {
        return !empty($this->findMainTerm($tax, $post_id)->term);
    }

    /**
     * @param string $tax
     * @param string $term
     * @param string $location
     *
     * @return \WP_Term|null
     */
    public function getTerm($tax, $term, $location = 'all') {
        $terms = $this->allTerms($tax, $location);

        return !empty($terms[$term]) ? $terms[$term] : null;
    }

    public function typeNiceName($type = null) {
        $name = '';
        $term = null;

        if(is_null($type)) {
            $term = $this->findMainTerm('resource-type')->term;
        } else {
            $term = $this->getTerm('resource-type', $type);
        }

        if(!empty($term)) {
            if(get_field('short_name', $term)) {
                $name = get_field('short_name', $term);
            } else {
                $name = $term->name;
            }
        } else {
            throw new AesirException("Term slug [$type] not found");
        }

        return $name;
    }

    public function registerPostType(PostType $postType) {
        $this->postTypes[$postType->slug()] = $postType;
    }

    public function getPostTypeSlugs() {
        return array_keys($this->postTypes);
    }

    /**
     * @param $name
     *
     * @return PostType
     * @throws AesirException
     */
    public function getPostType($name) {
        if(!empty($this->postTypes[$name])) {
            return $this->postTypes[$name];
        } else {
            throw new AesirException("No post type named [{$name}] has been registered.");
        }
    }

    public function getFilters($postType = null, $location = 'all') {
        $filters = [];
        $postTypes = [];

        if(empty($postType)) {
            $postTypes = $this->getPostTypeSlugs();
        } else {
            $postTypes = [$postType];
        }

        foreach($postTypes as $slug) {
            $type = $this->getPostType($slug);
            $taxonomies = $type->getTaxonomySlugs();

            foreach ($taxonomies as $taxonomy) {
                /** @var IResourceTaxonomy|Taxonomy $tax */
                $tax = $type->getTaxonomy($taxonomy);

                if($tax->showFilter()) {
                    $filters[$tax::slug()] = $this->allTerms($tax::slug(), $location);
                }
            }
        }

        return $filters;
    }

    public function hasDownload($post_id = null) {
        $type = $this->downloadType($post_id);

        return $type !== 'none'
            && $type !== null
            && ($type === 'video' ? ($this->requireForm($post_id) && !$this->playOnPage($post_id)) : true); // is a video that should be downloaded

    }

    public function emailEnabled($post_id = null) {
        return apply_filters('KMDG/ResourceCenter/Downloads/Email/Enabled',
            get_field('enable_email_downloads', 'resources'), $post_id);
    }

    public function embedPDF($post_id = null) {
        return get_field('embed_pdf', $post_id) && $this->hasDownload() && !$this->requireForm($post_id);
    }

    public function displayDownloadButton($post_id = null) {
        return $this->hasDownload($post_id)
            && !$this->requireForm($post_id)
            && (
                (get_field('display_download_button', $post_id) && $this->embedPDF($post_id)) || !$this->embedPDF($post_id)
            );
    }

    public function getDownloadFileName() {
        return sanitize_title(pathinfo($this->getDownloadURL(null, true), PATHINFO_FILENAME));
    }

    public function queryPosts($count = -1, $type = null, $location = null, array $args = []) {
        $defaults = [
            'post_type'     => $this->getPostTypeSlugs(),
            'post_status'   => 'publish',
            'posts_per_page'=> $count,
            'tax_query'     => []
        ];

        if(!empty($location) && $location != 'all') {
            $defaults['tax_query']['relation'] = 'AND';

            $defaults['tax_query'][] = [
                'taxonomy'  => ResourceLocationTax::slug(),
                'field'     => 'slug',
                'terms'     => $location
            ];
        }

        if(!empty($type)) {
            $defaults['tax_query']['relation'] = 'AND';

            $defaults['tax_query'][] = [
                'taxonomy'  => ResourceTypeTax::slug(),
                'field'     => 'slug',
                'terms'     => $type
            ];
        }

        if(!empty($args['tax_query'])) {
            foreach($args['tax_query'] as $key => $taxQuery) {
                if($key === 'relation') continue;

                $defaults['tax_query'][] = $taxQuery;
                unset($args['tax_query'][$key]);
            }
        }

        return new \WP_Query(
            $this->array_merge_recursive_distinct($defaults, $args)
        );
    }

    /**
     * @param array $args
     *
     * @return \WP_Query
     * @deprecated
     */
    public function query($args = []) {
        $defaults = [
            'post_type' => $this->getPostTypeSlugs(),
            'post_status' => 'publish'
        ];

        return new \WP_Query(
            $this->array_merge_recursive_distinct($defaults, $args)
        );
    }

    /**
     * @param $tax
     * @param $term
     * @param int $limit
     * @param array $args
     *
     * @return \WP_Query
     * @deprecated
     */
    public function query_by_term($tax, $term, $limit = -1, $args = array()) {

        if($term instanceof \WP_Term) {
            $id = $term->slug;
        } else {
            $id = $term;
        }

        $args['tax_query'] = array(
            array(
                'taxonomy'  => $tax,
                'field'     => 'slug',
                'terms'     => $id
            )
        );

        $args['posts_per_page'] = $limit;

        return $this->query($args);
    }

    // TODO
    public function buttonText($id = null) {
        return get_field('button_text', $id) ?: 'Download';
    }

    // TODO
    public function getDate($format = 'n.j.Y', $id = null) {
        $date = false;

        if(get_field('show_date', $id)) {
            if(get_field('use_post_date', $id)) {
                $date = get_the_time($format, $id);
            } else {
                $date = date_format(date_create(get_field('start_date', $id)), $format);

                if(get_field('end_date', $id)) {
                    $date .= ' - ' . date_format(date_create(get_field('end_date', $id)), $format);
                }
            }
        }

        return $date;
    }

    public function getFormObject($post_id = null) {
        $form = get_field("form_select", $post_id);

        if (!$form || is_null($form)){
            $form = get_field("default_form", "resources");
        }

        if($form !== false && !is_null($form)) {
            if(is_numeric($form) || is_string($form)) {
                $id = $form;
                $form = (object) [];
                $form->id = $id;
            } else {
                $form = (object) $form;
            }
        } else {
            $form = null;
        }

        $form = apply_filters('KMDG/ResourceCenter/Form/Object', $form, $post_id);

        return $form;
    }

    public function getFormID($post_id = null){
        $form = $this->getFormObject($post_id);

    }

    /**
     * Retrieves the URL needed to download the resource. If $raw is true, the returned link will always be unmasked.
     *
     * @param int|null $id
     * @param bool $raw
     *
     * @return mixed|null|void
     */
    public function getDownloadURL($id = null, $raw = false) {
        if($this->downloadType($id) == 'file'
            && (!get_field('override_download_url', $id) || $raw)) {

            if($raw) {
                return wp_get_attachment_url($this->getFileID($id));
            }

            return get_permalink($id).'download/';
        } else {
            return get_field('download_url', $id);
        }
    }

    /**
     * Retrieves the ID of the uploaded resource file (or null if none is selected)
     * @param int|null $post
     *
     * @return mixed|null|void
     */
    public function getFileID($post = null) {
        if($this->downloadType($post) == 'file') {
            return get_field('file_download', $post);
        } else {
            return null;
        }
    }

    /**
     * Checks to see if a form fill out is required to access this resource.
     *
     * @param int|null $id
     *
     * @return mixed|null|void
     */
    public function requireForm($id = null)
    {
        return get_field('require_form', $id) && !is_null($this->getFormObject());
    }

    /**
     * Checks to see if we can skip the landing (single) view of the resource and go directly to the download link
     *
     * @param int|null $id
     *
     * @return mixed|null|void
     */
    public function skipLandingPage($id = null)
    {
        return get_field('skip_landing_page', $id)
            && !$this->requireForm($id)
            && ($this->downloadType($id) == 'url' || $this->downloadType($id) == 'file')
            && $this->getDownloadURL($id, true);
    }

    /**
     * Returns the download type of the resource
     *
     * @param int|null $id
     *
     * @return mixed|null|void
     */
    public function downloadType($id = null)
    {
        return get_field('download_type', $id);
    }

    /**
     * Checks to see if a video should be played immediately after filling out the form, or if it should be emailed
     * like a standard resource.
     *
     * @param int|null $post_id
     *
     * @return mixed|null|void
     */
    public function playOnPage($post_id = null)
    {
        return get_field('play_on_page', $post_id) && $this->requireForm($post_id);
    }

    /**
     * Retrieves the thumbnail image from YouTube or Vimeo and caches it.
     *
     * @param int|null $id
     *
     * @param bool $nocache
     *
     * @return mixed|bool
     */
    public function getVideoThumbnail($id = null, $nocache = false)
    {
        $rID = $id ?: get_the_ID();

        if($this->downloadType($rID) != 'video') return null;

        $video = $this->getVideoInfo($rID);

        // Soft cache
        $image = wp_cache_get("kmdg_resource_video_thumbnail_{$rID}_{$video->id}");

        if(!$image && !$nocache) {
            // Hard cache
            $image = get_transient("kmdg_resource_video_thumbnail_{$rID}_{$video->id}");
        }

        if(!$image) {

            // Huge try/catch block because potential several points of failure involving third party services
            try {
                $ch = \curl_init();

                \curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                \curl_setopt($ch, CURLOPT_URL, $video->api);
                \curl_setopt($ch, CURLOPT_VERBOSE, true);

                $data = \curl_exec($ch);

                \curl_close($ch);

                $image = [
                    'full' => null,
                    'large'=> null,
                    'thumb'=> null
                ];

                if($data !== false) {
                    if($video->type == 'wistia') {
                        $image = ['full' => $data, 'large' => $data, 'thumb' => $data];
                    } else {
                        $json = json_decode($data);

                        if($video->type == 'youtube') {
                            if(isset($json->items[0]->snippet->thumbnails->maxres)) {
                                $image['full'] = $json->items[0]->snippet->thumbnails->maxres->url;
                            } elseif (isset($json->items[0]->snippet->thumbnails->high)) {
                                $image['full'] = $json->items[0]->snippet->thumbnails->high->url;
                            } elseif (isset($json->items[0]->snippet->thumbnails->medium)) {
                                $image['full'] = $json->items[0]->snippet->thumbnails->medium->url;
                            } else {
                                $image['full'] = $json->items[0]->snippet->thumbnails->default->url;
                            }

                            if (isset($json->items[0]->snippet->thumbnails->high)) {
                                $image['large'] = $json->items[0]->snippet->thumbnails->high->url;
                            } elseif (isset($json->items[0]->snippet->thumbnails->medium)) {
                                $image['large'] = $json->items[0]->snippet->thumbnails->medium->url;
                            } else {
                                $image['large'] = $json->items[0]->snippet->thumbnails->default->url;
                            }

                            $image['thumb'] = $json->items[0]->snippet->thumbnails->default->url;

                        } else {
                            $bigSize = 0;
                            $smallSize = 9999999;

                            // Look for biggest & smallest image
                            foreach($json->data[0]->sizes as $thumb) {
                                if($bigSize < $thumb->width) {
                                    $image['large'] = $image['full']; // Store last biggest size as "large"
                                    $image['full'] = $thumb->link;
                                    $bigSize = $thumb->width;
                                }

                                if($smallSize > $thumb->width) {
                                    $image['thumb'] = $thumb->link;
                                    $smallSize = $thumb->width;
                                }
                            }

                            if(empty($image['large'])) {
                                $image['large'] = $image['full']; // Default if not enough image sizes
                            }
                        }
                    }
                }

                wp_cache_add("kmdg_resource_video_thumbnail_{$rID}_{$video->id}", $image);
                set_transient("kmdg_resource_video_thumbnail_{$rID}_{$video->id}", $image);
            } catch (\Exception $e) {
                $image = false;
            }
        }

        return (object) $image;
    }

    /**
     * Gets video information from the specified (or current) resource's download link
     *
     * @param int|null $post_id
     *
     * @return bool|mixed|object
     */
    public function getVideoInfo($post_id = null) {
        $embed = $this->getDownloadURL($post_id);
        $data = wp_cache_get($embed);

        if($data === false) {
            $data = (object) [
                'type'  => false,
                'id'    => null,
                'api'   => null,
                'url'   => null
            ];

            $vimeo = $this->getVimeoId($embed);
            $youtube = $this->getYoutubeId($embed);
            $wistia = $this->getWistiaId($embed);

            if($vimeo !== false) {
                $data->type = 'vimeo';
                $data->id = $vimeo;
                $data->api = "https://api.vimeo.com/videos/{$vimeo}/pictures?access_token={$this->vimeoAPIKey}&page=1&per_page=1";
                $data->url = "https://player.vimeo.com/video/{$vimeo}";
            } elseif($youtube !== false) {
                $data->type = 'youtube';
                $data->id = $youtube;
                $data->api = "https://www.googleapis.com/youtube/v3/videos?key={$this->youTubeAPIKey}&part=snippet&id={$youtube}";
                $data->url = "https://www.youtube.com/embed/{$youtube}";
            } elseif($wistia !== false) {
                $data->type = 'wistia';
                $data->id = $wistia;
                $data->api = "http://embed.wistia.com/deliveries/{$wistia}.jpg?video_still_time=0";
                $data->url = "http://embed.wistia.com/deliveries/{$wistia}.bin";
            }

            $data->embed = $embed;
            wp_cache_set($embed, $data);

        }

        return $data;
    }


    protected function getVimeoId($embed) {
        $matches = array();

        if(preg_match("/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/", $embed, $matches)) {
            return $matches[5];
        }

        return false;
    }

    protected function getWistiaId($embed) {
        $matches = array();

        if(preg_match("/(http(?:s)?:\/\/)?(kahua\.wistia\.com\/medias)+\/([a-z\d]*)/", $embed, $matches)) {
            return $matches[3];
        }elseif(preg_match("/(http(?:s)?:\/\/)?(commotionengine\.wistia\.com\/medias)+\/([a-z\d]*)/", $embed, $matches)) {
            return $matches[3];
        }
        return false;
    }

    protected function getYoutubeId($embed) {
        $matches = array();

        if(preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $embed, $matches)) {
            return $matches[1];
        }

        return false;
    }

    /**
     * Gets the subject line for emails sent by the resource center to users requesting downloads
     *
     * @return mixed|null|void
     */
    public function emailDownloadSubject()
    {
        return get_field('resource_email_subject', 'resources');
    }

    /**
     * Gets the body text for emails sent by the resource center to users requesting downloads
     *
     * @return mixed|null|void
     */
    public function emailDownloadBody()
    {
        return get_field('resource_email_body', 'resources');
    }

    /**
     * Gets the from address to be used when sending email downloads
     *
     * @return mixed|null|void
     */
    public function emailFromAddr() {
        return get_field('resource_email_from_address', 'resources');
    }

    /**
     * Detects if we are in the resource center
     */
    public function isResourceCenter() {
        $isRC = is_post_type_archive($this->slug)
            || is_singular($this->slug)
            || is_tax(get_object_taxonomies($this->slug));

        return apply_filters('KMDG/ResourceCenter/IsResourceCenter', $isRC, $this);
    }

    public function hasJumpListRows($post_id = null) {
        return have_rows('jump_list', $post_id);
    }

    /**
     * Gets field data for jump lists, must be run after hasJumpListRows
     * @return object
     */
    public function getJumpData() {
        the_row();
        $seconds = get_sub_field('seconds');
        $minutes = get_sub_field('minutes');
        $hours = get_sub_field('hours');

        $data = [
            'text' => get_sub_field('link_text'),
            'time' => $this->convertToSeconds($seconds, $minutes, $hours),
            'formattedTime' => sprintf("%02d:%02d:%02d", $hours, $minutes, $seconds)
        ];

        return (object) $data;
    }

    /**
     * Checks to see if video specific extras are associated with a video
     *
     * @param null $post_id
     *
     * @return bool
     */
    public function hasVideoExtras($post_id = null) {
        $hasExtras = ($this->hasVideoCollateral($post_id) || $this->hasVideoPresenters($post_id))
            && $this->downloadType($post_id) == 'video'
            && (($this->playOnPage($post_id) && $this->requireForm($post_id)) || !$this->requireForm($post_id));

        return apply_filters('KMDG/ResourceCenter/Video/HasExtras', $hasExtras, $post_id, $this);
    }

    public function hasVideoCollateral($post_id = null) {
        return have_rows('collateral_downloads', $post_id);
    }

    public function hasVideoPresenters($post_id = null) {
        return have_rows('video_presenters', $post_id);
    }

    /**
     * Gets field data for a single collateral download, must be run after hasVideoCollateral()
     *
     * @return object
     */
    public function getVideoCollateralData() {
        the_row();

        $data = [
            'name' => get_sub_field('name'),
            'file' => get_sub_field('file'),
            'fileName' => basename(get_sub_field('file'))
        ];

        return (object) $data;
    }

    public function getResourceImage($post_id = NULL, $nocache = false) {
        global $post;

        $image = false;

        if(is_null($post_id)) {
            $post_id = $post->ID;
        }

        // Allow themes/plugins to override the returned image
        $override = apply_filters('KMDG/ResourceCenter/SingleResourceImage/override', $image, $this, $post_id);

        if($override !== false) {
            $image = (object) $override;
        } elseif(has_post_thumbnail($post_id)) {
            $med = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'medium');
            $lg = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'large');
            $full = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'full');

            $image = (object) [
                'full'  => $full[0],
                'large' => $lg[0],
                'thumb' => $med[0]
            ];
        } elseif($this->downloadType($post_id) == 'video') {
            $image = $this->getVideoThumbnail($post_id, $nocache);
        } else {
            // Default to by term
            $term = get_the_terms($post_id, 'resource-type');

            if($term && get_field('default_resource_image', $term[0])) {
                $img = get_field('default_resource_image', $term[0]);
                $med = wp_get_attachment_image_src($img, 'medium');
                $lg = wp_get_attachment_image_src($img, 'large');
                $full = wp_get_attachment_image_src($img, 'full');

                $image = (object) [
                    'full'      => $full[0],
                    'large'     => $lg[0],
                    'thumb'     => $med[0]
                ];
            } else {
                $image = apply_filters('KMDG/ResourceCenter/SingleResourceImage/default', $image, $this, $post_id);
            }
        }

        return $image;
    }

    public function getLocationSorting($location = null)
    {
        // Default settings
        $settings = [
            'mode'  => 'menu_order',    // Sorting method (orderBy)
            'dir'   => 'asc',           // Display direction (order)
            'key'   => null,            // Meta key (for use with mode => 'meta_value')
            'type'  => null,            // What type of data the meta key is (meta_type)
        ];

        $sortMode = null;

        if(!is_null($location)) {
            if(have_rows('sorting_options', 'resources')) {
                while(have_rows('sorting_options', 'resources')) {
                    the_row();

                    if(get_sub_field('location') == $location) {
                        $sortMode = get_sub_field('sorting');

                        if($sortMode == 'date') {
                            $settings = [
                                'mode'  => 'meta_value',
                                'dir'   => 'desc',
                                'type'  => 'NUMERIC',
                                'key'   => 'sort_date'
                            ];

                            break;
                        } elseif($sortMode == 'alpha') {
                            $settings = [
                                'mode'  => 'title',
                                'dir'   => 'desc',
                                'key'   => null,
                                'type'  => null
                            ];

                            break;
                        }
                    }
                }
            }
        }

        $filtered = apply_filters('KMDG/ResourceCenter/Posts/order', $settings, $location, $sortMode, $this);

        $filtered["dir"] = strtolower($filtered["dir"]);

        return $filtered;
    }

    public function doEmailShortcodes($text, $link) {
        return do_shortcode(
            str_replace('[resource_type]', $this->findMainTerm(ResourceTypeTax::slug())->term->name,
                str_replace('[resource_name]', get_the_title(),
                    str_replace('[resource_download_link]', "<a href='$link'>$link</a>", $text))));
    }

    /**
     * Converts hours/mins/seconds to a seconds and sums them up.
     * @param $seconds
     * @param int $minutes
     * @param int $hours
     *
     * @return int
     */
    private function convertToSeconds($seconds, $minutes = 0, $hours = 0) {
        return ($hours * 60 * 60) + ($minutes * 60) + $seconds;
    }

    /**
     * array_merge_recursive does indeed merge arrays, but it converts values with duplicate
     * keys to arrays rather than overwriting the value in the first array with the duplicate
     * value in the second array, as array_merge does. I.e., with array_merge_recursive,
     * this happens (documented behavior):
     *
     * array_merge_recursive(array('key' => 'org value'), array('key' => 'new value'));
     *     => array('key' => array('org value', 'new value'));
     *
     * array_merge_recursive_distinct does not change the datatypes of the values in the arrays.
     * Matching keys' values in the second array overwrite those in the first array, as is the
     * case with array_merge, i.e.:
     *
     * array_merge_recursive_distinct(array('key' => 'org value'), array('key' => 'new value'));
     *     => array('key' => array('new value'));
     *
     * Parameters are passed by reference, though only for performance reasons. They're not
     * altered by this function.
     *
     * @param array $array1
     * @param array $array2
     * @return array
     * @author Daniel <daniel (at) danielsmedegaardbuus (dot) dk>
     * @author Gabriel Sobrinho <gabriel (dot) sobrinho (at) gmail (dot) com>
     */
    private function array_merge_recursive_distinct( array &$array1, array &$array2 )
    {
        $merged = $array1;

        foreach ( $array2 as $key => &$value )
        {
            if ( is_array ( $value ) && isset ( $merged [$key] ) && is_array ( $merged [$key] ) )
            {
                $merged [$key] = static::array_merge_recursive_distinct ( $merged [$key], $value );
            } else {
                $merged [$key] = $value;
            }
        }

        return $merged;
    }

    /**
     * Recurses term objects looking for the uppermost parent term.
     *
     * @param WP_Term $term
     *
     * @return WP_Term
     */
    private function findRootTerm(\WP_Term $term) {
        if(empty($term->parent)) {
            return $term;
        }

        return $this->findRootTerm(get_term($term->parent));
    }
}