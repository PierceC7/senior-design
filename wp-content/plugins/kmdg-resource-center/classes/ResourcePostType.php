<?php
/**
 * Created by PhpStorm.
 * User: KMDG
 * Date: 2/20/2018
 * Time: 11:57 AM
 */

namespace KMDG\ResourceCenter;

use Aesir\v1\Components\PostType as AesirPostType;
use Aesir\v1\Components\Taxonomy;
use Aesir\v1\Exceptions\AesirException;

class ResourcePostType extends AesirPostType
{
    /**
     * Triggers the handling of rewrites for this post type. To prevent rewrite, set to false. Defaults to true, using
     * slug(). To specify rewrite rules, an array can be passed with any of these keys:
     * 'slug': (string) Customize the permastruct slug. Defaults to slug() key.
     * 'with_front': (bool) Whether the permastruct should be prepended with WP_Rewrite::$front. Default true.
     * 'feeds': (bool) Whether the feed permastruct should be built for this post type. Default is value of hasArchive().
     * 'pages': (bool) Whether the permastruct should provide for pagination. Default true.
     * 'ep_mask': (const) Endpoint mask to assign. If not specified and permalink_epmask is set, inherits from
     *            $permalink_epmask. If not specified and permalink_epmask is not set, defaults to EP_PERMALINK.
     *
     * @return mixed
     */
    protected function rewrite()
    {
        return [
            'slug' => apply_filters('KMDG/ResourceCenter/PostType/slug', 'resource'),
            'with_front'=> false
        ];
    }

    /**
     * Returns the singular display name of the post type
     *
     * @return string
     */
    public function singularName()
    {
        return apply_filters('KMDG/ResourceCenter/PostType/singular_name', 'Resource');
    }

    /**
     * Returns the plural display name of the post type
     *
     * @return string
     */
    public function pluralName()
    {
        return apply_filters('KMDG/ResourceCenter/PostType/plural_name','Resources');
    }

    /**
     * Returns the name of the post type for how it should be displayed in the menu
     *
     * @return string
     */
    protected function menuName() {
        return apply_filters('KMDG/ResourceCenter/PostType/menu_name',"Resource Center");
    }

    /**
     * Returns the post type description
     *
     * @return string
     */
    protected function description()
    {
        return 'Resource center posts.';
    }

    /**
     * Whether a post type is intended for use publicly either via the admin interface
     * or by front-end users. While the default settings of $exclude_from_search,
     * $publicly_queryable, $show_ui, and $show_in_nav_menus are inherited from public,
     * each does not rely on this relationship and controls a very specific intention.
     *
     * @return bool
     */
    protected function isPublic()
    {
        return true;
    }

    /**
     * Whether the post type is hierarchical (e.g. page).
     *
     * @return bool
     */
    protected function isHierarchical()
    {
        return true;
    }

    /**
     * Whether to add the post type route in the REST API 'wp/v2' namespace.
     *
     * @return bool
     */
    protected function showInRest()
    {
        return true;
    }

    /**
     * The position in the menu order the post type should appear.
     * To work, showInMenu() must return true.
     *
     * @return int
     */
    protected function menuPosition()
    {
        return 11;
    }

    /**
     * Core feature(s) the post type supports. Serves as an alias for calling add_post_type_support() directly.
     * Core features include 'title', 'editor', 'comments', 'revisions', 'trackbacks', 'author', 'excerpt',
     * 'page-attributes', 'thumbnail', 'custom-fields', and 'post-formats'. Additionally, the 'revisions'
     * feature dictates whether the post type will store revisions, and the 'comments' feature dictates whether
     * the comments count will show on the edit screen.
     *
     * @return array
     */
    protected function supports()
    {
        return ['title', 'editor', 'revisions', 'excerpt', 'post-attributes', 'thumbnail', 'permalink', 'custom-fields'];
    }

    /**
     * An array of Taxonomy objects that will be registered for the post type.
     * @return Taxonomy[]|null
     * @throws \Exception
     */
    protected function registerTaxonomies()
    {
        $taxList = [];

        if(apply_filters('KMDG/ResourceCenter/Taxonomies/EnableLocations', true)) {
            $taxList[] = new ResourceLocationTax($this); //insert first in list
        }

        $taxList[] = new ResourceTypeTax($this);

        $taxonomies = apply_filters('KMDG/ResourceCenter/Taxonomies', $taxList, $this);

        foreach($taxonomies as $taxonomy) {
            if(!$taxonomy instanceof IResourceTaxonomy) {
                throw new \Exception('Registered taxonomies must implement IResourceTaxonomy.');
            }
        }

        return $taxonomies;
    }

    /**
     * Whether there should be post type archives, or if a string, the archive slug to use.
     * Will generate the proper rewrite rules if $rewrite is enabled.
     *
     * @return bool|string
     */
    protected function hasArchive()
    {
        return true;
    }
}