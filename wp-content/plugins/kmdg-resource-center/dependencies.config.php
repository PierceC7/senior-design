<?php
use function DI\factory;

use KMDG\ResourceCenter\ResourceCenter;
use KMDG\ResourceCenter\ResourceModel;

return [
    ResourceCenter::class => factory(function() {
        return ResourceCenter::getInstance();
    }),

    ResourceModel::class => factory(function() {
        return ResourceCenter::getInstance()->getModel();
    }),
];