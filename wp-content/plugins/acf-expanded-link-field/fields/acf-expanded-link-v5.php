<?php

// exit if accessed directly
if( ! defined( 'ABSPATH' ) ) exit;


// check if class already exists
if( !class_exists('acf_field_menu_select') ) :


class acf_field_expanded_link extends acf_field {
	
	
	/*
	*  __construct
	*
	*  This function will setup the field type data
	*
	*  @type	function
	*  @date	5/03/2014
	*  @since	5.0.0
	*
	*  @param	n/a
	*  @return	n/a
	*/
	
	function __construct( $settings ) {
		
		/*
		*  name (string) Single word, no spaces. Underscores allowed
		*/
		
		$this->name = 'link_ex';
		
		
		/*
		*  label (string) Multiple words, can include spaces, visible when selecting a field type
		*/
		
		$this->label = __('Expanded Link Field', 'acf-expanded-link-field');
		
		
		/*
		*  category (string) basic | content | choice | relational | jquery | layout | CUSTOM GROUP NAME
		*/
		
		$this->category = 'relational';
		
		
		/*
		*  defaults (array) Array of default settings which are merged into the field object. These are used later in settings
		*/

        $this->defaults = array(
            'allow_multiple' => 0,
            'allow_null' => 0
        );

		
		/*
		*  l10n (array) Array of strings that are used in JavaScript. This allows JS strings to be translated in PHP and loaded via:
		*  var message = acf._e('FIELD_NAME', 'error');
		*/
		
		$this->l10n = array(
			'error'	=> __('Error! Please enter a higher value', 'acf-expanded-link-field'),
		);
		
		
		/*
		*  settings (array) Store plugin settings (url, path, version) as a reference for later use with assets
		*/
		
		$this->settings = $settings;
		
		
		// do not delete!
    	parent::__construct();
    	
	}
	
	
	/*
	*  render_field_settings()
	*
	*  Create extra settings for your field. These are visible when editing a field
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field (array) the $field being edited
	*  @return	n/a
	*/
	
	function render_field_settings( $field ) {
		
		/*
		*  acf_render_field_setting
		*
		*  This function will create a setting for your field. Simply pass the $field parameter and an array of field settings.
		*  The array of settings does not require a `value` or `prefix`; These settings are found from the $field array.
		*
		*  More than one setting can be added by copy/paste the above code.
		*  Please note that you must also have a matching $defaults value for the field name (font_size)
		*/

        acf_render_field_setting( $field, array(
            'label' => 'Allow Null?',
            'type'  =>  'radio',
            'name'  =>  'allow_null',
            'choices' =>  array(
                1 =>  __("Yes",'acf'),
                0 =>  __("No",'acf'),
            ),
            'layout'  =>  'horizontal'
        ));
	}
	
	
	
	/*
	*  render_field()
	*
	*  Create the HTML interface for your field
	*
	*  @param	$field (array) the $field being rendered
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field (array) the $field being edited
	*  @return	n/a
	*/

	function render_field($field) {
		/*
		*  Review the data of $field.
		*  This will show what data is available
		*/

		$pages = $this->get_pages();
        $post_types = get_post_types(['public' => true], 'objects');
        ?>

        <select name="<?php echo esc_attr($field['name']) ?>">
            <option value="">Select a Page...</option>

            <optgroup label="Archives">
                <?php foreach($post_types as $post_type) : ?>
                    <?php $val = 'A|'.$post_type->name ?>
                    <option value="<?= esc_attr($val) ?>"
                        <?= $field['value'] == $val ? 'selected="selected"' : '' ?>>
                        <?= $post_type->labels->name; ?>
                    </option>

                    <?php $terms = apply_filters('acf_expanded_link_field/archive_terms_options', [], $post_type); ?>
                    <?php foreach($terms as $term): /** @var WP_Term $term */ ?>
                        <?php $val = 'T|'.$term->slug.'|'.$term->taxonomy.'|'.$post_type->name ?>
                        <option value="<?= esc_attr($val) ?>"
                            <?= $field['value'] == $val ? 'selected="selected"' : '' ?>>
                            - <?= $term->name; ?>
                        </option>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </optgroup>

            <optgroup label="Pages">
                <?php $this->recursive_page_options($field, $pages); ?>
            </optgroup>

        </select>

        <?php
	}

	function get_pages($parent = 0) {
	    return get_pages([
             'sort_order' => 'ASC',
             'sort_column' => 'post_title',
             'parent' => $parent,
             'post_type' => 'page',
             'post_status' => 'publish'
        ]);
    }

    function recursive_page_options($field, $pages, $level = 0) {
	    foreach($pages as $page) {
            $val = 'P|' . $page->ID;

            echo '<option value="' . esc_attr($val) . '" ' . ($field['value'] == $val ? 'selected="selected"' : '') . '>';

            for ($i = 0; $i < $level; $i++) {
                echo "&mdash; ";
            }

            echo $page->post_title;

            echo '</option>';

            $children = $this->get_pages($page->ID);

            if(count($children) > 0) {
                $this->recursive_page_options($field, $children, $level+1);
            }
        }

    }

    /*
	*  format_value()
	*
	*  This filter is applied to the $value after it is loaded from the db and before it is returned to the template
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value (mixed) the value which was loaded from the database
	*  @param	$post_id (mixed) the $post_id from which the value was loaded
	*  @param	$field (array) the field array holding all the field options
	*
	*  @return	$value (mixed) the modified value
	*/
	function format_value( $value, $post_id, $field ) {

		// bail early if no value
		if( empty($value) ) {

			return (object) [
                'type'      => 'none',
                'id'        => 0,
                'link'      => '#',
                'tax'       => 'none',
                'postType'  => 'none',
            ];

		}

		$data = explode('|', $value);

		if(count($data) < 2) {
		    return [
                'type'      => 'none',
                'id'        => 0,
                'link'      => '#',
                'tax'       => 'none',
                'postType'  => 'none',
            ];
        }

        $type = $data[0];
		$id = $data[1];
        $link = '';
        $formatted = null;

		if($type == 'A') {
		    $link = get_post_type_archive_link($id);

            $formatted = (object) [
                'type'  => 'archive',
                'id'    => $id,
                'link'  => $link
            ];
        } elseif($type == 'P') {
		    $link = get_the_permalink($id);

            $formatted = (object) [
                'type'  => 'page',
                'id'    => $id,
                'link'  => $link
            ];
        } else {
		    $tax = $data[2];
		    $postType = $data[3];

            $formatted = (object) [
                'type'      => 'term',
                'id'        => $id,
                'tax'       => $tax,
                'postType'  => $postType,
                'link'      => get_term_link($id, $tax)
            ];
        }

		// return
		return $formatted;
	}
}


// initialize
new acf_field_expanded_link( $this->settings );


// class_exists check
endif;

?>