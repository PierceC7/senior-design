<?php
/*
 * Template Name: Resources
 */
get_header(); ?>
<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
        <?php theme()->part('site', 'wrapper', 'top'); ?>

            <?php theme()->part('title', 'banner') ?>

            <div class="u-builder-container-width">
                <div class="row">
                    <div class="col-md-8">
                        <?php
                        theme()->part('resources', 'featured', NULL, [
                            'type' => get_field('featured_resource'),
                            'query' => Resource()->query_by_type(get_sub_field('featured_resource'), 3)
                        ]);
                        ?>
                    </div>

                    <div class="col-md-4">
                        <?php theme()->part('resources', 'buttons'); ?>
                    </div>
                </div>

                <?php theme()->part('resources', 'cards'); ?>
            </div>


        <?php theme()->part('site', 'wrapper', 'bottom'); ?>
    <?php endwhile;?>
<?php endif;?>

<?php get_footer();