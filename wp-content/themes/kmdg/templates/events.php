<?php
/*
Template Name: Event Layout
*/
/**
 *
 * @package WordPress
 * @subpackage kmdg
 *
 * This is the custom template for Events set up by The Event Calendar
 */

get_header(); ?>
    <?php if(have_posts()): ?>
        <?php while(have_posts()): the_post(); ?>
            <?php theme()->part('site', 'wrapper', 'top'); ?>
                <?php theme()->part('layout', 'event'); ?>
            <?php theme()->part('site', 'wrapper', 'bottom'); ?>
        <?php endwhile; ?>
    <?php endif; ?>
<?php get_footer();