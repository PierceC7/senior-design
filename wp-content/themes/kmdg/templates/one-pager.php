<?php
/*
Template Name: One Pager (Homepage)
*/
/**
 * @package WordPress
 * @subpackage kmdg
 *
 * This template looks for child pages of the page it is assigned to. It loops through
 * each of the pages and attempts to load a template part named using this convention:
 *
 * "layout-{template_slug}-{onepage}.php" (so, for example, using this with the default
 * template would look for a file called "layout-page-onepage.php" and if that fails it
 * will look for "layout-page.php" instead, use the "onepage" subtype only if required
 * when setting up your templates)
 *
 * It also wraps each page in html and assigns several identifying classes and a unique ID
 * taken from the page's "slug" in wordpress.
 */

$query = array(
    'post_parent'               => $post->ID,
    'post_type'                 => 'page',
    'order_by'                  => 'menu_order',
    'order'                     => 'ASC',
    'posts_per_page'            => -1
);

get_header(); ?>

<?php $page_content = new WP_Query($query); ?>
<?php if($page_content->have_posts()) : ?>
    <?php while ($page_content->have_posts()) : $page_content->the_post(); ?>
        <?php theme()->part('site', 'wrapper', 'top'); ?>
            <?php theme()->part('layout', theme()->template_name(), 'onepage'); ?>
        <?php theme()->part('site', 'wrapper', 'bottom'); ?>
    <?php endwhile; ?>
    <?php wp_reset_query(); ?>
<?php endif; ?>

<?php get_footer() ?>