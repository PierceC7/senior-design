<?php
/**
 * @package WordPress
 * @subpackage kmdg
 *
 * This is the default WordPress template - unless otherwise edited, this
 * is compatible with the OnePager template. This is loaded via page.php
 * by default.
 */
get_header(); ?>
    <?php if(have_posts()): ?>
        <?php while(have_posts()): the_post(); ?>
            <?php theme()->part('site', 'wrapper', 'top'); ?>
                <?php theme()->part('layout', 'page'); ?>
            <?php theme()->part('site', 'wrapper', 'bottom'); ?>
        <?php endwhile; ?>
    <?php endif; ?>
<?php get_footer();