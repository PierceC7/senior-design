<?php
/*
Template Name: Homepage (Custom)
*/
/**
 * @package WordPress
 * @subpackage kmdg
 *
 * This is the homepage template - fill it in with something neat!
 */

get_header(); ?>
    <?php if(have_posts()): ?>
        <?php while(have_posts()): the_post(); ?>
            <?php theme()->part('site', 'wrapper', 'top'); ?>
                <?php theme()->part('layout', 'home'); ?>
            <?php theme()->part('site', 'wrapper', 'bottom'); ?>
        <?php endwhile; ?>
    <?php endif; ?>
<?php get_footer(); ?>