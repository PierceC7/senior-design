<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>
<form role="search" method="get" class="searchform" action="<?php echo esc_url(get_bloginfo('url')); ?>">
    <div>
        <input type="text" value="<?php get_search_query(); ?>" name="s" class="search-term" placeholder="SEARCH" />
    </div>
</form>