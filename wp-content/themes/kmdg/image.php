<?php
/**
 * @package WordPress
 * @subpackage kmdg
 *
 * This template is used to render WordPress attachment pages, which are evil.
 * Instead, we want to direct anyone reaching this template to the parent page of the
 * attachment they are trying to access, or failing that, the attachement itself.
 */

if($post->post_parent != 0) {
    wp_redirect(get_permalink($post->post_parent), 301);
} else {
    wp_redirect(wp_get_attachment_url($post->ID), 301);
}

exit;