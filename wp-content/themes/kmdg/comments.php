<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */

// Do not delete these lines, these are to prevent common spam/exploit techniques
    if(isset($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME'])) :
        die('Please do not load this page directly. Thanks!');
    endif;

    if(post_password_required()) : ?>
        <p class="nocomments">
            <?php _e('This post is password protected. Enter the password to view comments.', 'default'); ?>
        </p>
        <?php
        return;
    endif;

// Edit below here ?>

<section id="comments">
    <h3>Comments</h3>
    <div class="comment-list">
        <?php if (have_comments()) : ?>
            <?php wp_list_comments(); ?>
            <?php paginate_comments_links(); ?>
        <?php else : // this is displayed if there are no comments so far ?>
            <?php if (comments_open()) : // Comments open, no comments yet ?>

        	<?php else : // comments are closed ?>
        	   <p class="nocomments">
        	       <?php _e('Comments are closed.', 'default'); ?>
        	   </p>
        	<?php endif; ?>
        <?php endif; ?>
    </div>
</section>

<?php if(comments_open()) : ?>
    <?php $req = get_option('require_name_email'); ?>
    <div id="respond">
        <h3><?php comment_form_title( __('Leave a Reply', 'default'), __('Leave a Reply for %s' , 'default') ); ?></h3>

        <div id="cancel-comment-reply">
        	<small><?php cancel_comment_reply_link() ?></small>
        </div>

        <?php if(get_option('comment_registration') && !is_user_logged_in()) : ?>
            <p><?php printf(__('You must be <a href="%s">logged in</a> to post a comment.', 'default'), wp_login_url( get_permalink() )); ?></p>
        <?php else : ?>

            <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">

                <?php if (is_user_logged_in()) : ?>
                    <p>
                        <?php printf(__('Logged in as <a href="%1$s">%2$s</a>.', 'default'), get_option('siteurl') . '/wp-admin/profile.php', $user_identity); ?>
                        <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php _e('Log out of this account', 'default'); ?>">
                            <?php _e('Log out &raquo;', 'default'); ?>
                        </a>
                    </p>
                <?php else : ?>

                <p>
                    <input type="text" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" size="22" tabindex="1" <?php if ($req) echo 'required="required"'; ?> />
                    <label for="author">
                        <small><?php _e('Name', 'default'); ?> <?php if ($req) _e("*", "default"); ?></small>
                    </label>
                </p>

                <p>
                    <input type="text" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" size="22" tabindex="2" <?php if ($req) echo 'required="required"'; ?> />
                    <label for="email">
                        <small><?php _e('Mail (will not be published)', 'default'); ?> <?php if ($req) _e("*", "default"); ?></small>
                    </label>
                </p>

                <p>
                    <input type="text" name="url" id="url" value="<?php echo  esc_attr($comment_author_url); ?>" size="22" tabindex="3" />
                    <label for="url">
                        <small><?php _e('Website', 'default'); ?></small>
                    </label>
                </p>

            <?php endif; ?>

            <p>
                <small><?php printf(__('<strong>XHTML:</strong> You can use these tags: <code>%s</code>', 'default'), allowed_tags()); ?></small>
            </p>

            <p>
                <textarea name="comment" id="comment" cols="58" rows="10" tabindex="4"></textarea>
            </p>

            <p>
                <input name="submit" type="submit" id="submit" tabindex="5" value="<?php _e('Submit Comment', 'default'); ?>" />
                <?php comment_id_fields(); ?>
            </p>
            <?php do_action('comment_form', $post->ID); ?>
            </form>

        <?php endif; ?>
    </div><!-- respond -->

<?php endif; ?>
