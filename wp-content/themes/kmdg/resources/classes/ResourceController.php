<?php

namespace KMDG\Theme;

use Aesir\v1\Aesir;
use Aesir\v1\Components\Request;
use Aesir\v1\Components\Template\TemplateLocator;
use Aesir\v1\Components\View;
use Aesir\v1\Factories\PartFactory;
use KMDG\ResourceCenter\ResourceCenter;
use KMDG\ResourceCenter\ResourceController as PluginResourceController;
use KMDG\ResourceCenter\ResourceLocationTax;
use KMDG\ResourceCenter\ResourceModel;
use KMDG\Theme\Model as ThemeModel;
use WP_Query;

class ResourceController extends PluginResourceController
{
    public function __construct(ResourceModel $model, Aesir $aesir, TemplateLocator $locator, PartFactory $partFactory)
    {
        parent::__construct($model, $aesir, $locator, $partFactory);

        $this->locator->addThemePath('resources/templates', 1);
        $ThemeModel = new ThemeModel();
    }

    public function archive(View $view, ResourceCenter $rc, Request $request, WP_Query $query) {
        $postType = get_post_type();

        if(!($location = $this->model->getCurrentLocation())) {
            $location = 'all';
        }

        $template = $this->locator->find('archive', $location);

        $currentTerm = get_queried_object();

        $bannerPart = $this->partFactory->make('partials', 'banner', null,[
            'pageTitle'     => get_field('resource_center_title','resources'),
            'bgImage'       => theme()->utilities()->src(get_field('resource_banner', 'resources'), 'full'),
            'displayDate'   => false
        ]);

        $navPart = $this->partFactory->make('partials', 'nav', null, [
        	'archive'   => true,
        ]);

        $tilePart = $this->partFactory->make('partials', 'tile', $location,[
            'model'         => $this->model,
            'currentTerm'  => $currentTerm
        ]);

	    $singlePart = $this->partFactory->make('partials', 'main', 'single', [
		    'model'         => $this->model,
		    'currentTerm'   => null,
		    'resourceQuery' => null,
		    'moreLink'      => null,
		    'tilePart'      => $tilePart
	    ]);

        $mainPart = $this->partFactory->make('partials', 'main', null,[
            'model'         => $this->model,
            'currentTerm'   => $currentTerm,
            'tilePart'      => $tilePart,
            'resourceQuery' => $this->model->queryPosts(4,$location)
        ]);

        $paginationPart = $this->partFactory->make('partials', 'pagination');

        $featuredPart = $this->partFactory->make('partials', 'featured', $request->slug(), [
            'model'         => $this->model,
            'featuredQuery' => $this->model->queryPosts(1, null, $location,[
                'post__in'              => [get_field('featured_resource','resources')->ID],
                'ignore_sticky_posts'   => true,
            ]),
        ]);

        return $view->template($template)->make([
            'bannerPart'    => $bannerPart,
            'navPart'       => $navPart,
            'featuredPart'  => $featuredPart,
            'mainPart'      => $mainPart,
            'tilePart'      => $tilePart,
	        'singlePart'    => $singlePart,
            'model'         => $this->model
        ]);
    }

    public function taxonomy(View $view, ResourceCenter $rc, Request $request) {
        $postType = get_post_type();

        if(!($location = $this->model->getCurrentLocation())) {
            $location = 'all';
        }

        $template = $this->locator->find('landing', $location);

        $currentTerm = get_queried_object();

        $bannerPart = $this->partFactory->make('partials', 'banner', null,[
            'pageTitle'     => get_field('resource_center_title','resources'),
            'bgImage'       => theme()->utilities()->src(get_field('resource_banner', 'resources'), 'full'),
            'displayDate'   => false
        ]);

        $navPart = $this->partFactory->make('partials', 'nav', null, [
        	'archive'   => false,
        ]);

        $tilePart = $this->partFactory->make('partials', 'tile', $location,[
            'model'         => $this->model,
            'currentTerm'  => $currentTerm
        ]);

        $mainPart = $this->partFactory->make('partials', 'main', null,[
            'model'         => $this->model,
            'currentTerm'  => $currentTerm,
            'tilePart'      => $tilePart,
            'resourceQuery' => $this->model->queryPosts(-1,$currentTerm->slug,$location)
        ]);

        $paginationPart = $this->partFactory->make('partials', 'pagination');

        $featuredPart = $this->partFactory->make('partials', 'featured', 'single', [
            'model'         => $this->model,
            'featuredQuery' => $this->model->queryPosts(-1, $currentTerm->slug, $location,[
                'ignore_sticky_posts'   => true,
	            'tax_query' => [
	            	[
	            	    'taxonomy'  => ResourceFeaturedTax::slug(),
		                'field'     => 'slug',
		                'terms'     => array_keys($this->model->allTerms(ResourceFeaturedTax::slug()))
		            ]
	            ]
            ]),
        ]);

        return $view->template($template)->make([
            'bannerPart'    => $bannerPart,
            'navPart'       => $navPart,
            'featuredPart'  => $featuredPart,
            'mainPart'      => $mainPart,
            'tilePart'      => $tilePart
        ]);
    }

    public function single(View $view, \WP_Post $post)
    {
        $type       = $this->model->downloadType();
        $template   = $this->locator->find('single', $type);
	    $ThemeModel = new ThemeModel();
        $form               = $this->model->getFormObject();
        $image              = $this->model->getResourceImage();
        $hasForm            = $this->model->requireForm();
        $hasVideoExtras     = $this->model->hasVideoExtras();
        $hasCollateral      = $this->model->hasVideoCollateral();
        $mainTerm           = $this->model->findMainTerm('resource-type')->term;
        $bannerImg          = theme()->utilities()->src(get_field('resource_banner', 'resources'), 'full');
        $collateralContent  = null;
        $jumplistContent    = null;
        $hasLeftSidebar     = true;
        $hasRightSidebar    = $this->model->getResourceImage() && !$this->model->embedPDF();
        $videoContent       = null;
        $sidebarLeftContent = null;
        $presenterContent   = null;

        $classes = [
            "resource--download-type__{$type}",
        ];

        if($this->model->embedPDF()) {
            $classes[] = 'resource--pdf-embedded';
        }

        if($this->model->displayDownloadButton()) {
            $classes[] = 'resource--has-download-button';
        }

        if($this->model->requireForm()) {
            $classes[] = 'resource--form-required';
        }

        if($hasForm) {
            $classes[] = 'resource--has-LeftSidebar';
        } else {
            $hasLeftSidebar = false;
        }

        if($hasRightSidebar) {
            $classes[] = 'resource--has-RightSidebar';
        }

        if($hasVideoExtras) {
            $classes[] = 'resource--has-video-extras';
        }
        
        foreach($this->model->getResourceLocations() as $location) {
            $classes[] = 'resource--location__'.$location->slug;
        }

        $formContent = $this->partFactory->make('partials', 'form', null, [
            'model'         => $this->model,
            'form'          => $form,
            'hasForm'       => $hasForm
        ]);

        if($type == 'video') {
            $videoContent = $this->partFactory->make('partials', 'video', null, [
                'model'         => $this->model,
                'hasForm'       => $hasForm,
                'video'         => $this->model->getVideoInfo(),
                'placeholder'   => $image,
            ]);

            $presenterContent = $this->partFactory->make('partials', 'video', 'presenters', [
                'model' => $this->model
            ]);
            $jumplistContent = $this->partFactory->make('partials', 'video', 'jumplist', [
                'model' => $this->model
            ]);
        }

        if($hasLeftSidebar) {
            $sidebarLeftContent = $this->partFactory->make('partials', 'sidebar', null, [
                'model'         => $this->model,
                'formContent'   => $formContent,
            ]);
        }

        if($hasRightSidebar) {
            $sidebarRightContent = $this->partFactory->make('partials', 'sidebar', 'right', [
                'model'         => $this->model,
            ]);
        } else {
        	$sidebarRightContent = null;
        }

        if($hasCollateral) {
            $collateralContent = $this->partFactory->make('partials', 'collateral', null, [
                'hasDownloads'  => $hasCollateral,
                'model'         => $this->model
            ]);
        }

        $mainContent = $this->partFactory->make('partials', 'content', $type, [
            'model'             => $this->model,
        ]);

        if(get_field('banner_image', $mainTerm)) {
            $bannerImg = theme()->utilities()->src(get_field('banner_image', $mainTerm), 'full');
        }

        $bannerPart = $this->partFactory->make('partials', 'banner-single', null,[
            'pageTitle'     => get_the_title(),
            'bgImage'       => $bannerImg,
            'preTitle'      => get_field('location') ?: $ThemeModel->singular($mainTerm),
            'displayDate'   => $this->model->getDate('F d, Y')
        ]);

        $navPart = $this->partFactory->make('partials', 'nav', null, [
        	'archive'   => false,
        	]);

        return $view->template($template)->make([
            'resource'              => $post,
            'model'                 => $this->model,
            'classes'               => implode(' ', $classes),
            'bannerPart'            => $bannerPart,
            'mainContent'           => $mainContent,
            'navPart'               => $navPart,
            'formContent'           => $formContent,
            'sidebarLeftContent'    => $sidebarLeftContent,
            'sidebarRightContent'   => $sidebarRightContent,
            'hasLeftSidebar'        => $hasLeftSidebar,
            'hasRightSidebar'       => $hasRightSidebar,
            'hasVideoExtras'        => $hasVideoExtras,
            'collateralContent'     => $collateralContent,
            'videoContent'          => $videoContent,
            'presenterContent'      => $presenterContent,
            'jumplistContent'       => $jumplistContent,
            'executiveSummary'      => get_field('executive_summary')
        ]);
    }
}