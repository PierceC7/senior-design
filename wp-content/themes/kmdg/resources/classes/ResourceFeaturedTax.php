<?php
/**
 * Created by PhpStorm.
 * User: KMDG
 * Date: 2/21/2018
 * Time: 3:50 PM
 */

namespace KMDG\Theme;

use KMDG\ResourceCenter\IResourceTaxonomy;

require_once("ResourceCustomTaxBase.php");

class ResourceFeaturedTax extends ResourceCustomTaxBase implements IResourceTaxonomy
{
    /**
     * Returns the slug of the post type
     *
     * @return string
     */
    public static function slug()
    {
        return 'resource-featured';
    }

    public function friendly()
    {
        return 'featured';
    }

    public function showFilter()
    {
        return false;
    }

    protected function showAdminFilter()
    {
        return true;
    }

    /**
     * Returns the singular readable name of the post type
     *
     * @return string
     */
    public function singular()
    {
        return 'Featured';
    }

    /**
     * Returns the plural readable name of the post type
     *
     * @return string
     */
    public function plural()
    {
        return 'Featured';
    }

    /**
     * Include a description of the taxonomy
     *
     * @return string
     */
    protected function description()
    {
        return 'For organizing resources by Featured';
    }

    protected function showAdminColumn()
    {
        return true;
    }

    protected function showUI() {
	    return false;
    }

	protected function showInQuickEdit() {
		return true;
	}
}