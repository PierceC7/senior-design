<?php
/**
 * Created by PhpStorm.
 * User: KMDG
 * Date: 10/29/2018
 * Time: 3:40 PM
 */

namespace KMDG\Theme;

use KMDG\ResourceCenter\ResourceModel;
use WP_Query;
use WP_Term;

class Model
{
    public function getCategories(ResourceModel $resources) {
        $categories = [];

        if(have_rows('category_buttons', 'option')) {
            while(have_rows('category_buttons', 'option')) {
                the_row();

                $category = get_sub_field('single_category');

                $categories[] = (object) [
                    'query' => $resources->query_by_term('resource-type', $category->slug, 3),
                    'term' => $category
                ];
            }
        }

        return $categories;
    }


	public function singular(WP_Term $mainTerm){
		return get_field('singular_name', $mainTerm);
	}
}