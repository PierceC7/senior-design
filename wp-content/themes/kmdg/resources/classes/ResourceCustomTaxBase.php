<?php
/**
 * Created by PhpStorm.
 * User: KMDG
 * Date: 2/27/2018
 * Time: 2:28 PM
 */

namespace KMDG\Theme;

use Aesir\v1\Components\Taxonomy;

abstract class ResourceCustomTaxBase extends Taxonomy
{

    /**
     * Disable rewrites to disable archive view while still allowing the taxonomy to be queried on the frontend.
     *
     * @return bool
     */
    protected function rewrite()
    {
        return false;
    }

    /**
     * Whether a taxonomy is intended for use publicly either via the admin interface or by front-end users.
     *
     * @return bool
     */
    protected function isPublic()
    {
        return false;
    }

    /**
     * Whether to generate a default UI for managing this taxonomy.
     *
     * @return bool
     */
    protected function showUI() {
        return true;
    }

    /**
     * Whether to include the taxonomy in the REST API.
     *
     * @return bool
     */
    protected function showInRest()
    {
        return true;
    }

    /**
     * Whether to allow the Tag Cloud widget to use this taxonomy.
     *
     * @return bool
     */
    protected function showTagcloud()
    {
        return false;
    }

    /**
     * Whether to allow automatic creation of taxonomy columns on associated post-types table.
     *
     * @return bool
     */
    protected function showAdminColumn()
    {
        return true;
    }

    /**
     * Whether this taxonomy should remember the order in which terms are added to objects.
     *
     * @return bool
     */
    protected function sort()
    {
        return true;
    }

    /**
     * Is this taxonomy hierarchical (have descendants) like categories or not hierarchical like tags.
     *
     * @return bool
     */
    protected function isHierarchical()
    {
        return true;
    }
}