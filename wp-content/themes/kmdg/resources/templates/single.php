<?php
/** @var \KMDG\ResourceCenter\ResourceModel $model */
?>
<?php get_header(); ?>
<?php if (have_posts()): the_post(); ?>
    <section class="kmdg-resource-center resource resource--single <?= $classes ?>">

            <?= $bannerPart ?>

<!--            <div class="resource_menu">-->
<!--                <div class="resource--channel">-->
<!--                    --><?//= $navPart; ?>
<!--                </div>-->
<!--            </div>-->

        <div class="resource--channel">
            <div class="resource__layout">
                <div class="resource__wrapper">

                    <?php if ($hasLeftSidebar): ?>
                        <aside class="resource__left_wrapper">
                            <div class="resource__left_content">
                                <?= $sidebarLeftContent ?>
                            </div>
                        </aside>
                    <?php endif; ?>

                    <main class="resource__main_wrapper">
                        <div class="resource__main_content">
                            <?= $mainContent ?>
                        </div>
                    </main>

                    <?php if ($hasRightSidebar): ?>
                        <aside class="resource__right_wrapper">
                            <div class="resource__right_content">
                                <?= $sidebarRightContent ?>
                            </div>
                        </aside>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

<?php endif; ?>


<?php get_footer(); ?>
