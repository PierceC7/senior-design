<?php /** @var \KMDG\ResourceCenter\ResourceModel $model */ ?>
<?php if($model->requireForm()): ?>
    <div class="resource--form" id="download">
        <div class="resource--form__title">
            <?php if(get_field('action_text_override')): ?>
                <?= get_field('action_text_override') ?>
            <?php else: ?>
                Get the <?= $model->typeNiceName() ?>
            <?php endif; ?>
        </div>

        <form id="mktoForm_<?= $form->id ?>"></form>

        <script>
            MktoForms2.loadForm("//app-ab11.marketo.com", "690-OLN-597", <?= $form->id ?>, function(form) {


                jQuery(window).on('load', function() {
                    try {
                        // Attempt to fill hidden Asset Name field with resource title
                        form.setValues({'Asset_Name__c': kmdgResourceCenter.resourceName});
                    } catch (e) {}
                });

                form.onSuccess(function() {
                    var vals = form.getValues();
                    var email = vals.Email;

                    jQuery(window).trigger('kmdg_rc_form_submitted', [email, form.getId(), function(response) {
                        jQuery(form.getFormElem()).html(response);
                    }]);

                    return false;
                });
            });
        </script>
    </div>
<?php endif; ?>