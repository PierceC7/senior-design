<div class="resource--title-banner resource--title-banner__single" style="background-image: url(<?= $bgImage ?>);">
    <div class="resource--channel">
        <?php if(!empty($preTitle)): ?>
            <div class="resource--title-banner__pretitle">
                <?= $preTitle ?>
            </div>
        <?php endif; ?>

        <div class="resource--title-banner__title">
            <h1><?= $pageTitle ?></h1>
        </div>

        <?php if($displayDate): ?>
            <div class="resource--title-banner__date">
                <?= $displayDate ?>
            </div>
        <?php endif; ?>
    </div>
</div>