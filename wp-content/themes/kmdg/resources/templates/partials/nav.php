<?php
    $blogid = get_option('page_for_posts');
?>

<div class="tab_nav">
    <div class="tab_nav__wrap">
        <div class="tab_nav__single tab_nav__single--<?= $archive ? 'active' : 'inactive' ?>">
            <a class="tab_nav__button" href="/resource/">
                All Resources
            </a>
        </div>
    </div>
    <?php if (have_rows('category_buttons', 'resources')) : ?>
        <?php while (have_rows('category_buttons', 'resources')) : the_row();
                $cat = get_sub_field('single_category');
            ?>

            <div class="tab_nav__wrap">
                <div class="tab_nav__single tab_nav__single--<?= is_tax('resource-type', $cat->term_id) ? 'active' : 'inactive' ?>">
                    <a class="tab_nav__button" href="<?= get_category_link($cat->term_id) ?>">
                        <?= $cat->name; ?>
                    </a>
                </div>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>

    <div class="tab_nav__wrap">
        <div class="tab_nav__single tab_nav__single--<?= is_home() ? 'active' : 'inactive' ?>">
            <a class="tab_nav__button" href="<?= get_permalink($blogid); ?>">
                Blog
            </a>
        </div>
    </div>
</div>