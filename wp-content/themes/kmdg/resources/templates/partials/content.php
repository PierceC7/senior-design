<?php /** @var \KMDG\ResourceCenter\ResourceModel $model */ ?>
<div class="resource--content">

    <?php if(get_field('overview')): ?>
        <div class="resource--overview">
            <div class="resource--overview__title">
                <?= $model->typeNiceName() ?> Overview
            </div>

            <div class="resource--overview__content">


                <div class="resource--overview__text">
                    <?= get_field('overview') ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="resource--main-content">
        <?php if(get_field('subtitle')): ?>
            <h2 class="resource--main-content__subtitle">
                <?= get_field('subtitle') ?>
            </h2>
        <?php endif; ?>

        <div class="resource--main-content__wrapper">
            <?php the_content() ?>
        </div>

        <?php if($model->embedPDF()): ?>
            <div class="resource--embedded-pdf-viewer">
                <iframe src="<?= $model->getDownloadURL(null, true) ?>"></iframe>
            </div>
        <?php endif; ?>

    </div>

</div>