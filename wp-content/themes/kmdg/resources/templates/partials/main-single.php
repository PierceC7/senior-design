<?php

use Aesir\v1\Components\Template\Partial;
use KMDG\ResourceCenter\ResourceModel;

/* @var $model ResourceModel */
/* @var $currentTerm object */
/* @var $tilePart Partial */
/* @var $resourceQuery WP_Query */

?>

<section class="resource_tiles resource_type--<?= $currentTerm->slug; ?>">


    <div class="resource_tiles__meta">
        <div class="resource_tiles__info_wrap">
            <h3 class="resource_tiles__type">
                <?= $currentTerm->name; ?>
            </h3>

            <div class="resource_tiles__link">
                <div class="resource_tiles__btn">
                    <a href="<?= $moreLink ?>">
                        View All <i class="fas fa-caret-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!--The Loop-->

    <?php if ($resourceQuery->have_posts()) : ?>

        <?php while ($resourceQuery->have_posts()) : $resourceQuery->the_post(); ?>
            <?= $tilePart ?>
        <?php endwhile; ?>

        <?php wp_reset_postdata(); ?>

    <?php else : // No posts ?>
        <?php theme()->part('nothing', 'resource', theme()->post_display_mode()); ?>

    <?php endif; ?>

</section>


