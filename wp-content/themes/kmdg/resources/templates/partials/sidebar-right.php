<?php /** @var \KMDG\ResourceCenter\ResourceModel $model */ ?>
<div class="right_sidebar__wrap">

    <div class="right_sidebar__image" >
        <img src="<?= $model->getResourceImage()->large ?>">
    </div>

    <?php if (!$model->requireForm() && $model->hasDownload()): ?>
        <div class="right_sidebar__download_button">
            <a href="<?= $model->getDownloadURL() ?>" download="<?= $model->getDownloadFileName() ?>">
                Download
            </a>
        </div>
    <?php endif; ?>

</div>