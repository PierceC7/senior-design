<?php /** @var \KMDG\ResourceCenter\ResourceModel $model */ ?>
<?php if ($featuredQuery->have_posts()): ?>
    <section class="resource--featured-resources">
        <div>
            <div class="resource--featured-top">
                <h2 class="resource--featured-title">Featured Resource</h2>
                <div class="resource--slider-pager"></div>
            </div>
            <div class="resource--featured-wrapper">
                <div class="cycle-slideshow"
                     data-cycle-fx="fade"
                     data-cycle-timeout="5000"
                     data-cycle-slides="> .resource--slide"
                     data-cycle-pager=".resource--slider-pager"
                >
                    <?php while ($featuredQuery->have_posts()): $featuredQuery->the_post(); ?>
                        <div class="resource--slide" style="display: none">
                            <div class="resource--featured-post">
                                <div class="resource--post resource--download-type__<?= $model->downloadType() ?>">
                                    <div class="resource--post-wrapper">
                                        <a href="<?= get_the_permalink() ?>" class="resource--post-image">
                                            <div class="resource--featured-img"
                                                 style="background-image: url('<?= ResourceCenter()->getResourceImage(null, true)->large ?>');">
                                                <span class="screen-reader-text"><?= get_the_title() ?></span>
                                            </div>
                                        </a>
                                        <div class="resource--inner-wrap">
                                            <h3 class="resource--post-title">
                                                <a href="<?= get_the_permalink() ?>">
                                                    <?= get_the_title() ?>
                                                </a>
                                            </h3>
                                            <div class="resource--post-excerpt">
                                                <?= get_the_excerpt() ?>
                                            </div>
                                            <div class="resource--post-download">
                                                <a href="<?= get_the_permalink() ?>">
                                                    Download Now
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>