<?php if(have_rows('video_presenters')): ?>
    <div class="resource--presenters">
        <span class="resource--presenters__title">Presenters:</span>
        <?php while(have_rows('video_presenters')): the_row(); ?>
            <?= get_sub_field('link') ? "<a class='resource--presenters__item' href=".get_sub_field('link').">" : "<div class='resource--presenters__item'>" ?>
                <span class="resource--presenters__name"><?= get_sub_field('name') ?></span>

                <?php if(get_sub_field('company')): ?>
                    <span class="resource--presenters__company"><?= get_sub_field('company') ?></span>
                <?php endif; ?>

            <?= get_sub_field('link') ? "</a>" : "</div>" ?>
        <?php endwhile; ?>
    </div>
<?php endif; ?>
