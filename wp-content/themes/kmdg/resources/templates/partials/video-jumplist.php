<?php /** @var \KMDG\ResourceCenter\ResourceModel $model */ ?>
<?php if($model->hasJumpListRows()): ?>
    <div class="resource--video-jump-list">
        <span class="resource--video-jump-list__title">Bookmarks</span>
        <ol class="resource--video-jump-list__wrapper">
            <?php while($model->hasJumpListRows()): $data = $model->getJumpData() ?>
                <li class="resource--video-jump-list__item" data-time="<?= $data->time ?>"><?= $data->text ?> <time>(<?= $data->formattedTime ?>)</time></li>
            <?php endwhile; ?>
        </ol>
    </div>
<?php endif; ?>