<?php

    use KMDG\ResourceCenter\ResourceModel;

    /* @var $model ResourceModel */
    /* @var $currentTerm object */
    $currentTerm = $model->findMainTerm(\KMDG\ResourceCenter\ResourceTypeTax::slug());
?>
<article class="single_tile" data-title="<?php the_title(); ?>">

    <div class="single_tile__wrap">
        <div class="single_tile__image"
             style="background-image: url('<?= $model->getResourceImage(null, true)->thumb ?>');">
        </div>

        <div class="single_tile__info">

            <div class="single_tile__type">
                <?= $currentTerm->term->name; ?>
            </div>

            <div class="single_tile__name">
                <a href="<?php the_permalink(); ?>">
                    <?php the_title(); ?>
                </a>
            </div>

        </div>
    </div>
</article>