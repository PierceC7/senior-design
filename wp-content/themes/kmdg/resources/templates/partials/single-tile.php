<article class="single_tile" data-title="<?php the_title(); ?>">

    <div class="single_tile__wrap">
        <div class="single_tile__image">
            <?php theme()->responsive_image(get_field('headshot'), 300, 300, 'pb-medium'); ?>
        </div>

        <div class="single_tile__info">

            <div class="single_tile__type">

            </div>

            <div class="single_tile__name">
                <a href="<?php the_permalink(); ?>">
                    <?= $postTitle ?>
                </a>
            </div>

        </div>
    </div>
</article>