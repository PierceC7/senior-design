<?php
/** @var \KMDG\ResourceCenter\ResourceModel $model */
?>
<?php get_header(); ?>

<div class="kmdg-resource-center resource">

    <?= $bannerPart; ?>

    <div class="resource_menu">
        <div class="resource--channel">
            <?= $navPart; ?>
        </div>
    </div>

    <div class="resource--channel">
        <?= $featuredPart; ?>
    </div>
    <?php if (have_rows('category_buttons', 'resources')) : ?>
        <?php while (have_rows('category_buttons', 'resources')) : the_row(); ?>

            <?php $term = $model->getTerm(
                \KMDG\ResourceCenter\ResourceTypeTax::slug(),
                get_sub_field('single_category')->slug
            ); ?>

            <div class="resource--channel">
                <?= $singlePart([
                    'currentTerm' => $term,
                    'resourceQuery' => $model->queryPosts(4, $term->slug),
                    'moreLink' => get_term_link($term)
                ]); ?>
            </div>

        <?php endwhile ?>
    <?php else : // No posts ?>
        <?php theme()->part('nothing', 'resource', theme()->post_display_mode()); ?>
    <?php endif ?>
</div>

<?php get_footer(); ?>
