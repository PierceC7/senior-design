<?php
/** @var \KMDG\ResourceCenter\ResourceModel $model */
?>
<?php get_header(); ?>
<?php if (have_posts()): the_post(); ?>
    <section class="kmdg-resource-center resource resource--single <?= $classes ?>">

        <?= $bannerPart ?>

        <div class="resource--channel">
            <div class="resource__layout">
                <div class="resource__wrapper resource__wrapper--video">
                    <?php if ($hasLeftSidebar): ?>
                        <aside class="resource__left_wrapper resource__left_wrapper--video">
                            <div class="resource__left_content">
                                <?= $sidebarLeftContent ?>
                            </div>
                        </aside>
                    <?php endif ?>
                    <main class="resource__main_wrapper resource__main_wrapper--video">

                        <div class="resource--video-info">
                            <?= $mainContent ?>

                            <?php if ($hasVideoExtras): ?>
                                <div class="resource--video-sidebar">
                                    <?= $presenterContent ?>
                                    <?= $jumplistContent ?>
                                    <?= $collateralContent ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="resource__video_wrap">
                            <?= $videoContent ?>
                        </div>
                    </main>
                </div>

            </div>

    </section>
<?php endif; ?>
<?php get_footer(); ?>
