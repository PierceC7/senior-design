<?php
/** @var \KMDG\ResourceCenter\ResourceModel $model */
?>
<?php get_header(); ?>

<div class="kmdg-resource-center resource">

    <?= $bannerPart; ?>

    <div class="resource_menu">
        <div class="resource--channel">
            <?= $navPart; ?>
        </div>
    </div>

    <div class="resource--channel">

        <?= $featuredPart; ?>

        <?= $mainPart; ?>

    </div>
</div>

<?php get_footer(); ?>
