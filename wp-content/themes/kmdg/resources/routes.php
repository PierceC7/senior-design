<?php
/** @var $aesir Aesir\v1\Aesir */

use KMDG\ResourceCenter\ResourceCenter;
use KMDG\ResourceCenter\ResourceLocationTax;
use KMDG\ResourceCenter\ResourceTypeTax;
use KMDG\Theme\ResourceController as ThemeResourceController;


$router = ResourceCenter::getInstance()->router();

$rc = ResourceCenter::getInstance();

foreach($rc->getModel()->getPostTypeSlugs() as $slug) {
    $router->get($slug, 'single', '*', [ThemeResourceController::class, 'single']);
}

$router->get($rc->getSlug(), 'archive', '*', [ThemeResourceController::class, 'archive']);

$router->get('tax', ResourceTypeTax::slug(), '*', [ThemeResourceController::class, 'taxonomy']);

$router->ajax('GET', $rc->getAjaxDownloadSlug(), [ThemeResourceController::class, 'fileDownload'], 1);
$router->ajax('GET','kmdg_rc_ajax_email_file', [ThemeResourceController::class, 'emailFile'], 1);
$router->ajax('GET','kmdg_rc_ajax_video_play', [ThemeResourceController::class, 'playVideo'], 1);

//$router->shortcode('kmdg-rc-search', [ThemeResourceController::class, 'shortcode']);

$router->shortcode('kmdg-rc-recent-posts', [ThemeResourceController::class, 'shortcodeRecentResources']);