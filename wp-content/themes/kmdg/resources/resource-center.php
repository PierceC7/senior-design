<?php

use KMDG\ResourceCenter\ResourceCenter;
use KMDG\ResourceCenter\ResourceModel;
use KMDG\Theme\ResourceFeaturedTax;
use KMDG\Theme\ResourceSolutionTax;
use KMDG\Theme\ResourceProductTax;
use KMDG\Theme\ResourceTagsTax;
use KMDG\Theme\ResourceTopicsTax;

if(!class_exists('KMDG\ResourceCenter\ResourceCenter')) return;

include("routes.php");

include('classes/Model.php');
include('classes/ResourceController.php');
include('classes/ResourceFeaturedTax.php');

add_filter('KMDG/ResourceCenter/REST/Response/PostDateFormat', function() {
    return 'F d, Y';
});

add_filter('KMDG/ResourceCenter/REST/Response/PostExcerpt', function($default, ResourceModel $model, $id) {
    if(get_field('overview', $id)) {
        return get_field('overview', $id);
    }

    return $default;
}, 10, 3);

add_filter('KMDG/ResourceCenter/Downloads/Email/Enabled', function($default, $id) {
    if(get_field('disable_email', $id)) {
        return false;
    }

    return $default;
}, 10, 2);

//add_filter('KMDG/ResourceCenter/Video/HasExtras', function($default, $id, ResourceModel $model) {
//
//    return ($model->hasVideoCollateral($id) || $model->hasVideoPresenters($id) || have_rows('jump_list', $id))
//        && $model->downloadType($id) == 'video'
//        && (($model->playOnPage($id) && $model->requireForm($id)) || !$model->requireForm($id));
//
//}, 10, 3);

add_filter('KMDG/ResourceCenter/Taxonomies', function($taxonomies, $postType) {

    $taxonomies[] = new ResourceFeaturedTax($postType);
    return $taxonomies;

}, 10, 2);

add_filter('KMDG/ResourceCenter/SingleResourceImage/default', function($default, ResourceModel $model, $post_id) {

    $default = (object) [];

    $default->thumb = $default->large = $default->full = theme()->utilities()->src(get_field('logo', 'option'), 'large');


    return $default;
}, 10, 3);

// Load Assets on all pages, not just Resource Center
add_filter('KMDG/ResourceCenter/JS/load', function() {
    return true;
});

add_filter('KMDG/ResourceCenter/css/load', function() {
    return true;
});

add_filter('KMDG/ResourceCenter/SingleResourceImage/default', function($default, ResourceModel $model, $post_id) {

    if($model->downloadType($post_id) == 'file') {
        $pdfPreview = theme()->utilities()->src($model->getFileID($post_id), 'full');

        if($pdfPreview) {
            return (object) [
                'full' => $pdfPreview,
                'large' => theme()->utilities()->src($model->getFileID($post_id), 'large'),
                'thumb' => theme()->utilities()->src($model->getFileID($post_id), 'medium')
            ];
        }
    }

    if(get_field('default_image', 'resources')) {
        $img = get_field('default_image', 'resources');

        return (object) [
            'full' => theme()->utilities()->src($img, 'full'),
            'large' => theme()->utilities()->src($img, 'large'),
            'thumb' => theme()->utilities()->src($img, 'medium')
        ];
    }

    return $default;
}, 10, 3);

//add_filter('KMDG/ResourceCenter/REST/Response/MustacheTags', function($tags, $id, $post, ResourceModel $model) {
//    $tags['eventLocation'] = get_field('location', $id);
//
//    return $tags;
//}, 10, 4);

add_filter('KMDG/ResourceCenter/Search/postsPerPage', function($default, ResourceModel $model, $location) {

    if($location == 60) {
        return 8;
    }

    return $default;
}, 10, 3);