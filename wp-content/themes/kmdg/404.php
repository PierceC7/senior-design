<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */

get_header(); ?>
    <?php theme()->part('site', 'wrapper', 'top'); ?>

    <div class="banner_404" style="background-image: url(<?= get_field('main_banner','options') ?>);">
        <div class="u-builder-container-width">
            <h2 class="banner_404__title">Error 404 - Not Found</h2>
        </div>
    </div>

    <div class="u-builder-container-width">
        <div class="content">

            <p>We're sorry, the page you requested is not here.</p>

            <form>
                <input id="back" type="button" onclick="history.go(-1)" value="Back To Previous Page →">
            </form>
        </div>
    </div>

    <?php theme()->part('site', 'wrapper', 'bottom'); ?>
<?php get_footer(); ?>

