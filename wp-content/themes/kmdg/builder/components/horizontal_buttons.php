<?php
/* @var \KMDG\PageBuilder\Section $section */
/* @var \KMDG\PageBuilder\Layout $layout */
/* @var \KMDG\PageBuilder\Column $column */
/* @var \KMDG\PageBuilder\Component $component */

use KMDG\PageBuilder\Utilities;

?>

<?php $align = 'align-' . $component->field('alignment');?>

<div class="<?= $component->classes() ?>">
    <?php if( have_rows('single_button') ): ?>
        <div class="buttons-row <?= $align;?>">
            <?php while ( have_rows('single_button') ) : the_row(); ?>
                <div class="single-button style-<?= $component->field('color_scheme');?>">
                    <a href="<?= $component->field('button_url');?>"
                       target="<?= $component->field('open_new_window') ? '_blank' : '_self';?>"
                       style="width: <?= $component->field('width');?>px"
                    >

                        <?php if($component->field('icon') && $component->field('button_text')):?>
                            <?= $component->field('button_text');?>
                            <i class="<?= $component->field('icon');?>"></i>&nbsp;&nbsp;

                        <?php elseif ($component->field('icon')): ?>
                            <i class="solo-icon <?= $component->field('icon');?>" aria-label="Find Out More"></i>

                        <?php elseif($component->field('button_text')): ?>
                            <?= $component->field('button_text');?>
                        <?php endif;?>
                    </a>
                </div>
            <?php endwhile;?>
        </div>
    <?php endif;?>
</div>
