<?php /* @var \KMDG\PageBuilder\Section $section */ ?>
<?php /* @var \KMDG\PageBuilder\Layout $layout */ ?>
<?php /* @var \KMDG\PageBuilder\Column $column */ ?>
<?php /* @var \KMDG\PageBuilder\Component $component */ ?>

<div <?= $component->animations() ?> class="<?= $component->classes() ?>">
    <div class="single-accordion style-<?= $component->field('color_scheme'); ?>">
        <div class="accordion-header">
            <div class="accordion-title <?= $component->field('icon') ? 'title--icon' : '' ?>">
                <h2><?= $component->field('title'); ?></h2>
            </div>
            <div class="accordion_icons accordion_icons__wrap">
                <?php if ($component->field("icon_1") && $component->field('icon_1') != 'none'): ?>
                    <div class="accordion_icons__single accordion_icons__single--eu">
                        <img src="http://nlyte.kmdgideas.com/web/wp-content/uploads/2019/09/icon_eu_<?= $component->field('icon_1'); ?>.png"
                             alt="Icon 1">
                    </div>
                <?php endif; ?>
                <?php if ($component->field("icon_2") && $component->field('icon_2') != 'none'): ?>
                    <div class="accordion_icons__single accordion_icons__single--admin">
                        <img src="http://nlyte.kmdgideas.com/web/wp-content/uploads/2019/09/icon_admin_<?= $component->field('icon_2'); ?>.png"
                             alt="Icon 2">
                    </div>
                <?php endif; ?>
                <?php if ($component->field("icon_3") && $component->field('icon_3') != 'none'): ?>
                    <div class="accordion_icons__single accordion_icons__single--su_rep">
                        <img src="http://nlyte.kmdgideas.com/web/wp-content/uploads/2019/09/icon_su_reporting_<?= $component->field('icon_3'); ?>.png"
                             alt="Icon 3">
                    </div>
                <?php endif; ?>
                <?php if ($component->field("icon_4") && $component->field('icon_4') != 'none'): ?>
                    <div class="accordion_icons__single accordion_icons__single--su_pd">
                        <img src="http://nlyte.kmdgideas.com/web/wp-content/uploads/2019/09/icon_su_pd_<?= $component->field('icon_4'); ?>.png"
                             alt="Icon 4">
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="accordion-inner">
            <div class="content">
                <?= $component->field('content'); ?>
            </div>

            <?php if ($component->field('buttons')) : ?>
                <div class="buttons-row">
                    <?php foreach ($component->field('buttons', false, true) as $button): ?>
                        <a href="<?= $button['button_link']; ?>" class="basic-button"><?= $button['button_text']; ?></a>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>

        </div>
    </div>
</div>
