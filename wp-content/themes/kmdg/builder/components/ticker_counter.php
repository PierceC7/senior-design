<div <?= $component->animations() ?> class="<?= $component->classes() ?>">
    <div class="single-counter">

        <?php if ($component->field('icon')): ?>
            <div class="icon-wrap">
                <img src="<?= theme()->utilities()->src($component->field('icon'), 'full'); ?>">
            </div>
        <?php endif; ?>


        <div class="numbers-wrap">
            <div class="prepend"><?= $component->field('prepend'); ?></div>
            <div class="number" data-count="<?= $component->field('number'); ?>"></div>
            <div class="append"><?= $component->field('append'); ?></div>
        </div>

        <div class="desc">
            <?= $component->field('description'); ?>
        </div>
    </div>
</div>
