<?php
/* @var \KMDG\PageBuilder\Section $section */
/* @var \KMDG\PageBuilder\Layout $layout */
/* @var \KMDG\PageBuilder\Column $column */

/* @var \KMDG\PageBuilder\Component $component */

use KMDG\PageBuilder\Utilities;

$form_id = $component->field('form_id');
?>

<div class="<?= $component->classes() ?>">
    <form id="mktoForm_<?= $form_id; ?>" style="max-width: 100% !important;"></form>
    <script>MktoForms2.loadForm("//app-ab11.marketo.com", "690-OLN-597", <?= $form_id;?>);</script>
</div>