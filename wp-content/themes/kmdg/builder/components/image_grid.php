<div class="<?= $component->classes() ?>">
    <?php if( have_rows('single_image') ): ?>
        <div class="grid-row" data-packery='{ "itemSelector": ".single-image", "gutter": ".gutter-sizer" }'>
            <?php while ( have_rows('single_image') ) : the_row(); ?>

                <div class="gutter-sizer"></div>

                <div class="single-image <?= $component->field('image_orientation');?>">
                    <div class="block-wrap">
                        <div class="image-area"
                             style="background-image:url('<?= theme()->utilities()->src($component->field('image'), 'full'); ?>');"
                        ></div>
                    </div>
                </div>
            <?php endwhile;?>
        </div>
    <?php endif;?>
</div>