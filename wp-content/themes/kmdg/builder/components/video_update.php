<?php /* @var \KMDG\PageBuilder\Section $section */ ?>
<?php /* @var \KMDG\PageBuilder\Layout $layout */ ?>
<?php /* @var \KMDG\PageBuilder\Column $column */?>
<?php /* @var \KMDG\PageBuilder\Component $component */ ?>
<?php $data = theme()->getVideoInfo($component->field('link_source'));?>


<div class="<?= $component->classes() ?>">
    <a class="lightbox-trigger" href="<?= $data->url ?>?rel=0&autoplay=1" data-featherlight="iframe"
       data-featherlight-iframe-allowfullscreen="true" data-featherlight-iframe-frameborder="0" data-featherlight-iframe-allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture">
        <div class="thumbnail" style="background-image:url('<?= theme()->get_video_image($component) ?>');">
            <div class="playbtn">
                <i class="fab fa-youtube"></i>
            </div>
        </div>
    </a>
</div>
