<?php
/* @var \KMDG\PageBuilder\Section $section */
/* @var \KMDG\PageBuilder\Layout $layout */
/* @var \KMDG\PageBuilder\Column $column */
/* @var \KMDG\PageBuilder\Component $component */

$padding = 'style="padding: 0 ' . $component->field('padding') / 2 . '%;"';
?>

<div class="<?= $component->classes() ?>">
    <div class="rollover_icon">
        <div class="rollover_icon__single">
            <?php if ($component->field('link')): ?>
                <div class="rollover_icon__image-container" <?= $padding; ?>>
                    <div class="rollover_icon__image-subcontainer">
                        <a href="<?= $component->field('link') ?>">
                            <img class="rollover_icon__img rollover_icon__img--inactive"
                                 src="<?= theme()->utilities()->src($component->field('inactive_icon'), 'medium'); ?>">
                            <img class="rollover_icon__img rollover_icon__img--active"
                                 src="<?= theme()->utilities()->src($component->field('active_icon'), 'medium'); ?>">
                        </a>
                    </div>
                </div>
            <?php else: ?>
                <div class="rollover_icon__image-container" <?= $padding; ?>>
                    <div class="rollover_icon__image-subcontainer">
                        <img class="rollover_icon__img rollover_icon__img--inactive"
                             src="<?= theme()->utilities()->src($component->field('inactive_icon'), 'medium'); ?>">
                        <img class="rollover_icon__img rollover_icon__img--active"
                             src="<?= theme()->utilities()->src($component->field('active_icon'), 'medium'); ?>">
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>