<?php

$size = 'size--' . $component->field('size');

?>

<div class="<?= $component->classes() ?>">
    <div class="post-area">
        <?php
        $post_object = $component->field('single_post');

        if ($post_object):
            $id = $post_object->ID;
            $post_type = get_post_type_object(get_post_type($post_object));
            ?>

            <a href="<?php the_permalink($id); ?>">

                <div class="content-wrap">

                    <?php if (in_array($post_type, ResourceCenter()->getPostTypeSlugs())): ?>
                        <?php if (ResourceCenter()->getResourceImage($id)): ?>
                            <div class="post-image <?= $size ?>"
                                 style="background-image:url('<?= ResourceCenter()->getResourceImage($id)->full; ?>');"></div>
                        <?php else: ?>
                            <div class="post-image <?= $size ?>"
                                 style="background-image:url('<?= theme()->get_placeholder_image(360, 180); ?>');"></div>
                        <?php endif; ?>

                    <?php else: ?>
                        <?php if (has_post_thumbnail($id)): ?>
                            <div class="post-image <?= $size ?>"
                                 style="background-image:url('<?= theme()->utilities()->src(get_post_thumbnail_id($id), 'full'); ?>');"></div>
                        <?php else: ?>
                            <div class="post-image <?= $size ?>"
                                 style="background-image:url('<?= theme()->utilities()->src(get_field('default_ps_image')); ?>');"></div>
                        <?php endif; ?>


                    <?php endif; ?>

                    <div class="post-title">
                        <?= get_the_title($id); ?>
                    </div>
                </div>
            </a>

            <div class="resource-link">
                <a href="<?php the_permalink($id); ?>" class="resource-link">
                    <?php if (in_array($post_type, ResourceCenter()->getPostTypeSlugs())): ?>
                        <?= ResourceCenter()->buttonText($id); ?>
                    <?php else: ?>
                        Read More
                    <?php endif; ?>
                </a>
            </div>

        <?php endif; ?>
    </div><!--/. post-area-->
</div>