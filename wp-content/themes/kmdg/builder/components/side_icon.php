<div class="<?= $component->classes() ?>">
    <div class="flexed-row">
        <div class="icon">
            <img src="<?= theme()->utilities()->src($component->field('icon'), 'full'); ?>">
        </div>

        <div class="right-side">
            <div class="equal-hm">
                <?php if($component->field('title')) :?>
                    <div class="title">
                        <?= $component->field('title');?>
                    </div>
                <?php endif;?>

                <?php if($component->field('content')) :?>
                    <div class="content-wrap">
                        <?= $component->field('content');?>
                    </div>
                <?php endif;?>
            </div>

            <?php if($component->field('button_url')) :?>
                <div class="btn-wrap">
                    <a href="<?= $component->field('button_url');?>"
                       target="<?= $component->field('open_new_window') ? '_blank' : '_self';?>">
                        <?= $component->field('button_text');?>
                    </a>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>