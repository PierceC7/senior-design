<div class="<?= $component->classes() ?>">
    <div class="inner-wrap">
		<?php if($component->field('left_side')): ?>
            <div class="side left-side">
				<?= $component->field('left_side');?>
            </div>
		<?php endif;?>

		<?php if($component->field('right_side')): ?>
            <div class="side right-side">
				<?= $component->field('right_side');?>
            </div>
		<?php endif;?>
    </div>
</div>