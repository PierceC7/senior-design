<div class="<?= $component->classes() ?>">
    <div class="flexed-row">
        <div class="icon">
            <?= $component->field('icon');?>
        </div>

        <div class="heading-wrap">
            <h4 class="title"><?= $component->field('text_content');?></h4>
        </div>

        <div class="row-btn">
            <a href="<?= $component->field('button_link');?>"
                <?= $component->field('open_new_window') ? '_blank' : '_self' ?>>
                <?= $component->field('button_text');?>
            </a>
        </div>
    </div>
</div>