<?php
/* @var \KMDG\PageBuilder\Section $section */
/* @var \KMDG\PageBuilder\Layout $layout */
/* @var \KMDG\PageBuilder\Column $column */
/* @var \KMDG\PageBuilder\Component $component */

use KMDG\PageBuilder\Utilities;


    $bg = 'background-color: ' . get_sub_field('line_color') . ';';
    $height = 'height: ' . get_sub_field('line_height') . 'px;';

    $style = $bg . $height;
?>
<div class="<?= $component->classes() ?>">
    <div class="divider-line" style="<?= $style ?>"></div>
</div>