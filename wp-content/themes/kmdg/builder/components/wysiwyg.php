<?php /* @var \KMDG\PageBuilder\Section $section */ ?>
<?php /* @var \KMDG\PageBuilder\Layout $layout */ ?>
<?php /* @var \KMDG\PageBuilder\Column $column */ ?>
<?php /* @var \KMDG\PageBuilder\Component $component */ ?>

<?php
if($component->field('padding_one')):
    $padding_one = $component->field('padding_one') . '%';
else:
    $padding_one = '0%';
endif;

if($component->field('padding_two')):
    $padding_two = $component->field('padding_two') . '%';
else:
    $padding_two = '0%';
endif;

$padding = 'padding: ' . $padding_one . ' ' . $padding_two . ';';

$bg_color = 'background-color: ' . $component->field('background_color') . ';';

if ($component->field('border')): {
    $border = 'border: 1px solid #cacaca;';
}

else: {
    $border = '';
}

endif;

$options = $padding . $bg_color . $border;
?>


<div class="<?= $component->classes() ?>" style="<?= $options;?>">
    <?= $component->field("content"); ?>
</div>
