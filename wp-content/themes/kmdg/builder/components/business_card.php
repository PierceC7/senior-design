<?php
/* @var \KMDG\PageBuilder\Section $section */
/* @var \KMDG\PageBuilder\Layout $layout */
/* @var \KMDG\PageBuilder\Column $column */
/* @var \KMDG\PageBuilder\Component $component */

use KMDG\PageBuilder\Utilities;

?>

<div class="<?= $component->classes() ?>">
	<div class="inner-wrap">
		<?php if($component->field('left_side')): ?>
            <div class="side left-side">
                <?php theme()->responsive_image($component->field('left_side'), null, null, 'medium') ?>
            </div>
		<?php endif;?>

		<?php if($component->field('right_side')): ?>
			<div class="side right-side">
                <div class="right-text">
                    <?= $component->field('right_side');?>
                </div>
                <div class="card-link">
                    <a class="link-text" href="<?= $component->field('text_url');?>"
                       target="<?= $component->field('open_new_window') ? '_blank' : '_self';?>">
                        <?= $component->field('link_text');?>
                    </a>
                </div>
			</div>
		<?php endif;?>
	</div>
</div>
