<?php
/* @var \KMDG\PageBuilder\Section $section */
/* @var \KMDG\PageBuilder\Layout $layout */
/* @var \KMDG\PageBuilder\Column $column */
/* @var \KMDG\PageBuilder\Component $component */

?>
<div class="<?= $component->classes() ?>">
    <?php if (have_rows('single_icon')): ?>
        <div class="icons-row">
            <?php while (have_rows('single_icon')) : the_row(); ?>
                <?php
                $padding = 'style="padding: 0 ' . $component->field('padding') / 2 . '%;"';
                ?>

                <div class="single-icon">
                    <?php if ($component->field('link')): ?>
                        <div class="image-container" <?= $padding; ?>>
                            <a href="<?= $component->field('link') ?>">
                                <img src="<?= theme()->utilities()->src($component->field('image'), 'medium'); ?>">
                            </a>
                        </div>
                    <?php else: ?>
                        <div class="image-container" <?= $padding; ?>>
                            <img src="<?= theme()->utilities()->src($component->field('image'), 'medium'); ?>">
                        </div>
                    <?php endif; ?>
                </div>
            <?php endwhile; ?>
        </div>
    <?php endif; ?>
</div>