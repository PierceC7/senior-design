<?php
/* @var \KMDG\PageBuilder\Section $section */
/* @var \KMDG\PageBuilder\Layout $layout */
/* @var \KMDG\PageBuilder\Column $column */
/* @var \KMDG\PageBuilder\Component $component */

$tag = $style = $component->field('style');

if($component->field('seo_importance') != 'same'):
    $tag = $component->field('seo_importance');
endif;

// Wrap sections denoted with | in a span (used for styles)
$txtArr = explode('|', $component->field('text', '&nbsp;'));
$text = '';

if(is_array($txtArr)):
    $wrapped = false;
    foreach($txtArr as $segment):
        if($wrapped):
            $text .= '<span>'.$segment.'</span>';
        else:
            $text .= $segment;
        endif;

        $wrapped = !$wrapped;
    endforeach;
endif;

?>

<div <?= $component->animations() ?> class="<?= $component->classes() ?>">
    <div class="builder-column-header">
        <<?= $tag ?> class="builder-title builder-style--<?= $style ?>" style="text-align: <?= $component->field('alignment'); ?>;">
        <?= $text ?>
    </<?= $tag ?>>
</div>
</div>