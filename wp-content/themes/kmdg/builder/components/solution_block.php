<?php
/* @var \KMDG\PageBuilder\Section $section */
/* @var \KMDG\PageBuilder\Layout $layout */
/* @var \KMDG\PageBuilder\Column $column */
/* @var \KMDG\PageBuilder\Component $component */
?>

<div class="<?= $component->classes() ?>">
    <div class="solution-block">
        <?php if ($component->field('block_title')) : ?>
            <h2 class="solution-block__title"><?= $component->field('block_title'); ?></h2>
        <?php endif; ?>
        <div class="solution-block__content">
            <?php if (have_rows('solution_item')): ?>
                <?php while (have_rows('solution_item')):
                    the_row(); ?>
                    <div class="solution-item solution-item__content">
                        <div class="solution-item__wrapper">
                            <div class="solution-item__img">

                                <?php if ($component->field('icon_select') == 'fa_icon'): ?>

                                    <i class="solution-item__img--fa <?= $component->field('fa_icon') ?>"></i>

                                <?php elseif ($component->field('icon_select') == 'cust_icon'): ?>

                                    <img class="solution-item__img--image" src="<?= theme()->utilities()->src($component->field('cust_icon'), 'full'); ?>" alt="icon">

                                <?php endif; ?>

                            </div>

                            <div class="solution-item__desc">
                                <p class="solution-item__type"><?= $component->field('item_description'); ?></p>
                                <?php if ($component->field('item_url')): ?>
                                    <h4 class="solution-item__url">
                                        <a href="<?= $component->field('item_url'); ?>">
                                            <?= $component->field('item_text'); ?>
                                        </a>
                                    </h4>
                                <?php else : ?>
                                    <h4 class="solution-item__url">
                                        <?= $component->field('item_text'); ?>
                                    </h4>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</div>