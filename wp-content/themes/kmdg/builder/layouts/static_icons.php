<?php if( have_rows('single_icon') ): ?>
    <div class="icons-row">
        <?php while ( have_rows('single_icon') ) : the_row();?>
            <div class="single-icon">
                <div class="icon-wrap">
                    <img src="<?= theme()->utilities()->src(get_sub_field('icon'), 'full'); ?>">
                </div>

                <h3 class="title">
                    <?php the_sub_field('title');?>
                </h3>
            </div>
        <?php endwhile;?>
    </div>
<?php endif;?>
