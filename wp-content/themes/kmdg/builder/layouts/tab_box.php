<?php
/* @var \KMDG\PageBuilder\Layout $layout */

use KMDG\PageBuilder\Builder;
use KMDG\PageBuilder\Utilities;

$content = '';
$content_theme = get_sub_field('tab_content_theme');


?>

<?php if (have_rows('tabs')): ?>
    <div class="builder-tabs">
        <div class="builder-container u-builder-container-width">
            <?php while (have_rows('tabs')): the_row() ?>

                <?php
                $lightboxClass = null;
                $lightbox = null;

                if (get_sub_field('enable_lightbox')) {
                    $lightbox = 'data-featherlight="' . Utilities::src(get_sub_field('image'), 'full') . '"';
                    $lightboxClass = 'js-triggers-lightbox';
                }
                ?>

                <div class="overtab <?= get_row_index() == 1 ? 'active' : 'off' ?>" data-tab="<?= get_row_index() ?>">
                    <div class="tab <?= get_row_index() == 1 ? 'active' : 'off' ?>" data-tab="<?= get_row_index() ?>">
                        <div class="title">
                            <span><?= get_sub_field('title') ?></span>
                        </div>

                        <?php ob_start() ?>
                        <div class="tab--content <?= get_row_index() == 1 ? 'active' : '' ?>"
                             data-tab="<?= get_row_index() ?>">
                            <div class="innertab__wrap">
                                <?php if (get_sub_field('image')): ?>
                                    <div class="image__wrap <?= $lightboxClass; ?>">
                                        <div class="image-container">
                                            <div class="image-subcontainer" <?= $lightbox ?>>
                                                <img class="image__item"
                                                     src="<?= theme()->utilities()->src(get_sub_field('image'), 'large'); ?>"
                                                >
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="wrap">
                                    <div class="text">
                                        <?= get_sub_field('content') ?>
                                    </div>
                                    <?php if (get_sub_field('button_url')) : ?>
                                        <div class="button">
                                            <a href="<?= get_sub_field('button_url') ?>"
                                               target="<?= get_sub_field('open_new_window') ? '_blank' : '_self' ?> ">
                                                <?= get_sub_field('button_text') ?>
                                            </a>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <?php $content .= ob_get_clean() ?>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>

    </div>

    <div class="builder-tabs--content builder-theme-override builder-theme--<?= $content_theme ?>">
        <div class="u-builder-container-width">
            <?= $content ?>
        </div>
    </div>
<?php endif; ?>
