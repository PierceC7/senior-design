<?php if( have_rows('single_card') ): ?>
    <div class="cards-row">
        <?php while ( have_rows('single_card') ) : the_row();?>
            <a href="#" data-featherlight="#index-<?= get_row_index();?>">
                <div class="single-card">
                    <?php theme()->responsive_image(get_sub_field('image'), 340, 340, 'full'); ?>

                    <div class="black-bar">
                        <h4><?php the_sub_field('name');?></h4>
                        <h5><?php the_sub_field('job_title');?></h5>
                    </div>
                </div>
            </a>

            <div class="lightbox-content" id="index-<?= get_row_index();?>">
                <div class="row">
                    <div class="col-md-4">
                        <?php theme()->responsive_image(get_sub_field('image'), 340, 340, 'full'); ?>
                    </div>

                    <div class="col-md-8">
                        <div class="content-wrap">
                            <h4><?php the_sub_field('name');?></h4>
                            <h5><?php the_sub_field('job_title');?></h5>

                            <?php the_sub_field('card_content');?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile;?>
    </div>
<?php endif;?>
