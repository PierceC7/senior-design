<?php if (have_rows('single_tile')): ?>
    <div class="builder-layout-tile-cards__row">
        <?php while (have_rows('single_tile')) : the_row(); ?>
            <div class="tile-card tile-card__<?= get_sub_field('center_align') ? 'left' : 'center' ?>">
                <div class="tile-card__wrap  builder-theme-override builder-theme--<?= get_sub_field('tile_theme') ?>"
                     style="<?= get_sub_field('background_image') ? 'background-image:url(' . theme()->utilities()->src(get_sub_field('background_image'), 'large') . ');' : '' ?>">
                    <div class="tile-card__tile">

                        <div class="tile-card__top">
                            <?php if (get_sub_field('icon')): ?>
                                <div class="tile-card__img">
                                    <img class=""
                                         src="<?= theme()->utilities()->src(get_sub_field('icon'), 'medium'); ?>"
                                         alt="Icon">
                                </div>
                            <?php endif; ?>


                            <?php if (get_sub_field('title')): ?>
                                <div class="tile-card__title">
                                    <h3><?= get_sub_field('title') ?></h3>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="tile-card__bottom">
                            <div class="tile-card__content">
                                <?= get_sub_field('content'); ?>
                            </div>
                            <?php if (get_sub_field('link_url')) : ?>
                                <div class="tile-card__link">
                                    <a href="<?= get_sub_field('link_url'); ?>"
                                       target="<?= get_sub_field('open_new_window') ? '_blank' : '_self' ?> ">
                                        <?= get_sub_field('link_text'); ?>
                                        <i class="fas fa-long-arrow-alt-right"></i>
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
<?php endif; ?>