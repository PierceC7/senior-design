<?php $grid_type = get_sub_field('grid_style');?>

<?php if( have_rows('images') ): ?>
    <div class="grid-row style-<?= is_array($grid_type) ? $grid_type[0] : $grid_type ?>" data-packery='{ "itemSelector": ".single-image", "gutter": 0 }'>
        <?php while ( have_rows('images') ) : the_row();?>
            <div class="single-image">
                <div class="block-wrap">
                    <div class="image-area"
                         style="background-image:url('<?= theme()->utilities()->src(get_sub_field('single_image'), 'large'); ?>');"
                    ></div>
                </div>
            </div>
        <?php endwhile;?>
    </div>
<?php endif; ?>