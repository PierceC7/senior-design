<?php
/*
 * @package WordPress
 * @subpackage kmdg
 *
 * Label: Rollover Section
 * Name: rollover_section
 * Description: Displays a single content area (similar to standard wordpress)
 */

/* @var KMDG\PageBuilder\Layout $layout */
?>


<div class="builder-table">
    <?php $layout->column('background_column', 'single_column'); ?>
</div>

