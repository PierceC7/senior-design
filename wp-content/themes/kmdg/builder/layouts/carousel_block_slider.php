<?php if( have_rows('single_block_slide') ): ?>
    <div class="blocks-row">
        <?php while ( have_rows('single_block_slide') ) : the_row();?>
            <div class="single-block">
                <img src="<?= theme()->utilities()->src(get_sub_field('image'), 'full'); ?>">
            </div>
        <?php endwhile;?>
    </div>
<?php endif;?>