<?php
use KMDG\PageBuilder\Utilities;

/* @var \KMDG\PageBuilder\Section $section */
/* @var \KMDG\PageBuilder\Layout $layout */
/* @var \KMDG\PageBuilder\Column $column */

// Default
$bg_image = $column->field('background_image');
$bg_color = $column->field('background_color');

$bg_option = '';

if ($bg_image) {
    $bg_option .= "background-image: url(" . Utilities::src($bg_image, "full") . ");";
} elseif ($bg_color) {
    $bg_option .= 'background-color: ' . $bg_color . ';';
}

// Rollover Active
$roll_bg_image = $column->field('rollover_background_image');
$roll_bg_color = $column->field('rollover_background_color');

// Used for either transparent background or color
$roll_bg_option = '';

$opacity = '';
$gray_overlay = '';

// If there's a gray overlay, output it with opacity settings
if ($column->field('use_gray_overlay') == true) {
    //Sets opacity of overlay
    $opacity = 'opacity:' . $column->field('overlay_opacity') / 100 . ';';

    // Outputs gray overlay
    $gray_overlay = '<div class="gray-stripes" style=" '. $opacity .' "></div>';
}

// If no bg image OR bg color, set background to transparent
if (!$roll_bg_image && !$roll_bg_color || $column->field('use_transparent_bg') == true) {
    $roll_bg_option .= 'background-color: transparent;';
}

// Checks for background color and outputs it
if($roll_bg_color && $column->field('use_transparent_bg') == false) {
    $roll_bg_option .= 'background-color: ' . $roll_bg_color . ';';
}

// Checks for background image and outputs it
if ($roll_bg_image && $column->field('use_transparent_bg') == false) {
    $roll_bg_option .= "background-image: url(" . Utilities::src($roll_bg_image, "full") . ");";
}

?>

<div class="<?= $column->classes() ?>">
    <div class="builder-column-body">
        <div class="builder-content">
            <div class="rollover rollover-default" style="<?= $bg_option ?>">
                <?= $column->loop(); ?>
            </div>

            <?php if(have_rows('rollover_components')) :?>
                <?php while (have_rows('rollover_components')) : the_row();?>
                    <?php if(have_rows('components')):?>
                        <div class="rollover rollover-active" style="<?= $roll_bg_option ?>">
                            <?= $gray_overlay; ?>

                            <div class="active-content-wrap">
                                <?= $column->loop(); ?>
                            </div>
                        </div>
                    <?php endif;?>
                <?php endwhile;?>
            <?php endif;?>

        </div>
    </div>
</div>