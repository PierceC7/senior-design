<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */

use KMDG\PageBuilder\Builder;
use KMDG\PageBuilder\Layout;
use \KMDG\PageBuilder\Section;

defined('ABSPATH') or die('No direct access');

include "resources/resource-center.php";

add_filter("KMDG/PB/cache/enable", "__return_false");

// Basic Button Shortcode
add_shortcode('button', function ($atts, $content) {
    $atts = shortcode_atts([
        'url' => '#'
    ], $atts);

    return '<a class="custom-button" href="' . $atts['url'] . '">' . $content . '</a>';
});

add_filter('KMDG/PB/section/styles', function ($properties, Section $section) {
    if (!$section->option('use_custom_background_color')->get()) {
        unset($properties['background-color']);
    }

    return $properties;
}, 10, 2);


add_filter('KMDG/PB/layout/container/html_start', function ($original, Section $section, Layout $layout) {
    if ($layout->getName() == 'half_block' || $layout->getName() == 'rollover_section') {
        return '<div class="full-width-container">';
    }

    return $original;
}, 10, 3);

add_filter('KMDG/PB/background/html', function ($original, $id, $height, $opacity, $preloadImage, $enablePreload, $srcs, $style) {
    $use_grayscale = Builder::getCurrentSection()->option('use_gray')->get();

    if ($use_grayscale == true) {
        return "<div class='builder-img-bg grayscale " . ($enablePreload ? '' : 'loaded') . "'
        style='{$style}' " . ($enablePreload ? "data-srcs='" . esc_attr(json_encode($srcs)) . "'" : '') . ">
        <img src='{$preloadImage}' alt='' role='presentation'></div>";
    } else {
        return $original;
    }

}, 10, 8);

function get_vimeo_id($embed)
{
    $matches = array();

    if (preg_match("/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/", $embed, $matches)) {
        return $matches[5];
    }

    return false;
}

function get_wistia_id($embed)
{
    $matches = array();

    if (preg_match("/(http(?:s)?:\/\/)?(kahua\.wistia\.com\/medias)+\/([a-z\d]*)/", $embed, $matches)) {
        return $matches[3];
    } elseif (preg_match("/(http(?:s)?:\/\/)?(commotionengine\.wistia\.com\/medias)+\/([a-z\d]*)/", $embed, $matches)) {
        return $matches[3];
    }
    return false;
}

function get_youtube_id($embed)
{
    $matches = array();

    if (preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $embed, $matches)) {
        return $matches[1];
    }

    return false;
}

function get_video_info($embed)
{
    $data = wp_cache_get($embed);

    if ($data === false) {
        $data = (object)array();

        $vimeo = get_vimeo_id($embed);
        $youtube = get_youtube_id($embed);
        $wistia = get_wistia_id($embed);

        if ($vimeo !== false) {
            $data->type = 'vimeo';
            $data->id = $vimeo;
        } elseif ($youtube !== false) {
            $data->type = 'youtube';
            $data->id = $youtube;
        } elseif ($wistia !== false) {
            $data->type = 'wistia';
            $data->id = $wistia;
        } else {
            $data->type = false;
            $data->id = null;
        }

        $data->embed = $embed;
        wp_cache_set($embed, $data);

    }

    return $data;
}

add_filter('upload_mimes', function ($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
});


function get_eyebrow_color()
{

    if (is_array(get_sub_field('eyebrow_button_color'))) {
        return get_sub_field('eyebrow_button_color')[0];
    } else {
        return get_sub_field('eyebrow_button_color');
    }

}

function get_floating_color()
{

    if (is_array(get_sub_field('floating_buttons_color'))) {
        return get_sub_field('floating_buttons_color')[0];
    } else {
        return get_sub_field('floating_buttons_color');
    }

}

add_filter('excerpt_length', function ($length) {
    return 35;
}, 999);

add_action('init', function() {
    // Disable WP emoji's
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');

    // Remove from TinyMCE
    add_filter('tiny_mce_plugins', function($plugins) {
        if (is_array($plugins)) {
            return array_diff($plugins, ['wpemoji']);
        } else {
            return [];
        }
    });
});

add_action('rest_api_init', function () {
	register_rest_route( 'senior-design/', '(?P<post_id>\d+)',array(
		'methods'  => 'GET',
		'callback' => 'get_medication_info'
	));
});

function get_medication_info($request){
	global $post;
	$post = $request['post_id'];
	setup_postdata($post);
	$info = get_field('week_data');
	$response = new WP_REST_Response($info);
	$response->set_status(200);
	return $response;
}