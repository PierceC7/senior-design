var project = require('../../settings');
var utils = require('./modules/utils');
var Packery = require('packery');

/* Event Handlers  */
(function ($, $PB) {

    var ie = KMDGPB.builder.detectIE();

    // Automatically equalize height on specified elements
    $(window).on("load", function () {
        //Tile Card layout
        utils.equalHeight('.tile-card__img','.builder-layout');
        utils.equalHeight('.tile-card__content','.builder-layout');
        utils.equalHeight('.tile-card__top','.builder-layout');
        //Component layout adjustments
        utils.equalHeight('.builder-component-business-card','.builder-layout');
        utils.equalHeight('.post-title','.builder-table');
        //Custom post type adjustments
        utils.equalHeight('.single_tile__name','.resource_tiles');
        utils.equalHeight('.js-equal');
        $(window).trigger('resize').trigger('scroll');

        if(ie) {
            $('html').addClass('ms_ie').addClass('ms_ie'+ie);
        }

        setInterval(function() {
            jQuery(window).trigger('resize').trigger('scroll');
        }, 1000);
    });

    //Header top spacer
    $(window).on('resize', function() {
        $('.template, .resource, .archive_wrap').css('padding-top',
            $('.site_header').outerHeight()
        );
    });

    $("#menu-wrapper").mmenu(
        {
            "navbar": {
                "title": mmtitle,
            },
            "navbars": [
                {
                    "position": "bottom",
                    "content": [
                        "searchfield"
                    ]
                }
            ],
            "extensions": [
                "pagedim-black",
                "theme-dark",
                "position-right"
            ],
            "searchfield": {
                "search": false,
            },
            classNames: {
                fixedElements: {
                    fixed: "fix",
                    sticky: "stick"
                }
            }
        }, {
            "searchfield": {
                "form": {
                    "method": 'GET',
                    "action": homeurl
                },
                "input": {
                    "name": 's'
                }
            }
        }
    );

    /*$("#menu-wrapper").mmenu({
        "navbar": {
            "title": mmtitle,
        },
        "extensions": [
            "pagedim-black",
            "theme-dark",
            "position-right"
        ],
        classNames: {
            fixedElements: {
                fixed: "fix",
                sticky: "stick"
            }
        }
    });*/

    $( document ).ready(function() {
        $('.builder-layout-full-width-masonry-grid .block-wrap').show();
        $('.builder-component-image-grid .block-wrap').show();
    });

    $('.builder-layout-half-block .rollover').each(function () {
        if ($(this).siblings('.rollover-active').length) {
            $(this).addClass('has-active');
        }
    });

    $('.builder-layout-rollover-section .rollover').each(function () {
        if ($(this).siblings('.rollover-active').length) {
            $(this).addClass('has-active');
        }
    });

    $('.tabs-container ul.tabs-wrap li:first-child').addClass('open');
    $('.tab-outer-wrap .tab-content:first-child').addClass('open');

    $('.tabs-container ul.tabs-wrap li').on('click', function(){
        var $tab_id = $(this).attr('data-tab');

        $('.tabs-container ul.tabs-wrap li').removeClass('open');
        $('.tab-content').removeClass('open');

        $(this).addClass('open');
        $("#"+$tab_id).addClass('open');

    });

    function commaSeparateNumber(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
        }
        return val;
    }

    $PB('.builder-component-ticker-counter').on('builderAnimationStart', function() {
        var $this = $(this),
            $number = $this.find('.single-counter .number'),
            countTo = $number.attr('data-count'),
            delay = $this.attr('data-animation-delay') * 1000;

        setTimeout(function () {
            $({
                countNum: 0
            }).animate({
                    countNum: countTo
                },

                {
                    duration: 4000,
                    easing: 'linear',
                    step: function() {
                        $number.text(commaSeparateNumber(Math.floor(this.countNum)));
                    },
                    complete: function() {
                        $number.text(commaSeparateNumber(this.countNum));
                    }
                }
            );
        }, delay);
    });

    //Tab Box JS
    $('.builder-layout-tab-box .tab').on('click', function () {
        var num = $(this).attr('data-tab');
        var $parent = $(this).parents('.builder-layout');
        var $tabs = $parent.find('.builder-tabs');
        var $content = $parent.find('.builder-tabs--content');

        $content.find('.tab--content').removeClass('active').addClass('off');
        $content.find('[data-tab="' + num + '"]').addClass('active').removeClass('off');

        $tabs.find('.overtab').removeClass('active').addClass('off');
        $(this).parents('.overtab').addClass('active').removeClass('off');
    });

    //Accordion JS
    $('.single-accordion .accordion-header').on('click', function() {
        var $column = $(this).parent();

        $column.toggleClass('open');
        $column.find('.accordion-inner').slideToggle({
            duration: 250,
            progress: function() {
                $(window).trigger('resize');
            }
        });
    });

    $('.image-container').on("hover", function(){
        $('.image-container')
    });

    function youtube_parser(url){
        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        var match = url.match(regExp);
        return (match&&match[7].length==11)? match[7] : false;
    }

    $('a').each(function(i, el) {
        var url = $(el).attr("href");
        // use url.indexOf() to look for youtube embed urls
        // apply featherlight on those tags
        if(url) {
            if(url.indexOf("youtube.com/watch?v=") >= 0 || url.indexOf("youtu.be/") >= 0 || (url.indexOf("youtube.com/embed/") >= 0)){
                var id = youtube_parser(url);
                $(el).attr("data-featherlight", "iframe")
                    .attr("data-featherlight-iframe-width", "850")
                    .attr("data-featherlight-iframe-height", "468")
                    .attr("data-featherlight-iframe-frameborder", "0")
                    .attr("data-featherlight-iframe-allow", "autoplay; encrypted-media")
                    .attr("data-featherlight-iframe-allowfullscreen", "true")
                    .attr("href", "http://www.youtube.com/embed/" + id + "?rel=0&amp;autoplay=1" );

                $(el).featherlight();
            }
        }
    });

    //Search Bar open/close
    $('.search-bar .search-button').on('click', function(){
        $(this).parent().toggleClass('open');
    });

    if($('#roi-iframe').length > 0) {
        $('#roi-iframe').seamless();
    }

    //Language Toggle open/close
    // $('.language .language__toggle').on('click', function(){
    //     $(this).parent().toggleClass('language__open');
    // });

})(jQuery, KMDGPB.jQuery);