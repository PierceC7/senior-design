<?php
/**
 * @package WordPress
 * @subpackage kmdg
 *
 * This file redirects archive requests to the parts folder.
 */

theme()->part('archive', get_post_type());
