<?php
/**
 * @package WordPress
 * @subpackage kmdg
 * @author Jake Finley
 * This is the theme specific implementation of the KMDG class,
 * override functions here to change core functionality, or add
 * additional functionality. If this theme is a parent theme this
 * class needs to extend
 *
 */

use KMDG\Framework;
use KMDG\AJAX;
use KMDG\AJAX_Handler;
use KMDG\PageBuilder\Component;

class Theme extends Framework {
	protected $youTubeAPIKey = 'AIzaSyBEfVRXip7ITwAhAHubOcN8uZf-hXN71eU';
	protected $vimeoAPIKey = 'b8609b2948245d7c7db650060a465363';

    /**
     * @see \KMDG\Framework::boot()
     */
    protected function boot() {
        AJAX::set_default_ajax_handler(new AJAX_Handler(false)); // Provide AJAX handler dependency (must implement AJAX_Interface)
        $this->add_directive('php_value max_input_vars 5000', 'max_input_vars', 5000); // Required for Page Builder
    }

    protected function default_settings()
    {
        return array(
            'stylesheets'        => array(
//                'gray-css'              => array(
//                    'url'               => 'dist/css/gray.min.css',
//                    'media'             => 'screen',
//                    'location'          => 'public',
//                    'defer'             => false,
//                    'dependencies'      => ['kmdg-page-builder']
//                ),
                'main'              => array(
                    'url'               => 'dist/css/style.css',
                    'media'             => 'screen',
                    'location'          => 'public',
                    'defer'             => false,
                    'dependencies'      => ['kmdg-page-builder']
                ),
                'admin'             => array(
                    'url'               => 'dist/css/admin.css',
                    'media'             => 'screen',
                    'location'          => 'admin',
                    'defer'             => false
                ),
                'editor'            => array(
                    'url'               => 'dist/css/editor.css',
                    'media'             => 'screen',
                    'location'          => 'editor',
                    'defer'             => false
                )
            ),
            'scripts'           => array(
                'jquery'            => array(
                    'url'               => '//ajax.googleapis.com/ajax/libs/jquery/'.static::DEFAULT_JQUERY_VERSION.'/jquery.min.js',
                    'dependencies'      => NULL,
                    'footer'            => false,
                    'version'           => static::DEFAULT_JQUERY_VERSION,
                    'access'            => array('admin','public')
                ),
                'theme_library'      => array(
                    'url'               => 'dist/js/library.min.js',
                    'dependencies'      => array('jquery'),
                    'footer'            => true,
                    'version'           => false,
                    'access'            => array('public')
                ),
                'theme_admin'       => array(
                    'url'               => 'dist/js/admin.min.js',
                    'dependencies'      => array('jquery'),
                    'footer'            => true,
                    'version'           => false,
                    'access'            => array('admin')
                ),
                'mmenu_js'       => array(
                    'url'               => 'src/js/lib/mmenu.js',
                    'dependencies'      => array('jquery','theme_library'),
                    'footer'            => true,
                    'version'           => false,
                    'access'            => array('public')
                ),
                'theme_global'       => array(
                    'url'               => 'dist/js/app.min.js',
                    'dependencies'      => array('jquery','theme_library'),
                    'footer'            => true,
                    'version'           => false,
                    'access'            => array('public')
                ),
            ),
            'menu_locations'        => array(
                'main-nav'            => 'Main Navigation',
                'mobile-nav'          => 'Mobile Navigation',
                'footer-nav-1'        => 'Footer Nav 1',
                'footer-nav-2'        => 'Footer Nav 2',
                'footer-nav-3'        => 'Footer Nav 3',
                'footer-nav-4'        => 'Footer Nav 4',
                'terms-privacy'       => 'Terms and Privacy Menu'
            ),
            'supports'              => array(
                'menus',
                'post-thumbnails',
                'html5'
            ),
            'acf_options_pages'     => array('Options','Post Options')
        );
    }

    public function _action_add_ajaxurl() {
        echo '<script type="text/javascript">'."\n".
            '  var homeurl = "'.esc_url( home_url( '/' ) ).'";'."\n".
            '</script>'."\n";
    }

    public function get_video_image(Component $component, $nocache = false) {
        if($component->field('thumbnail')) {
            return $this->utilities()->src($component->field('thumbnail'), 'medium');
        }

        $vid = $this->getVideoThumbnail($component->field('link_source'), $nocache);

        if(!$vid) {
            return theme()->get_placeholder_image(150, 150);
        }

        return $vid->large;
    }

    /**
     * Retrieves the thumbnail image from YouTube or Vimeo and caches it.
     *
     * @param string $url
     *
     * @param bool $nocache
     *
     * @return mixed|bool
     */
    public function getVideoThumbnail($url, $nocache = false)
    {
        $video = $this->getVideoInfo($url, $nocache);

        // Soft cache
        $image = wp_cache_get("kmdg_video_thumbnail_{$video->id}");

        if(!$image && !$nocache) {
            // Hard cache
            $image = get_transient("kmdg_video_thumbnail_{$video->id}");
        }

        if(!$image) {

            // Huge try/catch block because potential several points of failure involving third party services
            try {

                $ch = \curl_init();

                \curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                \curl_setopt($ch, CURLOPT_URL, $video->api);
                \curl_setopt($ch, CURLOPT_VERBOSE, true);

                $data = \curl_exec($ch);
                \curl_close($ch);

                $image = [
                    'full' => null,
                    'large'=> null,
                    'thumb'=> null
                ];

                if($data !== false) {
                    if($video->type == 'wistia') {
                        $image = ['full' => $data, 'large' => $data, 'thumb' => $data];
                    } else {
                        $json = json_decode($data);

                        if($video->type == 'youtube') {
                            if(isset($json->items[0]->snippet->thumbnails->maxres)) {
                                $image['full'] = $json->items[0]->snippet->thumbnails->maxres->url;
                            } elseif (isset($json->items[0]->snippet->thumbnails->high)) {
                                $image['full'] = $json->items[0]->snippet->thumbnails->high->url;
                            } elseif (isset($json->items[0]->snippet->thumbnails->medium)) {
                                $image['full'] = $json->items[0]->snippet->thumbnails->medium->url;
                            } else {
                                $image['full'] = $json->items[0]->snippet->thumbnails->default->url;
                            }

                            if (isset($json->items[0]->snippet->thumbnails->high)) {
                                $image['large'] = $json->items[0]->snippet->thumbnails->high->url;
                            } elseif (isset($json->items[0]->snippet->thumbnails->medium)) {
                                $image['large'] = $json->items[0]->snippet->thumbnails->medium->url;
                            } else {
                                $image['large'] = $json->items[0]->snippet->thumbnails->default->url;
                            }

                            $image['thumb'] = $json->items[0]->snippet->thumbnails->default->url;

                        } else {
                            $bigSize = 0;
                            $smallSize = 9999999;

                            // Look for biggest & smallest image
                            foreach($json->data[0]->sizes as $thumb) {
                                if($bigSize < $thumb->width) {
                                    $image['large'] = $image['full']; // Store last biggest size as "large"
                                    $image['full'] = $thumb->link;
                                    $bigSize = $thumb->width;
                                }

                                if($smallSize > $thumb->width) {
                                    $image['thumb'] = $thumb->link;
                                    $smallSize = $thumb->width;
                                }
                            }

                            if(empty($image['large'])) {
                                $image['large'] = $image['full']; // Default if not enough image sizes
                            }
                        }
                    }
                }

                wp_cache_add("kmdg_resource_video_thumbnail_{$video->id}", $image);
                set_transient("kmdg_resource_video_thumbnail_{$video->id}", $image);
            } catch (\Exception $e) {
                $image = false;
            }
        }
        return (object) $image;
    }

    /**
     * Gets video information from the specified (or current) resource's download link
     *
     * @param int|null $post_id
     *
     * @return bool|mixed|object
     */
    public function getVideoInfo($url, $nocache = false) {
        $embed = $url;
        $data = wp_cache_get($embed);

        if($data === false || !$nocache) {
            $data = (object) [
                'type'  => false,
                'id'    => null,
                'api'   => null,
                'url'   => null
            ];

            $vimeo = $this->getVimeoId($embed);
            $youtube = $this->getYoutubeId($embed);
            $wistia = $this->getWistiaId($embed);

            if($vimeo !== false) {
                $data->type = 'vimeo';
                $data->id = $vimeo;
                $data->api = "https://api.vimeo.com/videos/{$vimeo}/pictures?access_token={$this->vimeoAPIKey}&page=1&per_page=1";
                $data->url = "https://player.vimeo.com/video/{$vimeo}";
            } elseif($youtube !== false) {
                $data->type = 'youtube';
                $data->id = $youtube;
                $data->api = "https://www.googleapis.com/youtube/v3/videos?key={$this->youTubeAPIKey}&part=snippet&id={$youtube}";
                $data->url = "https://www.youtube.com/embed/{$youtube}";
            } elseif($wistia !== false) {
                $data->type = 'wistia';
                $data->id = $wistia;
                $data->api = "http://embed.wistia.com/deliveries/{$wistia}.jpg?video_still_time=0";
                $data->url = "http://embed.wistia.com/deliveries/{$wistia}.bin";
            }
            $data->embed = $embed;
            wp_cache_set($embed, $data);

        }

        return $data;
    }

    protected function getVimeoId($embed) {
        $matches = array();

        if(preg_match("/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/", $embed, $matches)) {
            return $matches[5];
        }

        return false;
    }

    protected function getWistiaId($embed) {
        $matches = array();

        if(preg_match("/(http(?:s)?:\/\/)?(kahua\.wistia\.com\/medias)+\/([a-z\d]*)/", $embed, $matches)) {
            return $matches[3];
        }elseif(preg_match("/(http(?:s)?:\/\/)?(commotionengine\.wistia\.com\/medias)+\/([a-z\d]*)/", $embed, $matches)) {
            return $matches[3];
        }
        return false;
    }

    protected function getYoutubeId($embed) {
        $matches = array();

        if(preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $embed, $matches)) {
            return $matches[1];
        }

        return false;
    }
}