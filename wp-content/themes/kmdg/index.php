<?php
/**
 * @package WordPress
 * @subpackage default
 */

get_header(); ?>
    <?php theme()->part('site', 'wrapper', 'top'); ?>
        <?php theme()->part('layout', 'index'); ?>
    <?php theme()->part('site', 'wrapper', 'bottom'); ?>
<?php get_footer(); ?>