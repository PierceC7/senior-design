<?php
/**
 * @package WordPress
 * @subpackage kmdg
 *
 * Redirects requests for single post tempates to the parts folder.
 */

theme()->part('single', get_post_type());
