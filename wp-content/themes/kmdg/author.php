<?php
/**
 * @package WordPress
 * @subpackage kmdg
 *
 * This file redirects author archive requests to the parts folder.
 */

global $author;
global $author_name;

if(!empty($author_name)) {
    $current_author = get_user_by('slug', $author_name);
} else {
    $current_author = get_userdata(intval($author));
}

theme()->part('archive', 'author', $author_name, array('author_data' => $current_author));
