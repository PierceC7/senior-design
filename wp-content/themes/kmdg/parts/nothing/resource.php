<div class="u-builder-container-width">
    <div class="resource_none">
        <h4>Sorry, but there aren't any Resources in the <?php echo single_cat_title('', false); ?> category yet.</h4>

        <form>
            <input id="back" type="button" onclick="history.go(-1)" value="Back To Previous Page →">
        </form>
    </div>
</div>
