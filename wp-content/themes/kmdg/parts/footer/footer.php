<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */

$menu_1 = get_field('footer_menu_1', 'options');
$menu_2 = get_field('footer_menu_2', 'options');
$menu_3 = get_field('footer_menu_3', 'options');
$menu_4 = get_field('footer_menu_4', 'options');

if ($menu_1):
    $menu_1_url = $menu_1['url'];
    $menu_1_title = $menu_1['title'];
    $menu_1_target = $menu_1['target'] ? $menu_1['target'] : '_self';
endif;

if ($menu_2):
    $menu_2_url = $menu_2['url'];
    $menu_2_title = $menu_2['title'];
    $menu_2_target = $menu_2['target'] ? $menu_2['target'] : '_self';
endif;

if ($menu_3):
    $menu_3_url = $menu_3['url'];
    $menu_3_title = $menu_3['title'];
    $menu_3_target = $menu_3['target'] ? $menu_3['target'] : '_self';
endif;

if ($menu_4):
    $menu_4_url = $menu_4['url'];
    $menu_4_title = $menu_4['title'];
    $menu_4_target = $menu_4['target'] ? $menu_4['target'] : '_self';
endif;

?>

<footer class="site_footer">
    <div class="u-builder-container-width">
        <div class="site_footer__top">
            <div class="site_footer__social-icons">
                <?php if (get_field('twitter_url', 'options')) : ?>
                    <a class="site_footer__icon" href="<?php the_field('twitter_url', 'options'); ?>"
                       aria-label="Link to Twitter">
                        <i class="fab fa-twitter"></i>
                    </a>
                <?php endif; ?>

                <?php if (get_field('linkedin_url', 'options')) : ?>
                    <a class="site_footer__icon" href="<?php the_field('linkedin_url', 'options'); ?>"
                       aria-label="Link to LinkedIn">
                        <i class="fab fa-linkedin-in"></i>
                    </a>
                <?php endif; ?>

                <?php if (get_field('facebook_url', 'options')) : ?>
                    <a class="site_footer__icon" href="<?php the_field('facebook_url', 'options'); ?>"
                       aria-label="Link to Facebook">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                <?php endif; ?>
            </div>
        </div>
        <div class="site_footer__middle">
            <div class="site_footer__menu site_footer__menu--1">
                <div class="site_footer__menu_title site_footer__menu_title--1">
                    <?php if ($menu_1): ?>
                        <a href="<?= esc_url($menu_1_url); ?>"
                           target="<?= esc_attr($menu_1_target); ?>"><?= esc_html($menu_1_title); ?></a>
                    <?php else: ?>
                        <span>&nbsp</span>
                    <?php endif; ?>
                </div>
                <div class="site_footer__nav site_footer__nav--1">
                    <?php wp_nav_menu(array('theme_location' => 'footer-nav-1')); ?>
                </div>
            </div>
            <div class="site_footer__menu site_footer__menu--2">
                <div class="site_footer__menu_title site_footer__menu_title--2">
                    <?php if ($menu_2): ?>
                        <a href="<?= esc_url($menu_2_url); ?>"
                           target="<?= esc_attr($menu_2_target); ?>"><?= esc_html($menu_2_title); ?></a>
                    <?php else: ?>
                        <span>&nbsp</span>
                    <?php endif; ?>
                </div>
                <div class="site_footer__nav site_footer__nav--2">
                    <?php wp_nav_menu(array('theme_location' => 'footer-nav-2')); ?>

                </div>
            </div>
            <div class="site_footer__menu site_footer__menu--3">
                <div class="site_footer__menu_title site_footer__menu_title--3">
                    <?php if ($menu_3): ?>
                        <a href="<?= esc_url($menu_3_url); ?>"
                           target="<?= esc_attr($menu_3_target); ?>"><?= esc_html($menu_3_title); ?></a>
                    <?php else: ?>
                        <span>&nbsp</span>
                    <?php endif; ?>
                </div>
                <div class="site_footer__nav site_footer__nav--3">
                    <?php wp_nav_menu(array('theme_location' => 'footer-nav-3')); ?>

                </div>
            </div>
            <div class="site_footer__menu site_footer__menu--4">
                <div class="site_footer__menu_title site_footer__menu_title--4">
                    <?php if ($menu_4): ?>
                        <a href="<?= esc_url($menu_4_url); ?>"
                           target="<?= esc_attr($menu_4_target); ?>"><?= esc_html($menu_4_title); ?></a>
                    <?php else: ?>
                        <span>&nbsp</span>
                    <?php endif; ?>
                </div>
                <div class="site_footer__nav site_footer__nav--4">
                    <?php wp_nav_menu(array('theme_location' => 'footer-nav-4')); ?>
                </div>
            </div>
        </div>
        <div class="site_footer__bottom">
            <div class="site_footer__contact-info">
                <?php if (get_field('telephone', 'option')): ?>
                    <div class="site_footer__link site_footer__link--phone">
                        <i class="site_footer__contact-icon site_footer__contact-icon--phone fas fa-phone"></i>
                        <a class="site_footer__phone-number" href="tel:<?= get_field('telephone', 'option') ?>">
                            Tel: <?= get_field('telephone', 'option') ?>
                        </a>
                    </div>
                <?php endif ?>

                <?php if (get_field('email', 'option')): ?>
                    <div class="site_footer__link site_footer__link--email">
                        <i class="site_footer__contact-icon site_footer__contact-icon--email fas fa-envelope"></i>
                        <a class="site_footer__email-address" href="mailto:<?= get_field('email', 'option') ?>">
                            <?= get_field('email', 'option') ?>
                        </a>
                    </div>
                <?php endif ?>

            </div>
            <div class="site_footer__legal">
                <span class="site_footer__link site_footer__link--copyright">Copyright &copy; <?= date('Y') ?> <?php the_field('copyright', 'options'); ?></span>
                <span class="site_footer__link site_footer__link--privacy">
                    <a href="<?php the_field('privacy_policy', 'options'); ?>">Privacy Policy</a>
                </span>
            </div>
        </div>
    </div>
</footer>