<div class="floating-buttons">

        <?php if( have_rows('floating_buttons', 'option') ): ?>
            <div class="rotator">
                <?php while ( have_rows('floating_buttons', 'option') ) : the_row(); ?>

                    <a href="<?php the_sub_field('button_url', 'option'); ?>" class=" floating_buttons_color <?= get_floating_color(); ?>">
                        <?php the_sub_field('button_text', 'option'); ?>
                    </a>

                <?php endwhile; ?>
            </div>
        <?php endif; ?>

</div>