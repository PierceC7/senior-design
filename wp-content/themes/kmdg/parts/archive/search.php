<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */

get_header();



?>
<?php theme()->part('site', 'wrapper', 'top'); ?>

<div class="inner-width">
    <?php theme()->part('title', 'archive', 'search'); ?>

    <div class="u-builder-container-width">
        <div class="row search_row">
            <div class="col-md-8">
                <div class="left">
                    <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
                            <div class="left__single">
                                <?php theme()->part('layout', 'search'); ?>
                            </div>
                        <?php endwhile; ?>

                        <?php theme()->part('navigation', 'search'); ?>
                    <?php else : // No posts ?>
                        <?php theme()->part('nothing', get_post_type()); ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="col-md-4">
                <div class="right">
                    <?php theme()->part('sidebar', 'search'); ?>
                </div>
            </div>
        </div>

        <?php if ($wp_query->found_posts > $wp_query->get("posts_per_page") && $wp_query->get("posts_per_page") > 0): ?>
            <div class="row">
                <div class="col-md-12">
                    <?php theme()->part('pagination', get_post_type()); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php theme()->part('site', 'wrapper', 'bottom'); ?>
<?php get_footer(); ?>
