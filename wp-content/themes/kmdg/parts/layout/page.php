<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>
<div class="inner-width">
    <?php theme()->part('content', 'page'); ?>
</div>
