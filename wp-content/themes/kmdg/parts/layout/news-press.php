<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */

$events_code = get_field('events_shortcode', 'option');
$news_num = get_field('news_number', 'option');
$press_num = get_field('press_number', 'option');

$news_args = array(
    'post_type' => 'news',
    'posts_per_page' => $news_num
);

$news_posts = new WP_Query($news_args);

$press_args = array(
    'post_type' => 'press-releases',
    'posts_per_page' => $press_num
);

$press_posts = new WP_Query($press_args);
?>

<?php theme()->part('title', 'title'); ?>

<div class="u-builder-container-width">
    <div class="news_press">
        <div class="row news_press_row news_press_row--<?= !$events_code ? 'no_events' : '' ?>">
            <div class="col-md-4">
                <h2 class="news_press__title"><a href="/news/">Latest News Posts</a></h2>
                <div class="news_press__left">
                    <?php if ($news_posts->have_posts()) : ?>
                        <?php while ($news_posts->have_posts()) : $news_posts->the_post(); ?>
                            <div class="news_press__single news_press__single--news">
                                <?php theme()->part('excerpts', 'news'); ?>
                            </div>
                        <?php endwhile; ?>
                    <?php else : // No posts ?>
                        <?php theme()->part('nothing', get_post_type()); ?>
                    <?php endif; ?>
                </div>
                <div class="news_press__view_all news_press__view_all--news"><a href="/news/">View All</a></div>
            </div>

            <div class="col-md-8">
                <h2 class="news_press__title"><a href="/press-releases/">Latest Press Releases</a></h2>
                <div class="news_press__right">
                    <?php if ($press_posts->have_posts()) : ?>
                        <?php while ($press_posts->have_posts()) : $press_posts->the_post(); ?>
                            <div class="news_press__single news_press__single--press">
                                <?php theme()->part('excerpts', 'press'); ?>
                            </div>
                        <?php endwhile; ?>
                    <?php else : // No posts ?>
                        <?php theme()->part('nothing', get_post_type()); ?>
                    <?php endif; ?>
                </div>
                <div class="news_press__view_all news_press__view_all--press"><a href="/press-releases/">View All</a></div>
            </div>
        </div>
        <?php if ($events_code) : ?>
            <div class="row events_row">
                <div class="col-md-12">
                    <h2 class="news_press__title news_press__title--events"><a href="/events/">Upcoming Events</a></h2>
                    <div class="news_press__events">
                        <?= do_shortcode($events_code); ?>
                    </div>
                    <div class="news_press__view_all news_press__view_all--events"><a href="/events/">View All</a></div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>