<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>
<div class="u-builder-container-width">
    <div class="row event_row">
        <?php theme()->part('content', 'event'); ?>
    </div>
</div>
