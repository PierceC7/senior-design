<?php
/**
 * @package WordPress
 * @subpackage kmdg
 *
 * Posts Landing Page - Blog Posts Archive
 *
 */

if (has_post_thumbnail()):
    $featured_image = get_post_thumbnail_id();
endif;

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
    <div class="blog_post">
        <div class="blog_post__left">
            <h2 class="blog_post__title">
                <a href="<?php the_permalink(); ?>">
                    <?= get_the_title(); ?>
                </a>
            </h2>

            <div class="blog_post__excerpt">
                <?php theme()->part('excerpts', 'excerpt', theme()->post_display_mode()); ?>
            </div>

            <h4 class="blog_post__date">Published <?= get_the_date(); ?>,&nbsp</h4>

            <h4 class="blog_post__author"> by
                <span class="blog_post__author--name">
                    <?= get_the_author_meta('display_name'); ?>
                </span>
            </h4>
        </div>
        <div class="blog_post__right">
            <?php if (has_post_thumbnail()): ?>
                <div class="blog_post__image">
                    <?php theme()->responsive_image($featured_image, 325, 250, 'medium'); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</article>