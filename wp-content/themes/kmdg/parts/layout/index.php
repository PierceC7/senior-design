<?php
/*
 * Posts Page Landing - Blog
 *
*/
?>
<?php theme()->part('title', 'blog'); ?>


<div class="blog_menu">
    <div class="u-builder-container-width">
        <?php theme()->part('header', 'blog_menu'); ?>
    </div>
</div>

<div class="u-builder-container-width">
    <div class="row blog_row">
        <div class="col-md-8">
            <div class="left">
                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="left__single">
                            <?php theme()->part('layout', get_post_type()); ?>
                        </div>
                    <?php endwhile; ?>
                <?php else : // No posts ?>
                    <?php theme()->part('nothing', get_post_type()); ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="col-md-4">
            <div class="right">
                <?php theme()->part('sidebar', 'blog'); ?>
            </div>
        </div>
    </div>
    <?php if ($wp_query->found_posts > $wp_query->get("posts_per_page") && $wp_query->get("posts_per_page") > 0): ?>
        <div class="row">
            <div class="col-md-12">
                <?php theme()->part('pagination', get_post_type()); ?>
            </div>
        </div>
    <?php endif; ?>
</div>
