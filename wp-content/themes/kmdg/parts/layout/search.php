<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>
<div class="single_search">
    <article <?php post_class('post-type-'.get_post_type()); ?> id="post-<?php the_ID(); ?>">
        <?php theme()->part('title', 'post'); ?>
        <?php theme()->part('content', 'post'); ?>
<!--        --><?php //get_the_permalink();?>
    </article>
</div>
