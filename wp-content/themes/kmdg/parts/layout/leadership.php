<?php if (have_rows('leader_types', 'options')) :?>
    <?php while (have_rows('leader_types', 'options')) : the_row(); ?>

        <?php
            /* @var $role WP_Term */

            $role = get_sub_field('role');
            $the_query = get_leadership_query($role);
        ?>
        <section class="leader_role leader_role--cards leader_role--<?= $role->slug ?>">

            <h2 class="leader_role__title leader_role__title--<?= $role->slug ?>"><?= $role->name ?></h2>

            <!--The Loop-->
            <?php if ($the_query->have_posts()) :?>
                <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

                <?php theme()->part('leadership', 'leader_card'); ?>

                <?php endwhile; ?>

                <?php wp_reset_postdata(); ?>

            <?php else : // No posts ?>
                <?php theme()->part('nothing', get_post_type(), theme()->post_display_mode()); ?>
            <?php endif; ?>
        </section>

    <?php endwhile; ?>
<?php endif; ?>

