<?php
/**
 * @package WordPress
 * @subpackage kmdg
 *
 * Posts Landing Page - Blog Posts Archive
 *
*/

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

    <?php if(has_post_thumbnail()):?>
        <div class="featured-image">
            <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail();?>
            </a>
        </div>
    <?php endif;?>

    <div class="post-body">
        <?php theme()->part('title', get_post_type(), theme()->post_display_mode()); ?>
        <div class="date">
            <p><?php echo get_the_date('F d, Y');?></p>
        </div>
        <?php theme()->part('content', get_post_type(), theme()->post_display_mode()); ?>
    </div>
</article>