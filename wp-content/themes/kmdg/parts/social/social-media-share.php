<div class="social_media">
    <h4 class="social_media__share">Share:</h4>
    <div class="social_media__wrap">
        <a class="social_media__icon social_media__icon--twitter" href="https://twitter.com/share?url=<?php the_permalink(); ?>"
           target="_blank" rel="noopener noreferrer" data-title="Twitter" title="Twitter">
            <i class="fab fa-twitter"></i>
        </a>
    </div>
    <div class="social_media__wrap">
        <a class="social_media__icon social_media__icon--facebook"
           href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>"
           target="_blank" rel="noopener noreferrer" data-title="Facebook" title="Facebook">
            <i class="fab fa-facebook-f"></i>
        </a>
    </div>
    <div class="social_media__wrap">
        <a class="social_media__icon social_media__icon--linkedin"
           href="https://www.linkedin.com/shareArticle?url=<?php the_permalink(); ?>"
           target="_blank" rel="noopener noreferrer" title="Linkedin">
            <i class="fab fa-linkedin-in"></i>
        </a>
    </div>
</div>