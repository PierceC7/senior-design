<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>"/>
    <meta id="meta" name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!--Removed bloginfo('name') from Title, letting Yoast take care of it-->
    <title><?php wp_title('&laquo;', true, 'right'); ?></title>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>

    <script type="text/javascript">
        var mmtitle = "<?= get_field('mobile_menu_title', 'option');?>"
    </script>

    <?php if(defined('STAGING') && STAGING): ?>

    <?php endif; ?>



    <!--Template Specific Scripts-->
    <?php theme()->part('site','scripts',theme()->template_name()); ?>
    <!--End Template Specific Scripts-->

<?= get_field('header_code', 'option') ?>

<?php wp_head(); ?>
