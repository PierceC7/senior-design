<?php
/**
 * Wraps individual templates, regardless of where they are displayed.
 */
?>
<section id="page-<?php echo theme()->page_slug(); ?>" class="template template-<?php echo theme()->template_name(); ?>" role="main">
    <div class="inner-width">
        <div class="template-wrapper">