<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <?php theme()->part('site', 'head', get_post_type()); ?>
</head>
<body <?php body_class(); ?>>
    <?= get_field('body_code', 'option') ?>
    <div class="site-wrapper">
        <?php theme()->part('site', 'layout', 'start'); ?>
