<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>
    <?php theme()->part('site', 'layout', 'end'); ?>
</div><!-- site-wrapper -->
<?= get_field('footer_code', 'option') ?>
<?php wp_footer(); ?>
</body>
</html>