<article class="single_profile" data-title="<?php the_title(); ?>">

    <div class="single_profile__wrap">
        <div class="single_profile__image">
            <?php theme()->responsive_image(get_field('headshot'), 300, 300, 'pb-medium'); ?>
        </div>

        <div class="single_profile__info">

            <div class="single_profile__name">
                <a href="<?php the_permalink(); ?>">
                    <?php the_title(); ?>
                </a>
            </div>

            <div class="single_profile__role">
                <?php the_field('role'); ?>
            </div>

            <div class="single_profile__bottom">

                <div class="single_profile__link">
                    <a href="<?php the_permalink(); ?>">See Full Bio</a>
                </div>

                <?php if (get_field('linkedin_url')) : ?>
                    <a class="single_profile__linkedin" href="<?php the_field('linkedin_url'); ?>"
                       target="_blank" aria-label="Leaders's LinkedIn">
                        <i class="fab fa-linkedin"></i>
                    </a>
                <?php endif; ?>

            </div>
        </div>
    </div>
</article>
