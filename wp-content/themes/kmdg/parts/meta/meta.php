<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>
<div class="meta">
    <div class="categories">
        <span>Categories:</span>
        <?php the_category(', '); ?>
    </div>

    <div class="tags">
        <span>Tags:</span>
        <?php the_tags('', ', '); ?>
    </div>
</div>