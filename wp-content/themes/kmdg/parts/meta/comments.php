<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>
<div class="meta">
    <div class="categories">
        <span>Categories:</span>
        <?php the_category(', '); ?>
    </div>

    <div class="tags">
        <span>Tags:</span>
        <?php the_tags('', ', '); ?>
    </div>

    <div class="comments-link">
        <?php comments_popup_link(__('<span>0</span> Comments', 'default'), __('<span>1</span> Comment', 'default'), __('<span>%</span> Comments', 'default'), '', __('Comments Closed', 'default')); ?>
    </div>

    <?php edit_post_link(); ?>
</div>
