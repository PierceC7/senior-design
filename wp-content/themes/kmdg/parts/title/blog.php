<?php
    $blog = get_option('page_for_posts');
?>

<?php if (is_home()): ?>

    <div class="blog_banner" style="background-image: url(<?= get_field('main_banner', 'options') ?>);">
        <h1 class="blog_banner__title"><?php the_title(); ?></h1>
    </div>

<?php else: ?>

<?php endif; ?>