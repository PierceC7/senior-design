<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>
<div class="title_banner" style="background-image: url(<?= get_field('main_banner','options') ?>);">
    <h1 class="title_banner__title">
        <?php theme()->the_page_title(); ?>
    </h1>
</div>