<?php
?>
<!--This is the title for a single post object-->
<div class="title">
    <h2>
        <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'default'), the_title_attribute(array('echo'=>false))); ?>">
            <?php the_title(); ?>
        </a>
    </h2>
</div>
