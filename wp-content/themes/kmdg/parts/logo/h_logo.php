<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>
<?php if(get_field('logo', 'options')): ?>
<div class="site-logo">
    <a href="<?php bloginfo('url'); ?>/">
        <img src="<?php echo theme()->utilities()->src(get_field('logo', 'option'), 'full'); ?>" alt="Logo" />
    </a>
</div>
<?php endif; ?>