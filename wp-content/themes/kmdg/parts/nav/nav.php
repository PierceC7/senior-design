<nav class="primary u-max u-flex" role="navigation">
    <?php if(have_rows('main_menu', 'option')): ?>
        <ul class="menu u-max">
            <?php while(have_rows('main_menu', 'option')): the_row(); ?>
                <?php $linkData = get_sub_field('link'); ?>
                <?php
                $linkClasses = '';
                $linkType = '';

                if (get_sub_field('use_custom_url') == true) {
                    $linkClasses = 'menu-item u-flex u-flex-valign--mid menu-top-level-item menu-type--fatnav';
                    $linkType = get_sub_field('custom_link');

                } else {
                    $linkClasses = theme()->getMenuClasses($linkData);
                    $linkType = $linkData->link;
                }
                ?>

                <li class="<?= $linkClasses; ?>">
                    <div class="menu-item--wrapper">
                        <a href="<?= $linkType ?>">
                            <?= get_sub_field('link_text') ?>
                        </a>
                    </div>

                    <?php if(get_row_layout() == 'simple'): ?>
                        <?php if(get_sub_field('sub_menu')): ?>
                            <div class="menu-dropdown">
                                <?php wp_nav_menu(['menu' => get_sub_field('sub_menu')]); ?>
                            </div>
                        <?php endif; ?>
                    <?php else: ?>
                        <div class="menu-dropdown">
                            <div class="top">
                                <?php if(have_rows('columns')): ?>
                                    <div class="submenus">
                                        <?php while(have_rows('columns')): the_row(); ?>
                                            <?php if(get_sub_field('sub_menu')): ?>
                                                <div class="subcol">
                                                    <?php wp_nav_menu(['menu' => get_sub_field('sub_menu')]); ?>
                                                </div>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if(get_sub_field('resource')): ?>
                                    <?php theme()->part('header', 'resource', null, ['resource' => get_sub_field('resource')]) ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </li>
            <?php endwhile; ?>
        </ul>
    <?php endif; ?>
</nav>