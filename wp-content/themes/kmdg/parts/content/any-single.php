<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>

<div class="entry full">
    <?php the_content(); ?>
</div>
