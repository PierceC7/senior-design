<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>

<div class="entry abridged">
    <?php the_excerpt(); ?>
    <div class="read_more__button">
        <a href="<?php the_permalink(); ?>"
           title="<?php printf(__('Permanent Link to %s', 'default'), the_title_attribute(array('echo' => false))); ?>">
            Read More
        </a>
    </div>
</div>
