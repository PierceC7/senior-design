<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>
<article <?php post_class('content event_calendar'); ?> id="post-<?php the_ID(); ?>">
    <div class="entry full">
        <?php the_content(); ?>
    </div>
</article>