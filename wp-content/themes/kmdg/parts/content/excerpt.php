<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>

<div class="entry abridged blog_post__content_excerpt">
    <?php the_excerpt(); ?>
    <a href="<?php the_permalink(); ?>"
       title="<?php printf(__('Permanent Link to %s', 'default'), the_title_attribute(array('echo' => false))); ?>">
        Read More
    </a>
</div>
