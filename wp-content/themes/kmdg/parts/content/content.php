<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>

<!--This content/content field  is just pulling in the excerpt, should be removed/refactored-->
<div class="entry">
    <?= wp_trim_words(get_the_content(), 50, '...');?>
    <a href="<?php the_permalink(); ?>" class="read-more">
        Read More
    </a>
</div>