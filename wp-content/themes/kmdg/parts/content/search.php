<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>
<article <?php post_class('content search'); ?> id="post-<?php the_ID(); ?>">
    <div class="entry excerpt">
        <?php the_excerpt(); ?>
        <a href="<?php the_permalink(); ?>" class="read-more">
            Read More
        </a>
    </div>
</article>
