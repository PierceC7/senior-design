<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>
<article <?php post_class('content'); ?> id="post-<?php the_ID(); ?>">
    <div class="entry full">
        <?php the_content(); ?>
    </div>
</article>