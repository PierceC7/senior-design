<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */

if (has_post_thumbnail()):
    $featured_image = get_post_thumbnail_id();
endif;

$related = new WP_Query(
    array(
        'category__in' => wp_get_post_categories($post->ID),
        'posts_per_page' => 3,
        'post__not_in' => array($post->ID)
    )
);

get_header(); ?>

<?php theme()->part('site', 'wrapper', 'top'); ?>

<?php if (have_posts()) : ?>

<!--    --><?php //theme()->part('title', 'blog') ?>

    <div class="blog_menu">
        <div class="u-builder-container-width">
            <?php theme()->part('header', 'blog_menu'); ?>
        </div>
    </div>

    <div class="single_post">
        <div class="u-builder-container-width">
            <div class="row blog_row">
                <div class="col-md-8">
                    <div class="left">
                        <div class="single_post__top">
                            <?php while (have_posts()) : the_post(); ?>
                                <h2 class="single_post__title"> <?php the_title(); ?> </h2>

                                <h3 class="single_post__date">Published on <?= get_the_date(); ?>,</h3>

                                <h3 class="single_post__author"> by
                                    <span class="single_post__author--name"> <?= get_the_author_meta('display_name'); ?></span>
                                </h3>

                                <div class="single_post__share">
                                    <?php theme()->part('social', 'social-media-share'); ?>
                                </div>

                                <?php if (has_post_thumbnail()): ?>
                                    <div class="single_post__image">
                                        <?php theme()->responsive_image($featured_image, 700, 400, 'pb_large'); ?>
                                    </div>
                                <?php endif; ?>
                                <div class="single_post__content">
                                    <?php theme()->part('content', 'any-single'); ?>
                                </div>
                            <?php endwhile; ?>
                        </div>

                        <div class="related_post">
                            <h4 class="related_post__recent">Most Recent Related Stories</h4>
                            <div class="related_post__row">
                                <div class="related_post__wrap">

                                    <?php if ($related->have_posts()): ?>
                                        <?php while ($related->have_posts()) : $related->the_post() ?>
                                            <div class="related_post__single">
                                                <?php if (has_post_thumbnail()): ?>
                                                    <div class="related_post__image">
                                                        <?php theme()->responsive_image(get_post_thumbnail_id(), 225, 150, 'medium'); ?>
                                                    </div>
                                                <?php endif; ?>

                                                <a class="related_post__title" href="<?php the_permalink(); ?>">
                                                    <?= get_the_title(); ?>
                                                </a>

                                                <a class="related_post__link" href="<?php the_permalink(); ?>">
                                                    Read More
                                                </a>
                                            </div>
                                            <?php wp_reset_postdata(); ?>
                                        <?php endwhile ?>
                                    <?php else : ?>
                                        <!-- Content If No Posts -->
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="right">
                        <?php theme()->part('sidebar', 'blog'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php else: ?>
    <?php theme()->part('nothing', get_post_type(), 'single'); ?>
<?php endif; ?>

<?php wp_reset_query(); ?>

<?php get_footer(); ?>
