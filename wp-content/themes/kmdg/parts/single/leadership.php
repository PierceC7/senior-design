<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */

get_header(); ?>

<?php theme()->part('site', 'wrapper', 'top'); ?>

<?php theme()->part('title', 'leadership'); ?>

<div class="profile">

    <div class="u-builder-container-width">
        <?php if (have_posts()) : ?>
            <div class="profile__wrap">
                <div class="row ">
                    <div class="col-md-8">
                        <div class="profile__left">

                            <?php if (get_field('role')): ?>
                                <h3 class="profile__role"><?php the_field('role'); ?></h3>
                            <?php endif; ?>

                            <h2 class="profile__name"><?php the_title(); ?></h2>

                            <?php if (get_field('linkedin_url')) : ?>
                                <a class="profile__linkedin" href="<?php the_field('linkedin_url'); ?>"
                                   target="_blank" aria-label="Leaders's LinkedIn">
                                    <i class="fab fa-linkedin"></i>
                                </a>
                            <?php endif; ?>

                            <div class="profile__bio">
                                <?php the_field('bio'); ?>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-4">
                        <div class="profile__right">

                            <div class="profile__image">
                                <?php theme()->responsive_image(get_field('headshot'), 350, 400, 'pb-medium'); ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php wp_reset_query(); ?>

    </div>
</div>

<?php get_footer(); ?>
