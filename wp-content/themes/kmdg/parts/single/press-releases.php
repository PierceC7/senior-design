<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */

if (has_post_thumbnail()):
    $featured_image = get_post_thumbnail_id();
endif;

get_header(); ?>

<?php theme()->part('site', 'wrapper', 'top'); ?>

<?php if (have_posts()) : ?>


    <div class="single_post single_post--press">
        <div class="u-builder-container-width">
            <div class="row press_row">
                <div class="col-md-8">
                    <div class="press">
                        <div class="single_post__top">
                            <?php while (have_posts()) : the_post(); ?>
                                <h2 class="single_post__title"> <?php the_title(); ?> </h2>


                                <h4 class="single_post__date">Published <?= get_the_date(); ?>,&nbsp</h4>

                                <h4 class="single_post__author"> by
                                    <span class="single_post__author--name">
                                        <a href="<?= get_the_author_link(); ?>">
                                            <?= get_the_author_meta('display_name'); ?>
                                        </a>
                                    </span>
                                </h4>

                                <?php var_dump(get_author_posts_url());?>

                                <div class="single_post__share">
                                    <?php theme()->part('social', 'social-media-share'); ?>
                                </div>

                                <?php if (has_post_thumbnail()): ?>
                                    <div class="single_post__image">
                                        <?php theme()->responsive_image($featured_image, 700, 400, 'pb_large'); ?>
                                    </div>
                                <?php endif; ?>
                                <div class="single_post__content">
                                    <?php theme()->part('content', 'press'); ?>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="right">
                        <?php theme()->part('sidebar', 'press'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php else: ?>
    <?php theme()->part('nothing', get_post_type(), 'single'); ?>
<?php endif; ?>

<?php wp_reset_query(); ?>

<?php get_footer(); ?>
