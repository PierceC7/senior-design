<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
if (get_field('original_url')) {
    wp_redirect(get_field('original_url'), 301);
    exit;
}

get_header(); ?>

<?php theme()->part('site', 'wrapper', 'top'); ?>

<?php if (have_posts()) : ?>
    <div class="single-post">
        <div class="u-builder-container-width">
            <div class="row">
                <div class="col-md-12">
                    <?php while (have_posts()) : the_post(); ?>
                        <?php theme()->part('content', get_post_type(), theme()->post_display_mode()); ?>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>

<?php else: ?>
    <?php theme()->part('nothing', get_post_type(), 'single'); ?>
<?php endif; ?>

<?php wp_reset_query(); ?>

<?php theme()->part('site', 'wrapper', 'bottom'); ?>

<?php get_footer(); ?>
