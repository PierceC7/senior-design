<div class="language">
    <ul class="language__wrap">
        <li class="language__menu">
            <a  class="language__toggle" aria-label="Language menu toggle">
                <i class="language__icon fas fa-globe"></i>
            </a>

<!--Couldnt make arrow look good-->
<!--            <div class="language__arrow"></div>-->
            <div class="language__hover_wrap">
                <p>Language</p>
                <ul class="language__sub_menu">
<!--                    <li class="language__menu-item">-->
<!--                        <a href="--><?php //get_field('english_site_link', 'options'); ?><!--"-->
<!--                           class="language__link" aria-label="English Site Link">-->
<!--                            English-->
<!--                        </a>-->
<!--                    </li>-->
<!--                    <li class="language__menu-item">-->
<!--                        <a href="--><?php //get_field('french_site_link', 'options'); ?><!--"-->
<!--                           class="language__link" aria-label="French Site Link">-->
<!--                            French-->
<!--                        </a>-->
<!--                    </li>-->
                    <?php if (have_rows('language_nav', 'option')): ?>

                        <?php while (have_rows('language_nav', 'option')) : the_row(); $link = get_sub_field('english_site_link');?>
                            <li class="language__menu-item">
                                <a href="<?= $link['url'] ?>" target="<?= $link['target'] ?>" class="language__link" aria-label="English Site Link">
                                    <?= $link['title'] ?>
                                </a>
                            </li>

                        <?php endwhile; ?>

                        <?php while (have_rows('language_nav', 'option')) : the_row(); $link = get_sub_field('french_site_link');?>
                            <li class="language__menu-item">
                                <a href="<?= $link['url'] ?>" target="<?= $link['target'] ?>" class="language__link" aria-label="French Site Link">
                                    <?= $link['title'] ?>
                                </a>
                            </li>

                        <?php endwhile; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </li>
    </ul>
</div>