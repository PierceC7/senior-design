<!--Blog Menu Part-->

<div class="tab_nav">

    <?php if (have_rows('categories_to_display', 'options')) : ?>
        <?php while (have_rows('categories_to_display', 'options')) : the_row();
            $cat = get_sub_field('category')
            ?>

            <div class="tab_nav__wrap">
                <div class="tab_nav__single tab_nav__single--<?= is_category($cat->term_id) ? 'active' : 'inactive' ?>">
                    <a class="tab_nav__button tab_nav__button" href="<?= get_category_link($cat->term_id) ?>">
                        <?= $cat->name; ?>
                    </a>
                </div>
            </div>

        <?php endwhile; ?>
    <?php endif; ?>
</div>