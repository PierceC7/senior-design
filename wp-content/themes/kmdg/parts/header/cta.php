<?php

?>
<div class="cta">
    <div class="cta__wrap">
        <div class="cta__image">
            <img src="<?= ResourceCenter()->getResourceImage($cta_id)->thumb; ?>">
        </div>

        <div class="cta__text">
            <div class="cta__title"><?= get_the_title($cta_id) ?></div>
            <div class="cta__button">
                <a class="cta__link" href="<?php the_permalink($cta_id); ?>">
                    Read More
                </a>
            </div>
    </div>
    </div>
</div>