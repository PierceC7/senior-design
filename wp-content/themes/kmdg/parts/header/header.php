<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>

<header class="site_header">
    <div class="site_header__wrapper">

        <section class="top-bar">
            <div class="u-builder-container-width">
                <div class="inner-container">

                    <div class="inner-container inner-container__right">
                        <?php theme()->part('header', 'search') ?>

                        <?php theme()->part('header', 'eyebrow') ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="header-main">
            <div class="u-builder-container-width">

                <div class="header-row">
                    <?php theme()->part('logo', 'h_logo'); ?>

                    <nav class="primary-menu" role="navigation">
                        <?php wp_nav_menu(['theme_location' => 'main-nav', 'container' => false]); ?>
                    </nav>

                </div>

                <div class="mmenu-nav">
                    <a href="#menu-wrapper"><i class="fa fa-bars fa-2x"></i></a>
                </div>
            </div>
        </section>

        <nav id="menu-wrapper" class="mobile-nav" role="navigation">
            <?php wp_nav_menu(array('theme_location' => 'mobile-nav')); ?>
        </nav>
    </div>
</header>
