<div class="search-bar">

    <a href="#" class="search-button">
        <i class="fas fa-search search-icon"></i>
    </a>

    <form role="search" method="get" id="searchform" class="searchform"
          action="<?php echo esc_url(home_url('/')); ?>">
        <label class="screen-reader-text"
               for="s"><?php _x('Search for:', 'label'); ?></label>
        <input type="text" placeholder="Site Search"
               value="<?php echo get_search_query(); ?>" name="s" id="s" class="bar"/>
    </form>
</div>