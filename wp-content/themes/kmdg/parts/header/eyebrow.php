<div class="eyebrow-buttons">

    <?php if (have_rows('eyebrow_nav', 'option')): ?>
        <?php while (have_rows('eyebrow_nav', 'option')) : the_row(); ?>

            <a href="<?php the_sub_field('link_url'); ?>" class="top-link eyebrow_button_color <?= get_eyebrow_color(); ?>">
                <?php the_sub_field('link_text'); ?>
            </a>

        <?php endwhile; ?>
    <?php endif; ?>

</div>