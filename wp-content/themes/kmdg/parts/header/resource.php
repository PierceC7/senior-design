<?php
$post = get_post($resource);
setup_postdata($post);
?>
    <div class="resource">
        <?php if(has_post_thumbnail()): ?>
            <div class="image">
                <img src="<?= theme()->utilities()->src(get_post_thumbnail_id(), 'medium') ?>" alt="<?= get_the_title() ?>">
            </div>
        <?php endif; ?>

        <div class="text">
            <div class="title"><?= get_the_title() ?></div>
            <div class="excerpt"><?= wp_trim_words(get_the_excerpt(), 20) ?></div>
            <div class="button">
                <a href="<?= get_the_permalink() ?>">Read More</a>
            </div>
        </div>
    </div>
<?php wp_reset_postdata() ?>