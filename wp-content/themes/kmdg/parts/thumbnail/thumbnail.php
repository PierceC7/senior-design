<?php
?>
<div class="featured-image">
    <?php if(has_post_thumbnail()): ?>
        <?php the_post_thumbnail();?>
    <?php else: ?>
        <a href="<?php the_permalink(); ?>">
            <img src="<?php echo theme()->the_placeholder_image(400, 400); ?>" alt="Placeholder" />
        </a>
    <?php endif; ?>
</div>