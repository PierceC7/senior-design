<div class="ph-num">
    <a href="tel://<?php the_field('phone_number', 'options');?>">
        TOLL FREE <?php the_field('phone_number', 'options');?>
    </a>
</div>

<div class="search-wrap">
    <form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
        <input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" class="bar"/>
    </form>
</div>

<div class="social-icons">
    <?php if(get_field('linkedin_url', 'options')) :?>
        <a href="<?php the_field('linkedin_url', 'options');?>">
            <i class="fab fa-linkedin"></i>
        </a>
    <?php endif;?>

    <?php if(get_field('twitter_url', 'options')) :?>
        <a href="<?php the_field('twitter_url', 'options');?>">
            <i class="fab fa-twitter"></i>
        </a>
    <?php endif;?>

    <?php if(get_field('facebook_url', 'options')) :?>
        <a href="<?php the_field('facebook_url', 'options');?>">
            <i class="fab fa-facebook-f"></i>
        </a>
    <?php endif;?>

    <?php if(get_field('youtube_url', 'options')) :?>
        <a href="<?php the_field('youtube_url', 'options');?>">
            <i class="fab fa-youtube"></i>
        </a>
    <?php endif;?>
</div>