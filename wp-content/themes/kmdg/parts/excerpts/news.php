<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */

if (has_post_thumbnail()):
    $featured_image = get_the_post_thumbnail_url();
endif;

?>

<div class="entry abridged news_excerpt">
    <?php if (has_post_thumbnail()): ?>
        <div class="news_excerpt__image_container">
            <img class="news_excerpt__image" src="<?= $featured_image; ?>" alt="news icon">
        </div>
    <?php endif; ?>

    <h3 class="news_excerpt__title">
        <a class="news_excerpt__link" href="<?php the_permalink(); ?>" target="_blank">
            <?= get_the_title(); ?>
        </a>
    </h3>

    <div class="news_excerpt__bottom">
        <p class="news_excerpt__date_source"><?= get_field('news_date'); ?> / <?= get_field('news_source'); ?></p>
    </div>
</div>
