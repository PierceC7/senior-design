<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>
<div class="entry abridged small_excerpt">
    <?= wp_trim_words(get_the_content(), 20, '...');?>
    <a href="<?php the_permalink(); ?>" class="read-more">
        Read More
    </a>
</div>