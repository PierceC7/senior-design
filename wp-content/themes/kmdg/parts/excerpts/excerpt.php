<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>

<div class="entry abridged blog_post__content_excerpt">
    <?= wp_trim_words(get_the_content(), 35, '...');?>
    <a href="<?php the_permalink(); ?>" class="read-more blog__read_more--link">
        Read More
    </a>
</div>
