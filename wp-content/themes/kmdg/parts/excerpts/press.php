<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>
<div class="entry abridged press_excerpt">
    <h2 class="press_excerpt__title">
        <a href="<?php the_permalink(); ?>">
            <?= get_the_title(); ?>
        </a>
    </h2>
    <div class="press_excerpt__content">
        <?= wp_trim_words(get_the_content(), 35, '...'); ?>

        <a href="<?php the_permalink(); ?>" class="read-more press_excerpt__link">
            Read More
        </a>
    </div>
    <h4 class="press_excerpt__date">Published <?= get_the_date(); ?>,&nbsp</h4>

    <h4 class="press_excerpt__author"> by
        <span class="press_excerpt__author--name">
            <?= get_the_author('display_name'); ?>
        </span>
    </h4>
</div>