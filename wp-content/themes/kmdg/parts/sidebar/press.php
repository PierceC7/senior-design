<?php
/**
 * @package WordPress
 * @subpackage kmdg
 *
 * Blog Sidebar
 *
 */

$args = array(
    'post_type' => 'press-releases',
    'posts_per_page' => 4,
    'tax_query' => array(
        array(
            'taxonomy' => 'press-release-category',
            'field'    => 'slug',
            'terms'    => 'featured-press',
        ),
    ),
);

$sidebar_posts = new WP_Query($args);

?>

<div class="sidebar">
    <div class="sidebar__wrap">
        <div class="sidebar__bottom">
            <h3 class="sidebar__title sidebar__title--bold">Featured Press Releases</h3>
            <div class="sidebar_post">
                <?php if ($sidebar_posts->have_posts()) : ?>
                    <?php while ($sidebar_posts->have_posts()) : $sidebar_posts->the_post() ?>
                        <div class="sidebar_post__wrap">
                            <h5 class="sidebar_post__title">
                                <a href="<?php the_permalink(); ?>">
                                    <i class="fas fa-chevron-right"></i>
                                    <?= get_the_title(); ?>

                                </a>
                            </h5>

                            <div class="sidebar_post__excerpt">
                                <?php theme()->part('excerpts', 'small'); ?>

                            </div>

                            <?php wp_reset_postdata(); ?>
                        </div>
                    <?php endwhile ?>
                <?php else : ?>
                    <!-- Content If No Posts -->
                <?php endif ?>
            </div>
        </div>
    </div>
</div>
