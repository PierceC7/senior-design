<?php
/**
 * @package WordPress
 * @subpackage kmdg
 *
 * Blog Sidebar
 *
 */

$args = array(
    'post_type'      => 'post',
    'posts_per_page' => 4,
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field'    => 'slug',
            'terms'    => 'featured-posts',
        ),
    ),
);

$sidebar_posts = new WP_Query($args);

?>

<div class="sidebar">
    <div class="sidebar__wrap">
        <div class="sidebar__top">
            <div class="sidebar__search">
                <form role="search" method="get" id="searchform" class="searchform"
                      action="<?php echo esc_url(home_url('/')); ?>">
                    <label class="screen-reader-text" for="s"><?php _x('Search for:', 'label'); ?></label>
                    <input type="text" value="<?php echo get_search_query(); ?>"
                           name="s" id="s" class="bar" placeholder="Search Nlyte Blog"/>
                    <input type="hidden" name="post_type" value="post">
                    <i class="fas fa-search search-icon"></i>
                </form>
            </div>
        </div>

        <div class="sidebar__bottom">
            <h3 class="sidebar__title sidebar__title--bold">Most Recent Blog Posts</h3>
            <div class="sidebar_post">
                <?php if ($sidebar_posts->have_posts()) : ?>
                    <?php while ($sidebar_posts->have_posts()) : $sidebar_posts->the_post() ?>
                        <div class="sidebar_post__wrap">
                            <h5 class="sidebar_post__title">
                                <a href="<?php the_permalink(); ?>">
                                    <i class="fas fa-chevron-right"></i>
                                    <?= get_the_title(); ?>

                                </a>
                            </h5>

                            <div class="sidebar_post__excerpt">
                                <?php theme()->part('content', get_post_type(), theme()->post_display_mode()); ?>

                            </div>

                            <?php wp_reset_postdata(); ?>
                        </div>
                    <?php endwhile ?>
                <?php else : ?>
                    <!-- Content If No Posts -->
                <?php endif ?>
            </div>
        </div>
    </div>
</div>
