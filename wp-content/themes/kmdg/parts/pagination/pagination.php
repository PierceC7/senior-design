<?php
/**
 * @package WordPress
 * @subpackage kmdg
 */
?>
<nav class="post-navigation" role="navigation">
    <?php theme()->pagination([
        'end_size'  => 2,
        'mid_size'  => 3,
        'prev_text' => '<i class="fas fa fa-angle-left"></i>',
        'next_text' => '<i class="fas fa fa-angle-right"></i>'
    ]); ?>
</nav>